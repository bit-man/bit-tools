package bitman.fsm.test.core;

import bitman.fsm.core.*;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.HashSet;

/**
 * User: bit-man
 * Date: 7/2/11
 * Time: 5:07 PM
 */
public class FiniteStateMachineTest
        extends TestCase {

    @Test
    public void testWrongStartingState() throws WrongAcceptingTargetStatesException, WrongInitialStateException, WrongInitialAcceptingStatesException {
        State<String> initialState = new State<String>("id1", "state1");
        State<String> state2 = new State<String>("id2", "state2");

        States<String> states = new States<String>();
        states.add(state2);    // initial state is not in the list of valid states

        boolean thrownException = false;
        try {
            FiniteStateMachine<String> fsm = new FiniteStateMachine<String>(states,
                    initialState, null, null, null);
        } catch (WrongInitialStateException e) {
            thrownException = true;
        }

        assertTrue("initalState not present, should throw WrongInitialStateException ", thrownException);
    }

    @Test
    public void testWrongInitialAcceptingStatesException() throws WrongAcceptingTargetStatesException, WrongInitialStateException, WrongInitialAcceptingStatesException {
        State<String> initialState = new State<String>("id1", "state1");
        State<String> state2 = new State<String>("id2", "state2");

        States<String> states = new States<String>();
        states.add(initialState);

        HashSet<State<String>> statesA = new HashSet<State<String>>();
        statesA.add(initialState);
        AcceptingStates<String> acceptingStates = new AcceptingStates<String>();
        acceptingStates.add(initialState, statesA);
        acceptingStates.add(state2, statesA); // state2 is not in the list of accepted states

        boolean thrownException = false;
        try {
            FiniteStateMachine<String> fsm = new FiniteStateMachine<String>(states,
                    initialState, acceptingStates, null, null);
        } catch (WrongInitialAcceptingStatesException e) {
            thrownException = true;
        }

        assertTrue("initalState not present, should throw WrongInitialAcceptingStatesException ", thrownException);
    }


    @Test
    public void testWrongAcceptingStatesException() throws WrongAcceptingTargetStatesException, WrongInitialStateException, WrongInitialAcceptingStatesException {
        State<String> initialState = new State<String>("id1", "state1");
        State<String> state2 = new State<String>("id2", "state2");

        States<String> states = new States<String>();
        states.add(initialState);

        HashSet<State<String>> statesA = new HashSet<State<String>>();
        statesA.add(state2);    // state 2 is not in the list of declared states
        statesA.add(initialState);
        AcceptingStates<String> acceptingStates = new AcceptingStates<String>();
        acceptingStates.add(initialState, statesA);

        boolean thrownException = false;

        try {
            FiniteStateMachine<String> fsm = new FiniteStateMachine<String>(states,
                    initialState, acceptingStates, null, null);
        } catch (WrongAcceptingTargetStatesException e) {
            thrownException = true;
        }

        assertTrue("initalState not present, should throw WrongAcceptingTargetStatesException ", thrownException);
    }

    @Test
    public void testEndState() throws WrongAcceptingTargetStatesException, WrongInitialStateException, WrongInitialAcceptingStatesException {
        State<String> initialState = new State<String>("id1", "state1");

        States<String> states = new States<String>();
        states.add(initialState);

        AcceptingStates<String> acceptingStates = new AcceptingStates<String>();
        acceptingStates.add(initialState, null);

        FiniteStateMachine<String> fsm = new FiniteStateMachine<String>(states,
                initialState, acceptingStates, null, null);

    }
}
