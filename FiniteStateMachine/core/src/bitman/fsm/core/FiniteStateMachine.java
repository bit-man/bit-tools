package bitman.fsm.core;

import com.sun.istack.internal.NotNull;

import java.util.Iterator;
import java.util.Set;

/**
 * User: bit-man
 * Date: 7/1/11
 * Time: 8:56 AM
 */
public class FiniteStateMachine<S> {
    private States<S> states;
    private State<S> currentState;
    private IAlphabet inputAlphabet;
    private ITransitionFunction transitionFunction;
    private AcceptingStates<S> acceptingStates;

    public FiniteStateMachine(@NotNull States<S> states, @NotNull State<S> initialState,
                              @NotNull AcceptingStates<S> acceptingStates,
                              @NotNull IAlphabet inputAlphabet, @NotNull ITransitionFunction transitionFunction)
            throws WrongInitialStateException, WrongAcceptingTargetStatesException, WrongInitialAcceptingStatesException {


        // initial state must be into declared states
        chekInitialState(states, initialState);

        // All accepting states must be declared into declared states
        checkStartingStates(states, acceptingStates);

        // All destination accepting states must be declared into declared states
        checkDestinationStates(states, acceptingStates);


        this.states = states;
        this.currentState = initialState;
        this.acceptingStates = acceptingStates;
        this.inputAlphabet = inputAlphabet;
        this.transitionFunction = transitionFunction;
    }

    private void checkStartingStates(States<S> states, AcceptingStates<S> acceptingStates) throws WrongInitialAcceptingStatesException {
        if (!states.containsAll(acceptingStates.keySet()))
            throw new WrongInitialAcceptingStatesException();
    }

    private void checkDestinationStates(States<S> states, AcceptingStates<S> acceptingStates) throws WrongAcceptingTargetStatesException {
        Iterator iterator = states.iterator();
        while (iterator.hasNext()) {
            State state = (State<S>) iterator.next();
            Set<State<S>> targetStates = acceptingStates.get(state);
            // Testing for targetStates != null means that if 'state' is a final state
            // testing for containsAll returns false and incorrectly throwing an exception
            if (targetStates != null && !states.containsAll(targetStates))
                throw new WrongAcceptingTargetStatesException(state);
        }
    }

    private void chekInitialState(States<S> states, State<S> initialState) throws WrongInitialStateException {
        if (!states.contains(initialState)) {
            throw new WrongInitialStateException();
        }
    }
}
