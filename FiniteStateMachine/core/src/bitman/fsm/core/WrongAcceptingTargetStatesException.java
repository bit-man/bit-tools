package bitman.fsm.core;

/**
 * User: bit-man
 * Date: 7/2/11
 * Time: 6:24 AM
 */
public class WrongAcceptingTargetStatesException extends Throwable {
    private State state;

    public WrongAcceptingTargetStatesException(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }
}
