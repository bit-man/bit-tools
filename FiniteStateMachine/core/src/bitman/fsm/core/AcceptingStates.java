package bitman.fsm.core;

import com.sun.istack.internal.Nullable;

import java.util.HashMap;
import java.util.Set;

/**
 * User: bit-man
 * Date: 7/2/11
 * Time: 6:46 AM
 */
public class AcceptingStates<T>
        extends HashMap<State<T>, Set<State<T>>> {
    public void add(State<T> fromThisState, @Nullable Set<State<T>> toThese) {
        put(fromThisState, toThese);
    }
}
