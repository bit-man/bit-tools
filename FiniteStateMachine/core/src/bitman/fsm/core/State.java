package bitman.fsm.core;

/**
 * User: bit-man
 * Date: 7/1/11
 * Time: 8:52 AM
 */
public class State<T> {
    private T id;
    private String name;

    public State(T id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "State{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public T getID() {
        return id;
    }


}
