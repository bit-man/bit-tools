#!/bin/bash

. `dirname $0`/lib.bash


function usage() {
    cat <<EOH

    $0 --xml archivo_junit_xml --prefijo prefijo_clase_junit
        --path path_clases_junit

    archivo_junit_xml : path completo al archivo de salida de ejecución
                        de JUnit en formato XML
    prefijo_clase_junit : prefijo de las clases JUnit generadas por Randoop
                        (ej. RandoopTest0.java RandoopTest1.java ...
                        tienen como prefijo RandoopTest)
    path_clases_junit : directorio de las clases de JUnit
                        (ej. ej2/conInvariante

    Ejemplo de invocación del comando :

    $0 --xml ej2/conInvariante/salida/Randoop_con_invariante.xml
        --prefijo RandoopTestConInvariante --path ej2/conInvariante

EOH
}

while [[ $# -ne 0 ]]; do
    if [[ "$1" == "--xml" ]]; then
       shift
       __archivoXML=$1
    elif [[ "$1" == "--prefijo" ]]; then
       shift
       __prefijo=$1
    elif [[ "$1" == "--path" ]]; then
       shift
       __dir=$1
    else
       usage
       exit -1
    fi

    shift
done

[[ -z ${__archivoXML} || -z ${__prefijo} || -z ${__dir} ]] && \
    usage && exit -1

compilar
javaC ${__home}/ej2/util/EstadisticasJunit.java

java -classpath ${__out} EstadisticasJunit ${__archivoXML} ${__prefijo} ${__dir}
