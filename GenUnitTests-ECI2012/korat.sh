#!/bin/bash

. `dirname $0`/lib.bash

__debug=0

function usage() {
    cat <<EOH

    $0 --ser archivo_serializacion [--debug]

    archivo_serializacion : path del archivo donde se van a serializar
                            las entradas generadas

    debug : permite hacer debugging (port 5005)

EOH
}

while [[ $# -ne 0 ]]; do
    if [[ "$1" == "--debug" ]]; then
       __debug=1
    elif [[ "$1" == "--ser" ]]; then
       shift
       __archivoSer=$1
    else
       usage
       exit -1
    fi

    shift
done

[[ -z ${__archivoSer} ]] && \
    usage && exit -1


compilar

[[ ${__debug} -eq 1 ]] && __debugFlags=`getDebugFlags`
##
# args :
# tamMinCola (entero)       : tamaño de la cola más pequeña a generar
# tamMaxCola (entero)       : tamaño de la cola más grande a generar
# nodoValorMinimo (entero)  : valor más pequeño del nodo a generar
# nodoValorMaximo (entero)  : valor mas grande del nodo a generar

java ${__debugFlags}  -cp ${__koratJars}:${__out} korat.Korat --class ColaSobreListasEnlazadas --args 0,4,0,1 --visualize \
    --serialize "${__archivoSer}"