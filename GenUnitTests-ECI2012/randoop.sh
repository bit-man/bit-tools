#!/bin/bash

. `dirname $0`/lib.bash


function usage() {
    cat <<EOH

    $0 directorio_salida_junit

    directorio_salida_junit : nombre del directorio en el cual se grabarán los
                              archivos de JUnit

EOH
}

[[ -z $1 ]] && \
    usage && exit -1

mkdir -p $1

compilar

java -classpath ${__randoopJar}:${__out}:${__koratJars} randoop.main.Main gentests \
    --junit-output-dir=$1 --testclass=ColaSobreListasEnlazadas  --timelimit=10
