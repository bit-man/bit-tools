#!/bin/bash

. `dirname $0`/lib.bash



function usage() {
    cat <<EOH

    $0 --ser archivo_serializacion

    archivo_serializacion : path del archivo donde se serializaron
                            las entradas generadas

EOH
}

while [[ $# -ne 0 ]]; do
    if [[ "$1" == "--ser" ]]; then
       shift
       __archivoSer=$1
    else
       usage
       exit -1
    fi

    shift
done


[[ -z ${__archivoSer} ]] && \
    usage && exit -1

export __junitFlags="-Darchivo=${__archivoSer}"
${__home}/runJunit.sh --junit ${__ej3Test} --suite EncolarDesencolarTest