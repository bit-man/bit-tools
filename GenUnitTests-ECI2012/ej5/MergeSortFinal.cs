using System;

public class Program 
{
        public static int[] Puzzle(int[] array) {
          mergeSort(array, 0, array.Length);
          return  array;
        }
        
        // first : primer elemento
        // n : cant. de elementos
        public static void mergeSort(int[] array, int first, int n)
        {
            int n1;
            int n2;
            if (n > 1)
            {
                n1 = n / 2;
                n2 = n - n1;
                mergeSort(array, first, n1);
                mergeSort(array, first + n1, n2);
                merge(array, first, n1, n2);
            }
        }

        // first : primer elemento
        // n1 : ?
        // n2 : ?
        private static void merge(int[] array, int first, int n1, int n2)
        {
            int[] temp = new int[n1 + n2];
            int copied = 0;
            int copied1 = 0;
            int copied2 = 0;
            int i;
            while ((copied1 < n1) && (copied2 < n2))
            {
                if (array[first + copied1] < array[first + n1 + copied2])
                    temp[copied++] = array[first + (copied1++)];
                else
                    temp[copied++] = array[first + n1 + (copied2++)];
            }
            while (copied1 < n1)
                temp[copied++] = array[first + (copied1++)];
            while (copied2 < n2)
                temp[copied++] = array[first + n1 + (copied2++)];
            for (i = 0; i < n1 + n2; i++)
                array[first + i] = temp[i];
        }
}
