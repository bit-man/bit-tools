﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace MyVeryBasicPexExamples
{

    public class SortArrays
    {

        public static void selectionSort(int[] array)
        {
            int i = 0;
            while (i < array.Length)
            {
                int min, aux;
                int indiceMin, j;
                min = array[i];
                indiceMin = i;
                j = i;
                while (j < array.Length)
                {
                    if (array[j] < min)
                    {
                        min = array[j];
                        indiceMin = j;
                    }
                    j++;
                }
                aux = array[i];
                array[i] = array[indiceMin];
                array[indiceMin] = aux;
                i++;
            }
        }

        public static void mergeSort(int[] array, int first, int n)
        {
            int n1;
            int n2;
            if (n > 1)
            {
                n1 = n / 2;
                n2 = n - n1;
                mergeSort(array, first, n1);
                mergeSort(array, first + n1 - 1, n2);
                merge(array, first, n1, n2);
            }
        }

        private static void merge(int[] array, int first, int n1, int n2)
        {
            int[] temp = new int[n1 + n2];
            int copied = 0;
            int copied1 = 0;
            int copied2 = 0;
            int i;
            while ((copied1 < n1) && (copied2 < n2))
            {
                if (array[first + copied1] < array[first + n1 + copied2])
                    temp[copied++] = array[first + (copied1)];
                else
                    temp[copied++] = array[first + n1 + (copied2++)];
            }
            while (copied1 < n1)
                temp[copied++] = array[first + (copied1++)];
            while (copied2 < n2)
                temp[copied++] = array[first + n1 + (copied2++)];
            for (i = 0; i < n1 + n2; i++)
                array[first + i] = temp[i];
        }
    }

}
