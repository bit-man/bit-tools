﻿using System;

public class Program 
{
        // El método no puede tener void como tipo de dato a devolver
        public static int[] selectionSort(int[] array)
        {
            
            // No puede mutar ninguno de los argumentos
            // Para evitarlo se clona el único argumento sobre el mismo
            array = (int[]) array.Clone();
           
              
            int i = 0;
            while (i < array.Length)
            {
                int min, aux;
                int indiceMin, j;
                min = array[i];
                indiceMin = i;
                j = i;
                while (j < array.Length)
                {
                    if (array[j] < min)
                    {
                        min = array[j];
                        indiceMin = j;
                    }
                    j++;
                }
                aux = array[i];
                array[i] = array[indiceMin];
                array[indiceMin] = aux;
                i++;
            }
            
            // No puede devolver void
            return array;
        }
}
