package enunciado;

/**
 * Implementacion del TAD Cola, usando una estructura dinámica, más 
 * precisamente una lista enlazada.
 * Esta clase implementa los métodos abstractos declarados en Cola, y
 * corresponde a una implementación polimórfica.
 * @author Nazareno Aguirre
 * @version 0.2 29/7/2012
 */

public class ColaSobreListasEnlazadas {
	
	private Nodo fin;	// referencia al último nodo
						// de la lista enlazada circular.
						// La lista enlazada está vacía si
						// fin == null.
	
	private int numItems; // cantidad de elementos de la cola
    
	/** 
	 * Constructor de la clase ColaSobreListasEnlazadas.   
	 * @pre. true.
	 * @post. Se inicializa fin en null (cola vacía).
	 */	
    public ColaSobreListasEnlazadas() {
		
        fin = null;
		
    } 
	
	/** 
	 * Indica si la cola es vacía o no.
	 * @return true ssi la cola no tiene elementos
	 * @pre. true.
	 * @post. Retorna true ssi la cola no tiene elementos
	 */	
    public boolean esVacia() {
		
        return (fin == null);
		
    } 
	
	/** 
	 * inserta item al final de la cola de elementos.
	 * @param item es el objeto a insertar en la cola.
	 * @pre. true  
	 * @post. Si la estructura subyacente a la cola no está agotada en espacio,
	 * se inserta item al final de la cola. Si la inserción falla por algún
	 * motivo, lanza una excepción de tipo RuntimeException.
	 */	
	public void encolar(Object nuevoItem) throws RuntimeException {
        Nodo nuevoNodo = new Nodo(nuevoItem);
        if (esVacia()) {
            nuevoNodo.cambiarSiguiente(nuevoNodo);
        }
        else {
            nuevoNodo.cambiarSiguiente(fin.obtenerSiguiente());
            fin.cambiarSiguiente(nuevoNodo);
        }
        fin = nuevoNodo;
    }  
	
	/** 
	 * retorna item al comienzo de la cola de elementos.
	 * @return el objeto al comienzo de la cola
	 * @pre. la cola no está vacía  
	 * @post. Si la cola no está vacía, se retorna el elemento al comienzo de la
	 * misma (es decir, el siguiente del último, en la representación como cola 
	 * circular). Este método no modifica el estado de la cola.
	 */		
	public Object primero() {
		return (fin.obtenerSiguiente().obtenerItem());
	}
	
	/** 
	 * elimina item al comienzo de la cola de elementos.
	 * @pre. la cola no está vacía  
	 * @post. Si la cola no está vacía, se elimina el elemento al comienzo de la
	 * misma (es decir, el siguiente del último, en la representación como cola 
	 * circular). 
	 */			
	public void desencolar() {
        Nodo segundo = fin.obtenerSiguiente().obtenerSiguiente();
		fin.cambiarSiguiente(segundo);
	}
	
	/** 
	 * retorna cantidad de elementos en la cola
	 * @return cantidad de elementos de la cola
	 * @pre. true  
	 * @post. Se retorna la longitud de la cola (almacenada en numItems)
	 */			
	public int cantidad() {
		return numItems;
	}
	
	/** 
	 * invariante de representación de la estructura.
	 * @return true si y sólo si la estructura es internamente consistente
	 * @pre. true  
	 * @post. Se retorna true si y sólo si la cola es circular, cada elemento de la misma
	 * es no nulo, y numItems coincide con la cantidad de nodos en la cola circular.
	 */				
	public boolean repOK() {
		return true;
	}	
	
} 
