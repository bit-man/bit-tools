import java.io.Serializable;

/**
 * Clase Nodo: Usada para construir listas enlazadas para implementaciones
 * basadas en referencias de listas, pilas, colas, etc., polimórficas.
 * @author Nazareno Aguirre
 * @version 0.2 29/7/2012
 */
public class Nodo
        implements Serializable {

    private static final long serialVersionUID = -450893148162757390L;
    private Object item;
    private Nodo siguiente;

	/** 
	 * Constructor de la clase Nodo.   
	 * @pre. true.
	 * @post. item y siguiente se setean en null
	 */	
    public Nodo() {
        item = null;
	    siguiente = null;
    } 
    
	/** 
	 * Constructor de la clase Nodo.
	 * @param nuevoItem es el item usado para modificar el campo item.
	 * @pre. true.
	 * @post. item se setea con nuevoItem, y siguiente se setea en null.
	 */	
    public Nodo(Object nuevoItem) {
        item = nuevoItem;
		siguiente = null;
    } 
    
	/** 
	 * Constructor de la clase Nodo.  
	 * @param nuevoItem es el item usado para modificar el campo item.
	 * @param siguienteNodo es la referenica usada para modificar
	 * el campo siguiente.
	 * @pre. true.
	 * @post.  el atributo item se setea con nuevoItem y el atributo
	 * siguiente se setea con siguienteNodo.
	 */	
    public Nodo(Object nuevoItem, Nodo siguienteNodo) {
        item = nuevoItem;
		siguiente = siguienteNodo;
    } 
    
	/** 
	 * setea el atributo item del nodo. 
	 * @param nuevoItem es el item usado para modificar el campo item.
	 * @pre. true.
	 * @post. item se setea con nuevoItem.
	 */	
	public void cambiarItem(Object nuevoItem) {
        item = nuevoItem;
    } 
    
	/** 
	 * setea el atributo siguiente del nodo. 
	 * @param siguienteNodo es la referencia usada para modificar el
	 * campo siguiente.
	 * @pre. true.
	 * @post. siguiente se setea con siguienteNodo.
	 */	
    public void cambiarSiguiente(Nodo siguienteNodo) {
        siguiente = siguienteNodo;
    } 
	
	/** 
	 * retorna el item de un nodo.  
	 * return la referencia al campo item del ndo.
	 * @pre. true.
	 * @post. se retorna la referencia al campo item del nodo.
	 */	
    public Object obtenerItem() {
        return item;
    } 
	
	/** 
	 * retorna el siguiente de un nodo.  
	 * return la referencia al campo siguiente del ndo.
	 * @pre. true.
	 * @post. se retorna la referencia al campo siguiente del nodo.
	 */	
    public Nodo obtenerSiguiente() {
        return siguiente;
    } 

} 
