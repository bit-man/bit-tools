package ej3.test;

import enunciado.ColaSobreListasEnlazadas;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.*;
import java.util.*;

/**
 * Description : Test de desencolar/encolar para ColaSobreListasEnlazadas
 * Date: 8/24/12
 * Time: 2:51 PM
 */

@RunWith(Parameterized.class)
public class EncolarDesencolarTest
        extends TestCase {

    public static final String ARCHIVO = "archivo";

    private ColaSobreListasEnlazadas cola;

    public EncolarDesencolarTest(ColaSobreListasEnlazadas cola) {
        this.cola = cola;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> datos() throws IOException, ClassNotFoundException {

        String archivo = obtenerNombreArchivo();
        List<ColaSobreListasEnlazadas> cola = deserializar(archivo);
        ColaSobreListasEnlazadas[][] ret = convertir(cola);

        return Arrays.asList((Object[][]) ret);

    }

    private static String obtenerNombreArchivo() throws FileNotFoundException {
        String archivo = System.getProperty(ARCHIVO);
        if (archivo == null)
            throw new FileNotFoundException("Nombre de archivo no especificado");

        return archivo;
    }

    private static ColaSobreListasEnlazadas[][] convertir(List<ColaSobreListasEnlazadas> cola) {
        ColaSobreListasEnlazadas[][] ret = new ColaSobreListasEnlazadas[cola.size()][1];
        int i = 0;
        for (ColaSobreListasEnlazadas c : cola)
            ret[i++][0] = c;
        return ret;
    }

    private static List<ColaSobreListasEnlazadas> deserializar(String archivo) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(archivo);
        ObjectInputStream in = new ObjectInputStream(fis);

        List<ColaSobreListasEnlazadas> cola = new LinkedList<ColaSobreListasEnlazadas>();

        try {
            ColaSobreListasEnlazadas cola1 = null;
            while ((cola1 = (ColaSobreListasEnlazadas) in.readObject()) != null)
                cola.add(cola1);

        } catch (EOFException e) {
            // Todos los objetos fueron leídos
        }

        in.close();
        return cola;
    }

    @Test
    public void testEncolar() {
        final int cantidadAntes = cola.cantidad();
        cola.encolar(new Object());

        assertEquals("Tamaño de cola incorrecto", cantidadAntes + 1, cola.cantidad());
    }

    @Test
    public void testDesencolar() {
        final int cantidadAntes = cola.cantidad();
        if ( cantidadAntes > 0 ) { // Cumplir con la precondición
            cola.desencolar();
            assertEquals("Tamaño de cola incorrecto", cantidadAntes - 1, cola.cantidad());
        }

    }
}
