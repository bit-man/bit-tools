package ej2.conInvariante;

import enunciado.ColaSobreListasEnlazadas;
import junit.framework.*;

public class RandoopTestConInvariante3 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test1");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    var0.desencolar();

  }

  public void test2() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test2");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    var0.desencolar();

  }

  public void test3() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test3");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    int var16 = var11.cantidad();
    boolean var17 = var11.esVacia();
    int var18 = var11.cantidad();
    boolean var19 = var11.esVacia();
    int var20 = var11.cantidad();
    boolean var21 = var11.esVacia();
    boolean var22 = var11.esVacia();
    int var23 = var11.cantidad();
    int var24 = var11.cantidad();
    int var25 = var11.cantidad();
    var0.encolar((java.lang.Object)var25);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test4() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test4");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    int var15 = var13.cantidad();
    boolean var16 = var13.esVacia();
    int var17 = var13.cantidad();
    boolean var18 = var13.esVacia();
    boolean var19 = var13.esVacia();
    var0.encolar((java.lang.Object)var19);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test5() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test5");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    java.lang.Object var11 = var0.primero();

  }

  public void test6() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test6");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    java.lang.Object var14 = var0.primero();

  }

  public void test7() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test7");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    var0.desencolar();

  }

  public void test8() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test8");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    java.lang.Object var13 = var0.primero();

  }

  public void test9() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test9");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    java.lang.Object var11 = var0.primero();

  }

  public void test10() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test10");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    int var15 = var12.cantidad();
    boolean var16 = var12.esVacia();
    var0.encolar((java.lang.Object)var16);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test11() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test11");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    int var16 = var0.cantidad();
    boolean var17 = var0.esVacia();
    int var18 = var0.cantidad();
    ColaSobreListasEnlazadas var19 = new ColaSobreListasEnlazadas();
    int var20 = var19.cantidad();
    boolean var21 = var19.esVacia();
    boolean var22 = var19.esVacia();
    boolean var23 = var19.esVacia();
    int var24 = var19.cantidad();
    boolean var25 = var19.esVacia();
    boolean var26 = var19.esVacia();
    int var27 = var19.cantidad();
    boolean var28 = var19.esVacia();
    int var29 = var19.cantidad();
    int var30 = var19.cantidad();
    var0.encolar((java.lang.Object)var30);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test12() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test12");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    boolean var12 = var11.esVacia();
    int var13 = var11.cantidad();
    boolean var14 = var11.esVacia();
    int var15 = var11.cantidad();
    int var16 = var11.cantidad();
    boolean var17 = var11.esVacia();
    boolean var18 = var11.esVacia();
    var0.encolar((java.lang.Object)var18);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test13() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test13");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    int var16 = var11.cantidad();
    boolean var17 = var11.esVacia();
    int var18 = var11.cantidad();
    boolean var19 = var11.esVacia();
    boolean var20 = var11.esVacia();
    int var21 = var11.cantidad();
    int var22 = var11.cantidad();
    boolean var23 = var11.esVacia();
    var0.encolar((java.lang.Object)var23);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test14() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test14");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    java.lang.Object var11 = var0.primero();

  }

  public void test15() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test15");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    java.lang.Object var12 = var0.primero();

  }

  public void test16() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test16");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    var0.desencolar();

  }

  public void test17() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test17");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    ColaSobreListasEnlazadas var8 = new ColaSobreListasEnlazadas();
    int var9 = var8.cantidad();
    boolean var10 = var8.esVacia();
    boolean var11 = var8.esVacia();
    boolean var12 = var8.esVacia();
    int var13 = var8.cantidad();
    boolean var14 = var8.esVacia();
    boolean var15 = var8.esVacia();
    boolean var16 = var8.esVacia();
    boolean var17 = var8.esVacia();
    int var18 = var8.cantidad();
    int var19 = var8.cantidad();
    boolean var20 = var8.esVacia();
    int var21 = var8.cantidad();
    var0.encolar((java.lang.Object)var8);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test18() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test18");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    java.lang.Object var13 = var0.primero();

  }

  public void test19() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test19");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    boolean var15 = var0.esVacia();
    java.lang.Object var16 = var0.primero();

  }

  public void test20() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test20");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    var0.encolar((java.lang.Object)(short)(-1));
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test21() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test21");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    int var14 = var13.cantidad();
    boolean var15 = var13.esVacia();
    boolean var16 = var13.esVacia();
    boolean var17 = var13.esVacia();
    boolean var18 = var13.esVacia();
    boolean var19 = var13.esVacia();
    boolean var20 = var13.esVacia();
    boolean var21 = var13.esVacia();
    boolean var22 = var13.esVacia();
    var0.encolar((java.lang.Object)var13);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test22() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test22");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    var0.desencolar();

  }

  public void test23() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test23");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    java.lang.Object var13 = var0.primero();

  }

  public void test24() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test24");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    var0.desencolar();

  }

  public void test25() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test25");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    java.lang.Object var15 = var0.primero();

  }

  public void test26() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test26");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    var0.desencolar();

  }

  public void test27() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test27");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    var0.encolar((java.lang.Object)0);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test28() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test28");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    java.lang.Object var13 = var0.primero();

  }

  public void test29() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test29");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    java.lang.Object var14 = var0.primero();

  }

  public void test30() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test30");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    java.lang.Object var14 = var0.primero();

  }

  public void test31() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test31");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    var0.desencolar();

  }

  public void test32() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test32");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    int var15 = var13.cantidad();
    int var16 = var13.cantidad();
    int var17 = var13.cantidad();
    int var18 = var13.cantidad();
    boolean var19 = var13.esVacia();
    int var20 = var13.cantidad();
    int var21 = var13.cantidad();
    boolean var22 = var13.esVacia();
    int var23 = var13.cantidad();
    int var24 = var13.cantidad();
    boolean var25 = var13.esVacia();
    boolean var26 = var13.esVacia();
    int var27 = var13.cantidad();
    var0.encolar((java.lang.Object)var27);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test33() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test33");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    boolean var13 = var12.esVacia();
    int var14 = var12.cantidad();
    int var15 = var12.cantidad();
    int var16 = var12.cantidad();
    boolean var17 = var12.esVacia();
    boolean var18 = var12.esVacia();
    int var19 = var12.cantidad();
    boolean var20 = var12.esVacia();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test34() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test34");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    ColaSobreListasEnlazadas var5 = new ColaSobreListasEnlazadas();
    int var6 = var5.cantidad();
    boolean var7 = var5.esVacia();
    boolean var8 = var5.esVacia();
    boolean var9 = var5.esVacia();
    int var10 = var5.cantidad();
    boolean var11 = var5.esVacia();
    int var12 = var5.cantidad();
    boolean var13 = var5.esVacia();
    int var14 = var5.cantidad();
    boolean var15 = var5.esVacia();
    boolean var16 = var5.esVacia();
    var0.encolar((java.lang.Object)var16);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test35() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test35");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    var0.desencolar();

  }

  public void test36() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test36");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    boolean var12 = var11.esVacia();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    int var16 = var11.cantidad();
    int var17 = var11.cantidad();
    int var18 = var11.cantidad();
    var0.encolar((java.lang.Object)var11);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test37() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test37");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    java.lang.Object var9 = var0.primero();

  }

  public void test38() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test38");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    java.lang.Object var9 = var0.primero();

  }

  public void test39() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test39");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    boolean var15 = var14.esVacia();
    int var16 = var14.cantidad();
    int var17 = var14.cantidad();
    int var18 = var14.cantidad();
    int var19 = var14.cantidad();
    boolean var20 = var14.esVacia();
    boolean var21 = var14.esVacia();
    int var22 = var14.cantidad();
    int var23 = var14.cantidad();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test40() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test40");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    ColaSobreListasEnlazadas var15 = new ColaSobreListasEnlazadas();
    int var16 = var15.cantidad();
    boolean var17 = var15.esVacia();
    boolean var18 = var15.esVacia();
    boolean var19 = var15.esVacia();
    int var20 = var15.cantidad();
    int var21 = var15.cantidad();
    int var22 = var15.cantidad();
    boolean var23 = var15.esVacia();
    boolean var24 = var15.esVacia();
    boolean var25 = var15.esVacia();
    int var26 = var15.cantidad();
    boolean var27 = var15.esVacia();
    boolean var28 = var15.esVacia();
    var0.encolar((java.lang.Object)var28);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test41() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test41");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    var0.desencolar();

  }

  public void test42() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test42");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    ColaSobreListasEnlazadas var8 = new ColaSobreListasEnlazadas();
    boolean var9 = var8.esVacia();
    boolean var10 = var8.esVacia();
    int var11 = var8.cantidad();
    boolean var12 = var8.esVacia();
    boolean var13 = var8.esVacia();
    int var14 = var8.cantidad();
    boolean var15 = var8.esVacia();
    boolean var16 = var8.esVacia();
    var0.encolar((java.lang.Object)var16);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test43() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test43");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    boolean var13 = var12.esVacia();
    int var14 = var12.cantidad();
    int var15 = var12.cantidad();
    int var16 = var12.cantidad();
    int var17 = var12.cantidad();
    boolean var18 = var12.esVacia();
    boolean var19 = var12.esVacia();
    boolean var20 = var12.esVacia();
    boolean var21 = var12.esVacia();
    int var22 = var12.cantidad();
    int var23 = var12.cantidad();
    int var24 = var12.cantidad();
    int var25 = var12.cantidad();
    int var26 = var12.cantidad();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test44() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test44");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    boolean var16 = var0.esVacia();
    int var17 = var0.cantidad();
    int var18 = var0.cantidad();
    ColaSobreListasEnlazadas var19 = new ColaSobreListasEnlazadas();
    int var20 = var19.cantidad();
    boolean var21 = var19.esVacia();
    boolean var22 = var19.esVacia();
    boolean var23 = var19.esVacia();
    int var24 = var19.cantidad();
    int var25 = var19.cantidad();
    int var26 = var19.cantidad();
    boolean var27 = var19.esVacia();
    boolean var28 = var19.esVacia();
    int var29 = var19.cantidad();
    boolean var30 = var19.esVacia();
    boolean var31 = var19.esVacia();
    int var32 = var19.cantidad();
    int var33 = var19.cantidad();
    boolean var34 = var19.esVacia();
    int var35 = var19.cantidad();
    int var36 = var19.cantidad();
    boolean var37 = var19.esVacia();
    var0.encolar((java.lang.Object)var19);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test45() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test45");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    java.lang.Object var12 = var0.primero();

  }

  public void test46() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test46");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    var0.desencolar();

  }

  public void test47() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test47");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    java.lang.Object var12 = var0.primero();

  }

  public void test48() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test48");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    int var14 = var13.cantidad();
    boolean var15 = var13.esVacia();
    int var16 = var13.cantidad();
    var0.encolar((java.lang.Object)var13);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test49() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test49");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    int var11 = var10.cantidad();
    boolean var12 = var10.esVacia();
    boolean var13 = var10.esVacia();
    boolean var14 = var10.esVacia();
    int var15 = var10.cantidad();
    boolean var16 = var10.esVacia();
    int var17 = var10.cantidad();
    int var18 = var10.cantidad();
    boolean var19 = var10.esVacia();
    boolean var20 = var10.esVacia();
    var0.encolar((java.lang.Object)var10);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test50() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test50");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    java.lang.Object var14 = var0.primero();

  }

  public void test51() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test51");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    java.lang.Object var13 = var0.primero();

  }

  public void test52() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test52");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    java.lang.Object var12 = var0.primero();

  }

  public void test53() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test53");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    int var16 = var0.cantidad();
    boolean var17 = var0.esVacia();
    int var18 = var0.cantidad();
    boolean var19 = var0.esVacia();
    var0.desencolar();

  }

  public void test54() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test54");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    var0.encolar((java.lang.Object)100.0f);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test55() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test55");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    java.lang.Object var11 = var0.primero();

  }

  public void test56() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test56");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    java.lang.Object var11 = var0.primero();

  }

  public void test57() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test57");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    boolean var16 = var0.esVacia();
    int var17 = var0.cantidad();
    int var18 = var0.cantidad();
    java.lang.Object var19 = var0.primero();

  }

  public void test58() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test58");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    int var13 = var11.cantidad();
    int var14 = var11.cantidad();
    int var15 = var11.cantidad();
    int var16 = var11.cantidad();
    int var17 = var11.cantidad();
    int var18 = var11.cantidad();
    boolean var19 = var11.esVacia();
    int var20 = var11.cantidad();
    int var21 = var11.cantidad();
    var0.encolar((java.lang.Object)var21);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test59() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test59");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    int var16 = var11.cantidad();
    boolean var17 = var11.esVacia();
    boolean var18 = var11.esVacia();
    int var19 = var11.cantidad();
    boolean var20 = var11.esVacia();
    boolean var21 = var11.esVacia();
    int var22 = var11.cantidad();
    boolean var23 = var11.esVacia();
    var0.encolar((java.lang.Object)var23);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test60() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test60");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    boolean var10 = var9.esVacia();
    int var11 = var9.cantidad();
    boolean var12 = var9.esVacia();
    int var13 = var9.cantidad();
    int var14 = var9.cantidad();
    int var15 = var9.cantidad();
    boolean var16 = var9.esVacia();
    int var17 = var9.cantidad();
    boolean var18 = var9.esVacia();
    boolean var19 = var9.esVacia();
    boolean var20 = var9.esVacia();
    var0.encolar((java.lang.Object)var9);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test61() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test61");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    ColaSobreListasEnlazadas var4 = new ColaSobreListasEnlazadas();
    int var5 = var4.cantidad();
    boolean var6 = var4.esVacia();
    boolean var7 = var4.esVacia();
    boolean var8 = var4.esVacia();
    int var9 = var4.cantidad();
    boolean var10 = var4.esVacia();
    boolean var11 = var4.esVacia();
    int var12 = var4.cantidad();
    boolean var13 = var4.esVacia();
    int var14 = var4.cantidad();
    boolean var15 = var4.esVacia();
    int var16 = var4.cantidad();
    var0.encolar((java.lang.Object)var4);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test62() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test62");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    java.lang.Object var8 = var0.primero();

  }

  public void test63() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test63");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    boolean var12 = var11.esVacia();
    int var13 = var11.cantidad();
    int var14 = var11.cantidad();
    int var15 = var11.cantidad();
    int var16 = var11.cantidad();
    int var17 = var11.cantidad();
    int var18 = var11.cantidad();
    boolean var19 = var11.esVacia();
    int var20 = var11.cantidad();
    int var21 = var11.cantidad();
    int var22 = var11.cantidad();
    boolean var23 = var11.esVacia();
    var0.encolar((java.lang.Object)var23);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test64() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test64");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    java.lang.Object var6 = var0.primero();

  }

  public void test65() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test65");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    boolean var13 = var12.esVacia();
    int var14 = var12.cantidad();
    int var15 = var12.cantidad();
    int var16 = var12.cantidad();
    int var17 = var12.cantidad();
    int var18 = var12.cantidad();
    boolean var19 = var12.esVacia();
    boolean var20 = var12.esVacia();
    int var21 = var12.cantidad();
    boolean var22 = var12.esVacia();
    int var23 = var12.cantidad();
    int var24 = var12.cantidad();
    boolean var25 = var12.esVacia();
    boolean var26 = var12.esVacia();
    boolean var27 = var12.esVacia();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test66() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test66");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    java.lang.Object var14 = var0.primero();

  }

  public void test67() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test67");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    boolean var16 = var14.esVacia();
    boolean var17 = var14.esVacia();
    int var18 = var14.cantidad();
    int var19 = var14.cantidad();
    int var20 = var14.cantidad();
    int var21 = var14.cantidad();
    int var22 = var14.cantidad();
    int var23 = var14.cantidad();
    int var24 = var14.cantidad();
    int var25 = var14.cantidad();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test68() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test68");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    var0.desencolar();

  }

  public void test69() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test69");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    java.lang.Object var14 = var0.primero();

  }

  public void test70() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test70");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    var0.desencolar();

  }

  public void test71() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test71");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    java.lang.Object var14 = var0.primero();

  }

  public void test72() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test72");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    var0.desencolar();

  }

  public void test73() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test73");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    java.lang.Object var11 = var0.primero();

  }

  public void test74() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test74");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    var0.desencolar();

  }

  public void test75() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test75");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    java.lang.Object var12 = var0.primero();

  }

  public void test76() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test76");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    var0.encolar((java.lang.Object)100L);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test77() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test77");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    int var14 = var13.cantidad();
    boolean var15 = var13.esVacia();
    boolean var16 = var13.esVacia();
    boolean var17 = var13.esVacia();
    boolean var18 = var13.esVacia();
    boolean var19 = var13.esVacia();
    boolean var20 = var13.esVacia();
    boolean var21 = var13.esVacia();
    int var22 = var13.cantidad();
    int var23 = var13.cantidad();
    int var24 = var13.cantidad();
    boolean var25 = var13.esVacia();
    var0.encolar((java.lang.Object)var25);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test78() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test78");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    boolean var16 = var12.esVacia();
    boolean var17 = var12.esVacia();
    boolean var18 = var12.esVacia();
    boolean var19 = var12.esVacia();
    boolean var20 = var12.esVacia();
    int var21 = var12.cantidad();
    int var22 = var12.cantidad();
    int var23 = var12.cantidad();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test79() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test79");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    boolean var16 = var14.esVacia();
    boolean var17 = var14.esVacia();
    boolean var18 = var14.esVacia();
    int var19 = var14.cantidad();
    boolean var20 = var14.esVacia();
    int var21 = var14.cantidad();
    boolean var22 = var14.esVacia();
    int var23 = var14.cantidad();
    boolean var24 = var14.esVacia();
    int var25 = var14.cantidad();
    boolean var26 = var14.esVacia();
    boolean var27 = var14.esVacia();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test80() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test80");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    var0.desencolar();

  }

  public void test81() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test81");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    java.lang.Object var10 = var0.primero();

  }

  public void test82() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test82");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    int var16 = var12.cantidad();
    int var17 = var12.cantidad();
    int var18 = var12.cantidad();
    int var19 = var12.cantidad();
    int var20 = var12.cantidad();
    int var21 = var12.cantidad();
    boolean var22 = var12.esVacia();
    boolean var23 = var12.esVacia();
    int var24 = var12.cantidad();
    var0.encolar((java.lang.Object)var24);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test83() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test83");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    ColaSobreListasEnlazadas var15 = new ColaSobreListasEnlazadas();
    boolean var16 = var15.esVacia();
    int var17 = var15.cantidad();
    boolean var18 = var15.esVacia();
    int var19 = var15.cantidad();
    int var20 = var15.cantidad();
    boolean var21 = var15.esVacia();
    boolean var22 = var15.esVacia();
    var0.encolar((java.lang.Object)var15);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test84() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test84");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    int var16 = var0.cantidad();
    boolean var17 = var0.esVacia();
    int var18 = var0.cantidad();
    ColaSobreListasEnlazadas var19 = new ColaSobreListasEnlazadas();
    int var20 = var19.cantidad();
    int var21 = var19.cantidad();
    int var22 = var19.cantidad();
    boolean var23 = var19.esVacia();
    int var24 = var19.cantidad();
    var0.encolar((java.lang.Object)var19);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test85() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test85");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    boolean var16 = var12.esVacia();
    int var17 = var12.cantidad();
    int var18 = var12.cantidad();
    int var19 = var12.cantidad();
    boolean var20 = var12.esVacia();
    boolean var21 = var12.esVacia();
    int var22 = var12.cantidad();
    boolean var23 = var12.esVacia();
    boolean var24 = var12.esVacia();
    int var25 = var12.cantidad();
    boolean var26 = var12.esVacia();
    int var27 = var12.cantidad();
    int var28 = var12.cantidad();
    boolean var29 = var12.esVacia();
    var0.encolar((java.lang.Object)var29);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test86() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test86");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    ColaSobreListasEnlazadas var7 = new ColaSobreListasEnlazadas();
    int var8 = var7.cantidad();
    boolean var9 = var7.esVacia();
    boolean var10 = var7.esVacia();
    boolean var11 = var7.esVacia();
    int var12 = var7.cantidad();
    int var13 = var7.cantidad();
    boolean var14 = var7.esVacia();
    var0.encolar((java.lang.Object)var7);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test87() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test87");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    var0.desencolar();

  }

  public void test88() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test88");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    ColaSobreListasEnlazadas var8 = new ColaSobreListasEnlazadas();
    boolean var9 = var8.esVacia();
    int var10 = var8.cantidad();
    int var11 = var8.cantidad();
    int var12 = var8.cantidad();
    int var13 = var8.cantidad();
    int var14 = var8.cantidad();
    boolean var15 = var8.esVacia();
    boolean var16 = var8.esVacia();
    boolean var17 = var8.esVacia();
    int var18 = var8.cantidad();
    int var19 = var8.cantidad();
    boolean var20 = var8.esVacia();
    var0.encolar((java.lang.Object)var8);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test89() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test89");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    int var10 = var9.cantidad();
    boolean var11 = var9.esVacia();
    boolean var12 = var9.esVacia();
    int var13 = var9.cantidad();
    int var14 = var9.cantidad();
    int var15 = var9.cantidad();
    int var16 = var9.cantidad();
    int var17 = var9.cantidad();
    boolean var18 = var9.esVacia();
    int var19 = var9.cantidad();
    boolean var20 = var9.esVacia();
    int var21 = var9.cantidad();
    var0.encolar((java.lang.Object)var9);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test90() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test90");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    var0.desencolar();

  }

  public void test91() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test91");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    int var10 = var9.cantidad();
    boolean var11 = var9.esVacia();
    int var12 = var9.cantidad();
    int var13 = var9.cantidad();
    int var14 = var9.cantidad();
    int var15 = var9.cantidad();
    int var16 = var9.cantidad();
    var0.encolar((java.lang.Object)var16);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test92() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test92");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    int var16 = var11.cantidad();
    boolean var17 = var11.esVacia();
    boolean var18 = var11.esVacia();
    int var19 = var11.cantidad();
    int var20 = var11.cantidad();
    boolean var21 = var11.esVacia();
    boolean var22 = var11.esVacia();
    int var23 = var11.cantidad();
    var0.encolar((java.lang.Object)var23);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test93() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test93");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    int var14 = var13.cantidad();
    boolean var15 = var13.esVacia();
    boolean var16 = var13.esVacia();
    int var17 = var13.cantidad();
    int var18 = var13.cantidad();
    int var19 = var13.cantidad();
    boolean var20 = var13.esVacia();
    int var21 = var13.cantidad();
    int var22 = var13.cantidad();
    int var23 = var13.cantidad();
    int var24 = var13.cantidad();
    var0.encolar((java.lang.Object)var13);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test94() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test94");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    boolean var15 = var0.esVacia();
    int var16 = var0.cantidad();
    boolean var17 = var0.esVacia();
    int var18 = var0.cantidad();
    java.lang.Object var19 = var0.primero();

  }

  public void test95() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test95");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    java.lang.Object var9 = var0.primero();

  }

  public void test96() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test96");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    var0.desencolar();

  }

  public void test97() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test97");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    java.lang.Object var11 = var0.primero();

  }

  public void test98() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test98");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    java.lang.Object var13 = var0.primero();

  }

  public void test99() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test99");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    boolean var15 = var14.esVacia();
    int var16 = var14.cantidad();
    int var17 = var14.cantidad();
    int var18 = var14.cantidad();
    int var19 = var14.cantidad();
    boolean var20 = var14.esVacia();
    int var21 = var14.cantidad();
    boolean var22 = var14.esVacia();
    int var23 = var14.cantidad();
    boolean var24 = var14.esVacia();
    int var25 = var14.cantidad();
    var0.encolar((java.lang.Object)var25);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test100() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test100");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    int var16 = var0.cantidad();
    int var17 = var0.cantidad();
    java.lang.Object var18 = var0.primero();

  }

  public void test101() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test101");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    var0.desencolar();

  }

  public void test102() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test102");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    java.lang.Object var11 = var0.primero();

  }

  public void test103() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test103");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    int var16 = var0.cantidad();
    int var17 = var0.cantidad();
    int var18 = var0.cantidad();
    int var19 = var0.cantidad();
    ColaSobreListasEnlazadas var20 = new ColaSobreListasEnlazadas();
    int var21 = var20.cantidad();
    boolean var22 = var20.esVacia();
    boolean var23 = var20.esVacia();
    boolean var24 = var20.esVacia();
    int var25 = var20.cantidad();
    boolean var26 = var20.esVacia();
    boolean var27 = var20.esVacia();
    int var28 = var20.cantidad();
    boolean var29 = var20.esVacia();
    boolean var30 = var20.esVacia();
    var0.encolar((java.lang.Object)var20);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test104() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test104");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    boolean var10 = var9.esVacia();
    int var11 = var9.cantidad();
    int var12 = var9.cantidad();
    int var13 = var9.cantidad();
    int var14 = var9.cantidad();
    boolean var15 = var9.esVacia();
    int var16 = var9.cantidad();
    boolean var17 = var9.esVacia();
    int var18 = var9.cantidad();
    boolean var19 = var9.esVacia();
    boolean var20 = var9.esVacia();
    int var21 = var9.cantidad();
    var0.encolar((java.lang.Object)var21);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test105() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test105");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    int var15 = var13.cantidad();
    boolean var16 = var13.esVacia();
    int var17 = var13.cantidad();
    int var18 = var13.cantidad();
    boolean var19 = var13.esVacia();
    boolean var20 = var13.esVacia();
    int var21 = var13.cantidad();
    int var22 = var13.cantidad();
    var0.encolar((java.lang.Object)var13);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test106() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test106");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    var0.desencolar();

  }

  public void test107() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test107");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    boolean var12 = var11.esVacia();
    int var13 = var11.cantidad();
    int var14 = var11.cantidad();
    int var15 = var11.cantidad();
    int var16 = var11.cantidad();
    boolean var17 = var11.esVacia();
    boolean var18 = var11.esVacia();
    boolean var19 = var11.esVacia();
    boolean var20 = var11.esVacia();
    int var21 = var11.cantidad();
    int var22 = var11.cantidad();
    int var23 = var11.cantidad();
    int var24 = var11.cantidad();
    var0.encolar((java.lang.Object)var11);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test108() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test108");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    int var11 = var10.cantidad();
    boolean var12 = var10.esVacia();
    boolean var13 = var10.esVacia();
    boolean var14 = var10.esVacia();
    int var15 = var10.cantidad();
    boolean var16 = var10.esVacia();
    boolean var17 = var10.esVacia();
    int var18 = var10.cantidad();
    boolean var19 = var10.esVacia();
    int var20 = var10.cantidad();
    boolean var21 = var10.esVacia();
    int var22 = var10.cantidad();
    var0.encolar((java.lang.Object)var10);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test109() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test109");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    boolean var16 = var14.esVacia();
    boolean var17 = var14.esVacia();
    boolean var18 = var14.esVacia();
    int var19 = var14.cantidad();
    int var20 = var14.cantidad();
    int var21 = var14.cantidad();
    boolean var22 = var14.esVacia();
    boolean var23 = var14.esVacia();
    int var24 = var14.cantidad();
    int var25 = var14.cantidad();
    int var26 = var14.cantidad();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test110() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test110");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    var0.desencolar();

  }

  public void test111() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test111");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    ColaSobreListasEnlazadas var15 = new ColaSobreListasEnlazadas();
    int var16 = var15.cantidad();
    int var17 = var15.cantidad();
    int var18 = var15.cantidad();
    int var19 = var15.cantidad();
    boolean var20 = var15.esVacia();
    boolean var21 = var15.esVacia();
    int var22 = var15.cantidad();
    var0.encolar((java.lang.Object)var15);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test112() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test112");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    var0.desencolar();

  }

  public void test113() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test113");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    int var14 = var13.cantidad();
    boolean var15 = var13.esVacia();
    boolean var16 = var13.esVacia();
    boolean var17 = var13.esVacia();
    boolean var18 = var13.esVacia();
    int var19 = var13.cantidad();
    boolean var20 = var13.esVacia();
    int var21 = var13.cantidad();
    int var22 = var13.cantidad();
    var0.encolar((java.lang.Object)var13);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test114() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test114");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    boolean var11 = var10.esVacia();
    boolean var12 = var10.esVacia();
    int var13 = var10.cantidad();
    int var14 = var10.cantidad();
    int var15 = var10.cantidad();
    int var16 = var10.cantidad();
    var0.encolar((java.lang.Object)var10);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test115() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test115");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    java.lang.Object var7 = var0.primero();

  }

  public void test116() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test116");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    var0.desencolar();

  }

  public void test117() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test117");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    var0.desencolar();

  }

  public void test118() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test118");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    var0.desencolar();

  }

  public void test119() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test119");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    boolean var11 = var10.esVacia();
    int var12 = var10.cantidad();
    int var13 = var10.cantidad();
    int var14 = var10.cantidad();
    int var15 = var10.cantidad();
    int var16 = var10.cantidad();
    boolean var17 = var10.esVacia();
    int var18 = var10.cantidad();
    boolean var19 = var10.esVacia();
    boolean var20 = var10.esVacia();
    int var21 = var10.cantidad();
    int var22 = var10.cantidad();
    boolean var23 = var10.esVacia();
    var0.encolar((java.lang.Object)var23);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test120() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test120");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    java.lang.Object var15 = var0.primero();

  }

  public void test121() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test121");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    ColaSobreListasEnlazadas var16 = new ColaSobreListasEnlazadas();
    int var17 = var16.cantidad();
    int var18 = var16.cantidad();
    boolean var19 = var16.esVacia();
    int var20 = var16.cantidad();
    int var21 = var16.cantidad();
    int var22 = var16.cantidad();
    int var23 = var16.cantidad();
    boolean var24 = var16.esVacia();
    var0.encolar((java.lang.Object)var24);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test122() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test122");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    var0.desencolar();

  }

  public void test123() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test123");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    int var14 = var11.cantidad();
    boolean var15 = var11.esVacia();
    boolean var16 = var11.esVacia();
    int var17 = var11.cantidad();
    var0.encolar((java.lang.Object)var17);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test124() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test124");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    java.lang.Object var8 = var0.primero();

  }

  public void test125() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test125");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    java.lang.Object var13 = var0.primero();

  }

  public void test126() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test126");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    var0.desencolar();

  }

  public void test127() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test127");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    boolean var12 = var11.esVacia();
    int var13 = var11.cantidad();
    int var14 = var11.cantidad();
    int var15 = var11.cantidad();
    boolean var16 = var11.esVacia();
    var0.encolar((java.lang.Object)var16);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test128() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test128");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    var0.desencolar();

  }

  public void test129() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test129");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    boolean var13 = var12.esVacia();
    boolean var14 = var12.esVacia();
    int var15 = var12.cantidad();
    boolean var16 = var12.esVacia();
    int var17 = var12.cantidad();
    boolean var18 = var12.esVacia();
    boolean var19 = var12.esVacia();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test130() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test130");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    boolean var16 = var14.esVacia();
    boolean var17 = var14.esVacia();
    int var18 = var14.cantidad();
    boolean var19 = var14.esVacia();
    int var20 = var14.cantidad();
    boolean var21 = var14.esVacia();
    var0.encolar((java.lang.Object)var21);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test131() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test131");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    var0.encolar((java.lang.Object)(-1.0f));
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test132() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test132");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    boolean var11 = var10.esVacia();
    int var12 = var10.cantidad();
    boolean var13 = var10.esVacia();
    int var14 = var10.cantidad();
    int var15 = var10.cantidad();
    int var16 = var10.cantidad();
    boolean var17 = var10.esVacia();
    int var18 = var10.cantidad();
    boolean var19 = var10.esVacia();
    boolean var20 = var10.esVacia();
    var0.encolar((java.lang.Object)var10);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test133() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test133");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    var0.encolar((java.lang.Object)0);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test134() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test134");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    var0.desencolar();

  }

  public void test135() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test135");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    java.lang.Object var9 = var0.primero();

  }

  public void test136() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test136");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    ColaSobreListasEnlazadas var6 = new ColaSobreListasEnlazadas();
    boolean var7 = var6.esVacia();
    int var8 = var6.cantidad();
    int var9 = var6.cantidad();
    int var10 = var6.cantidad();
    int var11 = var6.cantidad();
    int var12 = var6.cantidad();
    boolean var13 = var6.esVacia();
    boolean var14 = var6.esVacia();
    int var15 = var6.cantidad();
    int var16 = var6.cantidad();
    boolean var17 = var6.esVacia();
    int var18 = var6.cantidad();
    var0.encolar((java.lang.Object)var6);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test137() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test137");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    ColaSobreListasEnlazadas var16 = new ColaSobreListasEnlazadas();
    boolean var17 = var16.esVacia();
    int var18 = var16.cantidad();
    boolean var19 = var16.esVacia();
    int var20 = var16.cantidad();
    boolean var21 = var16.esVacia();
    boolean var22 = var16.esVacia();
    int var23 = var16.cantidad();
    int var24 = var16.cantidad();
    var0.encolar((java.lang.Object)var24);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test138() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test138");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    java.lang.Object var14 = var0.primero();

  }

  public void test139() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test139");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    int var16 = var11.cantidad();
    boolean var17 = var11.esVacia();
    int var18 = var11.cantidad();
    boolean var19 = var11.esVacia();
    boolean var20 = var11.esVacia();
    int var21 = var11.cantidad();
    int var22 = var11.cantidad();
    var0.encolar((java.lang.Object)var11);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test140() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test140");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    boolean var16 = var12.esVacia();
    int var17 = var12.cantidad();
    int var18 = var12.cantidad();
    boolean var19 = var12.esVacia();
    int var20 = var12.cantidad();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test141() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test141");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    int var11 = var10.cantidad();
    boolean var12 = var10.esVacia();
    boolean var13 = var10.esVacia();
    boolean var14 = var10.esVacia();
    int var15 = var10.cantidad();
    int var16 = var10.cantidad();
    var0.encolar((java.lang.Object)var16);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test142() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test142");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    var0.desencolar();

  }

  public void test143() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test143");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    java.lang.Object var9 = var0.primero();

  }

  public void test144() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test144");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    int var16 = var0.cantidad();
    int var17 = var0.cantidad();
    int var18 = var0.cantidad();
    int var19 = var0.cantidad();
    ColaSobreListasEnlazadas var20 = new ColaSobreListasEnlazadas();
    int var21 = var20.cantidad();
    boolean var22 = var20.esVacia();
    boolean var23 = var20.esVacia();
    boolean var24 = var20.esVacia();
    int var25 = var20.cantidad();
    boolean var26 = var20.esVacia();
    boolean var27 = var20.esVacia();
    int var28 = var20.cantidad();
    boolean var29 = var20.esVacia();
    boolean var30 = var20.esVacia();
    int var31 = var20.cantidad();
    var0.encolar((java.lang.Object)var31);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test145() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test145");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    java.lang.Object var12 = var0.primero();

  }

  public void test146() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test146");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    int var16 = var0.cantidad();
    boolean var17 = var0.esVacia();
    var0.encolar((java.lang.Object)10.0d);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test147() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test147");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    java.lang.Object var12 = var0.primero();

  }

  public void test148() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test148");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    var0.desencolar();

  }

  public void test149() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test149");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    boolean var15 = var14.esVacia();
    int var16 = var14.cantidad();
    int var17 = var14.cantidad();
    int var18 = var14.cantidad();
    int var19 = var14.cantidad();
    boolean var20 = var14.esVacia();
    boolean var21 = var14.esVacia();
    boolean var22 = var14.esVacia();
    boolean var23 = var14.esVacia();
    boolean var24 = var14.esVacia();
    int var25 = var14.cantidad();
    boolean var26 = var14.esVacia();
    boolean var27 = var14.esVacia();
    var0.encolar((java.lang.Object)var27);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test150() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test150");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    int var11 = var10.cantidad();
    int var12 = var10.cantidad();
    int var13 = var10.cantidad();
    int var14 = var10.cantidad();
    int var15 = var10.cantidad();
    boolean var16 = var10.esVacia();
    int var17 = var10.cantidad();
    boolean var18 = var10.esVacia();
    int var19 = var10.cantidad();
    int var20 = var10.cantidad();
    var0.encolar((java.lang.Object)var20);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test151() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test151");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    java.lang.Object var14 = var0.primero();

  }

  public void test152() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test152");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    boolean var12 = var11.esVacia();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    int var15 = var11.cantidad();
    boolean var16 = var11.esVacia();
    var0.encolar((java.lang.Object)var16);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test153() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test153");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    java.lang.Object var12 = var0.primero();

  }

  public void test154() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test154");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    boolean var11 = var10.esVacia();
    int var12 = var10.cantidad();
    int var13 = var10.cantidad();
    int var14 = var10.cantidad();
    boolean var15 = var10.esVacia();
    int var16 = var10.cantidad();
    var0.encolar((java.lang.Object)var10);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test155() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test155");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    var0.desencolar();

  }

  public void test156() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test156");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    boolean var15 = var14.esVacia();
    boolean var16 = var14.esVacia();
    boolean var17 = var14.esVacia();
    int var18 = var14.cantidad();
    boolean var19 = var14.esVacia();
    int var20 = var14.cantidad();
    int var21 = var14.cantidad();
    var0.encolar((java.lang.Object)var21);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test157() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test157");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    int var11 = var10.cantidad();
    boolean var12 = var10.esVacia();
    boolean var13 = var10.esVacia();
    int var14 = var10.cantidad();
    int var15 = var10.cantidad();
    int var16 = var10.cantidad();
    boolean var17 = var10.esVacia();
    int var18 = var10.cantidad();
    var0.encolar((java.lang.Object)var10);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test158() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test158");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    ColaSobreListasEnlazadas var8 = new ColaSobreListasEnlazadas();
    boolean var9 = var8.esVacia();
    int var10 = var8.cantidad();
    boolean var11 = var8.esVacia();
    int var12 = var8.cantidad();
    boolean var13 = var8.esVacia();
    boolean var14 = var8.esVacia();
    boolean var15 = var8.esVacia();
    boolean var16 = var8.esVacia();
    boolean var17 = var8.esVacia();
    boolean var18 = var8.esVacia();
    boolean var19 = var8.esVacia();
    boolean var20 = var8.esVacia();
    var0.encolar((java.lang.Object)var8);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test159() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test159");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    boolean var15 = var14.esVacia();
    int var16 = var14.cantidad();
    int var17 = var14.cantidad();
    int var18 = var14.cantidad();
    int var19 = var14.cantidad();
    int var20 = var14.cantidad();
    int var21 = var14.cantidad();
    boolean var22 = var14.esVacia();
    int var23 = var14.cantidad();
    int var24 = var14.cantidad();
    int var25 = var14.cantidad();
    boolean var26 = var14.esVacia();
    boolean var27 = var14.esVacia();
    boolean var28 = var14.esVacia();
    boolean var29 = var14.esVacia();
    int var30 = var14.cantidad();
    var0.encolar((java.lang.Object)var30);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test160() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test160");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    java.lang.Object var11 = var0.primero();

  }

  public void test161() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test161");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    ColaSobreListasEnlazadas var15 = new ColaSobreListasEnlazadas();
    boolean var16 = var15.esVacia();
    int var17 = var15.cantidad();
    boolean var18 = var15.esVacia();
    int var19 = var15.cantidad();
    int var20 = var15.cantidad();
    boolean var21 = var15.esVacia();
    int var22 = var15.cantidad();
    boolean var23 = var15.esVacia();
    int var24 = var15.cantidad();
    int var25 = var15.cantidad();
    var0.encolar((java.lang.Object)var25);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test162() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test162");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    var0.desencolar();

  }

  public void test163() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test163");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    ColaSobreListasEnlazadas var8 = new ColaSobreListasEnlazadas();
    int var9 = var8.cantidad();
    boolean var10 = var8.esVacia();
    int var11 = var8.cantidad();
    boolean var12 = var8.esVacia();
    boolean var13 = var8.esVacia();
    int var14 = var8.cantidad();
    boolean var15 = var8.esVacia();
    var0.encolar((java.lang.Object)var15);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test164() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test164");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    java.lang.Object var15 = var0.primero();

  }

  public void test165() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test165");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    java.lang.Object var11 = var0.primero();

  }

  public void test166() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test166");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    java.lang.Object var14 = var0.primero();

  }

  public void test167() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test167");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    boolean var15 = var14.esVacia();
    int var16 = var14.cantidad();
    boolean var17 = var14.esVacia();
    int var18 = var14.cantidad();
    boolean var19 = var14.esVacia();
    boolean var20 = var14.esVacia();
    boolean var21 = var14.esVacia();
    boolean var22 = var14.esVacia();
    int var23 = var14.cantidad();
    boolean var24 = var14.esVacia();
    int var25 = var14.cantidad();
    boolean var26 = var14.esVacia();
    boolean var27 = var14.esVacia();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test168() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test168");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    java.lang.Object var8 = var0.primero();

  }

  public void test169() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test169");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    boolean var16 = var14.esVacia();
    boolean var17 = var14.esVacia();
    int var18 = var14.cantidad();
    int var19 = var14.cantidad();
    int var20 = var14.cantidad();
    int var21 = var14.cantidad();
    int var22 = var14.cantidad();
    boolean var23 = var14.esVacia();
    int var24 = var14.cantidad();
    boolean var25 = var14.esVacia();
    boolean var26 = var14.esVacia();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test170() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test170");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    var0.desencolar();

  }

  public void test171() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test171");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    java.lang.Object var13 = var0.primero();

  }

  public void test172() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test172");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    boolean var16 = var0.esVacia();
    int var17 = var0.cantidad();
    int var18 = var0.cantidad();
    var0.desencolar();

  }

  public void test173() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test173");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    var0.desencolar();

  }

  public void test174() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test174");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    var0.desencolar();

  }

  public void test175() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test175");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    var0.desencolar();

  }

  public void test176() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test176");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    ColaSobreListasEnlazadas var3 = new ColaSobreListasEnlazadas();
    boolean var4 = var3.esVacia();
    int var5 = var3.cantidad();
    int var6 = var3.cantidad();
    int var7 = var3.cantidad();
    int var8 = var3.cantidad();
    int var9 = var3.cantidad();
    int var10 = var3.cantidad();
    boolean var11 = var3.esVacia();
    int var12 = var3.cantidad();
    int var13 = var3.cantidad();
    var0.encolar((java.lang.Object)var3);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test177() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test177");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    var0.desencolar();

  }

  public void test178() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test178");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    boolean var16 = var0.esVacia();
    boolean var17 = var0.esVacia();
    boolean var18 = var0.esVacia();
    ColaSobreListasEnlazadas var19 = new ColaSobreListasEnlazadas();
    boolean var20 = var19.esVacia();
    boolean var21 = var19.esVacia();
    boolean var22 = var19.esVacia();
    boolean var23 = var19.esVacia();
    int var24 = var19.cantidad();
    int var25 = var19.cantidad();
    int var26 = var19.cantidad();
    var0.encolar((java.lang.Object)var26);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test179() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test179");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    java.lang.Object var6 = var0.primero();

  }

  public void test180() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test180");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    boolean var16 = var14.esVacia();
    boolean var17 = var14.esVacia();
    int var18 = var14.cantidad();
    int var19 = var14.cantidad();
    int var20 = var14.cantidad();
    boolean var21 = var14.esVacia();
    int var22 = var14.cantidad();
    int var23 = var14.cantidad();
    int var24 = var14.cantidad();
    boolean var25 = var14.esVacia();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test181() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test181");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    boolean var16 = var12.esVacia();
    int var17 = var12.cantidad();
    boolean var18 = var12.esVacia();
    int var19 = var12.cantidad();
    int var20 = var12.cantidad();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test182() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test182");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    var0.desencolar();

  }

  public void test183() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test183");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    int var16 = var12.cantidad();
    int var17 = var12.cantidad();
    int var18 = var12.cantidad();
    int var19 = var12.cantidad();
    int var20 = var12.cantidad();
    boolean var21 = var12.esVacia();
    int var22 = var12.cantidad();
    boolean var23 = var12.esVacia();
    int var24 = var12.cantidad();
    boolean var25 = var12.esVacia();
    boolean var26 = var12.esVacia();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test184() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test184");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    var0.desencolar();

  }

  public void test185() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test185");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    java.lang.Object var12 = var0.primero();

  }

  public void test186() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test186");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    var0.desencolar();

  }

  public void test187() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test187");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    var0.encolar((java.lang.Object)(short)(-1));
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test188() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test188");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    boolean var13 = var12.esVacia();
    int var14 = var12.cantidad();
    boolean var15 = var12.esVacia();
    int var16 = var12.cantidad();
    var0.encolar((java.lang.Object)var16);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test189() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test189");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    ColaSobreListasEnlazadas var7 = new ColaSobreListasEnlazadas();
    int var8 = var7.cantidad();
    boolean var9 = var7.esVacia();
    boolean var10 = var7.esVacia();
    boolean var11 = var7.esVacia();
    boolean var12 = var7.esVacia();
    boolean var13 = var7.esVacia();
    boolean var14 = var7.esVacia();
    boolean var15 = var7.esVacia();
    int var16 = var7.cantidad();
    int var17 = var7.cantidad();
    int var18 = var7.cantidad();
    boolean var19 = var7.esVacia();
    var0.encolar((java.lang.Object)var19);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test190() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test190");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    int var15 = var13.cantidad();
    int var16 = var13.cantidad();
    int var17 = var13.cantidad();
    int var18 = var13.cantidad();
    boolean var19 = var13.esVacia();
    int var20 = var13.cantidad();
    int var21 = var13.cantidad();
    boolean var22 = var13.esVacia();
    int var23 = var13.cantidad();
    int var24 = var13.cantidad();
    boolean var25 = var13.esVacia();
    var0.encolar((java.lang.Object)var25);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test191() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test191");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    var0.desencolar();

  }

  public void test192() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test192");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    int var16 = var14.cantidad();
    int var17 = var14.cantidad();
    int var18 = var14.cantidad();
    int var19 = var14.cantidad();
    boolean var20 = var14.esVacia();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test193() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test193");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    boolean var16 = var14.esVacia();
    int var17 = var14.cantidad();
    boolean var18 = var14.esVacia();
    var0.encolar((java.lang.Object)var18);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test194() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test194");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    var0.desencolar();

  }

  public void test195() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test195");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    java.lang.Object var12 = var0.primero();

  }

  public void test196() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test196");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    java.lang.Object var12 = var0.primero();

  }

  public void test197() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test197");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    boolean var16 = var11.esVacia();
    int var17 = var11.cantidad();
    boolean var18 = var11.esVacia();
    int var19 = var11.cantidad();
    var0.encolar((java.lang.Object)var11);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test198() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test198");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    var0.desencolar();

  }

  public void test199() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test199");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    var0.desencolar();

  }

  public void test200() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test200");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    int var15 = var13.cantidad();
    int var16 = var13.cantidad();
    int var17 = var13.cantidad();
    int var18 = var13.cantidad();
    int var19 = var13.cantidad();
    boolean var20 = var13.esVacia();
    int var21 = var13.cantidad();
    int var22 = var13.cantidad();
    int var23 = var13.cantidad();
    boolean var24 = var13.esVacia();
    var0.encolar((java.lang.Object)var24);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test201() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test201");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    java.lang.Object var6 = var0.primero();

  }

  public void test202() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test202");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    var0.encolar((java.lang.Object)100);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test203() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test203");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    ColaSobreListasEnlazadas var7 = new ColaSobreListasEnlazadas();
    int var8 = var7.cantidad();
    boolean var9 = var7.esVacia();
    boolean var10 = var7.esVacia();
    boolean var11 = var7.esVacia();
    int var12 = var7.cantidad();
    int var13 = var7.cantidad();
    int var14 = var7.cantidad();
    boolean var15 = var7.esVacia();
    int var16 = var7.cantidad();
    int var17 = var7.cantidad();
    boolean var18 = var7.esVacia();
    int var19 = var7.cantidad();
    int var20 = var7.cantidad();
    var0.encolar((java.lang.Object)var20);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test204() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test204");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    java.lang.Object var11 = var0.primero();

  }

  public void test205() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test205");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    int var15 = var13.cantidad();
    int var16 = var13.cantidad();
    int var17 = var13.cantidad();
    int var18 = var13.cantidad();
    int var19 = var13.cantidad();
    int var20 = var13.cantidad();
    boolean var21 = var13.esVacia();
    boolean var22 = var13.esVacia();
    int var23 = var13.cantidad();
    int var24 = var13.cantidad();
    var0.encolar((java.lang.Object)var24);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test206() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test206");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    ColaSobreListasEnlazadas var8 = new ColaSobreListasEnlazadas();
    int var9 = var8.cantidad();
    boolean var10 = var8.esVacia();
    boolean var11 = var8.esVacia();
    boolean var12 = var8.esVacia();
    int var13 = var8.cantidad();
    boolean var14 = var8.esVacia();
    boolean var15 = var8.esVacia();
    int var16 = var8.cantidad();
    boolean var17 = var8.esVacia();
    int var18 = var8.cantidad();
    int var19 = var8.cantidad();
    boolean var20 = var8.esVacia();
    boolean var21 = var8.esVacia();
    boolean var22 = var8.esVacia();
    int var23 = var8.cantidad();
    var0.encolar((java.lang.Object)var8);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test207() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test207");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    var0.desencolar();

  }

  public void test208() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test208");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    var0.desencolar();

  }

  public void test209() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test209");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    boolean var15 = var0.esVacia();
    int var16 = var0.cantidad();
    var0.desencolar();

  }

  public void test210() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test210");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    int var10 = var9.cantidad();
    boolean var11 = var9.esVacia();
    boolean var12 = var9.esVacia();
    boolean var13 = var9.esVacia();
    int var14 = var9.cantidad();
    boolean var15 = var9.esVacia();
    boolean var16 = var9.esVacia();
    int var17 = var9.cantidad();
    boolean var18 = var9.esVacia();
    boolean var19 = var9.esVacia();
    int var20 = var9.cantidad();
    var0.encolar((java.lang.Object)var20);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test211() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test211");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    var0.desencolar();

  }

  public void test212() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test212");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    boolean var10 = var9.esVacia();
    int var11 = var9.cantidad();
    int var12 = var9.cantidad();
    int var13 = var9.cantidad();
    int var14 = var9.cantidad();
    int var15 = var9.cantidad();
    boolean var16 = var9.esVacia();
    int var17 = var9.cantidad();
    var0.encolar((java.lang.Object)var9);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test213() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test213");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    var0.desencolar();

  }

  public void test214() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test214");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    java.lang.Object var15 = var0.primero();

  }

  public void test215() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test215");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    ColaSobreListasEnlazadas var5 = new ColaSobreListasEnlazadas();
    boolean var6 = var5.esVacia();
    int var7 = var5.cantidad();
    int var8 = var5.cantidad();
    int var9 = var5.cantidad();
    int var10 = var5.cantidad();
    boolean var11 = var5.esVacia();
    boolean var12 = var5.esVacia();
    boolean var13 = var5.esVacia();
    int var14 = var5.cantidad();
    boolean var15 = var5.esVacia();
    var0.encolar((java.lang.Object)var5);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test216() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test216");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    ColaSobreListasEnlazadas var4 = new ColaSobreListasEnlazadas();
    int var5 = var4.cantidad();
    int var6 = var4.cantidad();
    boolean var7 = var4.esVacia();
    int var8 = var4.cantidad();
    boolean var9 = var4.esVacia();
    int var10 = var4.cantidad();
    int var11 = var4.cantidad();
    var0.encolar((java.lang.Object)var4);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test217() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test217");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    boolean var16 = var14.esVacia();
    boolean var17 = var14.esVacia();
    boolean var18 = var14.esVacia();
    int var19 = var14.cantidad();
    int var20 = var14.cantidad();
    int var21 = var14.cantidad();
    boolean var22 = var14.esVacia();
    boolean var23 = var14.esVacia();
    int var24 = var14.cantidad();
    boolean var25 = var14.esVacia();
    boolean var26 = var14.esVacia();
    int var27 = var14.cantidad();
    int var28 = var14.cantidad();
    boolean var29 = var14.esVacia();
    int var30 = var14.cantidad();
    int var31 = var14.cantidad();
    int var32 = var14.cantidad();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test218() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test218");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    var0.desencolar();

  }

  public void test219() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test219");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    java.lang.Object var13 = var0.primero();

  }

  public void test220() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test220");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    int var14 = var13.cantidad();
    boolean var15 = var13.esVacia();
    boolean var16 = var13.esVacia();
    boolean var17 = var13.esVacia();
    int var18 = var13.cantidad();
    boolean var19 = var13.esVacia();
    boolean var20 = var13.esVacia();
    int var21 = var13.cantidad();
    int var22 = var13.cantidad();
    boolean var23 = var13.esVacia();
    int var24 = var13.cantidad();
    var0.encolar((java.lang.Object)var24);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test221() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test221");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    java.lang.Object var15 = var0.primero();

  }

  public void test222() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test222");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    java.lang.Object var11 = var0.primero();

  }

  public void test223() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test223");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    var0.desencolar();

  }

  public void test224() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test224");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    var0.desencolar();

  }

  public void test225() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test225");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    java.lang.Object var14 = var0.primero();

  }

  public void test226() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test226");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    java.lang.Object var13 = var0.primero();

  }

  public void test227() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test227");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    boolean var15 = var13.esVacia();
    int var16 = var13.cantidad();
    boolean var17 = var13.esVacia();
    boolean var18 = var13.esVacia();
    int var19 = var13.cantidad();
    var0.encolar((java.lang.Object)var13);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test228() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test228");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    int var10 = var9.cantidad();
    boolean var11 = var9.esVacia();
    boolean var12 = var9.esVacia();
    boolean var13 = var9.esVacia();
    int var14 = var9.cantidad();
    int var15 = var9.cantidad();
    int var16 = var9.cantidad();
    int var17 = var9.cantidad();
    boolean var18 = var9.esVacia();
    int var19 = var9.cantidad();
    boolean var20 = var9.esVacia();
    var0.encolar((java.lang.Object)var9);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test229() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test229");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    java.lang.Object var12 = var0.primero();

  }

  public void test230() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test230");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    var0.desencolar();

  }

  public void test231() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test231");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    ColaSobreListasEnlazadas var5 = new ColaSobreListasEnlazadas();
    boolean var6 = var5.esVacia();
    int var7 = var5.cantidad();
    boolean var8 = var5.esVacia();
    var0.encolar((java.lang.Object)var8);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test232() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test232");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    var0.desencolar();

  }

  public void test233() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test233");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    var0.desencolar();

  }

  public void test234() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test234");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    boolean var16 = var11.esVacia();
    int var17 = var11.cantidad();
    boolean var18 = var11.esVacia();
    boolean var19 = var11.esVacia();
    var0.encolar((java.lang.Object)var11);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test235() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test235");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    boolean var16 = var0.esVacia();
    ColaSobreListasEnlazadas var17 = new ColaSobreListasEnlazadas();
    boolean var18 = var17.esVacia();
    int var19 = var17.cantidad();
    int var20 = var17.cantidad();
    int var21 = var17.cantidad();
    int var22 = var17.cantidad();
    int var23 = var17.cantidad();
    int var24 = var17.cantidad();
    boolean var25 = var17.esVacia();
    int var26 = var17.cantidad();
    boolean var27 = var17.esVacia();
    var0.encolar((java.lang.Object)var17);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test236() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test236");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    int var11 = var10.cantidad();
    boolean var12 = var10.esVacia();
    boolean var13 = var10.esVacia();
    boolean var14 = var10.esVacia();
    int var15 = var10.cantidad();
    int var16 = var10.cantidad();
    int var17 = var10.cantidad();
    boolean var18 = var10.esVacia();
    boolean var19 = var10.esVacia();
    int var20 = var10.cantidad();
    boolean var21 = var10.esVacia();
    boolean var22 = var10.esVacia();
    int var23 = var10.cantidad();
    boolean var24 = var10.esVacia();
    int var25 = var10.cantidad();
    boolean var26 = var10.esVacia();
    int var27 = var10.cantidad();
    int var28 = var10.cantidad();
    var0.encolar((java.lang.Object)var10);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test237() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test237");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    java.lang.Object var9 = var0.primero();

  }

  public void test238() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test238");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    boolean var16 = var0.esVacia();
    ColaSobreListasEnlazadas var17 = new ColaSobreListasEnlazadas();
    int var18 = var17.cantidad();
    boolean var19 = var17.esVacia();
    boolean var20 = var17.esVacia();
    boolean var21 = var17.esVacia();
    int var22 = var17.cantidad();
    int var23 = var17.cantidad();
    int var24 = var17.cantidad();
    boolean var25 = var17.esVacia();
    boolean var26 = var17.esVacia();
    boolean var27 = var17.esVacia();
    int var28 = var17.cantidad();
    boolean var29 = var17.esVacia();
    boolean var30 = var17.esVacia();
    var0.encolar((java.lang.Object)var30);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test239() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test239");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    java.lang.Object var14 = var0.primero();

  }

  public void test240() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test240");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    boolean var12 = var11.esVacia();
    int var13 = var11.cantidad();
    int var14 = var11.cantidad();
    int var15 = var11.cantidad();
    int var16 = var11.cantidad();
    boolean var17 = var11.esVacia();
    boolean var18 = var11.esVacia();
    boolean var19 = var11.esVacia();
    boolean var20 = var11.esVacia();
    int var21 = var11.cantidad();
    int var22 = var11.cantidad();
    int var23 = var11.cantidad();
    int var24 = var11.cantidad();
    var0.encolar((java.lang.Object)var24);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test241() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test241");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    boolean var11 = var10.esVacia();
    int var12 = var10.cantidad();
    int var13 = var10.cantidad();
    int var14 = var10.cantidad();
    int var15 = var10.cantidad();
    int var16 = var10.cantidad();
    boolean var17 = var10.esVacia();
    boolean var18 = var10.esVacia();
    boolean var19 = var10.esVacia();
    int var20 = var10.cantidad();
    int var21 = var10.cantidad();
    var0.encolar((java.lang.Object)var10);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test242() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test242");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    boolean var13 = var12.esVacia();
    int var14 = var12.cantidad();
    int var15 = var12.cantidad();
    int var16 = var12.cantidad();
    int var17 = var12.cantidad();
    boolean var18 = var12.esVacia();
    boolean var19 = var12.esVacia();
    boolean var20 = var12.esVacia();
    boolean var21 = var12.esVacia();
    int var22 = var12.cantidad();
    int var23 = var12.cantidad();
    int var24 = var12.cantidad();
    int var25 = var12.cantidad();
    int var26 = var12.cantidad();
    boolean var27 = var12.esVacia();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test243() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test243");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    var0.desencolar();

  }

  public void test244() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test244");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    ColaSobreListasEnlazadas var16 = new ColaSobreListasEnlazadas();
    int var17 = var16.cantidad();
    boolean var18 = var16.esVacia();
    boolean var19 = var16.esVacia();
    boolean var20 = var16.esVacia();
    int var21 = var16.cantidad();
    int var22 = var16.cantidad();
    int var23 = var16.cantidad();
    boolean var24 = var16.esVacia();
    int var25 = var16.cantidad();
    int var26 = var16.cantidad();
    int var27 = var16.cantidad();
    boolean var28 = var16.esVacia();
    var0.encolar((java.lang.Object)var28);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test245() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test245");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    ColaSobreListasEnlazadas var7 = new ColaSobreListasEnlazadas();
    boolean var8 = var7.esVacia();
    int var9 = var7.cantidad();
    boolean var10 = var7.esVacia();
    int var11 = var7.cantidad();
    int var12 = var7.cantidad();
    int var13 = var7.cantidad();
    boolean var14 = var7.esVacia();
    int var15 = var7.cantidad();
    boolean var16 = var7.esVacia();
    int var17 = var7.cantidad();
    boolean var18 = var7.esVacia();
    var0.encolar((java.lang.Object)var7);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test246() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test246");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    int var14 = var0.cantidad();
    java.lang.Object var15 = var0.primero();

  }

  public void test247() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test247");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    var0.desencolar();

  }

  public void test248() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test248");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    ColaSobreListasEnlazadas var15 = new ColaSobreListasEnlazadas();
    boolean var16 = var15.esVacia();
    boolean var17 = var15.esVacia();
    boolean var18 = var15.esVacia();
    boolean var19 = var15.esVacia();
    int var20 = var15.cantidad();
    int var21 = var15.cantidad();
    var0.encolar((java.lang.Object)var15);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test249() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test249");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    var0.desencolar();

  }

  public void test250() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test250");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    java.lang.Object var8 = var0.primero();

  }

  public void test251() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test251");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    boolean var15 = var13.esVacia();
    var0.encolar((java.lang.Object)var15);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test252() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test252");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    var0.desencolar();

  }

  public void test253() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test253");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    java.lang.Object var11 = var0.primero();

  }

  public void test254() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test254");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    int var15 = var12.cantidad();
    int var16 = var12.cantidad();
    boolean var17 = var12.esVacia();
    int var18 = var12.cantidad();
    boolean var19 = var12.esVacia();
    var0.encolar((java.lang.Object)var19);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test255() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test255");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    int var15 = var0.cantidad();
    ColaSobreListasEnlazadas var16 = new ColaSobreListasEnlazadas();
    boolean var17 = var16.esVacia();
    int var18 = var16.cantidad();
    int var19 = var16.cantidad();
    int var20 = var16.cantidad();
    int var21 = var16.cantidad();
    boolean var22 = var16.esVacia();
    int var23 = var16.cantidad();
    int var24 = var16.cantidad();
    boolean var25 = var16.esVacia();
    int var26 = var16.cantidad();
    int var27 = var16.cantidad();
    boolean var28 = var16.esVacia();
    boolean var29 = var16.esVacia();
    int var30 = var16.cantidad();
    int var31 = var16.cantidad();
    var0.encolar((java.lang.Object)var31);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test256() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test256");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    int var11 = var10.cantidad();
    boolean var12 = var10.esVacia();
    boolean var13 = var10.esVacia();
    boolean var14 = var10.esVacia();
    int var15 = var10.cantidad();
    int var16 = var10.cantidad();
    int var17 = var10.cantidad();
    boolean var18 = var10.esVacia();
    boolean var19 = var10.esVacia();
    int var20 = var10.cantidad();
    boolean var21 = var10.esVacia();
    boolean var22 = var10.esVacia();
    boolean var23 = var10.esVacia();
    var0.encolar((java.lang.Object)var23);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test257() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test257");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    java.lang.Object var12 = var0.primero();

  }

  public void test258() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test258");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    java.lang.Object var9 = var0.primero();

  }

  public void test259() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test259");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    ColaSobreListasEnlazadas var8 = new ColaSobreListasEnlazadas();
    boolean var9 = var8.esVacia();
    boolean var10 = var8.esVacia();
    int var11 = var8.cantidad();
    boolean var12 = var8.esVacia();
    int var13 = var8.cantidad();
    boolean var14 = var8.esVacia();
    boolean var15 = var8.esVacia();
    var0.encolar((java.lang.Object)var15);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test260() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test260");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    java.lang.Object var15 = var0.primero();

  }

  public void test261() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test261");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    var0.desencolar();

  }

  public void test262() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test262");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    var0.encolar((java.lang.Object)' ');
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test263() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test263");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    var0.desencolar();

  }

  public void test264() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test264");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    var0.encolar((java.lang.Object)10L);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test265() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test265");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    int var16 = var0.cantidad();
    boolean var17 = var0.esVacia();
    var0.desencolar();

  }

  public void test266() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test266");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    java.lang.Object var16 = var0.primero();

  }

  public void test267() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test267");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    java.lang.Object var14 = var0.primero();

  }

  public void test268() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test268");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var1 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == 0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == 0);

  }

  public void test269() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test269");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    int var16 = var14.cantidad();
    int var17 = var14.cantidad();
    int var18 = var14.cantidad();
    boolean var19 = var14.esVacia();
    int var20 = var14.cantidad();
    boolean var21 = var14.esVacia();
    int var22 = var14.cantidad();
    boolean var23 = var14.esVacia();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test270() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test270");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    java.lang.Object var7 = var0.primero();

  }

  public void test271() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test271");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    boolean var12 = var11.esVacia();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test272() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test272");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    int var15 = var12.cantidad();
    int var16 = var12.cantidad();
    boolean var17 = var12.esVacia();
    boolean var18 = var12.esVacia();
    var0.encolar((java.lang.Object)var18);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test273() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test273");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    var0.desencolar();

  }

  public void test274() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test274");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    boolean var16 = var12.esVacia();
    int var17 = var12.cantidad();
    boolean var18 = var12.esVacia();
    boolean var19 = var12.esVacia();
    boolean var20 = var12.esVacia();
    boolean var21 = var12.esVacia();
    int var22 = var12.cantidad();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test275() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test275");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    var0.desencolar();

  }

  public void test276() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test276");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    var0.desencolar();

  }

  public void test277() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test277");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    int var15 = var13.cantidad();
    boolean var16 = var13.esVacia();
    int var17 = var13.cantidad();
    int var18 = var13.cantidad();
    int var19 = var13.cantidad();
    boolean var20 = var13.esVacia();
    int var21 = var13.cantidad();
    boolean var22 = var13.esVacia();
    boolean var23 = var13.esVacia();
    boolean var24 = var13.esVacia();
    int var25 = var13.cantidad();
    var0.encolar((java.lang.Object)var25);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test278() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test278");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    boolean var13 = var12.esVacia();
    int var14 = var12.cantidad();
    boolean var15 = var12.esVacia();
    int var16 = var12.cantidad();
    int var17 = var12.cantidad();
    int var18 = var12.cantidad();
    boolean var19 = var12.esVacia();
    int var20 = var12.cantidad();
    boolean var21 = var12.esVacia();
    boolean var22 = var12.esVacia();
    boolean var23 = var12.esVacia();
    int var24 = var12.cantidad();
    int var25 = var12.cantidad();
    boolean var26 = var12.esVacia();
    int var27 = var12.cantidad();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test279() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test279");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    int var14 = var13.cantidad();
    boolean var15 = var13.esVacia();
    boolean var16 = var13.esVacia();
    boolean var17 = var13.esVacia();
    int var18 = var13.cantidad();
    boolean var19 = var13.esVacia();
    int var20 = var13.cantidad();
    boolean var21 = var13.esVacia();
    int var22 = var13.cantidad();
    boolean var23 = var13.esVacia();
    boolean var24 = var13.esVacia();
    int var25 = var13.cantidad();
    int var26 = var13.cantidad();
    int var27 = var13.cantidad();
    var0.encolar((java.lang.Object)var13);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test280() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test280");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    var0.desencolar();

  }

  public void test281() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test281");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    java.lang.Object var15 = var0.primero();

  }

  public void test282() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test282");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    int var16 = var11.cantidad();
    int var17 = var11.cantidad();
    int var18 = var11.cantidad();
    boolean var19 = var11.esVacia();
    boolean var20 = var11.esVacia();
    int var21 = var11.cantidad();
    boolean var22 = var11.esVacia();
    boolean var23 = var11.esVacia();
    boolean var24 = var11.esVacia();
    var0.encolar((java.lang.Object)var11);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test283() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test283");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    boolean var16 = var0.esVacia();
    boolean var17 = var0.esVacia();
    ColaSobreListasEnlazadas var18 = new ColaSobreListasEnlazadas();
    boolean var19 = var18.esVacia();
    int var20 = var18.cantidad();
    int var21 = var18.cantidad();
    int var22 = var18.cantidad();
    int var23 = var18.cantidad();
    var0.encolar((java.lang.Object)var23);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test284() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test284");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    ColaSobreListasEnlazadas var7 = new ColaSobreListasEnlazadas();
    boolean var8 = var7.esVacia();
    int var9 = var7.cantidad();
    int var10 = var7.cantidad();
    int var11 = var7.cantidad();
    int var12 = var7.cantidad();
    int var13 = var7.cantidad();
    int var14 = var7.cantidad();
    boolean var15 = var7.esVacia();
    int var16 = var7.cantidad();
    int var17 = var7.cantidad();
    int var18 = var7.cantidad();
    boolean var19 = var7.esVacia();
    boolean var20 = var7.esVacia();
    boolean var21 = var7.esVacia();
    var0.encolar((java.lang.Object)var21);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test285() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test285");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    var0.desencolar();

  }

  public void test286() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test286");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    var0.desencolar();

  }

  public void test287() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test287");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    int var16 = var0.cantidad();
    boolean var17 = var0.esVacia();
    int var18 = var0.cantidad();
    boolean var19 = var0.esVacia();
    ColaSobreListasEnlazadas var20 = new ColaSobreListasEnlazadas();
    int var21 = var20.cantidad();
    boolean var22 = var20.esVacia();
    boolean var23 = var20.esVacia();
    var0.encolar((java.lang.Object)var23);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test288() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test288");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    ColaSobreListasEnlazadas var2 = new ColaSobreListasEnlazadas();
    int var3 = var2.cantidad();
    boolean var4 = var2.esVacia();
    boolean var5 = var2.esVacia();
    boolean var6 = var2.esVacia();
    int var7 = var2.cantidad();
    int var8 = var2.cantidad();
    int var9 = var2.cantidad();
    boolean var10 = var2.esVacia();
    boolean var11 = var2.esVacia();
    boolean var12 = var2.esVacia();
    int var13 = var2.cantidad();
    boolean var14 = var2.esVacia();
    boolean var15 = var2.esVacia();
    int var16 = var2.cantidad();
    boolean var17 = var2.esVacia();
    boolean var18 = var2.esVacia();
    int var19 = var2.cantidad();
    int var20 = var2.cantidad();
    boolean var21 = var2.esVacia();
    var0.encolar((java.lang.Object)var21);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test289() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test289");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    java.lang.Object var15 = var0.primero();

  }

  public void test290() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test290");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    var0.desencolar();

  }

  public void test291() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test291");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    java.lang.Object var14 = var0.primero();

  }

  public void test292() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test292");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    boolean var15 = var0.esVacia();
    int var16 = var0.cantidad();
    int var17 = var0.cantidad();
    int var18 = var0.cantidad();
    ColaSobreListasEnlazadas var19 = new ColaSobreListasEnlazadas();
    int var20 = var19.cantidad();
    boolean var21 = var19.esVacia();
    boolean var22 = var19.esVacia();
    boolean var23 = var19.esVacia();
    int var24 = var19.cantidad();
    boolean var25 = var19.esVacia();
    boolean var26 = var19.esVacia();
    boolean var27 = var19.esVacia();
    int var28 = var19.cantidad();
    int var29 = var19.cantidad();
    boolean var30 = var19.esVacia();
    int var31 = var19.cantidad();
    boolean var32 = var19.esVacia();
    boolean var33 = var19.esVacia();
    int var34 = var19.cantidad();
    var0.encolar((java.lang.Object)var19);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test293() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test293");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    java.lang.Object var10 = var0.primero();

  }

  public void test294() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test294");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    boolean var12 = var11.esVacia();
    int var13 = var11.cantidad();
    boolean var14 = var11.esVacia();
    int var15 = var11.cantidad();
    boolean var16 = var11.esVacia();
    int var17 = var11.cantidad();
    int var18 = var11.cantidad();
    var0.encolar((java.lang.Object)var18);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test295() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test295");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    boolean var13 = var12.esVacia();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    boolean var16 = var12.esVacia();
    int var17 = var12.cantidad();
    int var18 = var12.cantidad();
    boolean var19 = var12.esVacia();
    int var20 = var12.cantidad();
    int var21 = var12.cantidad();
    boolean var22 = var12.esVacia();
    boolean var23 = var12.esVacia();
    var0.encolar((java.lang.Object)var23);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test296() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test296");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    int var10 = var9.cantidad();
    int var11 = var9.cantidad();
    boolean var12 = var9.esVacia();
    int var13 = var9.cantidad();
    boolean var14 = var9.esVacia();
    boolean var15 = var9.esVacia();
    var0.encolar((java.lang.Object)var9);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test297() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test297");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    java.lang.Object var14 = var0.primero();

  }

  public void test298() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test298");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    java.lang.Object var8 = var0.primero();

  }

  public void test299() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test299");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    int var11 = var10.cantidad();
    int var12 = var10.cantidad();
    boolean var13 = var10.esVacia();
    int var14 = var10.cantidad();
    int var15 = var10.cantidad();
    int var16 = var10.cantidad();
    int var17 = var10.cantidad();
    boolean var18 = var10.esVacia();
    var0.encolar((java.lang.Object)var18);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test300() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test300");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    boolean var12 = var11.esVacia();
    int var13 = var11.cantidad();
    boolean var14 = var11.esVacia();
    int var15 = var11.cantidad();
    boolean var16 = var11.esVacia();
    boolean var17 = var11.esVacia();
    int var18 = var11.cantidad();
    int var19 = var11.cantidad();
    var0.encolar((java.lang.Object)var19);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test301() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test301");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    boolean var15 = var0.esVacia();
    int var16 = var0.cantidad();
    int var17 = var0.cantidad();
    int var18 = var0.cantidad();
    var0.desencolar();

  }

  public void test302() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test302");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    var0.desencolar();

  }

  public void test303() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test303");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    ColaSobreListasEnlazadas var7 = new ColaSobreListasEnlazadas();
    boolean var8 = var7.esVacia();
    int var9 = var7.cantidad();
    boolean var10 = var7.esVacia();
    int var11 = var7.cantidad();
    int var12 = var7.cantidad();
    boolean var13 = var7.esVacia();
    int var14 = var7.cantidad();
    boolean var15 = var7.esVacia();
    int var16 = var7.cantidad();
    boolean var17 = var7.esVacia();
    int var18 = var7.cantidad();
    var0.encolar((java.lang.Object)var7);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test304() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test304");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    var0.desencolar();

  }

  public void test305() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test305");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    ColaSobreListasEnlazadas var8 = new ColaSobreListasEnlazadas();
    int var9 = var8.cantidad();
    boolean var10 = var8.esVacia();
    boolean var11 = var8.esVacia();
    boolean var12 = var8.esVacia();
    boolean var13 = var8.esVacia();
    boolean var14 = var8.esVacia();
    boolean var15 = var8.esVacia();
    int var16 = var8.cantidad();
    boolean var17 = var8.esVacia();
    int var18 = var8.cantidad();
    var0.encolar((java.lang.Object)var18);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test306() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test306");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    boolean var13 = var12.esVacia();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    int var16 = var12.cantidad();
    int var17 = var12.cantidad();
    int var18 = var12.cantidad();
    int var19 = var12.cantidad();
    int var20 = var12.cantidad();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test307() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test307");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    java.lang.Object var9 = var0.primero();

  }

  public void test308() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test308");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    int var14 = var13.cantidad();
    var0.encolar((java.lang.Object)var13);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test309() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test309");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    var0.desencolar();

  }

  public void test310() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test310");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    boolean var15 = var0.esVacia();
    boolean var16 = var0.esVacia();
    boolean var17 = var0.esVacia();
    int var18 = var0.cantidad();
    ColaSobreListasEnlazadas var19 = new ColaSobreListasEnlazadas();
    int var20 = var19.cantidad();
    int var21 = var19.cantidad();
    int var22 = var19.cantidad();
    int var23 = var19.cantidad();
    int var24 = var19.cantidad();
    int var25 = var19.cantidad();
    boolean var26 = var19.esVacia();
    int var27 = var19.cantidad();
    var0.encolar((java.lang.Object)var19);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test311() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test311");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    boolean var13 = var12.esVacia();
    int var14 = var12.cantidad();
    int var15 = var12.cantidad();
    int var16 = var12.cantidad();
    int var17 = var12.cantidad();
    boolean var18 = var12.esVacia();
    boolean var19 = var12.esVacia();
    boolean var20 = var12.esVacia();
    boolean var21 = var12.esVacia();
    boolean var22 = var12.esVacia();
    boolean var23 = var12.esVacia();
    boolean var24 = var12.esVacia();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test312() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test312");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    int var10 = var9.cantidad();
    boolean var11 = var9.esVacia();
    boolean var12 = var9.esVacia();
    boolean var13 = var9.esVacia();
    int var14 = var9.cantidad();
    boolean var15 = var9.esVacia();
    int var16 = var9.cantidad();
    boolean var17 = var9.esVacia();
    int var18 = var9.cantidad();
    boolean var19 = var9.esVacia();
    int var20 = var9.cantidad();
    boolean var21 = var9.esVacia();
    var0.encolar((java.lang.Object)var9);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test313() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test313");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    java.lang.Object var14 = var0.primero();

  }

  public void test314() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test314");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    var0.desencolar();

  }

  public void test315() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test315");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    ColaSobreListasEnlazadas var8 = new ColaSobreListasEnlazadas();
    int var9 = var8.cantidad();
    boolean var10 = var8.esVacia();
    boolean var11 = var8.esVacia();
    boolean var12 = var8.esVacia();
    int var13 = var8.cantidad();
    boolean var14 = var8.esVacia();
    int var15 = var8.cantidad();
    boolean var16 = var8.esVacia();
    int var17 = var8.cantidad();
    boolean var18 = var8.esVacia();
    boolean var19 = var8.esVacia();
    int var20 = var8.cantidad();
    int var21 = var8.cantidad();
    boolean var22 = var8.esVacia();
    int var23 = var8.cantidad();
    var0.encolar((java.lang.Object)var23);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test316() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test316");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    java.lang.Object var12 = var0.primero();

  }

  public void test317() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test317");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    int var14 = var0.cantidad();
    var0.desencolar();

  }

  public void test318() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test318");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    boolean var16 = var0.esVacia();
    var0.desencolar();

  }

  public void test319() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test319");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    boolean var11 = var10.esVacia();
    int var12 = var10.cantidad();
    boolean var13 = var10.esVacia();
    int var14 = var10.cantidad();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test320() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test320");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    int var14 = var0.cantidad();
    ColaSobreListasEnlazadas var15 = new ColaSobreListasEnlazadas();
    boolean var16 = var15.esVacia();
    int var17 = var15.cantidad();
    int var18 = var15.cantidad();
    int var19 = var15.cantidad();
    int var20 = var15.cantidad();
    int var21 = var15.cantidad();
    int var22 = var15.cantidad();
    int var23 = var15.cantidad();
    boolean var24 = var15.esVacia();
    int var25 = var15.cantidad();
    int var26 = var15.cantidad();
    boolean var27 = var15.esVacia();
    var0.encolar((java.lang.Object)var27);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test321() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test321");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    var0.desencolar();

  }

  public void test322() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test322");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    var0.desencolar();

  }

  public void test323() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test323");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    java.lang.Object var12 = var0.primero();

  }

  public void test324() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test324");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    int var15 = var13.cantidad();
    int var16 = var13.cantidad();
    int var17 = var13.cantidad();
    int var18 = var13.cantidad();
    int var19 = var13.cantidad();
    boolean var20 = var13.esVacia();
    boolean var21 = var13.esVacia();
    boolean var22 = var13.esVacia();
    int var23 = var13.cantidad();
    int var24 = var13.cantidad();
    var0.encolar((java.lang.Object)var24);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test325() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test325");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    var0.desencolar();

  }

  public void test326() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test326");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    ColaSobreListasEnlazadas var8 = new ColaSobreListasEnlazadas();
    int var9 = var8.cantidad();
    int var10 = var8.cantidad();
    boolean var11 = var8.esVacia();
    boolean var12 = var8.esVacia();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test327() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test327");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    int var16 = var14.cantidad();
    int var17 = var14.cantidad();
    int var18 = var14.cantidad();
    boolean var19 = var14.esVacia();
    int var20 = var14.cantidad();
    boolean var21 = var14.esVacia();
    var0.encolar((java.lang.Object)var21);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test328() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test328");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    int var16 = var11.cantidad();
    boolean var17 = var11.esVacia();
    int var18 = var11.cantidad();
    boolean var19 = var11.esVacia();
    boolean var20 = var11.esVacia();
    int var21 = var11.cantidad();
    var0.encolar((java.lang.Object)var21);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test329() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test329");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    ColaSobreListasEnlazadas var4 = new ColaSobreListasEnlazadas();
    int var5 = var4.cantidad();
    boolean var6 = var4.esVacia();
    boolean var7 = var4.esVacia();
    boolean var8 = var4.esVacia();
    int var9 = var4.cantidad();
    boolean var10 = var4.esVacia();
    int var11 = var4.cantidad();
    int var12 = var4.cantidad();
    int var13 = var4.cantidad();
    boolean var14 = var4.esVacia();
    int var15 = var4.cantidad();
    boolean var16 = var4.esVacia();
    var0.encolar((java.lang.Object)var4);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test330() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test330");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    boolean var16 = var0.esVacia();
    java.lang.Object var17 = var0.primero();

  }

  public void test331() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test331");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    ColaSobreListasEnlazadas var8 = new ColaSobreListasEnlazadas();
    boolean var9 = var8.esVacia();
    int var10 = var8.cantidad();
    boolean var11 = var8.esVacia();
    int var12 = var8.cantidad();
    int var13 = var8.cantidad();
    boolean var14 = var8.esVacia();
    int var15 = var8.cantidad();
    boolean var16 = var8.esVacia();
    int var17 = var8.cantidad();
    boolean var18 = var8.esVacia();
    int var19 = var8.cantidad();
    var0.encolar((java.lang.Object)var8);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test332() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test332");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    boolean var16 = var14.esVacia();
    boolean var17 = var14.esVacia();
    int var18 = var14.cantidad();
    boolean var19 = var14.esVacia();
    int var20 = var14.cantidad();
    boolean var21 = var14.esVacia();
    int var22 = var14.cantidad();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test333() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test333");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    ColaSobreListasEnlazadas var7 = new ColaSobreListasEnlazadas();
    int var8 = var7.cantidad();
    boolean var9 = var7.esVacia();
    boolean var10 = var7.esVacia();
    boolean var11 = var7.esVacia();
    int var12 = var7.cantidad();
    int var13 = var7.cantidad();
    int var14 = var7.cantidad();
    boolean var15 = var7.esVacia();
    boolean var16 = var7.esVacia();
    int var17 = var7.cantidad();
    boolean var18 = var7.esVacia();
    boolean var19 = var7.esVacia();
    int var20 = var7.cantidad();
    boolean var21 = var7.esVacia();
    var0.encolar((java.lang.Object)var21);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test334() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test334");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    int var14 = var0.cantidad();
    int var15 = var0.cantidad();
    boolean var16 = var0.esVacia();
    ColaSobreListasEnlazadas var17 = new ColaSobreListasEnlazadas();
    int var18 = var17.cantidad();
    boolean var19 = var17.esVacia();
    boolean var20 = var17.esVacia();
    boolean var21 = var17.esVacia();
    int var22 = var17.cantidad();
    int var23 = var17.cantidad();
    int var24 = var17.cantidad();
    int var25 = var17.cantidad();
    boolean var26 = var17.esVacia();
    boolean var27 = var17.esVacia();
    boolean var28 = var17.esVacia();
    var0.encolar((java.lang.Object)var28);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test335() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test335");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    ColaSobreListasEnlazadas var7 = new ColaSobreListasEnlazadas();
    boolean var8 = var7.esVacia();
    int var9 = var7.cantidad();
    int var10 = var7.cantidad();
    int var11 = var7.cantidad();
    int var12 = var7.cantidad();
    int var13 = var7.cantidad();
    boolean var14 = var7.esVacia();
    boolean var15 = var7.esVacia();
    boolean var16 = var7.esVacia();
    var0.encolar((java.lang.Object)var16);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test336() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test336");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    boolean var13 = var12.esVacia();
    int var14 = var12.cantidad();
    int var15 = var12.cantidad();
    int var16 = var12.cantidad();
    int var17 = var12.cantidad();
    boolean var18 = var12.esVacia();
    int var19 = var12.cantidad();
    boolean var20 = var12.esVacia();
    int var21 = var12.cantidad();
    int var22 = var12.cantidad();
    boolean var23 = var12.esVacia();
    var0.encolar((java.lang.Object)var23);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test337() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test337");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    int var15 = var13.cantidad();
    int var16 = var13.cantidad();
    int var17 = var13.cantidad();
    int var18 = var13.cantidad();
    int var19 = var13.cantidad();
    boolean var20 = var13.esVacia();
    boolean var21 = var13.esVacia();
    boolean var22 = var13.esVacia();
    boolean var23 = var13.esVacia();
    boolean var24 = var13.esVacia();
    int var25 = var13.cantidad();
    boolean var26 = var13.esVacia();
    var0.encolar((java.lang.Object)var13);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test338() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test338");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    ColaSobreListasEnlazadas var8 = new ColaSobreListasEnlazadas();
    int var9 = var8.cantidad();
    boolean var10 = var8.esVacia();
    boolean var11 = var8.esVacia();
    int var12 = var8.cantidad();
    int var13 = var8.cantidad();
    int var14 = var8.cantidad();
    int var15 = var8.cantidad();
    int var16 = var8.cantidad();
    int var17 = var8.cantidad();
    boolean var18 = var8.esVacia();
    int var19 = var8.cantidad();
    int var20 = var8.cantidad();
    var0.encolar((java.lang.Object)var8);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test339() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test339");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    boolean var16 = var12.esVacia();
    int var17 = var12.cantidad();
    int var18 = var12.cantidad();
    int var19 = var12.cantidad();
    int var20 = var12.cantidad();
    var0.encolar((java.lang.Object)var20);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test340() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test340");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    int var16 = var0.cantidad();
    int var17 = var0.cantidad();
    int var18 = var0.cantidad();
    java.lang.Object var19 = var0.primero();

  }

  public void test341() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test341");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    java.lang.Object var13 = var0.primero();

  }

  public void test342() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test342");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    var0.desencolar();

  }

  public void test343() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test343");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    var0.desencolar();

  }

  public void test344() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test344");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    java.lang.Object var12 = var0.primero();

  }

  public void test345() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test345");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    boolean var11 = var10.esVacia();
    int var12 = var10.cantidad();
    boolean var13 = var10.esVacia();
    int var14 = var10.cantidad();
    boolean var15 = var10.esVacia();
    boolean var16 = var10.esVacia();
    boolean var17 = var10.esVacia();
    boolean var18 = var10.esVacia();
    boolean var19 = var10.esVacia();
    int var20 = var10.cantidad();
    boolean var21 = var10.esVacia();
    var0.encolar((java.lang.Object)var21);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test346() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test346");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    ColaSobreListasEnlazadas var6 = new ColaSobreListasEnlazadas();
    boolean var7 = var6.esVacia();
    int var8 = var6.cantidad();
    int var9 = var6.cantidad();
    int var10 = var6.cantidad();
    int var11 = var6.cantidad();
    int var12 = var6.cantidad();
    boolean var13 = var6.esVacia();
    boolean var14 = var6.esVacia();
    boolean var15 = var6.esVacia();
    boolean var16 = var6.esVacia();
    boolean var17 = var6.esVacia();
    int var18 = var6.cantidad();
    int var19 = var6.cantidad();
    boolean var20 = var6.esVacia();
    int var21 = var6.cantidad();
    int var22 = var6.cantidad();
    var0.encolar((java.lang.Object)var22);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test347() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test347");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    var0.desencolar();

  }

  public void test348() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test348");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    ColaSobreListasEnlazadas var15 = new ColaSobreListasEnlazadas();
    int var16 = var15.cantidad();
    boolean var17 = var15.esVacia();
    boolean var18 = var15.esVacia();
    boolean var19 = var15.esVacia();
    int var20 = var15.cantidad();
    int var21 = var15.cantidad();
    int var22 = var15.cantidad();
    boolean var23 = var15.esVacia();
    boolean var24 = var15.esVacia();
    boolean var25 = var15.esVacia();
    boolean var26 = var15.esVacia();
    boolean var27 = var15.esVacia();
    boolean var28 = var15.esVacia();
    var0.encolar((java.lang.Object)var15);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test349() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test349");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    var0.desencolar();

  }

  public void test350() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test350");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    boolean var16 = var14.esVacia();
    boolean var17 = var14.esVacia();
    boolean var18 = var14.esVacia();
    boolean var19 = var14.esVacia();
    boolean var20 = var14.esVacia();
    boolean var21 = var14.esVacia();
    boolean var22 = var14.esVacia();
    boolean var23 = var14.esVacia();
    int var24 = var14.cantidad();
    boolean var25 = var14.esVacia();
    boolean var26 = var14.esVacia();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test351() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test351");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    var0.desencolar();

  }

  public void test352() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test352");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    var0.desencolar();

  }

  public void test353() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test353");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    var0.desencolar();

  }

  public void test354() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test354");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    java.lang.Object var15 = var0.primero();

  }

  public void test355() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test355");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    int var10 = var9.cantidad();
    int var11 = var9.cantidad();
    int var12 = var9.cantidad();
    int var13 = var9.cantidad();
    int var14 = var9.cantidad();
    boolean var15 = var9.esVacia();
    var0.encolar((java.lang.Object)var9);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test356() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test356");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    java.lang.Object var15 = var0.primero();

  }

  public void test357() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test357");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    int var15 = var13.cantidad();
    boolean var16 = var13.esVacia();
    int var17 = var13.cantidad();
    int var18 = var13.cantidad();
    boolean var19 = var13.esVacia();
    int var20 = var13.cantidad();
    int var21 = var13.cantidad();
    boolean var22 = var13.esVacia();
    var0.encolar((java.lang.Object)var22);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test358() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test358");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    var0.desencolar();

  }

  public void test359() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test359");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    java.lang.Object var7 = var0.primero();

  }

  public void test360() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test360");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    java.lang.Object var6 = var0.primero();

  }

  public void test361() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test361");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    var0.desencolar();

  }

  public void test362() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test362");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    boolean var10 = var9.esVacia();
    boolean var11 = var9.esVacia();
    int var12 = var9.cantidad();
    boolean var13 = var9.esVacia();
    boolean var14 = var9.esVacia();
    int var15 = var9.cantidad();
    boolean var16 = var9.esVacia();
    int var17 = var9.cantidad();
    boolean var18 = var9.esVacia();
    boolean var19 = var9.esVacia();
    int var20 = var9.cantidad();
    var0.encolar((java.lang.Object)var20);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test363() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test363");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    ColaSobreListasEnlazadas var7 = new ColaSobreListasEnlazadas();
    int var8 = var7.cantidad();
    boolean var9 = var7.esVacia();
    boolean var10 = var7.esVacia();
    boolean var11 = var7.esVacia();
    int var12 = var7.cantidad();
    int var13 = var7.cantidad();
    int var14 = var7.cantidad();
    boolean var15 = var7.esVacia();
    int var16 = var7.cantidad();
    int var17 = var7.cantidad();
    boolean var18 = var7.esVacia();
    var0.encolar((java.lang.Object)var18);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test364() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test364");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    boolean var12 = var11.esVacia();
    int var13 = var11.cantidad();
    boolean var14 = var11.esVacia();
    int var15 = var11.cantidad();
    int var16 = var11.cantidad();
    boolean var17 = var11.esVacia();
    boolean var18 = var11.esVacia();
    var0.encolar((java.lang.Object)var11);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test365() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test365");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    int var15 = var11.cantidad();
    int var16 = var11.cantidad();
    int var17 = var11.cantidad();
    boolean var18 = var11.esVacia();
    int var19 = var11.cantidad();
    int var20 = var11.cantidad();
    int var21 = var11.cantidad();
    int var22 = var11.cantidad();
    var0.encolar((java.lang.Object)var22);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test366() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test366");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    java.lang.Object var11 = var0.primero();

  }

  public void test367() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test367");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    boolean var16 = var0.esVacia();
    ColaSobreListasEnlazadas var17 = new ColaSobreListasEnlazadas();
    int var18 = var17.cantidad();
    boolean var19 = var17.esVacia();
    boolean var20 = var17.esVacia();
    boolean var21 = var17.esVacia();
    boolean var22 = var17.esVacia();
    boolean var23 = var17.esVacia();
    boolean var24 = var17.esVacia();
    boolean var25 = var17.esVacia();
    int var26 = var17.cantidad();
    int var27 = var17.cantidad();
    boolean var28 = var17.esVacia();
    var0.encolar((java.lang.Object)var17);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test368() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test368");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    boolean var16 = var0.esVacia();
    ColaSobreListasEnlazadas var17 = new ColaSobreListasEnlazadas();
    int var18 = var17.cantidad();
    boolean var19 = var17.esVacia();
    boolean var20 = var17.esVacia();
    boolean var21 = var17.esVacia();
    int var22 = var17.cantidad();
    int var23 = var17.cantidad();
    int var24 = var17.cantidad();
    boolean var25 = var17.esVacia();
    int var26 = var17.cantidad();
    boolean var27 = var17.esVacia();
    boolean var28 = var17.esVacia();
    boolean var29 = var17.esVacia();
    boolean var30 = var17.esVacia();
    boolean var31 = var17.esVacia();
    var0.encolar((java.lang.Object)var31);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test369() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test369");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    boolean var15 = var0.esVacia();
    var0.desencolar();

  }

  public void test370() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test370");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    var0.desencolar();

  }

  public void test371() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test371");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    boolean var15 = var0.esVacia();
    boolean var16 = var0.esVacia();
    boolean var17 = var0.esVacia();
    var0.desencolar();

  }

  public void test372() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test372");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    var0.encolar((java.lang.Object)' ');
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test373() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test373");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    var0.desencolar();

  }

  public void test374() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test374");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    java.lang.Object var10 = var0.primero();

  }

  public void test375() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test375");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    java.lang.Object var14 = var0.primero();

  }

  public void test376() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test376");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    var0.desencolar();

  }

  public void test377() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test377");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    java.lang.Object var15 = var0.primero();

  }

  public void test378() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test378");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    int var11 = var10.cantidad();
    boolean var12 = var10.esVacia();
    boolean var13 = var10.esVacia();
    boolean var14 = var10.esVacia();
    boolean var15 = var10.esVacia();
    int var16 = var10.cantidad();
    var0.encolar((java.lang.Object)var16);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test379() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test379");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    boolean var12 = var11.esVacia();
    int var13 = var11.cantidad();
    int var14 = var11.cantidad();
    int var15 = var11.cantidad();
    int var16 = var11.cantidad();
    int var17 = var11.cantidad();
    boolean var18 = var11.esVacia();
    boolean var19 = var11.esVacia();
    boolean var20 = var11.esVacia();
    int var21 = var11.cantidad();
    boolean var22 = var11.esVacia();
    int var23 = var11.cantidad();
    int var24 = var11.cantidad();
    boolean var25 = var11.esVacia();
    boolean var26 = var11.esVacia();
    int var27 = var11.cantidad();
    var0.encolar((java.lang.Object)var11);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test380() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test380");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    boolean var16 = var12.esVacia();
    int var17 = var12.cantidad();
    int var18 = var12.cantidad();
    int var19 = var12.cantidad();
    boolean var20 = var12.esVacia();
    boolean var21 = var12.esVacia();
    int var22 = var12.cantidad();
    boolean var23 = var12.esVacia();
    boolean var24 = var12.esVacia();
    int var25 = var12.cantidad();
    boolean var26 = var12.esVacia();
    boolean var27 = var12.esVacia();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test381() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test381");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    var0.desencolar();

  }

  public void test382() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test382");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    boolean var12 = var11.esVacia();
    int var13 = var11.cantidad();
    int var14 = var11.cantidad();
    int var15 = var11.cantidad();
    int var16 = var11.cantidad();
    int var17 = var11.cantidad();
    int var18 = var11.cantidad();
    boolean var19 = var11.esVacia();
    int var20 = var11.cantidad();
    int var21 = var11.cantidad();
    int var22 = var11.cantidad();
    boolean var23 = var11.esVacia();
    boolean var24 = var11.esVacia();
    boolean var25 = var11.esVacia();
    boolean var26 = var11.esVacia();
    boolean var27 = var11.esVacia();
    int var28 = var11.cantidad();
    var0.encolar((java.lang.Object)var28);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test383() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test383");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    int var15 = var13.cantidad();
    int var16 = var13.cantidad();
    int var17 = var13.cantidad();
    int var18 = var13.cantidad();
    int var19 = var13.cantidad();
    boolean var20 = var13.esVacia();
    boolean var21 = var13.esVacia();
    boolean var22 = var13.esVacia();
    int var23 = var13.cantidad();
    var0.encolar((java.lang.Object)var13);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test384() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test384");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    java.lang.Object var12 = var0.primero();

  }

  public void test385() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test385");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    var0.desencolar();

  }

  public void test386() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test386");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    int var15 = var13.cantidad();
    int var16 = var13.cantidad();
    int var17 = var13.cantidad();
    int var18 = var13.cantidad();
    boolean var19 = var13.esVacia();
    int var20 = var13.cantidad();
    boolean var21 = var13.esVacia();
    boolean var22 = var13.esVacia();
    boolean var23 = var13.esVacia();
    var0.encolar((java.lang.Object)var23);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test387() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test387");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    int var14 = var13.cantidad();
    boolean var15 = var13.esVacia();
    boolean var16 = var13.esVacia();
    boolean var17 = var13.esVacia();
    int var18 = var13.cantidad();
    boolean var19 = var13.esVacia();
    int var20 = var13.cantidad();
    boolean var21 = var13.esVacia();
    int var22 = var13.cantidad();
    boolean var23 = var13.esVacia();
    var0.encolar((java.lang.Object)var13);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test388() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test388");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    int var16 = var11.cantidad();
    boolean var17 = var11.esVacia();
    int var18 = var11.cantidad();
    int var19 = var11.cantidad();
    int var20 = var11.cantidad();
    boolean var21 = var11.esVacia();
    boolean var22 = var11.esVacia();
    var0.encolar((java.lang.Object)var11);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test389() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test389");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    var0.desencolar();

  }

  public void test390() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test390");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    java.lang.Object var13 = var0.primero();

  }

  public void test391() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test391");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    var0.desencolar();

  }

  public void test392() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test392");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    var0.desencolar();

  }

  public void test393() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test393");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    ColaSobreListasEnlazadas var6 = new ColaSobreListasEnlazadas();
    int var7 = var6.cantidad();
    int var8 = var6.cantidad();
    int var9 = var6.cantidad();
    var0.encolar((java.lang.Object)var6);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test394() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test394");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    var0.desencolar();

  }

  public void test395() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test395");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    int var11 = var10.cantidad();
    boolean var12 = var10.esVacia();
    boolean var13 = var10.esVacia();
    int var14 = var10.cantidad();
    boolean var15 = var10.esVacia();
    int var16 = var10.cantidad();
    int var17 = var10.cantidad();
    int var18 = var10.cantidad();
    var0.encolar((java.lang.Object)var18);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test396() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test396");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    boolean var15 = var0.esVacia();
    int var16 = var0.cantidad();
    boolean var17 = var0.esVacia();
    int var18 = var0.cantidad();
    ColaSobreListasEnlazadas var19 = new ColaSobreListasEnlazadas();
    boolean var20 = var19.esVacia();
    int var21 = var19.cantidad();
    int var22 = var19.cantidad();
    int var23 = var19.cantidad();
    int var24 = var19.cantidad();
    int var25 = var19.cantidad();
    boolean var26 = var19.esVacia();
    boolean var27 = var19.esVacia();
    boolean var28 = var19.esVacia();
    int var29 = var19.cantidad();
    boolean var30 = var19.esVacia();
    int var31 = var19.cantidad();
    int var32 = var19.cantidad();
    boolean var33 = var19.esVacia();
    boolean var34 = var19.esVacia();
    var0.encolar((java.lang.Object)var19);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test397() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test397");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    int var10 = var9.cantidad();
    boolean var11 = var9.esVacia();
    boolean var12 = var9.esVacia();
    boolean var13 = var9.esVacia();
    int var14 = var9.cantidad();
    boolean var15 = var9.esVacia();
    boolean var16 = var9.esVacia();
    boolean var17 = var9.esVacia();
    int var18 = var9.cantidad();
    int var19 = var9.cantidad();
    int var20 = var9.cantidad();
    var0.encolar((java.lang.Object)var20);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test398() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test398");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    ColaSobreListasEnlazadas var7 = new ColaSobreListasEnlazadas();
    boolean var8 = var7.esVacia();
    int var9 = var7.cantidad();
    boolean var10 = var7.esVacia();
    int var11 = var7.cantidad();
    int var12 = var7.cantidad();
    int var13 = var7.cantidad();
    boolean var14 = var7.esVacia();
    int var15 = var7.cantidad();
    var0.encolar((java.lang.Object)var7);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test399() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test399");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    boolean var11 = var10.esVacia();
    int var12 = var10.cantidad();
    int var13 = var10.cantidad();
    int var14 = var10.cantidad();
    int var15 = var10.cantidad();
    int var16 = var10.cantidad();
    boolean var17 = var10.esVacia();
    boolean var18 = var10.esVacia();
    boolean var19 = var10.esVacia();
    int var20 = var10.cantidad();
    int var21 = var10.cantidad();
    int var22 = var10.cantidad();
    boolean var23 = var10.esVacia();
    boolean var24 = var10.esVacia();
    boolean var25 = var10.esVacia();
    var0.encolar((java.lang.Object)var10);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test400() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test400");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    boolean var16 = var12.esVacia();
    int var17 = var12.cantidad();
    int var18 = var12.cantidad();
    int var19 = var12.cantidad();
    boolean var20 = var12.esVacia();
    boolean var21 = var12.esVacia();
    int var22 = var12.cantidad();
    boolean var23 = var12.esVacia();
    boolean var24 = var12.esVacia();
    int var25 = var12.cantidad();
    boolean var26 = var12.esVacia();
    int var27 = var12.cantidad();
    int var28 = var12.cantidad();
    boolean var29 = var12.esVacia();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test401() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test401");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    boolean var15 = var14.esVacia();
    int var16 = var14.cantidad();
    int var17 = var14.cantidad();
    int var18 = var14.cantidad();
    int var19 = var14.cantidad();
    int var20 = var14.cantidad();
    int var21 = var14.cantidad();
    boolean var22 = var14.esVacia();
    int var23 = var14.cantidad();
    int var24 = var14.cantidad();
    int var25 = var14.cantidad();
    boolean var26 = var14.esVacia();
    boolean var27 = var14.esVacia();
    boolean var28 = var14.esVacia();
    boolean var29 = var14.esVacia();
    boolean var30 = var14.esVacia();
    var0.encolar((java.lang.Object)var30);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test402() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test402");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    var0.desencolar();

  }

  public void test403() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test403");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    boolean var16 = var12.esVacia();
    int var17 = var12.cantidad();
    boolean var18 = var12.esVacia();
    int var19 = var12.cantidad();
    boolean var20 = var12.esVacia();
    int var21 = var12.cantidad();
    boolean var22 = var12.esVacia();
    int var23 = var12.cantidad();
    int var24 = var12.cantidad();
    int var25 = var12.cantidad();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test404() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test404");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    boolean var16 = var14.esVacia();
    boolean var17 = var14.esVacia();
    boolean var18 = var14.esVacia();
    int var19 = var14.cantidad();
    int var20 = var14.cantidad();
    int var21 = var14.cantidad();
    boolean var22 = var14.esVacia();
    boolean var23 = var14.esVacia();
    int var24 = var14.cantidad();
    int var25 = var14.cantidad();
    int var26 = var14.cantidad();
    var0.encolar((java.lang.Object)var26);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test405() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test405");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    int var16 = var14.cantidad();
    boolean var17 = var14.esVacia();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test406() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test406");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    int var16 = var11.cantidad();
    boolean var17 = var11.esVacia();
    int var18 = var11.cantidad();
    boolean var19 = var11.esVacia();
    boolean var20 = var11.esVacia();
    var0.encolar((java.lang.Object)var20);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test407() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test407");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    boolean var11 = var10.esVacia();
    int var12 = var10.cantidad();
    boolean var13 = var10.esVacia();
    int var14 = var10.cantidad();
    int var15 = var10.cantidad();
    int var16 = var10.cantidad();
    boolean var17 = var10.esVacia();
    int var18 = var10.cantidad();
    boolean var19 = var10.esVacia();
    boolean var20 = var10.esVacia();
    int var21 = var10.cantidad();
    var0.encolar((java.lang.Object)var10);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test408() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test408");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    ColaSobreListasEnlazadas var6 = new ColaSobreListasEnlazadas();
    int var7 = var6.cantidad();
    boolean var8 = var6.esVacia();
    boolean var9 = var6.esVacia();
    boolean var10 = var6.esVacia();
    int var11 = var6.cantidad();
    int var12 = var6.cantidad();
    int var13 = var6.cantidad();
    boolean var14 = var6.esVacia();
    int var15 = var6.cantidad();
    boolean var16 = var6.esVacia();
    int var17 = var6.cantidad();
    var0.encolar((java.lang.Object)var17);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test409() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test409");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    boolean var15 = var13.esVacia();
    int var16 = var13.cantidad();
    boolean var17 = var13.esVacia();
    int var18 = var13.cantidad();
    boolean var19 = var13.esVacia();
    boolean var20 = var13.esVacia();
    boolean var21 = var13.esVacia();
    int var22 = var13.cantidad();
    boolean var23 = var13.esVacia();
    boolean var24 = var13.esVacia();
    var0.encolar((java.lang.Object)var24);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test410() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test410");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    java.lang.Object var14 = var0.primero();

  }

  public void test411() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test411");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    int var15 = var11.cantidad();
    int var16 = var11.cantidad();
    int var17 = var11.cantidad();
    int var18 = var11.cantidad();
    int var19 = var11.cantidad();
    boolean var20 = var11.esVacia();
    int var21 = var11.cantidad();
    boolean var22 = var11.esVacia();
    int var23 = var11.cantidad();
    var0.encolar((java.lang.Object)var23);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test412() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test412");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    var0.desencolar();

  }

  public void test413() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test413");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    int var16 = var0.cantidad();
    int var17 = var0.cantidad();
    int var18 = var0.cantidad();
    java.lang.Object var19 = var0.primero();

  }

  public void test414() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test414");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    var0.desencolar();

  }

  public void test415() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test415");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    java.lang.Object var13 = var0.primero();

  }

  public void test416() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test416");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    boolean var11 = var10.esVacia();
    boolean var12 = var10.esVacia();
    int var13 = var10.cantidad();
    boolean var14 = var10.esVacia();
    boolean var15 = var10.esVacia();
    int var16 = var10.cantidad();
    boolean var17 = var10.esVacia();
    int var18 = var10.cantidad();
    boolean var19 = var10.esVacia();
    boolean var20 = var10.esVacia();
    var0.encolar((java.lang.Object)var20);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test417() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test417");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    boolean var10 = var9.esVacia();
    int var11 = var9.cantidad();
    int var12 = var9.cantidad();
    int var13 = var9.cantidad();
    int var14 = var9.cantidad();
    int var15 = var9.cantidad();
    int var16 = var9.cantidad();
    boolean var17 = var9.esVacia();
    boolean var18 = var9.esVacia();
    int var19 = var9.cantidad();
    int var20 = var9.cantidad();
    boolean var21 = var9.esVacia();
    boolean var22 = var9.esVacia();
    int var23 = var9.cantidad();
    boolean var24 = var9.esVacia();
    boolean var25 = var9.esVacia();
    int var26 = var9.cantidad();
    boolean var27 = var9.esVacia();
    var0.encolar((java.lang.Object)var9);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test418() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test418");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    java.lang.Object var6 = var0.primero();

  }

  public void test419() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test419");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    ColaSobreListasEnlazadas var7 = new ColaSobreListasEnlazadas();
    int var8 = var7.cantidad();
    boolean var9 = var7.esVacia();
    boolean var10 = var7.esVacia();
    boolean var11 = var7.esVacia();
    int var12 = var7.cantidad();
    boolean var13 = var7.esVacia();
    boolean var14 = var7.esVacia();
    int var15 = var7.cantidad();
    int var16 = var7.cantidad();
    int var17 = var7.cantidad();
    boolean var18 = var7.esVacia();
    var0.encolar((java.lang.Object)var7);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test420() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test420");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    java.lang.Object var11 = var0.primero();

  }

  public void test421() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test421");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    var0.desencolar();

  }

  public void test422() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test422");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    var0.desencolar();

  }

  public void test423() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test423");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    java.lang.Object var14 = var0.primero();

  }

  public void test424() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test424");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    ColaSobreListasEnlazadas var6 = new ColaSobreListasEnlazadas();
    int var7 = var6.cantidad();
    boolean var8 = var6.esVacia();
    boolean var9 = var6.esVacia();
    boolean var10 = var6.esVacia();
    int var11 = var6.cantidad();
    boolean var12 = var6.esVacia();
    int var13 = var6.cantidad();
    int var14 = var6.cantidad();
    int var15 = var6.cantidad();
    int var16 = var6.cantidad();
    int var17 = var6.cantidad();
    boolean var18 = var6.esVacia();
    boolean var19 = var6.esVacia();
    boolean var20 = var6.esVacia();
    int var21 = var6.cantidad();
    boolean var22 = var6.esVacia();
    var0.encolar((java.lang.Object)var22);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test425() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test425");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    java.lang.Object var9 = var0.primero();

  }

  public void test426() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test426");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    int var10 = var9.cantidad();
    boolean var11 = var9.esVacia();
    boolean var12 = var9.esVacia();
    boolean var13 = var9.esVacia();
    int var14 = var9.cantidad();
    boolean var15 = var9.esVacia();
    int var16 = var9.cantidad();
    boolean var17 = var9.esVacia();
    boolean var18 = var9.esVacia();
    int var19 = var9.cantidad();
    int var20 = var9.cantidad();
    var0.encolar((java.lang.Object)var9);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test427() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test427");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    boolean var16 = var0.esVacia();
    java.lang.Object var17 = var0.primero();

  }

  public void test428() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test428");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    var0.desencolar();

  }

  public void test429() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test429");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    java.lang.Object var13 = var0.primero();

  }

  public void test430() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test430");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    ColaSobreListasEnlazadas var8 = new ColaSobreListasEnlazadas();
    boolean var9 = var8.esVacia();
    int var10 = var8.cantidad();
    int var11 = var8.cantidad();
    int var12 = var8.cantidad();
    int var13 = var8.cantidad();
    boolean var14 = var8.esVacia();
    int var15 = var8.cantidad();
    int var16 = var8.cantidad();
    boolean var17 = var8.esVacia();
    var0.encolar((java.lang.Object)var8);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test431() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test431");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    var0.desencolar();

  }

  public void test432() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test432");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    java.lang.Object var9 = var0.primero();

  }

  public void test433() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test433");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    int var10 = var9.cantidad();
    int var11 = var9.cantidad();
    int var12 = var9.cantidad();
    int var13 = var9.cantidad();
    int var14 = var9.cantidad();
    boolean var15 = var9.esVacia();
    boolean var16 = var9.esVacia();
    int var17 = var9.cantidad();
    int var18 = var9.cantidad();
    var0.encolar((java.lang.Object)var9);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test434() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test434");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    var0.desencolar();

  }

  public void test435() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test435");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    java.lang.Object var10 = var0.primero();

  }

  public void test436() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test436");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    boolean var16 = var0.esVacia();
    int var17 = var0.cantidad();
    java.lang.Object var18 = var0.primero();

  }

  public void test437() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test437");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    var0.desencolar();

  }

  public void test438() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test438");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    int var16 = var0.cantidad();
    int var17 = var0.cantidad();
    ColaSobreListasEnlazadas var18 = new ColaSobreListasEnlazadas();
    int var19 = var18.cantidad();
    boolean var20 = var18.esVacia();
    boolean var21 = var18.esVacia();
    int var22 = var18.cantidad();
    int var23 = var18.cantidad();
    int var24 = var18.cantidad();
    int var25 = var18.cantidad();
    int var26 = var18.cantidad();
    boolean var27 = var18.esVacia();
    int var28 = var18.cantidad();
    boolean var29 = var18.esVacia();
    int var30 = var18.cantidad();
    boolean var31 = var18.esVacia();
    boolean var32 = var18.esVacia();
    var0.encolar((java.lang.Object)var32);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test439() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test439");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    ColaSobreListasEnlazadas var16 = new ColaSobreListasEnlazadas();
    boolean var17 = var16.esVacia();
    int var18 = var16.cantidad();
    int var19 = var16.cantidad();
    boolean var20 = var16.esVacia();
    var0.encolar((java.lang.Object)var16);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test440() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test440");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    java.lang.Object var14 = var0.primero();

  }

  public void test441() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test441");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    ColaSobreListasEnlazadas var7 = new ColaSobreListasEnlazadas();
    int var8 = var7.cantidad();
    boolean var9 = var7.esVacia();
    boolean var10 = var7.esVacia();
    boolean var11 = var7.esVacia();
    int var12 = var7.cantidad();
    int var13 = var7.cantidad();
    int var14 = var7.cantidad();
    boolean var15 = var7.esVacia();
    boolean var16 = var7.esVacia();
    boolean var17 = var7.esVacia();
    boolean var18 = var7.esVacia();
    boolean var19 = var7.esVacia();
    boolean var20 = var7.esVacia();
    int var21 = var7.cantidad();
    var0.encolar((java.lang.Object)var7);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test442() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test442");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    var0.encolar((java.lang.Object)100.0f);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test443() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test443");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    var0.desencolar();

  }

  public void test444() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test444");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    java.lang.Object var12 = var0.primero();

  }

  public void test445() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test445");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    boolean var11 = var10.esVacia();
    int var12 = var10.cantidad();
    boolean var13 = var10.esVacia();
    int var14 = var10.cantidad();
    boolean var15 = var10.esVacia();
    boolean var16 = var10.esVacia();
    boolean var17 = var10.esVacia();
    int var18 = var10.cantidad();
    boolean var19 = var10.esVacia();
    boolean var20 = var10.esVacia();
    int var21 = var10.cantidad();
    var0.encolar((java.lang.Object)var21);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test446() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test446");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    int var15 = var13.cantidad();
    boolean var16 = var13.esVacia();
    int var17 = var13.cantidad();
    boolean var18 = var13.esVacia();
    boolean var19 = var13.esVacia();
    boolean var20 = var13.esVacia();
    boolean var21 = var13.esVacia();
    int var22 = var13.cantidad();
    boolean var23 = var13.esVacia();
    int var24 = var13.cantidad();
    boolean var25 = var13.esVacia();
    boolean var26 = var13.esVacia();
    var0.encolar((java.lang.Object)var13);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test447() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test447");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    int var15 = var0.cantidad();
    boolean var16 = var0.esVacia();
    boolean var17 = var0.esVacia();
    ColaSobreListasEnlazadas var18 = new ColaSobreListasEnlazadas();
    boolean var19 = var18.esVacia();
    int var20 = var18.cantidad();
    boolean var21 = var18.esVacia();
    int var22 = var18.cantidad();
    boolean var23 = var18.esVacia();
    boolean var24 = var18.esVacia();
    boolean var25 = var18.esVacia();
    boolean var26 = var18.esVacia();
    boolean var27 = var18.esVacia();
    boolean var28 = var18.esVacia();
    int var29 = var18.cantidad();
    var0.encolar((java.lang.Object)var18);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test448() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test448");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    java.lang.Object var13 = var0.primero();

  }

  public void test449() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test449");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    ColaSobreListasEnlazadas var8 = new ColaSobreListasEnlazadas();
    boolean var9 = var8.esVacia();
    boolean var10 = var8.esVacia();
    boolean var11 = var8.esVacia();
    int var12 = var8.cantidad();
    boolean var13 = var8.esVacia();
    var0.encolar((java.lang.Object)var8);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test450() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test450");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    var0.desencolar();

  }

  public void test451() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test451");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    boolean var10 = var9.esVacia();
    int var11 = var9.cantidad();
    int var12 = var9.cantidad();
    int var13 = var9.cantidad();
    int var14 = var9.cantidad();
    int var15 = var9.cantidad();
    int var16 = var9.cantidad();
    boolean var17 = var9.esVacia();
    boolean var18 = var9.esVacia();
    int var19 = var9.cantidad();
    int var20 = var9.cantidad();
    boolean var21 = var9.esVacia();
    boolean var22 = var9.esVacia();
    int var23 = var9.cantidad();
    boolean var24 = var9.esVacia();
    boolean var25 = var9.esVacia();
    int var26 = var9.cantidad();
    boolean var27 = var9.esVacia();
    var0.encolar((java.lang.Object)var9);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test452() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test452");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    int var16 = var11.cantidad();
    boolean var17 = var11.esVacia();
    int var18 = var11.cantidad();
    boolean var19 = var11.esVacia();
    int var20 = var11.cantidad();
    boolean var21 = var11.esVacia();
    boolean var22 = var11.esVacia();
    int var23 = var11.cantidad();
    int var24 = var11.cantidad();
    var0.encolar((java.lang.Object)var11);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test453() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test453");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    ColaSobreListasEnlazadas var15 = new ColaSobreListasEnlazadas();
    boolean var16 = var15.esVacia();
    int var17 = var15.cantidad();
    int var18 = var15.cantidad();
    int var19 = var15.cantidad();
    int var20 = var15.cantidad();
    int var21 = var15.cantidad();
    int var22 = var15.cantidad();
    boolean var23 = var15.esVacia();
    int var24 = var15.cantidad();
    int var25 = var15.cantidad();
    int var26 = var15.cantidad();
    boolean var27 = var15.esVacia();
    boolean var28 = var15.esVacia();
    boolean var29 = var15.esVacia();
    boolean var30 = var15.esVacia();
    int var31 = var15.cantidad();
    int var32 = var15.cantidad();
    int var33 = var15.cantidad();
    var0.encolar((java.lang.Object)var15);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test454() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test454");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    boolean var16 = var12.esVacia();
    int var17 = var12.cantidad();
    boolean var18 = var12.esVacia();
    int var19 = var12.cantidad();
    int var20 = var12.cantidad();
    boolean var21 = var12.esVacia();
    boolean var22 = var12.esVacia();
    boolean var23 = var12.esVacia();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test455() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test455");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    java.lang.Object var14 = var0.primero();

  }

  public void test456() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test456");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    var0.desencolar();

  }

  public void test457() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test457");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    var0.desencolar();

  }

  public void test458() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test458");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    boolean var13 = var0.esVacia();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    boolean var16 = var14.esVacia();
    boolean var17 = var14.esVacia();
    boolean var18 = var14.esVacia();
    int var19 = var14.cantidad();
    int var20 = var14.cantidad();
    int var21 = var14.cantidad();
    boolean var22 = var14.esVacia();
    boolean var23 = var14.esVacia();
    boolean var24 = var14.esVacia();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test459() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test459");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    int var16 = var11.cantidad();
    int var17 = var11.cantidad();
    var0.encolar((java.lang.Object)var17);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test460() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test460");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    boolean var14 = var13.esVacia();
    int var15 = var13.cantidad();
    int var16 = var13.cantidad();
    int var17 = var13.cantidad();
    int var18 = var13.cantidad();
    boolean var19 = var13.esVacia();
    int var20 = var13.cantidad();
    boolean var21 = var13.esVacia();
    int var22 = var13.cantidad();
    boolean var23 = var13.esVacia();
    boolean var24 = var13.esVacia();
    int var25 = var13.cantidad();
    var0.encolar((java.lang.Object)var13);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test461() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test461");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    java.lang.Object var12 = var0.primero();

  }

  public void test462() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test462");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    java.lang.Object var8 = var0.primero();

  }

  public void test463() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test463");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    java.lang.Object var7 = var0.primero();

  }

  public void test464() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test464");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    boolean var13 = var12.esVacia();
    int var14 = var12.cantidad();
    int var15 = var12.cantidad();
    int var16 = var12.cantidad();
    int var17 = var12.cantidad();
    int var18 = var12.cantidad();
    boolean var19 = var12.esVacia();
    boolean var20 = var12.esVacia();
    int var21 = var12.cantidad();
    int var22 = var12.cantidad();
    int var23 = var12.cantidad();
    int var24 = var12.cantidad();
    var0.encolar((java.lang.Object)var24);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test465() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test465");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    int var10 = var9.cantidad();
    boolean var11 = var9.esVacia();
    boolean var12 = var9.esVacia();
    boolean var13 = var9.esVacia();
    int var14 = var9.cantidad();
    int var15 = var9.cantidad();
    int var16 = var9.cantidad();
    boolean var17 = var9.esVacia();
    boolean var18 = var9.esVacia();
    int var19 = var9.cantidad();
    boolean var20 = var9.esVacia();
    boolean var21 = var9.esVacia();
    int var22 = var9.cantidad();
    boolean var23 = var9.esVacia();
    int var24 = var9.cantidad();
    boolean var25 = var9.esVacia();
    int var26 = var9.cantidad();
    int var27 = var9.cantidad();
    boolean var28 = var9.esVacia();
    var0.encolar((java.lang.Object)var9);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test466() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test466");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    java.lang.Object var11 = var0.primero();

  }

  public void test467() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test467");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    java.lang.Object var14 = var0.primero();

  }

  public void test468() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test468");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    boolean var13 = var11.esVacia();
    boolean var14 = var11.esVacia();
    boolean var15 = var11.esVacia();
    int var16 = var11.cantidad();
    boolean var17 = var11.esVacia();
    boolean var18 = var11.esVacia();
    int var19 = var11.cantidad();
    boolean var20 = var11.esVacia();
    int var21 = var11.cantidad();
    int var22 = var11.cantidad();
    boolean var23 = var11.esVacia();
    boolean var24 = var11.esVacia();
    boolean var25 = var11.esVacia();
    int var26 = var11.cantidad();
    boolean var27 = var11.esVacia();
    var0.encolar((java.lang.Object)var27);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test469() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test469");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    boolean var10 = var9.esVacia();
    int var11 = var9.cantidad();
    int var12 = var9.cantidad();
    int var13 = var9.cantidad();
    int var14 = var9.cantidad();
    boolean var15 = var9.esVacia();
    int var16 = var9.cantidad();
    int var17 = var9.cantidad();
    boolean var18 = var9.esVacia();
    int var19 = var9.cantidad();
    var0.encolar((java.lang.Object)var19);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test470() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test470");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    var0.desencolar();

  }

  public void test471() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test471");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    java.lang.Object var13 = var0.primero();

  }

  public void test472() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test472");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    java.lang.Object var13 = var0.primero();

  }

  public void test473() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test473");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    int var15 = var14.cantidad();
    boolean var16 = var14.esVacia();
    boolean var17 = var14.esVacia();
    int var18 = var14.cantidad();
    int var19 = var14.cantidad();
    int var20 = var14.cantidad();
    int var21 = var14.cantidad();
    int var22 = var14.cantidad();
    int var23 = var14.cantidad();
    boolean var24 = var14.esVacia();
    boolean var25 = var14.esVacia();
    boolean var26 = var14.esVacia();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test474() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test474");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    var0.desencolar();

  }

  public void test475() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test475");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    ColaSobreListasEnlazadas var13 = new ColaSobreListasEnlazadas();
    int var14 = var13.cantidad();
    boolean var15 = var13.esVacia();
    boolean var16 = var13.esVacia();
    boolean var17 = var13.esVacia();
    int var18 = var13.cantidad();
    boolean var19 = var13.esVacia();
    int var20 = var13.cantidad();
    int var21 = var13.cantidad();
    var0.encolar((java.lang.Object)var13);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test476() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test476");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    var0.desencolar();

  }

  public void test477() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test477");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    boolean var5 = var0.esVacia();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    java.lang.Object var10 = var0.primero();

  }

  public void test478() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test478");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    java.lang.Object var15 = var0.primero();

  }

  public void test479() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test479");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    boolean var11 = var10.esVacia();
    int var12 = var10.cantidad();
    int var13 = var10.cantidad();
    int var14 = var10.cantidad();
    int var15 = var10.cantidad();
    boolean var16 = var10.esVacia();
    int var17 = var10.cantidad();
    int var18 = var10.cantidad();
    boolean var19 = var10.esVacia();
    boolean var20 = var10.esVacia();
    boolean var21 = var10.esVacia();
    var0.encolar((java.lang.Object)var10);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test480() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test480");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    boolean var16 = var12.esVacia();
    int var17 = var12.cantidad();
    boolean var18 = var12.esVacia();
    int var19 = var12.cantidad();
    boolean var20 = var12.esVacia();
    int var21 = var12.cantidad();
    boolean var22 = var12.esVacia();
    int var23 = var12.cantidad();
    int var24 = var12.cantidad();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test481() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test481");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    ColaSobreListasEnlazadas var5 = new ColaSobreListasEnlazadas();
    int var6 = var5.cantidad();
    boolean var7 = var5.esVacia();
    boolean var8 = var5.esVacia();
    boolean var9 = var5.esVacia();
    int var10 = var5.cantidad();
    int var11 = var5.cantidad();
    int var12 = var5.cantidad();
    boolean var13 = var5.esVacia();
    boolean var14 = var5.esVacia();
    boolean var15 = var5.esVacia();
    int var16 = var5.cantidad();
    boolean var17 = var5.esVacia();
    boolean var18 = var5.esVacia();
    int var19 = var5.cantidad();
    boolean var20 = var5.esVacia();
    boolean var21 = var5.esVacia();
    var0.encolar((java.lang.Object)var21);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test482() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test482");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    var0.desencolar();

  }

  public void test483() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test483");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    var0.desencolar();

  }

  public void test484() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test484");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    int var12 = var0.cantidad();
    var0.desencolar();

  }

  public void test485() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test485");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    ColaSobreListasEnlazadas var14 = new ColaSobreListasEnlazadas();
    boolean var15 = var14.esVacia();
    int var16 = var14.cantidad();
    int var17 = var14.cantidad();
    int var18 = var14.cantidad();
    int var19 = var14.cantidad();
    boolean var20 = var14.esVacia();
    boolean var21 = var14.esVacia();
    boolean var22 = var14.esVacia();
    boolean var23 = var14.esVacia();
    int var24 = var14.cantidad();
    int var25 = var14.cantidad();
    int var26 = var14.cantidad();
    int var27 = var14.cantidad();
    int var28 = var14.cantidad();
    boolean var29 = var14.esVacia();
    var0.encolar((java.lang.Object)var14);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test486() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test486");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    ColaSobreListasEnlazadas var15 = new ColaSobreListasEnlazadas();
    boolean var16 = var15.esVacia();
    int var17 = var15.cantidad();
    int var18 = var15.cantidad();
    int var19 = var15.cantidad();
    int var20 = var15.cantidad();
    int var21 = var15.cantidad();
    boolean var22 = var15.esVacia();
    boolean var23 = var15.esVacia();
    boolean var24 = var15.esVacia();
    int var25 = var15.cantidad();
    boolean var26 = var15.esVacia();
    int var27 = var15.cantidad();
    boolean var28 = var15.esVacia();
    var0.encolar((java.lang.Object)var15);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test487() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test487");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    int var13 = var12.cantidad();
    boolean var14 = var12.esVacia();
    boolean var15 = var12.esVacia();
    int var16 = var12.cantidad();
    int var17 = var12.cantidad();
    int var18 = var12.cantidad();
    boolean var19 = var12.esVacia();
    boolean var20 = var12.esVacia();
    var0.encolar((java.lang.Object)var20);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test488() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test488");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    java.lang.Object var13 = var0.primero();

  }

  public void test489() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test489");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    boolean var15 = var0.esVacia();
    boolean var16 = var0.esVacia();
    boolean var17 = var0.esVacia();
    int var18 = var0.cantidad();
    var0.desencolar();

  }

  public void test490() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test490");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    int var10 = var0.cantidad();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    boolean var14 = var0.esVacia();
    ColaSobreListasEnlazadas var15 = new ColaSobreListasEnlazadas();
    int var16 = var15.cantidad();
    int var17 = var15.cantidad();
    int var18 = var15.cantidad();
    int var19 = var15.cantidad();
    boolean var20 = var15.esVacia();
    int var21 = var15.cantidad();
    boolean var22 = var15.esVacia();
    var0.encolar((java.lang.Object)var15);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test491() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test491");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    int var12 = var0.cantidad();
    int var13 = var0.cantidad();
    boolean var14 = var0.esVacia();
    ColaSobreListasEnlazadas var15 = new ColaSobreListasEnlazadas();
    int var16 = var15.cantidad();
    boolean var17 = var15.esVacia();
    boolean var18 = var15.esVacia();
    boolean var19 = var15.esVacia();
    int var20 = var15.cantidad();
    boolean var21 = var15.esVacia();
    int var22 = var15.cantidad();
    boolean var23 = var15.esVacia();
    boolean var24 = var15.esVacia();
    int var25 = var15.cantidad();
    boolean var26 = var15.esVacia();
    int var27 = var15.cantidad();
    var0.encolar((java.lang.Object)var15);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test492() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test492");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    int var15 = var0.cantidad();
    var0.desencolar();

  }

  public void test493() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test493");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    boolean var11 = var0.esVacia();
    ColaSobreListasEnlazadas var12 = new ColaSobreListasEnlazadas();
    boolean var13 = var12.esVacia();
    int var14 = var12.cantidad();
    boolean var15 = var12.esVacia();
    int var16 = var12.cantidad();
    boolean var17 = var12.esVacia();
    boolean var18 = var12.esVacia();
    boolean var19 = var12.esVacia();
    boolean var20 = var12.esVacia();
    int var21 = var12.cantidad();
    boolean var22 = var12.esVacia();
    int var23 = var12.cantidad();
    boolean var24 = var12.esVacia();
    int var25 = var12.cantidad();
    var0.encolar((java.lang.Object)var12);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test494() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test494");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    boolean var11 = var10.esVacia();
    int var12 = var10.cantidad();
    int var13 = var10.cantidad();
    int var14 = var10.cantidad();
    int var15 = var10.cantidad();
    boolean var16 = var10.esVacia();
    int var17 = var10.cantidad();
    int var18 = var10.cantidad();
    boolean var19 = var10.esVacia();
    boolean var20 = var10.esVacia();
    boolean var21 = var10.esVacia();
    int var22 = var10.cantidad();
    var0.encolar((java.lang.Object)var10);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test495() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test495");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    int var8 = var0.cantidad();
    ColaSobreListasEnlazadas var9 = new ColaSobreListasEnlazadas();
    boolean var10 = var9.esVacia();
    int var11 = var9.cantidad();
    int var12 = var9.cantidad();
    int var13 = var9.cantidad();
    int var14 = var9.cantidad();
    int var15 = var9.cantidad();
    boolean var16 = var9.esVacia();
    boolean var17 = var9.esVacia();
    int var18 = var9.cantidad();
    boolean var19 = var9.esVacia();
    int var20 = var9.cantidad();
    boolean var21 = var9.esVacia();
    var0.encolar((java.lang.Object)var9);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test496() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test496");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    int var3 = var0.cantidad();
    boolean var4 = var0.esVacia();
    boolean var5 = var0.esVacia();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    java.lang.Object var8 = var0.primero();

  }

  public void test497() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test497");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    boolean var6 = var0.esVacia();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    ColaSobreListasEnlazadas var11 = new ColaSobreListasEnlazadas();
    int var12 = var11.cantidad();
    int var13 = var11.cantidad();
    int var14 = var11.cantidad();
    boolean var15 = var11.esVacia();
    int var16 = var11.cantidad();
    var0.encolar((java.lang.Object)var16);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test498() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test498");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    boolean var7 = var0.esVacia();
    boolean var8 = var0.esVacia();
    int var9 = var0.cantidad();
    ColaSobreListasEnlazadas var10 = new ColaSobreListasEnlazadas();
    int var11 = var10.cantidad();
    int var12 = var10.cantidad();
    int var13 = var10.cantidad();
    int var14 = var10.cantidad();
    int var15 = var10.cantidad();
    boolean var16 = var10.esVacia();
    int var17 = var10.cantidad();
    boolean var18 = var10.esVacia();
    boolean var19 = var10.esVacia();
    var0.encolar((java.lang.Object)var10);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

  public void test499() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test499");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    boolean var1 = var0.esVacia();
    int var2 = var0.cantidad();
    int var3 = var0.cantidad();
    int var4 = var0.cantidad();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    int var8 = var0.cantidad();
    boolean var9 = var0.esVacia();
    int var10 = var0.cantidad();
    java.lang.Object var11 = var0.primero();

  }

  public void test500() throws Throwable {

    if (debug) System.out.printf("%nRandoopTest3.test500");


    ColaSobreListasEnlazadas var0 = new ColaSobreListasEnlazadas();
    int var1 = var0.cantidad();
    boolean var2 = var0.esVacia();
    boolean var3 = var0.esVacia();
    boolean var4 = var0.esVacia();
    int var5 = var0.cantidad();
    int var6 = var0.cantidad();
    int var7 = var0.cantidad();
    boolean var8 = var0.esVacia();
    boolean var9 = var0.esVacia();
    boolean var10 = var0.esVacia();
    int var11 = var0.cantidad();
    boolean var12 = var0.esVacia();
    boolean var13 = var0.esVacia();
    int var14 = var0.cantidad();
    boolean var15 = var0.esVacia();
    int var16 = var0.cantidad();
    int var17 = var0.cantidad();
    int var18 = var0.cantidad();
    int var19 = var0.cantidad();
    ColaSobreListasEnlazadas var20 = new ColaSobreListasEnlazadas();
    boolean var21 = var20.esVacia();
    boolean var22 = var20.esVacia();
    boolean var23 = var20.esVacia();
    int var24 = var20.cantidad();
    var0.encolar((java.lang.Object)var24);
    
    // Check representation invariant.
    assertTrue("Representation invariant failed: Check rep invariant (method repOK) for var0", var0.repOK());

  }

}
