package ej2.conInvariante;

import junit.framework.*;
import junit.textui.*;

public class RandoopTestConInvariante extends TestCase {

  public static void main(String[] args) {
    TestRunner runner = new TestRunner();
    TestResult result = runner.doRun(suite(), false);
    if (! result.wasSuccessful()) {
      System.exit(1);
    }
  }

  public RandoopTestConInvariante(String name) {
    super(name);
  }

  public static Test suite() {
    TestSuite result = new TestSuite();
    result.addTest(new TestSuite(RandoopTestConInvariante0.class));
    result.addTest(new TestSuite(RandoopTestConInvariante1.class));
    result.addTest(new TestSuite(RandoopTestConInvariante2.class));
    result.addTest(new TestSuite(RandoopTestConInvariante3.class));
    result.addTest(new TestSuite(RandoopTestConInvariante4.class));
    result.addTest(new TestSuite(RandoopTestConInvariante5.class));
    result.addTest(new TestSuite(RandoopTestConInvariante6.class));
    result.addTest(new TestSuite(RandoopTestConInvariante7.class));
    return result;
  }

}
