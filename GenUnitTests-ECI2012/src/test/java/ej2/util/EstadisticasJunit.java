package ej2.util;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static javax.xml.xpath.XPathConstants.*;

/**
 * Description : Estadísticas de la ejecución de Randoop
 * Date: 8/10/12
 * Time: 5:20 PM
 */
public class EstadisticasJunit {

    private Stats st = new Stats();
    private File archivo;
    private String prefijoClaseJunit;
    private File pathClasesJunit;

    public EstadisticasJunit(String archivo, String prefijoClaseJunit, String pathClasesJunit) {
        this.archivo = new File(archivo);
        this.prefijoClaseJunit = prefijoClaseJunit;
        this.pathClasesJunit = new File( pathClasesJunit );
    }

    public static void main(String[] args) throws Exception {

        final EstadisticasJunit estadisticasJunit = new EstadisticasJunit(args[0], args[1], args[2]);
        estadisticasJunit.ejecutar();
    }

    private void ejecutar() throws Exception {

        Document doc = parsingXML(archivo);
        final NodeList testFallidos = obtenerTestsFallidos(doc);
        calcularEstadisticas(testFallidos);
        mostrarEstadisticas();

    }

    private void mostrarEstadisticas() {
        System.out.println("\nEstadisticas\n============");
        System.out.println(st.toString());
    }

    private void calcularEstadisticas(NodeList testFallidos) throws Exception {
        System.out.println("Extrayendo estadísticas");
        for ( int i=0; i<  testFallidos.getLength(); i++) {
            final String salida = testFallidos.item(i).getTextContent();
            if ( salida.contains(ValidStats.NPE.getText()) ){
                st.incStat(ValidStats.NPE);
                if (salida.contains(ValidStats.NPE_PRIMERO.getText())) {
                    st.incStat(ValidStats.NPE_PRIMERO);
                } else if (salida.contains(ValidStats.NPE_DESENCOLAR.getText())) {
                    st.incStat(ValidStats.NPE_DESENCOLAR);
                }
                    
            } else if (salida.contains(ValidStats.NO_REPOK.getText())) {
                st.incStat(ValidStats.NO_REPOK);
                agregarMetodosInvolucrados(salida);
            } else
                st.incStat(ValidStats.OTROS);
        }
    }

    private void agregarMetodosInvolucrados(String salida) throws Exception {
        /***
         * recupera de una linea como la que sigue la línea en la que se
         * produjo el error y el nombre de la clase
         *
         * at RandoopTestConInvariante0.test16(RandoopTestConInvariante0.java:205)
         */

        final int inicioNombreClase = salida.indexOf(prefijoClaseJunit);
        if (inicioNombreClase == -1)
            throw new Exception("Prefijo de clase JUnit no existente");


        Extractor extractor = new Extractor(salida, inicioNombreClase).invoke();
        File archivoClase = extractor.getArchivoClase();
        int lineaError = extractor.getLineaError();
        String metodo = extractor.getMetodo();

        actualizarEstadisticaMetodosInvolucrados(archivoClase, lineaError, metodo);

    }

    private void actualizarEstadisticaMetodosInvolucrados(File archivoClase, int lineaError, String metodo) throws IOException {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(archivoClase)));


        int lineaActual = 0;
        boolean metodoEncontrado = false;
        String linea;
        while((linea = reader.readLine() ) != null && (lineaActual < lineaError) && ! metodoEncontrado) {
            lineaActual++;
            if (linea.contains("public void " + metodo + "()")) {
                metodoEncontrado = true;
                agregarMetodosPresentesAEstadistica(reader, lineaActual, lineaError);
            }

        }
    }

    private void agregarMetodosPresentesAEstadistica(BufferedReader reader, int lineaActual, int lineaError) throws IOException {
        String linea;
        boolean falloEncolar = false;
        boolean falloDesencolar = false;
        while( lineaActual < lineaError &&  (linea = reader.readLine() ) != null) {
            if (linea.contains(".encolar(") && ! falloEncolar) {
                st.incStat(ValidStats.NO_REPOK_ENCOLAR);
                falloEncolar = true;
            } else if (linea.contains(".desencolar(") && ! falloDesencolar) {
                st.incStat(ValidStats.NO_REPOK_DESENCOLAR);
                falloDesencolar = true;
            }
            lineaActual++;
        }
    }

    private static NodeList obtenerTestsFallidos(Document doc) throws XPathExpressionException {
        System.out.println("Búsqueda de test fallidos");
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        xpath.setNamespaceContext( new EstadisticasNamespace() );
        XPathExpression expr
                = xpath.compile("//test/output");

        return (NodeList) expr.evaluate(doc, NODESET);

    }

    private static Document parsingXML(File archivo) throws ParserConfigurationException, SAXException, IOException {
        System.out.println("Parsing de XML (" + archivo.getAbsolutePath() + ")");
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        // No usar DTDs externas -
        // http://stackoverflow.com/questions/3172420/java-io-ioexception-server-returned-http-response-code-503-for-url-http-www
        domFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        domFactory.setNamespaceAware(true); // never forget this!
        DocumentBuilder builder = domFactory.newDocumentBuilder();
        return builder.parse(archivo);
    }

    private static class EstadisticasNamespace implements NamespaceContext {
        @Override
        public String getNamespaceURI(String prefix) {
            return XMLConstants.NULL_NS_URI;
        }

        @Override
        public String getPrefix(String namespaceURI) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Iterator getPrefixes(String namespaceURI) {
            throw new UnsupportedOperationException();
        }
    }

    private class Stats {
        private Map<ValidStats, Integer> db = new HashMap<ValidStats, Integer>();

        public void incStat(ValidStats stat) {
            if (db.containsKey(stat))
                db.put(stat, new Integer( db.get(stat) + 1) );
            else
                db.put(stat, new Integer(1));
        }

        public String toString() {
            String ts = new String();

            for( ValidStats clave : db.keySet()) {
                ts += clave.getNombreLindo() + " -> " + db.get(clave) + "\n";
            }

            return ts;
        }
    }

    private enum ValidStats {
        NPE("NullPointerException", "Null Pointer Exception"),
        NPE_PRIMERO("NullPointerException\n\tat ColaSobreListasEnlazadas.primero", "Null Pointer Exception sobre primero()"),
        NPE_DESENCOLAR("NullPointerException\n\tat ColaSobreListasEnlazadas.desencolar", "Null Pointer Exception sobre desencolar()"),
        NO_REPOK("AssertionFailedError: Representation invariant failed", "Fallo del invariante"),
        OTROS("", "Otros errores"),
        NO_REPOK_ENCOLAR("","Fallo del invariante : encolar()" ),
        NO_REPOK_DESENCOLAR("", "Fallo del invariante : desencolar");

        private String text;

        public String getNombreLindo() {
            return nombreLindo;
        }

        private String nombreLindo;

        ValidStats(String text, String nombreLindo) {
            this.text = text;
            this.nombreLindo = nombreLindo;
        }

        public String getText() {
            return text;
        }
    }

    private class Extractor {
        private String salida;
        private int inicioNombreClase;
        private String metodo;
        private File archivoClase;
        private int lineaError;

        public Extractor(String salida, int inicioNombreClase) {
            this.salida = salida;
            this.inicioNombreClase = inicioNombreClase;
        }

        public String getMetodo() {
            return metodo;
        }

        public File getArchivoClase() {
            return archivoClase;
        }

        public int getLineaError() {
            return lineaError;
        }

        public Extractor invoke() {
            final int finNombreClase = salida.indexOf('.', inicioNombreClase);
            String clase = salida.substring(inicioNombreClase, finNombreClase);
            final int abreParentesis = salida.indexOf('(', finNombreClase);
            metodo = salida.substring(finNombreClase + 1, abreParentesis);
            archivoClase = new File(pathClasesJunit, clase + ".java");
            int dosPuntos = salida.indexOf(':', abreParentesis);
            int cierraParentesis = salida.indexOf(')', dosPuntos);
            lineaError = Integer.parseInt(salida.substring(dosPuntos + 1, cierraParentesis));
            return this;
        }
    }
}
