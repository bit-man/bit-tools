#!/bin/bash

. `dirname $0`/lib.bash

__debug=0

function usage() {
    cat <<EOH

    $0 --junit dir_tests_junit --suite clase_suite_junit
          [--debug]
    dir_tests_junit : directorio donde están ubicados
                      los tests de JUnit
    clase_suite_junit : nombre de la clase conteniendo
                        la suite de JUnit

    debug : permite hacer debugging (port 5005)

EOH
}

while [[ $# -ne 0 ]]; do
    if [[ "$1" == "--junit" ]]; then
       shift
       __junit=$1
    elif [[ "$1" == "--suite" ]]; then
       shift
       __junitSuite=$1
    elif  [[ "$1" == "--debug" ]]; then
       __debug=1
    else
       usage
       exit -1
    fi

    shift
done

[[ -z ${__junit} || -z ${__junitSuite} ]] && \
    usage && exit -1

compilar
for __arch in  `ls ${__junit}/*java`;  do
    javaC ${__arch}
done


[[ ${__debug} -eq 1 ]] && __junitFlags=`getDebugFlags`

runJunit ${__junitSuite}
