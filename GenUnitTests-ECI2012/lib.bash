#!/bin/bash

__home=`dirname $0`
__out=${__home}/output
__src=${__home}/src
__lib=${__home}/lib
__ej3Test=${__home}/ej3/test/
__randoopJar=${__lib}/randoop.1.3.2.jar
__junitJar=${__lib}/junit.jar
__koratJars="${__lib}/korat/korat.jar:${__lib}/korat/alloy4viz.jar:${__lib}/korat/commons-cli-1.0.jar:${__lib}/korat/javassist.jar"
__classPath=${__out}:${__randoopJar}:${__koratJars}:${__junitJar}

[[ ! -d ${__out} ]] && mkdir  ${__out}

# Compiador Java
# (1) : nombre de archivo
function javaC() {
    echo "Compilando $1"
    javac -g -cp ${__classPath} -d ${__out} $1
}

function compilar() {
    ${__home}/compile.sh
}

function getDebugFlags() {
    echo "-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"
}

function runJunit() {
    java ${__junitFlags} -ea -cp ${__out}:${__randoopJar}:${__koratJars}:${__junitJar} org.junit.runner.JUnitCore $1
}