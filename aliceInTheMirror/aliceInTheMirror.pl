#!/usr/bin/perl

use strict;
use warnings;


# internal settings, please don't touch !!!
###################################################################################################

use File::DirSync;
use Getopt::Long;

my $build = '$Rev$';
$build =~ /(\d+)/;
my $VERSION="0.0.beta.$1";

my $pgmName = "aliceInTheMirror.pl";
my $totalTime;

sub printHeader() {
	print   "$pgmName - version : $VERSION\n" .
            "brought to you by Victor A. Rodriguez (http://www.bit-man.com.ar)\n" . 
            "GNU General Public License v2.0\n\n";
}

sub usage() {
	printHeader();
	print "usage:\n";
	print "        $pgmName --src=source_folder  --dst=destination folder\n"
		. "                 [--ignore=comma_separated_lis_of_folders_to_ignore]\n"
		. "                 [--pre=file_to_execute] [--post=file_to_execute]\n";
}

sub showTime($$) {
	my ($pre, $post) = @_;
	print "$pre : " . localtime() . " $post\n\n";
}                                                               

sub showTotalTime($) {
	print "Total execution time (secs.) : " . $_[0] . "\n";
}

sub showSummary($) {
	my $dir = shift;

	print "\nUpdated entries :\n\n";
	printArray( $dir->entries_updated() );
	print "\n\nRemoved entries :\n\n";
	printArray( $dir->entries_removed() );
	print "\n\nSkipped entries :\n\n";
	printArray( $dir->entries_skipped() );
	print "\n\nFailed entries :\n\n";
	printArray( $dir->entries_failed() );
	print "\n";
}

sub printArray {
	my $count = 0;
	foreach ( @_ ) {
		print "* $_\n"   if $_;
		$count++;
	};
	print "* none\n"    if ! $count;
}

sub checkExec($) {
	return ( ! -d $_[0] && -x $_[0] );	
};

sub checkOptions($$$$$) {
	my ($ok, $toFolder, $fromFolder,  $preExec, $postExec ) = @_;

	if ( ( ! $ok ) || 
	     ( ! $toFolder || ! $fromFolder ) ) {
		usage();
		die "Wrong options :-(\n";
	};
	
	{
		no warnings; ## To avoid warnings if $preExec or $postExec are empty
		foreach my $file ($preExec,$postExec) {
			next   if ( ! $file );
		    die "$pgmName: File '$file' doesn't exists or can't be executed :-(\n"
		    		unless checkExec( $file );
		};
	}
}

sub executePgm($) {
	print "------------ Executing program $_[0] ------------\n";
	system($_[0]);
	print "------------------------------------------------------\n";
}
# main
###################################################################################################

my $fromFolder;
my $toFolder;
my @ignore;
my ($preExec, $postExec);

my $ok = GetOptions ( "src=s"     => \$fromFolder,
                      "dst=s"     => \$toFolder,
                      "ignore=s"  => \@ignore ,
                      "pre=s"     => \$preExec,
                      "post=s"    => \$postExec
                     );

checkOptions($ok, $toFolder, $fromFolder, $preExec, $postExec );

printHeader();
$totalTime = time;
showTime("----> Starting time", "<----");

executePgm($preExec)   if $preExec;

@ignore = split(/,/,join(',',@ignore));

my $dirsync = new File::DirSync {
  verbose => 0,
  nocache => 0,
  localmode => 0,
};

print  "Starting ...\n";
$dirsync->src($fromFolder);
$dirsync->dst($toFolder);

foreach ( @ignore ) {
	$dirsync->ignore($_);
}

print "Rebuilding ...\n";
$dirsync->rebuild();
print "Syncing ...\n";
$dirsync->dirsync();

executePgm($postExec)   if $postExec;

$totalTime = time - $totalTime;
showTime("\n----> Ending time", "<----");
showTotalTime($totalTime);
showSummary($dirsync);

