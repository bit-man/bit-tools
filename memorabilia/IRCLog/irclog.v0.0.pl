#!/usr/bin/perl -T

use warnings;
use strict;

use Getopt::Long; 
use Net::IRC;
use Log::Log4perl;
use Error qw(:try);

### Default settings

my $host;
my $nick;
my $channel;
my $logFile;
my @debug;         ## debug vector 
my $debugMax = 0;  ## number of subs that allow debugging
my $debugAll = 0;
my $debugList;
my $TZ = "-3";
my $email;

### internal settings and structure
### Non Plus Ultra !!

my $NAME = "IRC log";
my $VERSION = "0.0c";
my $AUTHOR = "Victor A. Rodriguez (Bit-Man)";
my $CONTACT = 'http://www.bit-man.com.ar';
my $SCREENWIDTH = 70;

my $sender = 'irclog@bit-man.com.ar';
     
my %logConf = (
    "log4perl.rootLogger"          => "INFO, Screen, Logfile",

#    "log4perl.appender.Logfile"          => "Log::Log4perl::Appender::File",
    "log4perl.appender.Logfile"          => "Log::Dispatch::FileRotate",
    "log4perl.appender.Logfile.mode"     => 'append',
    "log4perl.appender.Logfile.max"      => 15,   ## max. log files to keep
    "log4perl.appender.Logfile.DatePattern"      => 'yyyy-MM-dd', ## rotate daily
    "log4perl.appender.Logfile.TZ"      => 'ADT',    ## GMT  -3:00
    
    "log4perl.appender.Logfile.filename" => $logFile,
    "log4perl.appender.Logfile.layout"   => "Log::Log4perl::Layout::PatternLayout",
    "log4perl.appender.Logfile.layout.ConversionPattern" => "[%d] %m%n",

    "log4perl.appender.Screen"         => "Log::Log4perl::Appender::Screen",
    "log4perl.appender.Screen.stderr"  => "0",
    "log4perl.appender.Screen.layout"  => "PatternLayout",
    "log4perl.appender.Screen.layout.ConversionPattern" => "[%d] %m%n"
);

my $log;

sub dailyMaint();
sub setAlarm();

$SIG{ALRM} = sub {  dailyMaint();
                    setAlarm(); };

my $cmdSub = [ \&printInfo, \&printHelp, \&run];
my $cmd = [ 0, 0, 1 ];    ## 0: disabled, 1: enabled
my $errStr;    ## Extended error string


#############  setDebugFlags  ################################################
## sets accordingly the debug vector @debug 
sub setDebugFlags($$) {
    my ($debugList, $debugAll) = @_;
    
    ## inits the debug vector
    for my $i (0..($debugMax -1) ) {
        $debug[$i] = 0
    }

    return if ( ! $debugList && ! $debugAll);

    if ($debugAll) {  ## sets all debugging flags
        for my $i (0..($debugMax -1) ) {
            $debug[$i] = 1;
        };
    } else {  ## just set the passed ones
        foreach my $i (split /,/, $debugList) {
            dieSoft( "Debugging element '$i' is greater than the maximum allowed of '".($debugMax -1)."'\n", 1)
                if ($i > ($debugMax -1));
            $debug[$i] = 1;
        };
    };
};

#############  initLogger  ##################################################
sub initLogger() {
    $logFile = "irclog.$channel.log"   if ! $logFile;
    $logConf{ "log4perl.appender.Logfile.filename" } = $logFile;

    Log::Log4perl::init( \%logConf );
    $log = Log::Log4perl->get_logger();
};

#############  secsToMidnight  ##################################################
sub secsToMidnight() {
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday) = 
                            gmtime( time + $TZ * 60 * 60 );   # makes time zone correction
    return 60 * 60 * 24 - ($hour * 60 * 60) - ($min * 60) - $sec; ## secs to midnight
}

#############  setAlarm  ##################################################
sub setAlarm() {
    alarm secsToMidnight() + 30; ## adds a 30 secs shift, to avoid overlp with LogRotate
}


#############  sendLog  ##################################################
sub sendLog($) {
     my ($logFile) = @_;
     
     my $from = $sender;
     my $to = $email;
     my $subject = "IRC log for channel $channel";
     my $fh;
     my $msg;
     my $SMTP_server = 'bit-man.com.ar';
     my $SMTP_port = 25;
          
     if ( open ( $fh, $logFile ) ) {
         local $/;      # enable "slurp" mode
         $msg = <$fh>;
         close $fh;
     } else {
         $msg = "FATAL: Can't open file $log: $!";
     };

     use Net::SMTP;
     use Fatal qw(:void Net::SMTP::mail Net::SMTP::to
                  Net::SMTP::data Net::Cmd::datasend Net::Cmd::dataend
                  Net::SMTP::quit);

     try {
         my $smtp = Net::SMTP->new( $SMTP_server,  Port => $SMTP_port );
         throw Error::Simple( "No puedo mandar e-mail: $!" ) unless $smtp;

         $smtp->mail( $from );
         $smtp->to( $to );

         ## TODO : enviar como adjunto, para evitar problemas con encoding y
         ##        reducir el tama�o
         $smtp->data();
         $smtp->datasend("From: $from\n");
         $smtp->datasend("To: $to\n");
         $smtp->datasend("Subject: $subject\n");
         $smtp->datasend("Content-Type: text/plain\n");
         $smtp->datasend("\n");
         $smtp->datasend($msg);
         $smtp->dataend();

         $smtp->quit;
         $log->info("** File $logFile was sent to $email");
    }
    catch Error with {
        my $error = shift;
        $log->info( "** ". $error->{-text} );
    }
    otherwise {
        $log->info( "** Uncaught exception in sendLog()" );
    };
};

#############  dailyMaint  ##################################################
sub dailyMaint() {
    my $DIR;

    opendir ( $DIR, '.' );
    foreach ( readdir $DIR ) {
        next if ! /^($logFile\.\d*)$/;
        $_ = $1;  ## to avoid error because $_ is tainted
        sendLog( $_ );
        rename $_, "$_.sent";
    };
    close $DIR;
}
#############  ProcessArgs  ##################################################
sub ProcessArgs(){
    
    ## no_pass_through: stops command processing if unknown options are present
    Getopt::Long::Configure( qw(no_pass_through) );  
    GetOptions( 'host=s' => \$host,
                'nick=s' => \$nick,
                'chan=s' => \$channel,
                'log=s'  => \$logFile,
                'tz=s'   => \$TZ,
                'email=s' => \$email,
                'debugAll' => \$debugAll,
                'debug=s' => \$debugList,
                'help' => \($cmd->[1]),
                'info' => \($cmd->[0]) )
        || do {   initLogger();
                  PrintHeader();
                  dieSoft("Unknown arguments were passed", 1); };
    
    setDebugFlags( $debugList, $debugAll );
    $cmd->[1] = ( ! $host || ! $nick || ! $channel || ! $email );
    $cmd->[2] = ! $cmd->[0] &&  ! $cmd->[1];

    initLogger();
    PrintHeader();
    setAlarm();


    ## The options are loaded and the command processing can begin ...
    ## in case no commands are needed, just leave $cmdSub and $cmd empty

    my ($i, $cmdExec) = (0, 0);
    foreach (@$cmdSub) {
        if ($cmd->[$i++]) {
		    PrintSeparator2();
		    &$_();
            $cmdExec++;
        };
    }
    
    if (! $cmdExec) {
        PrintSeparator2();
        printHelp();
    };
};

###############################################################################
#############  IRC handlers  ##################################################
###############################################################################

# Once connection is done, just join
sub on_connect {
    my $self = shift;

    $log->info(  "Joining '$channel'..." );
    $self->join( $channel );
    $self->privmsg( $channel, "Hi there. I'm logging this channel for my master's private use !!");
}

# I'm connected to the IRC server
sub on_init {
    my ($self, $event) = @_;
    my (@args) = ($event->args);
    shift (@args);
    
    $log->info(  "*** @args" );
}

# What to do when someone leaves a channel the bot is on.
sub on_part {
    my ($self, $event) = @_;
    my ($channel) = ($event->to)[0];

    $log->info(  "*** '". $event->nick ."' has left channel '$channel'" );
}

# What to do when someone joins a channel the bot is on.
sub on_join {
    my ($self, $event) = @_;
    my ($channel) = ($event->to)[0];

    $log->info(  "*** '". $event->nick ."' (". $event->userhost .") has join channel '$channel'" );
}

# What to do when we receive a private PRIVMSG.
sub on_msg {
    my ($self, $event) = @_;
    my ($nick) = $event->nick;

    $log->info(  "*$nick*  ", ($event->args) );
    $self->privmsg($nick, "I'm just collecting channel activity for my master. Bye.");
}

# What to do when we receive channel text.
sub on_p