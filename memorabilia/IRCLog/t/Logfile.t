
use Test::More qw(no_plan);
use Test::Exception;
use Test::File;
use Test::Differences;

##########################  Settings ###########################################

$module = "IRCLog::Logfile";
$class = $module;
@methods = qw/new filename filepath filefolder getFilesToProcess mark/;
$channel = '#perl-es';

##########################  Internal variables ##################################
my $log;
my @logFileToProcess = ( "irclog.$channel.log.1",
                         "irclog.$channel.log.2" );
my @logFile = ( "irclog.$channel.log", 
                @logFileToProcess );
my @logFileToProcessWithPath;
my @logFileAfterMarking;
################################################################################
########                          Misc functions                       #########
################################################################################

sub makeLogFiles($) {
    my $refArray = shift;
    
    foreach ( @$refArray ) {
        my $fh;
        open $fh, "> $_";
        close $fh;
    }
};

sub eraseLogFiles($) {
    my $refArray = shift;
    
    foreach ( @$refArray ) {
        unlink;
    }
};

sub existLogFiles($) {
    my $refArray = shift;
    
    foreach ( @$refArray ) {
        file_exists_ok( $_ );
        file_empty_ok( $_ );
    };
}

sub notExistLogFiles($) {
    my $refArray = shift;
    
    foreach ( @$refArray ) {
        file_not_exists_ok( $_ );
    };
};

sub init() {
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday) =  gmtime(time);
    $year += 1900; $mon++;

    foreach ( @logFileToProcess ) {
        push @logFileToProcessWithPath, "./$_";
        push @logFileAfterMarking, "$_.$year-$mon-$mday";
    };
};
##############################  Test ###########################################
## Keep an eye on leting it to be sooooo linear as possible (no if and 
## such that disturb the testing logic)

init();

use_ok( $module );
can_ok( $module, @methods);

throws_ok { $module->new() } "Error::Simple", "No IRC channel specified";
throws_ok { $module->new() } qr/No IRC channel specified/, "No IRC channel specified";
lives_ok { $log = $module->new( channel => $channel ) } 'gets handler via new()';
isa_ok($log, $class);

is( $log->filename(), "irclog.$channel.log", "gets filename");
is( $log->filepath(), "./irclog.$channel.log", "gets filepath");
is( $log->filefolder(), ".", "gets filefolder");

my $filepath = $log->{'filepath'}; ## backups filepath
$log->{'filepath'} = 'non_existent_path';
throws_ok { $log->getFilesToProcess() } qr/Can\'t open folder/, 'Invalid path';
throws_ok { $log->getFilesToProcess() } "Error::Simple", 'Invalid path';
$log->{'filepath'} = $filepath;    ## restores filepath

makeLogFiles( \@logFile );
existLogFiles( \@logFile );

my @toProcess = $log->getFilesToProcess();
eq_or_diff \@toProcess, \@logFileToProcess, "Files to process";

@toProcess = $log->getFilesToProcess( 'with-path' => 1 );
eq_or_diff \@toProcess, \@logFileToProcessWithPath, "Files to process (option with-path)";

eraseLogFiles( \@logFile );
notExistLogFiles( \@logFile );

throws_ok { $log->mark() } "Error::Simple", "No filename passed";
throws_ok { $log->mark() } qr/No filename passed/, "No filename passed";

makeLogFiles( \@logFile );
existLogFiles( \@logFile );

foreach ( @logFileToProcess ) {
    $log->mark( $_ );
};

existLogFiles( \@logFileAfterMarking );

foreach ( @logFileToProcess ) {
    throws_ok { $log->mark( $_ ) } qr/already exists/, "File $_ already exists";
};

eraseLogFiles( \@logFileAfterMarking );
