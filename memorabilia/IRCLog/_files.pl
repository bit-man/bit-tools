#!/usr/bin/perl

use warnings;
use strict;


# internal variables. Non-plus ultra !!!
###############################################################################

my $fh1;
my $fh2;
my %db;

################## usage() ####################################################
sub usage() {
    return "usage:\n"
        .  "        $0 file_path1 file_path2\n";
};


sub myOpen($$) {
    my ($fh, $file) = @_;
    my $_fh;
    
    open ( $_fh, $file ) || return 0;
    
    $$fh = sub {
        <$_fh>;
    };
    return 1;
};

# main
###############################################################################

$| = 1;
my $file1 = shift || die usage();
my $file2 = shift || die usage();

myOpen( \$fh1, $file1) || die "Can't open file '$file1': $!\n";
myOpen( \$fh2, $file2) || die "Can't open file '$file2': $!\n";

while ( my $log = $fh1->() ) {
    chomp $log;
    $db{$log}++;
};

while ( my $log = $fh2->() ) {
    chomp $log;
    $db{$log}--;
};

foreach my $x ( keys %db ) {
    print "$x * " . $db{$x} . "\n"
        if $db{$x};
};
