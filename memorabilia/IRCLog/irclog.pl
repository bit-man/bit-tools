#!/usr/bin/perl -T

use warnings;
use strict;

use lib '..';
use Getopt::Long; 
use Bot::NotBasic;
use Log::Log4perl;
use Error qw(:try);
use IRCLog::Logfile;

### Default settings
$| = 1;
my $host;
my $nick;
my $channel;
my $logFile;
my @debug;         ## debug vector 
my $debugMax = 3;  ## number of subs that allow debugging
my $debugAll = 0;
my $debugList;
my $TZ = "-3";
my $email;
my $safetyShift = 120; ## secs after midnight that dailyMaint() sub will run.
                       ## This is to avoid an overlap with LogRotate

my $waitForReconnect = 5;   ## in case of connection lost, secs to wait until  
                            ## the next attempt to reconnect

### internal settings and structure
### Non Plus Ultra !!

my $NAME = "IRC log";
my $VERSION = "0.1";
my $AUTHOR = "Victor A. Rodriguez (Bit-Man)";
my $CONTACT = 'http://www.bit-man.com.ar';
my $SCREENWIDTH = 70;

my $sender = 'irclog@bit-man.com.ar';
     
my %logConf = (
    "log4perl.rootLogger"          => "DEBUG, Screen, Logfile",

    "log4perl.appender.Logfile"          => "Log::Dispatch::FileRotate",
    "log4perl.appender.Logfile.mode"     => 'append',
    "log4perl.appender.Logfile.max"      => 15,   ## max. log files to keep
    "log4perl.appender.Logfile.DatePattern"      => 'yyyy-MM-dd', ## rotate daily
    "log4perl.appender.Logfile.TZ"      => 'ADT',    ## GMT  -3:00
    
    "log4perl.appender.Logfile.filename" => $logFile,
    "log4perl.appender.Logfile.layout"   => "Log::Log4perl::Layout::PatternLayout",
    "log4perl.appender.Logfile.layout.ConversionPattern" => "[%d] %m%n",

    "log4perl.appender.Screen"         => "Log::Log4perl::Appender::Screen",
    "log4perl.appender.Screen.stderr"  => "0",
    "log4perl.appender.Screen.layout"  => "PatternLayout",
    "log4perl.appender.Screen.layout.ConversionPattern" => "[%d] %m%n"
);

my $log;

sub dailyMaint();
sub setAlarm();

$SIG{ALRM} = sub {  dailyMaint();
                    setAlarm(); };

my $cmdSub = [ \&printInfo, \&printHelp, \&run, \&dailyMaint ];
my $cmd    = [ 0, 0, 1, 0 ];    ## 0: disabled, 1: enabled
my $errStr;    ## Extended error string



#############  setDebugFlags  ################################################
## sets accordingly the debug vector @debug 
sub setDebugFlags($$) {
    my ($debugList, $debugAll) = @_;
    
    ## inits the debug vector
    for my $i (0..($debugMax -1) ) {
        $debug[$i] = 0
    }

    return if ( ! $debugList && ! $debugAll);

    if ($debugAll) {  ## sets all debugging flags
        for my $i (0..($debugMax -1) ) {
            $debug[$i] = 1;
        };
    } else {  ## just set the passed ones
        foreach my $i (split /,/, $debugList) {
            dieSoft( "Debugging element '$i' is greater than the maximum allowed of '".($debugMax -1)."'\n", 1)
                if ($i > ($debugMax -1));
            $debug[$i] = 1;
        };
    };
};

#############  initLogger  ##################################################
sub initLogger() {
    $channel = 'noChannel'     if ( ! $channel );
    ## At this point it's possible that the channel parameter is missing
    ## because of how ProcessArgs works
    my $logName = initLogProcessing();
    if ( ref ($logName) eq "Error::Simple" ) {
        dieSoft( "Can't initialize log processing : '" . $logName->{-text} . "' "
               . "(Error number : ". $logName->{-value}.")", 1 );
        return;
    };

    $logFile = $logName->filename();
    $logConf{ "log4perl.appender.Logfile.filename" } = $logFile;

    Log::Log4perl::init( \%logConf );
    $log = Log::Log4perl->get_logger();
};

#############  secsToMidnight  ##################################################
sub secsToMidnight() {
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday) = 
                            gmtime( time + $TZ * 60 * 60 );   # makes time zone correction
    return 60 * 60 * 24 - ($hour * 60 * 60) - ($min * 60) - $sec; ## secs to midnight
}

#############  setAlarm  ##################################################
sub setAlarm() {
    alarm secsToMidnight() + $safetyShift; ## adds time shift, to avoid overlp with LogRotate
}


#############  sendLog  ##################################################
sub sendLog($) {
     my ($fileToSend) = @_;
     
     my $from = $sender;
     my $to = $email;
     my $subject = "IRC log for channel $channel";
     my $fh;
     my $msg;
     my $SMTP_server = 'bit-man.com.ar';
     my $SMTP_port = 25;
     my $returnOK = 1;
          
     if ( open ( $fh, $fileToSend ) ) {
         local $/;      # enable "slurp" mode
         $msg = <$fh>;
         close $fh;
     } else {
         $msg = "FATAL: Can't open file $fileToSend: $!";
         $returnOK = 0;
     };

     use Net::SMTP;
     use Fatal qw(:void Net::SMTP::mail Net::SMTP::to
                  Net::SMTP::data Net::Cmd::datasend Net::Cmd::dataend
                  Net::SMTP::quit);

     try {
         my $smtp = Net::SMTP->new( $SMTP_server,  Port => $SMTP_port );
         throw Error::Simple( "No puedo mandar e-mail: $!" ) unless $smtp;

         $smtp->mail( $from );
         $smtp->to( $to );

         ## TODO : enviar como adjunto, para evitar problemas con encoding y
         ##        reducir el tama�o, anche gzipped
         $smtp->data();
         $smtp->datasend("From: $from\n");
         $smtp->datasend("To: $to\n");
         $smtp->datasend("Subject: $subject\n");
         $smtp->datasend("Content-Type: text/plain\n");
         $smtp->datasend("\n");
         $smtp->datasend($msg);
         $smtp->dataend();

         $smtp->quit;
         $log->info("** File $fileToSend was sent to $email");
    }
    catch Error with {
        my $error = shift;
        $log->info( "** ". $error->{-text} );
        $returnOK = 0;
    }
    otherwise {
        $log->info( "** Uncaught exception in sendLog()" );
        $returnOK = 0;
    };
    
    return $returnOK;
};


#############  filesInFolder  ##################################################
sub filesInFolder($) {
    my ($folder) = @_;
    my $dir;

    opendir($dir, $folder) || throw  Error::Simple("Can't open folder '$folder'");
    my @files = readdir $dir;
    closedir $dir          || throw  Error::Simple("Can't close folder '$folder'");
    return @files;
};

#############  endLogProcessing  ##################################################
sub endLogProcessing($) {
    $_[0] = 0;
};

#############  initLogProcessing  ##################################################
sub initLogProcessing() {
    my ($logName, $E);
    try {
        $logName = IRCLog::Logfile->new( channel => $channel );
    }
    catch Error::Simple with {
        $E = shift;
    };
    
    return $E    if $E;
    return $logName;
};

#############  dailyMaint  ##################################################
sub dailyMaint() {
    my $logName = initLogProcessing();
    if ( ref ($logName) eq "Error::Simple" ) {
        $log->debug( "Error : '" . $logName->{-text} . "' "
                   . "(Error number : ". $logName->{-value}.")" );
        return;
    };

    try {
        if ( $debug[2] ) { ## debugging !!!
            $log->debug("Log folder contents BEFORE file processing (folder: ".$logName->filefolder().")");
            $log->debug($_)
                foreach ( filesInFolder( $logName->filefolder() ) );
        };
    
        foreach (  $logName->getFilesToProcess() ) {
            $logName->mark( $_ )
                if sendLog( $_ );
        };

        if ( $debug[2] ) { ## debugging !!!
            $log->debug("Log folder contents AFTER file processing (folder: ".$logName->filefolder().")");
            $log->debug($_)
                foreach ( filesInFolder( $logName->filefolder() ) );
        };
        
        endLogProcessing( $logName );
    }
    catch Error::Simple with {
        my $E = shift;
        $log->debug( "Error  : " . $E->{-text} );
    }
}


#############  ProcessArgs  ##################################################
sub ProcessArgs(){
    
    print STDERR "Going to parse options\n"    if $debug[1];
    ## no_pass_through: stops command processing if unknown options are present
    Getopt::Long::Configure( qw(no_pass_through) );  
    GetOptions( 'hos