package IRCLog::Logfile;

use warnings;
use strict;

use Error;
use Cwd;

use constant DEBUG_STR => "debug (" . __PACKAGE__ . ") => ";
use constant ENOARG         => -1;
use constant EOPENFOLDER    => -2;
use constant ECLOSEFOLDER   => -3;
use constant ECHGFOLDER     => -4;
use constant ERENAMEFILE    => -5;
use constant EFILEEXISTS    => -6;

=pod

=head1 NAME

IRCLog::Logfile - manage file naming for log files (only to be used in irclog.pl)

=head1 SYNOPSIS

 #!/usr/bin/perl

 use warnings;
 use strict;

 use IRCLog::Logfile;
 use Error qw(:try);

 try {
    my $logfile = IRCLog::Logfile->new( channel => "#perl-es" );
    print "Filename : " . $logfile->filename() . "\n";
    print "Filepath : " . $logfile->filepath() . "\n";
    foreach ( $logfile->getFilesToProcess( 'with-path' => 1 ) ) {
        print "Processing file $_\n";
        $logfile->mark( $_ );
    };
 }
 catch Error::Simple with {
         my $E = shift;
         print "Error : " . $E->{-text} . "\n"
             . "        (Error number : ". $E->{-value}.")\n";
 }


=head1 METHODS

All methods throw an Error::Simple exception in case of error, which contains the
error text in -text and its numeric error in -value

=over 1

=item * new() : creates a handler to communicate with this package

=item * filename() : returns the name of the file

=item * filefolder() : returns only the path to the file

=item * filepath() : returns the whole path of the file (filefolder() + filename())

=item * getFilesToProcess() : returns an array of the files ready to be processed
(the ones that end in a number because of log rotation)

=item * mark() : marks the passed file as processed

=back

=head2 new

Create a handler for IRCLog::Logfile.

 my $logfile = IRCLog::Logfile->new(
                    channel => '#perl-es', 
                    debug => 1
               );

=head3 options

=over 1

=item * channel

mandatory option (not an option at all ;-D ) which establishes the name of the IRC channel

=item * path

establish the path where the logfile will be created. If none passed the current
dir is assumed. (.)

=item * debug

set the flag to allow printing of debug messages 

=back

=cut

sub new {
    my $class = shift;
    my %option = @_;

    my $self;

    $self->{'channel'} = $option{'channel'} 
        || throw Error::Simple("No IRC channel specified", ENOARG);
    $self->{'filename'} =  "irclog.". $self->{'channel'} .".log";
    $self->{'filepath'} = $option{'path'} || '.';
    $self->{'_debug-LF'} = $option{'debug'} || 0;

    if ( $option{'debug'} ) {
        foreach my $key ( keys %$self ) {
            print DEBUG_STR . "$key : " . $self->{$key} . "\n";
        };
    };

    return bless $self, $class;
};


=pod

=head2 filename

Returns the filename of the current log file

 my $filename = $logfile->filename();

=cut

sub filename {
    my $self = shift;

    return $self->{'filename'};
}

=pod

=head2 filepath

Returns the complete path of the current log file

 my $filepath = $logfile->filepath();

=cut


sub filepath {
    my $self = shift;

    ## ToDo : change '/' for the directory separator
    return $self->{'filepath'} . '/' . $self->{'filename'};
}

=pod

=head2 filefolder

Returns the folder containing the current log file

 my $filefolder = $logfile->filefolder();

=cut

sub filefolder {
    my $self = shift;

    return $self->{'filepath'};
}

=pod

=head2 getFilesToProcess

Returns an array containing the filenames to be processed.
In case that 'with-path' is indicated, then the filepath is appended

 my @file = $logfile->getFilesToProcess();
 my @fileWithPath = $logfile->getFilesToProcess( 'with-path' => 1 );

=cut

sub getFilesToProcess {
    my $self = shift;
    my %option = @_;
    my $logFile =  $self->{'filename'};
    my $DIR;
    my @file;

    opendir ( $DIR, $self->{'filepath'} ) 
        || throw Error::Simple("Can't open folder " . $self->{'filepath'} . ": $!", EOPENFOLDER );

    foreach ( readdir $DIR ) {
        next if ! /^($logFile\.\d*)$/;   ### log files that end in number were "rotated" by LogRotate
        $_ = $1;  ## to avoid error because $_ is tainted
        
        ## ToDo : change '/' for the directory separator
        push @file, 
             ( defined $option{'with-path'} && ! $option{'with-path'} ) ?
                        $_ :
                        $self->{'filepath'} . '/' . $_;
    };
    closedir $DIR
        || throw Error::Simple("Can't close folder " . $self->{'filepath'} . ": $!", ECLOSEFOLDER );
    return @file;
}

=pod

=head2 mark

Marks the passed filename as processed

 $logfile->mark( $thisFile );

=cut

sub mark {
    my ($self, $file) = @_;
    
    throw Error::Simple("No filename passed", ENOARG)   unless $file;

    my $cwd = cwd();
    $cwd =~ /^(\/[\w\ \-\.]*)*$/;
    $cwd = $&; ## to avoid error because $cwd is tainted
    
    chdir $self->{'filepath'}
        || throw Error::Simple("Can't change to folder " . $self->{'filepath'} . ": $!", ECHGFOLDER );
        
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday) =  gmtime(time);
    $year += 1900; $mon++;
    my $newName = "$file.$year-$mon-$mday";
    
    if ( -e $newName ) {
        chdir $cwd;
        throw Error::Simple("'$newName' already exists", EFILEEXISTS );
    };
    rename $file, $newName
        || do { chdir $cwd;
                throw Error::Simple("Can't rename file '$file' to '$newName': $!", ERENAMEFILE );  };
    
    chdir $cwd
        || throw Error::Simple("Can't change to folder '$cwd': $!", ECHGFOLDER );

}

1;
