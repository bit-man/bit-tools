package Text::StrangeChars;

use strict;
use warnings;

use Error ':try';

use Exporter;
use vars qw(@ISA @EXPORT @EXPORT_OK);
@ISA = qw( Exporter );
@EXPORT = qw( new );
@EXPORT_OK = qw ( new );


=pod

=head1 Strange chars database :-)

=head2 Methods

=over 1

=item new() : generates a handler for database usage

=item getReplacement() : obtains a replacement for the given word

=item close() : closes db relationship

=back


=head2 ToDo

=over 1

=item A string explaining the error that has just occurred

=back

=cut


######################### new() ###############################################
sub new {
    my $class = shift;
    my $dbFile = shift;

    my %db;
    my $self;
    my $error = 0;
    return undef    if ! $dbFile;

    try {
        my ($word, $replacement, $fh);
        open ($fh, $dbFile) || throw Error::Simple ("Can't open database $dbFile");
        while( <$fh> ) {
            ($word, $replacement) = split /,/;
            chomp $replacement;
            $db{$word} = $replacement;
        }
        close $fh  || throw Error::Simple ("Can't close database $dbFile");
    }
    catch Error::Simple with {
        my $E = shift;                                        ##
        $self->{Error} = $E->{-text};                         ## Just for debugging
        #print "Error : >>>> " . $self->{Error} . " <<<<\n\n"; ##
        $error = 1;
    };

    return undef if $error;
    $self->{db} = \%db;
    return bless $self, $class;
}

######################### getReplacement() ###############################################
sub getReplacement {
    my $self = shift;
    
    return $self->{db}{$_[0]};
};

######################### close() ###############################################
sub close {
    my $self = shift;
    
    return 1;
};

1;
