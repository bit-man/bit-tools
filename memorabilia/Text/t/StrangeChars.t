
use Test::More qw(no_plan);
use Test::File;
use Test::File::Contents;

##########################  Settings ###########################################

$module = "Text::StrangeChars";
$class = $module;
@methods = qw/ new getReplacement close /;
$dbFile = "t/Text/strangechars.db";
$dbFileContents = "replace_this,with_this\n"
                . "and_this,with_this_one\n"
                . "and_so_on,and_on_and_on";

##########################  Internal variables ##################################
my %db;

################################################################################
########                          Misc functions                       #########
################################################################################
sub createDB($) {
    open( FH, "> $dbFile" );
    print FH $dbFileContents;
    close FH;
    setDBinMemory();
};

sub eraseDB($) {
    unlink $dbFile;
};

sub setDBinMemory() {
    foreach ( split /\n/, $dbFileContents ) {
        my ($word, $replacement) = split /,/;
        chomp $replacement;
        $db{$word} = $replacement;
    };
}
##############################  Test ###########################################
## Keep an eye on leting it to be sooooo linear as possible (no if and 
## such that disturb the testing logic)

#file_not_exists_ok( $dbFile );  ## this one and file_exists_ok works swaped in Win32
createDB( $dbFile );
file_exists_ok( $dbFile );
file_contents_is ($dbFile,  $dbFileContents,  "testing DB contents");

use_ok( $module );
can_ok( $module, @methods);

my $qry = $module->new();
ok( ! $qry, "New without arguments");

$qry =  Text::StrangeChars->new( "nonexistent.db" );
ok( ! $qry, "DB non existent");

$qry =  Text::StrangeChars->new( $dbFile );
isa_ok( $qry, $class );
is_deeply( \%db, $qry->{db}, "Databases comparison (in module and created in .t)");

foreach my $key( keys %db ) {
    is( $qry->getReplacement($key), $db{$key}, "Testing if '$db{$key}' is a replacement for '$key'");
};

ok( $qry->close(), "Erasing object" );

file_exists_ok( $dbFile );  ## this one and file_not_exists_ok works swaped in Win32
eraseDB( $dbFile );
#file_not_exists_ok( $dbFile ); 
