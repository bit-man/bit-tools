package Text::StrangeChars::Admin;

use strict;
use warnings;

use Error qw(:try);

use Exporter;
use vars qw(@ISA @EXPORT @EXPORT_OK);
@ISA = qw( Exporter );
@EXPORT = qw( new );
@EXPORT_OK = qw ( new );


=pod

=head1 Strange chars administration kit :-)

=head2 Methods

=over 1

=item new() : generates a handler for administrative tasks

=item createDB() : creates a new database

=item eraseDB() : erases an existing database (NOT IMPLEMENTED YET)

=item compressDB() : adding a word doesn't means that it already exists, then this
                     one deletes duplicates (and something else ... or not ??)
                     (NOT IMPLEMENTED YET).

=item addWord() : adds a word to the dictionary

=item delWord() : deletes an existing word from the dictionary (NOT IMPLEMENTED YET)

=item changeWord() : changes the translation code for a word (NOT IMPLEMENTED YET)

=item close() : ends the admin cycle

=back


=head2 ToDo

=over 1

=item A string explaining the error that has just occurred

=item Detect duplicated entries 

=back

=cut


######################### new() ###############################################
sub new {
    my $class = shift;
    my $dbFile = shift;

    my ($self, $fh);
    return 0    if ! $dbFile;
    return 0    if ! open ($fh, ">> $dbFile" ); ## The database will be opened
                                                ## for administration purposes
                                                ## so, it will be created OR
                                                ## modified, then (for any purpose)
                                                ## it MUST exist !!

    $self->{dbFile} = $dbFile;
    $self->{FH} = $fh;

    return bless $self, $class;
}

######################### new() ###############################################
sub createDB {
    my $self = shift;
    my $fh;
    my $error = 0;
    
    return 1   if ( $self->{dbFile} && $self->{FH} ); ## It was created in new() :-D
    return 0;
}


######################### _buildRegister() ###############################################
sub _buildRegister($$) {
    return "$_[0],$_[1]\n"
}

######################### new() ###############################################
sub addWord {
    my $self = shift;
    my $word = shift;
    my $replacement = shift;

    my $error = 0;
    try {
        my $fh = $self->{FH};
        my $reg = _buildRegister( $word, $replacement );
        seek ( $fh, 0, 2 )
            || throw Error::Simple("Can't go to the EOF: $!");
        print $fh $reg
            || throw Error::Simple("Can't add word to database: $!");
    }
    catch Error::Simple with {
        $error = 1;
    };
    
    return (! $error) ;
}


######################### close() ###############################################
sub close {
    my $self = shift;

    CORE::close  $self->{FH};
};

1;
