#!/usr/bin/perl -w

use strict;
use SOAP::Lite;

# main
##############################################################################
sub PrintHeader();
sub ProcessArgs();
sub PrintSeparator();
sub ConnectToWebService($);

my $SETIhost = 'localhost';
my $SETIport = 31416;
my $SETIhandler;      ### Web service handler;
my $VERSION = 'v0.2.4';
my $NAME = 'SETI@Home command line wrapper';
my $AUTHOR = 'Victor A. Rodriguez (victor@Bit-Man.com.ar)';
my $SETIversion = "unknown";
my $SETIArgs = '-nice 19 -email > /dev/null 2> /dev/null';

ConnectToWebService( $SETIhost );
$SETIversion = $SETIhandler->GetSETIversion()->result;

PrintSeparator();
PrintHeader();
ProcessArgs();
PrintSeparator();

exit;

#############  ConnectToWebService  ##########################################
sub ConnectToWebService($) {
    my $host = shift;
    
    $SETIhandler = SOAP::Lite                                             
        -> uri('setiathome')                                             
        -> proxy('http://'.$host .':'. $SETIport .'/')
        -> on_fault(sub { my($soap, $res) = @_; 
             die ref $res ? $res->faultstring : $soap->transport->status, "\n";
           });
}

#############  ProcessArgs  ##################################################
sub ProcessArgs(){
	my $param;
	my ($progress, @finish, @results, $results);
    my @cpu;
    
	if( $#ARGV == -1 ) {
		PrintSeparator();
	    PrintHelp();
		PrintSeparator();
		exit;
	};
	
	while ($_ = shift @ARGV) {
		PrintSeparator();
        
		/^--host$/ &&          do { $SETIhost = GetModifier('localhost');
                                    ConnectToWebService( $SETIhost );
                                    $SETIversion = $SETIhandler->GetSETIversion()->result;
		                            PrintSETIversion();
                                    next; };
        
        ### This hack allows the use of commands without a host definition
        ### (the default is localhost), and makes more usable and
        ### compatible with previous versions
        
        ConnectToWebService( $SETIhost ) if ( ! defined $SETIhandler );
                                    
		/^--progress$/ &&      do { Progress();
		                            next; };
		/^--help$/ &&          do { PrintHelp();  
		                            next; };
		/^--stop$/ &&          do { StopSeti();
                                    next; };
		/^--start$/ &&         do { StartSeti();
                                    next; };
		/^--info$/ &&          do { ShowInfo();  
		                            next; };
		/^--stopaftersend$/ && do { StopAfterSend();    
		                             next; };
		print "Unknown command: $_\n\n";
		PrintHelp();
	}
};


#############  StopAfterSend  #####################################################
sub StopAfterSend() {
    my $param = GetModifier( 'query' );
    
	if ( ! ($param eq 'start' || $param eq 'stop'  || $param eq 'query' ) ) {
		 print "Wrong parameters for 'stop after send'.\n\n";
		 PrintHelp();
		 return;
	};

    print "Signaling 'stop after send'...\n";
    print "'Stop after send' is " .
                  ( ($SETIhandler->StopAfterSend( $param )->result) ? 
                      "activated" : "deactivated") . "\n";
    print "Signaling done.\n";
}


#############  ShowInfo  #####################################################
sub ShowInfo() {
    my $info = $SETIhandler->ShowInfo()->result;
    my $none = '- none -';
    my ($h, $m, $s);
    my %unknown;

    foreach ( sort keys %$info ) {

		/^email_addr/ && do { print "E-mail: " . ($$info{$_} || $none) . "\n";
                              next; };
		/^name/ &&       do { print "User name: " .($$info{$_} || $none)."\n";
			                  next; };
		/^url/ &&        do { print "URL: " .($$info{$_} || $none). "\n";
			                  next; };
		/^country/ &&    do { print "Country: " .($$info{$_} || $none). "\n";
			                  next; };
		/^postal_code/ && do { print "Postal code: " .($$info{$_} || $none). "\n";
			                        next; };
		/^register_time/ && do { $$info{$_} =~ /.*\((.*)\)/;
		                          print "Register date: " .( $1 || $none). "\n";
								  next; };
		/^last_result_time/ && do { $$info{$_} =~ /.*\((.*)\)/;
		                          print "Last result date: " .($1 || $none). "\n";
								  next; };
		/^nresults/ && do { print "Data units: " .($$info{$_} || $none). "\n";
			                     next; };
		/^total_cpu/ && do { my $hh = $SETIhandler->SecsToHMS($$info{$_});
                             $h = $hh->result;
                             ($m, $s) = $hh->paramsout;
		                          print "Total CPU time: ";
		                          print sprintf "%d hs. %d min %.2f sec.\n",$h, $m, $s;
								  next; };

        $unknown{$_}=$$info{$_};
    };
    
    print "\nNon interpreted values\n";
    print   "----------------------\n";
    
    foreach ( sort keys %unknown ) {
        print "$_: $unknown{$_}\n";
    }
}

#############  StopSeti  #####################################################
sub StopSeti() {
    print "Stopping SETI\@home ...\n";
    $SETIhandler->StopSeti(); 
    print "SETI\@home stopped.\n";
}
		                            
#############  StartSeti  #####################################################
sub StartSeti(){
    my $param = GetModifier( $SETIArgs );
    print "Starting SETI\@Home...\n";
	print "SETI\@Home started !!!\n" 
        unless $SETIhandler->StartSeti( $param )->result != 0;  	                            
}

#############  Progress  ###################################################

sub Progress() {
    my ($progress, @cpu);

    $progress = $SETIhandler->Progress();
    printf "Progress:  %.2f%%\n", $progress->result * 100;
    @cpu = $progress->paramsout;
    printf "CPU Time:  %d hs. %d min %.2f sec.\n",$cpu[0][0],$cpu[0][1],$cpu[0][2];
    printf "Estimated:  %d hs. %d min %.2f sec.\n",$cpu[1][0],$cpu[1][1],$cpu[1][2]
        if ($SETIversion < 3);
}

#############  GetModifier  ###################################################
sub GetModifier($) {
	my $default = shift;
	my $param;
	
	if ( ! defined $ARGV[0] ) {    ## Is it defined ??
        $param = $default;
    } else {                       ## Is it a modifier or a new parameter ??
        $param = ( ($ARGV[0] =~ /^--/ ) ? $default : shift @ARGV);
    };
	
	return $param;
};

#############  PrintSeparator  ################################################
sub PrintSeparator(){
    print "===================================== >>>>> " .
          $SETIhost . " <<<<<\n";
}

#############  PrintHelp  #####################################################
sub PrintHelp(){
	print <<EOT
    
    --host:       Hostname or IP address to work on
        default:  localhost
    
    --help:       This help screen         
    --progress:   Progress statistics
    
    --start:      Starts SETI\@Home
        options: new parameters
        default: '$SETIArgs'

    --stop:       Stops SETI\@Home
    --stopaftersend:  Stop processing after sending the results
        options: [start|stop|query]
        default: query
    
    --info:       Shows user information
EOT
}	
	
#############  PrintHeader  ###################################################
sub PrintHeader(){
    print <<EOH;
$NAME - $VERSION
$AUTHOR
EOH
}
#############  PrintSETIversion  ##############################################
sub PrintSETIversion(){
    print <<EOH;
SETI\@home version v$SETIversion running in host "$SETIhost"
EOH
}

