#!/usr/bin/perl -wT

package setiathome;

use strict;
use aMail::Sec;

my $VERISON = "0.2.3";
my $SETIHome = '/Users/bitman/Download/setiathome-3.03.powerpc-apple-darwin1.2/';
my $pid_file = $SETIHome . 'pid.sah';
my $state_file = $SETIHome . 'state.sah';
my $info_file = $SETIHome . 'user_info.sah';
my $version_file = $SETIHome . 'version.sah';
my $sas_file = $SETIHome . 'stop_after_send.txt';


my $SETIexec = 'setiathome';
my $SHELL = '/bin/sh';           ## Use shell
my $SHELLArgs = '-c';            ## Execute the commands and exit


#############  StopAfterSend  #################################################
sub StopAfterSend($) {
	my ($class,$action) = @_;

	if ( $action eq 'start' ) {
	    safe_open_create( *FH{IO}, $sas_file );
		return 1;           ## Activated
	} elsif ( $action eq 'stop' ) {
		safe_unlink( $sas_file );
		return 0;           ## Deactivated
	} elsif ( $action eq 'query' ) {
		return (-e $sas_file);
	} else {
		return -1;          ## Unknown parameter 
	}
}

#############  ShowInfo  ######################################################
sub ShowInfo() {
    my @info;
    my %info;
	
	GetFile( $info_file, \@info );
	foreach  (@info) {
        /^(.*)=(.*)/;
        $info{$1}=$2;
    }
    
    return \%info;
}

#############  StartSeti  #####################################################
sub StartSeti($){
	my ($class,$args) = @_;
    
	$ENV{'PATH'} = "";
	system $SHELL, $SHELLArgs, "cd $SETIHome; ./$SETIexec $args \&";
    
    return $? ;
}

#############  StopSeti   #####################################################
sub StopSeti(){
    my @process;
    
	GetFile( $pid_file, \@process );
    foreach my $process ( @process ) {
        chomp $process;
		kill 9, $process    unless  ! GoodPID( \$process );
    }
}

#############  GoodPID  ######################################################
sub GoodPID($){
	my $ref_PID = shift;
	
	return 0   unless ( $$ref_PID =~ /^(\d)+$/ );
    $$ref_PID = $&;
	return 1;
}

#############  Progress  ##################################################
sub Progress(){
    my ($progress, $time);
    my (@cpu, @finish, @file);

    
    GetFile( $state_file, \@file );
	
    foreach (@file) {
        /^prog=(.*)/ && do { $progress = $1;
                             next; };
        /^cpu=(.*)/  && do { @cpu = SecsToHMS('class', $1);
                             $time = $1;
                             next; };
    }
    
	if ( GetSETIversion() < 3 ) {
        @finish = SecsToHMS( 'class', $time * ( 1 - $progress ) / $progress );
	}
    
    return  $progress, \@cpu, \@finish;
}

#############  SecsToHMS  ######################################################
sub SecsToHMS($) {
    my ($class,$secs) = @_;

    my $hours = int ( $secs / 3600 );
    $secs -= $hours * 3600;

    my $min = int ( $secs / 60 );
    $secs -= $min * 60;

    return $hours, $min, $secs; 
}


#############  GetSETIversion #################################################
sub GetSETIversion() {
	my @file;
	my ($major, $minor);

    GetFile( $version_file, \@file );
	
    foreach (@file) {
		/^major_version=(.*)/ && do ( $major = $1 );
		/^minor_version=(.*)/ && do ( $minor = $1 );		
	}
	
	return "$major.$minor";
}

#############  GetFile  #######################################################
sub GetFile($@) {
	my $file = shift;
	my $ref_array = shift;
	
    safe_open_read( *FH{IO}, $file ); 
    @$ref_array= <FH>;
    close FH;
}

1;
