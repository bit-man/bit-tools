#!/usr/bin/perl -wT

use strict;
use aMail::Sec;

my $SetiCMDLine = "./setiathome -nice 19 -email > /dev/null 2> /dev/null &";

# main
##############################################################################
my $VERSION = 'v0.0';
my $NAME = 'SETI@Home command line wrapper';
my $AUTHOR = 'Victor A. Rodriguez (http://www.Bit-Man.com.ar)';

PrintHeader();
ProcessArgs();
PrintSeparator();
exit;

#############  ProcessArgs  ##################################################
sub ProcessArgs(){
	if( $#ARGV == -1 ) {
	    PrintHelp();
		exit;
	};
	
	foreach (@ARGV) {
		/^--progress$/ && do { PrintSeparator();
			                   Progress();
		                     };
		/^--help$/ &&     do { PrintSeparator();
			                   PrintHelp();
		                     };
		/^--stop$/ &&     do { PrintSeparator();
			                   StopSeti();
		                     };
		/^--start$/ &&    do { PrintSeparator();
			                   StartSeti();
		                     };	
	}
};

#############  StartSeti  #####################################################
sub StartSeti(){
	print "Starting SETI\@Home\n";
	$ENV{'PATH'} = "";
    exec( $SetiCMDLine );
}

#############  StopSeti   #####################################################
sub StopSeti(){
    my $pid_file = 'pid.sah';
    my @process;
	
	GetFile( $pid_file, \@process );
	foreach my $process ( @process ) {
		chomp $process;
		print "Killing SETI\@Home with PID $process...\n";
		kill 9, $process    unless  ! GoodPID( \$process );
	};
}

#############  GoodPID  ######################################################
sub GoodPID($){
	my $ref_PID = shift;
	
	return 0   unless ( $$ref_PID =~ /^(\d)+$/ );
    $$ref_PID = $&;
	return 1;
}

#############  PrintSeparator  ################################################
sub PrintSeparator(){
	print <<EOT
===============================================================================
EOT
}

#############  PrintHelp  #####################################################
sub PrintHelp(){
	print <<EOT
    --help:   This help screen         --progress: Progress statistics
    --start:  starts SETI\@Home         --stop:     Stops SETI\@Home
EOT
}	
	
#############  PrintHeader  ###################################################
sub PrintHeader(){
	PrintSeparator();
    print <<EOH;
$NAME - $VERSION
$AUTHOR
EOH
}

#############  Progress  #####################################################
sub Progress(){

    my $file = 'state.sah';
    my @file;
    my $progress;
    my $time;
    my ($h, $m, $s);

    GetFile( $file, \@file );
	
    foreach (@file) {
        /^prog=(.*)/ && do { print "Progress: ", $1 * 100, "\%\n"; 
                             $progress = $1;
                             };
        /^cpu=(.*)/  && do { print "CPU time: ", $1, " secs.\n";
                             $time = $1;
                             };
    }

    ($h, $m, $s) = SecsToHMS( $time * ( 1 - $progress ) / $progress );

    print "Estimated CPU time to finish:\n",
          "      $h hs. $m min. $s sec. \n";
}

#############  SecToHMS  ######################################################
sub SecsToHMS($) {
    my $secs = shift;

    my $hours = int ( $secs / 3600 );
    $secs -= $hours * 3600;

    my $min = int ( $secs / 60 );
    $secs -= $min * 60;

    return $hours, $min, $secs; 
}

#############  GetFile  #######################################################
sub GetFile($@) {
	my $file = shift;
	my $ref_array = shift;
	
    safe_open_read( *FH{IO}, $file ); 
    @$ref_array= <FH>;
    close FH;
}
