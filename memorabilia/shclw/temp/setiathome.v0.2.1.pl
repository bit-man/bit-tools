#!/usr/bin/perl -w

use strict;
use SOAP::Lite;

# main
##############################################################################
sub PrintHeader();
sub ProcessArgs();
sub PrintSeparator();

my $SETIhost = 'localhost';
my $SETIport = 31416;
my $SETIhandler;      ### Web service handler;
my $VERSION = 'v0.2.1';
my $NAME = 'SETI@Home command line wrapper';
my $AUTHOR = 'Victor A. Rodriguez (victor@Bit-Man.com.ar)';
my $SETIversion = "unknown";
my $SETIArgs = '-nice 19 -email > /dev/null 2> /dev/null';

ProcessArgs();
PrintSeparator();
exit;

#############  ConnectToWebService  ##########################################
sub ConnectToWebService($) {
    my $host = shift;
    
    $SETIhandler = SOAP::Lite                                             
        -> uri('setiathome')                                             
        -> proxy('http://'.$host .':'. $SETIport .'/')
        -> on_fault(sub { my($soap, $res) = @_; 
             die ref $res ? $res->faultstring : $soap->transport->status, "\n";
           });
}

#############  ProcessArgs  ##################################################
sub ProcessArgs(){
	my $param;
	
	if( $#ARGV == -1 ) {
		PrintSeparator();
	    PrintHelp();
		PrintSeparator();
		exit;
	};
	
	while ($_ = shift @ARGV) {
		PrintSeparator();
        
		/^--host$/ &&          do { $SETIhost = GetModifier('localhost');
                                    ConnectToWebService( $SETIhost );
                                    $SETIversion = $SETIhandler->GetSETIversion()->result;
                                    PrintHeader();
		                            next; };
        
        ### This hack allows the use of commands without a host definition
        ### (the default is localhost), and makes more usable and
        ### compatible with previous versions
        
        ConnectToWebService( $SETIhost ) if ( ! defined $SETIhandler );
                                    
		/^--progress$/ &&      do { print $SETIhandler->Progress()->result;   
		                            next; };
		/^--help$/ &&          do { PrintHelp();  
		                            next; };
		/^--stop$/ &&          do { print $SETIhandler->StopSeti()->result;  
		                            next; };
		/^--start$/ &&         do { $param = GetModifier( $SETIArgs ); 
			                        print $SETIhandler->StartSeti( $param )->result;  
		                            next; };	
		/^--info$/ &&          do { print $SETIhandler->ShowInfo()->result;  
		                            next; };
		/^--stopaftersend$/ && do { $param = GetModifier( 'query' );
									if ( ! ($param eq 'start' ||
								            $param eq 'stop'  ||
											$param eq 'query' ) ) {
										 print "Wrong parameters for ".
										       "'stop after send'.\n\n";
									     PrintHelp();
										 next;
								     };

		                             print $SETIhandler->StopAfterSend( $param )->result;     
		                             next; };
		print "Unknown command: $_\n\n";
		PrintHelp();
	}
};

#############  GetModifier  ###################################################
sub GetModifier($) {
	my $default = shift;
	my $param;
	
	if ( ! defined $ARGV[0] ) {    ## Is it defined ??
        $param = $default;
    } else {                       ## Is it a modifier or a new parameter ??
        $param = ( ($ARGV[0] =~ /^--/ ) ? $default : shift @ARGV);
    };
	
	return $param;
};

#############  PrintSeparator  ################################################
sub PrintSeparator(){
    print "===================================== >>>>> " .
          $SETIhost . " <<<<<\n";
}

#############  PrintHelp  #####################################################
sub PrintHelp(){
	print <<EOT
    --help:       This help screen         
    --progress:   Progress statistics
    
    --start:      Starts SETI\@Home
        options: new parameters
        default: '$SETIArgs'

    --stop:       Stops SETI\@Home
    --stopaftersend:  Stop processing after sending the results
        options: [start|stop|query]
        default: query
    
    --info:       Shows user information
EOT
}	
	
#############  PrintHeader  ###################################################
sub PrintHeader(){
    print <<EOH;
$NAME - $VERSION
$AUTHOR
-------------------------------------------------------------------------------
SETI\@home version v$SETIversion running in host "$SETIhost"
EOH
}

