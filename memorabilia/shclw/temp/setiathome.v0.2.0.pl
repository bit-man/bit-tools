#!/usr/bin/perl -wT

use strict;
use setiathome;

# main
##############################################################################
sub PrintHeader();
sub ProcessArgs();
sub PrintSeparator();
sub GetSETIversion();

my $VERSION = 'v0.2.0';
my $NAME = 'SETI@Home command line wrapper';
my $AUTHOR = 'Victor A. Rodriguez (victor@Bit-Man.com.ar)';
my $SETIversion = GetSETIversion();

my $SETIArgs = '-nice 19 -email > /dev/null 2> /dev/null';

PrintHeader();
ProcessArgs();
PrintSeparator();
exit;

#############  ProcessArgs  ##################################################
sub ProcessArgs(){
	my $param;
	
	if( $#ARGV == -1 ) {
	    PrintHelp();
		exit;
	};
	
	while ($_ = shift @ARGV) {
		PrintSeparator();
		
		/^--progress$/ &&      do { Progress();   
		                            next; };
		/^--help$/ &&          do { PrintHelp();  
		                            next; };
		/^--stop$/ &&          do { StopSeti();  
		                            next; };
		/^--start$/ &&         do { $param = GetModifier( $SETIArgs ); 
			                        StartSeti( $param );  
		                            next; };	
		/^--info$/ &&          do { ShowInfo();  
		                            next; };
		/^--stopaftersend$/ && do { $param = GetModifier( 'query' );
									if ( ! ($param eq 'start' ||
								            $param eq 'stop'  ||
											$param eq 'query' ) ) {
										 print "Wrong parameters for ".
										       "'stop after send'.\n\n";
									     PrintHelp();
										 next;
								     };

		                             StopAfterSend( $param );     
		                             next; };
		print "Unknown command: $_\n\n";
		PrintHelp();
	}
};

#############  GetModifier  ###################################################
sub GetModifier($) {
	my $default = shift;
	my $param;
	
	if ( ! defined $ARGV[0] ) {    ## Is it defined ??
        $param = $default;
    } else {                       ## Is it a modifier or a new parameter ??
        $param = ( ($ARGV[0] =~ /^--/ ) ? $default : shift @ARGV);
    };
	
	return $param;
};

#############  PrintSeparator  ################################################
sub PrintSeparator(){
	print <<EOT
===============================================================================
EOT
}

#############  PrintHelp  #####################################################
sub PrintHelp(){
	print <<EOT
    --help:       This help screen         
    --progress:   Progress statistics
    --start:      Starts SETI\@Home
        options: new parameters
        default: '$SETIArgs'

    --stop:       Stops SETI\@Home
    --stopaftersend:  Stop processing after sending the results
        options: [start|stop|query]
        default: query
EOT
}	
	
#############  PrintHeader  ###################################################
sub PrintHeader(){
	PrintSeparator();
    print <<EOH;
$NAME - $VERSION
$AUTHOR
-------------------------------------------------------------------------------
SETI\@home version v$SETIversion
EOH
}

