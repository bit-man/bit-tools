#!/usr/bin/perl -wT

package setiathome;

use strict;
use aMail::Sec;

my $VERISON = "0.2.2";
my $SETIHome = '/Users/bitman/Download/setiathome-3.03.powerpc-apple-darwin1.2/';
my $pid_file = $SETIHome . 'pid.sah';
my $state_file = $SETIHome . 'state.sah';
my $info_file = $SETIHome . 'user_info.sah';
my $version_file = $SETIHome . 'version.sah';
my $sas_file = $SETIHome . 'stop_after_send.txt';


my $SETIexec = 'setiathome';
my $SHELL = '/bin/sh';           ## Use shell
my $SHELLArgs = '-c';            ## Execute the commands and exit


sub test() {
    return 'Hi !!!';
}


#############  StopAfterSend  #################################################
sub StopAfterSend($) {
	my ($class,$action) = @_;
	my $output;

	$output = "Signaling 'stop after send'...\n";
	if ( $action eq 'start' ) {
	    safe_open_create( *FH{IO}, $sas_file );
		$output .= "Activated.\n";
	} elsif ( $action eq 'stop' ) {
		safe_unlink( $sas_file );
		$output .= "Deactivated.\n";
	} elsif ( $action eq 'query' ) {
		$output .= "'Stop after send' is " .
                  ( (-e $sas_file) ? "activated" : "deactivated") . "\n";
	} else {
		$output .= "Unknown parameter \'$action\'\n";
	}
	$output .= "Signaling done.\n";
    
    return $output;
}

#############  ShowInfo  ######################################################
sub ShowInfo() {
    my @info;
	my $none = "- none -";
	my ($h, $m, $s);
    my $output;
	
	GetFile( $info_file, \@info );
	foreach  (@info) {
		/^email_addr=(.*)/ && do { $output .= "E-mail: " . ($1 || $none) . "\n";
		                           next; };
		/^name=(.*)/ && do {$output .= "User name: " .($1 || $none)."\n";
			                 next; };
		/^url=(.*)/ && do { $output .= "URL: " .($1 || $none). "\n";
			                next; };
		/^country=(.*)/ && do { $output .= "Country: " .($1 || $none). "\n";
			                    next; };
		/^postal_code=(.*)/ && do { $output .= "Postal code: " .($1 || $none). "\n";
			                        next; };
		/^register_time=.*\((.*)\)/ && do { 
		                          $output .= "Register date: " .($1 || $none). "\n";
								  next; };
		/^last_result_time=.*\((.*)\)/ && do { 
		                          $output .= "Last result date: " .($1 || $none). "\n";
								  next; };
		/^nresults=(.*)/ && do { $output .= "Data units: " .($1 || $none). "\n";
			                     next; };
		/^total_cpu=(.*)/ && do { ($h, $m, $s) = SecsToHMS($1);
		                          $output .= "Total CPU time: ";
		                          $output .= sprintf "%d hs. %d min %.2f sec.\n",$h, $m, $s;
								  next; };
	}
    
    return $output;
}

#############  StartSeti  #####################################################
sub StartSeti($){
	my ($class,$args) = @_;
    my $output;
    
	$output = "Starting SETI\@Home...\n";
	$ENV{'PATH'} = "";
	system $SHELL, $SHELLArgs, "cd $SETIHome; ./$SETIexec $args \&";
    $output .=  "SETI\@Home started !!!\n" unless $? != 0;
    
    return $output;
}

#############  StopSeti   #####################################################
sub StopSeti(){
    my @process;
	my $output;

	GetFile( $pid_file, \@process );
	foreach my $process ( @process ) {
		chomp $process;
		$output = "Killing SETI\@Home with PID $process...\n";
		kill 9, $process    unless  ! GoodPID( \$process );
	};
    
    return $output;
}

#############  GoodPID  ######################################################
sub GoodPID($){
	my $ref_PID = shift;
	
	return 0   unless ( $$ref_PID =~ /^(\d)+$/ );
    $$ref_PID = $&;
	return 1;
}




#############  Progress  #####################################################
sub Progress(){
    my @file;
    my $progress;
    my $time;
    my ($h, $m, $s);
    my $output;
    
    GetFile( $state_file, \@file );
	
    foreach (@file) {
        /^prog=(.*)/ && do { $output .= sprintf "Progress: %.2f%%\n", $1 * 100; 
                             $progress = $1;
                             next; };
        /^cpu=(.*)/  && do { ($h, $m, $s) = SecsToHMS($1);
			                 $output .= "CPU time: ";
					         $output .= sprintf "%d hs. %d min %.2f sec.\n",$h, $m, $s;
                             $time = $1;
                             next; };
    }

    ($h, $m, $s) = SecsToHMS( $time * ( 1 - $progress ) / $progress );

	if ( GetSETIversion() < 3 ) {
        $output .=  "Estimated CPU time to finish:\n";
	    $output .= sprintf "      %d hs. %d min %.2f sec.\n",$h, $m, $s;
	}
    
    return $output;
}

#############  SecToHMS  ######################################################
sub SecsToHMS($) {
    my $secs = shift;

    my $hours = int ( $secs / 3600 );
    $secs -= $hours * 3600;

    my $min = int ( $secs / 60 );
    $secs -= $min * 60;

    return $hours, $min, $secs; 
}


#############  GetSETIversion #################################################
sub GetSETIversion() {
	my @file;
	my ($major, $minor);

    GetFile( $version_file, \@file );
	
    foreach (@file) {
		/^major_version=(.*)/ && do ( $major = $1 );
		/^minor_version=(.*)/ && do ( $minor = $1 );		
	}
	
	return "$major.$minor";
}

#############  GetFile  #######################################################
sub GetFile($@) {
	my $file = shift;
	my $ref_array = shift;
	
    safe_open_read( *FH{IO}, $file ); 
    @$ref_array= <FH>;
    close FH;
}

1;
