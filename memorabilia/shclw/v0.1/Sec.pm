package aMail::Sec;

=pod

=head1 NAME

aMail::Sec - provides security functions (including safe
options for mkdir, open, etc.)

=head1 SYNOPSIS

    use aMail::Sec;

=head1 DESCRIPTION

The first goal for aMail is security, so let's start 
programming safe. The base for this guidelines is the 
perlsec page. Some additions are being made, from different 
sources, while we continue to advance in securing aMail. 

This modules provides the common security subroutines for 
aMail operation (this helps us to achieve the "Security"
goal).

=head2 Are you sure about that data ?

Perl has a very clever way to deal with data from "unchecked" 
sources. Essentially it treat it as "tainted" (dirty) data 
and is tagged as such. The way to laundering it is to pass it 
through a filter, and decide what data (or part of it) will 
be used (sorry, but for a detailed explanation of this method
take a look at perlsec, it's very carefully explained). The 
default is not to check for tainted data, but for aMail it is 
selected to check for it (the -T switch activates the checking). 

In perlsec is specified to use a subroutine to check fot tainted
data, or use Taint.pm (not shipped as a standard distributed 
module) to check for tainted data and to insert tainted data 
(mainly). There's another module (Untaint.pm) that is a 
complement for the first one and it untaints vectors, arrays, and
scalars. 

After a few tests was seen that those modules very not very 
suitable for aMail (despite its clear and well done code), 
because specifics aMail desired internal usage and it should be 
installed as extra modules (a little burden added to the 
installation process). 

=head2 What to filter and how

Then the point is to check each piece of data to be used, 
mainly against metacharacters that could subvert the operating
system. So each data from outside the program, or data 
generated inside aMail but using external data, must be checked
for correctness. Now the subject is to define how each data
should look like, then accepting the one that match this p
attern and reject otherwise. 

In this implementation each subroutine checks for proper
conditions to be satisfied. If these don't conform to it then
there's no safe environment for them to run, then it dies. 
This mainly helps the subverted use of the system and, in a 
lateral way, keeps the code cleaner and simple (if the 
subroutine returns is because its execution was sucessful, 
dies otherwise).

The subrotines that enhance Perl functions (such as
open, unlink, etc.) mimic the syntax of its original 
counterparts. If the extra customization needs to be used
for these functions then some extra parameters can be
passed as an anonymous hash (take a look at the examples
for further information).

    ##########################################################
    ###
    ###  NOTICE for all SAFE operations
    ###  
    ###  Basically the checks made are on:
    ###  - complete filename 
    ###  - file existence or inexistence (depending on read, 
    ###    write, creation, etc.)
    ###  - permissions checking for the intended operation
    ###
    ##########################################################

=cut

use Exporter;
@ISA = ('Exporter');
@EXPORT = qw/check_path check_lang safe_unlink safe_open_read safe_open_write safe_open_update safe_rename safe_opendir safe_open_create safe_mkdir safe_chmod/;

=pod

=head1 METHODS

=head2 Common usage

All the methods in these package share common usage
characteristics, that will be shown here for simplicity,
clarity and space considerations.

All methods inherited a standard use with mandatory parameters
that have no common characteristics, but all the extensions made
share the usage and format: must be passed as an anonymous hash,
are not mandatory and have similar name for each of them.

The first one is 'die_text' which let us specify the text added
to the standard die text when the function fails to perform (the
original die text is also added an can't be avoided).

The second one is 'die_sub' which let us specify a reference to
a subroutine that will be performed instead of die; after the 
execution of this routine the script will end (instead of die).
This was implemented because some impossibility to access a file,
or inability to execute some statement doesn't means a violation,
simply a coding error or some in the style, and dieing is not the
best option (a graceful ending is better suited).

Usage:

    use aMail::Sec;

    $file = "/home/bit-man/.profile";

    safe_open_read( *FH{IO}, \$file,
                    {'die_sub' => \&bye }
                  );

    sub bye() {
        print "I didn't want to read it, anyway\n";
    }

NOTICE: If 'die_sub' is specified the 'die_text' parameter
is not used (either dies with the 'die_text' or executes 'die_sub')

=head2 check_path($%)

Checks a filename or a path against proper format.
It expects reference to a scalar, which contains a path to a 
file (the filename also can be included). It only checks for 
the correct structure of a path and filename, it doesn't check 
for its existence, access permissions or whatever extra 
conditions could be checked. 

Usage:

    use aMail::Sec;

    $file = "/home/bit-man/.profile";

    check_path( \$file );        ## Standard use
    check_path( \$file,          ## Enhanced use
            {'die_text' => "This file should pass"} ); ## Die text to be used

=cut

sub check_path($%) {
    my $ref = shift;
    my $extra = shift;


    ### Legal characters are :
    ###  \ / alpanumeric _ - : .
    ### As seen are not permited chars as redirections and pipes

    DieHard( $$extra{'die_text'} . " --- Bad data in $$ref", $$extra{'die_sub'} )
                 unless ( $$ref =~ /^([\\\/\w\-\:\.]+)$/)  ;

    $$ref = $&;    ## SEC: Cleans the data (clears the 'taint' bit)

	## Checks for double dot (..)
    DieHard( $$extra{'die_text'} . " --- Bad data in $$ref", $$extra{'die_sub'} )
                  if ( $$ref =~ /\.\./)  ;

}

=pod

=head2 check_lang($%)
       
Checks for proper formaed language name.
It expects a reference to the scalar containing the language name
to be checked.
    
Usage:

    use aMail::Sec;

    $lang = "es";

    check_lang( \$lang );        ## Standard use
    check_lang( \$lang,          ## Enhanced use
               {'die_text' => "This language is admitted"} ); ## Die text to be used

=cut

sub check_lang($%) {
    my $ref = shift;
    my $extra = shift;

    DieHard( $$extra{'die_text'} . " --- Illegal characters in language ($$ref)",
             $$extra{'die_sub'}  )
                    unless ( $$ref =~ /([a-z]|[A-Z]|\-)+/ );
    $$ref = $&;  ## SEC: Cleans the data (clears the 'taint' bit)
}


=pod

=head2 safe_unlink($%)

Unlinks (deletes) a file.
Expects a scalar containing the path for the file to be 
deleted.

Usage:

    use aMail::Sec;

    $file = "/home/bit-man/.profile";

    safe_unlink( $file );        ## Standard use
    safe_unlink( $file,          ## Enhanced use
            {'die_text' => "This file should be deleted"} ); ## Die text to be used

=cut

sub safe_unlink($%) {
    my $file = shift;
    my $extra = shift;
        
    check_path( \$file,       # SEC: Is a well established filename
                { 'die_text' => $$extra{'die_text'} } );
                
    if ( -f $file && -w $file ) {  # SEC: Is a file and writable by the actual uid ? 
        unlink( $file ) ||  
               DieHard( $$extra{'die_text'} . " --- Unable to delete $file",
                        $$extra{'die_sub'}  ); 
    } else {
        DieHard( $$extra{'die_text'} . " --- Unable to delete $file",
                 $$extra{'die_sub'}  );
    }
}

=pod

=head2 safe_open_read($$%)

Opens a file for reading.
Expects a reference to the file handler and a scalar containing 
the path for the file to be opened.

Usage:

    use aMail::Sec;

    $file = "/home/bit-man/.profile";

    safe_open_read( *FH{IO}, $file );        ## Standard use
    safe_open_read( *FH{IO}, $file,          ## Enhanced use
                {'die_text' => "This file should be read"} ); ## Die text to be used

=cut

sub safe_open_read($$%) {
    my $desc = shift;
    my $file = 