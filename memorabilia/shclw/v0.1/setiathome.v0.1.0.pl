#!/usr/bin/perl -wT

use strict;
use aMail::Sec;

my $SETIHome = '/Users/bitman/Download/setiathome-3.03.powerpc-apple-darwin1.2/';
my $SetiCMDLine = $SETIHome . 'setiathome -nice 19 -email > /dev/null 2> /dev/null &';
my $pid_file = $SETIHome . 'pid.sah';
my $state_file = $SETIHome . 'state.sah';
my $info_file = $SETIHome . 'user_info.sah';
my $version_file = $SETIHome . 'version.sah';
	
# main
##############################################################################
sub PrintHeader();
sub ProcessArgs();
sub PrintSeparator();
sub GetSETIversion();

my $VERSION = 'v0.1.0';
my $NAME = 'SETI@Home command line wrapper';
my $AUTHOR = 'Victor A. Rodriguez (victor@Bit-Man.com.ar)';
my $SETIversion = GetSETIversion();

PrintHeader();
ProcessArgs();
PrintSeparator();
exit;

#############  ProcessArgs  ##################################################
sub ProcessArgs(){
	if( $#ARGV == -1 ) {
	    PrintHelp();
		exit;
	};
	
	foreach (@ARGV) {
		PrintSeparator();
		
		/^--progress$/ && do { Progress();   };
		/^--help$/ &&     do { PrintHelp();  };
		/^--stop$/ &&     do { StopSeti();   };
		/^--start$/ &&    do { StartSeti();  };	
		/^--info$/ &&     do { ShowInfo();   };
	}
};

#############  ShowInfo  ######################################################
sub ShowInfo() {
    my @info;
	my $none = "- none -";
	my ($h, $m, $s);
	
	GetFile( $info_file, \@info );
	foreach  (@info) {
		/^email_addr=(.*)/ && do { print "E-mail: ", $1 || $none, "\n";
		                           next; };
		/^name=(.*)/ && do { print "User name: ", $1 || $none, "\n";
			                 next; };
		/^url=(.*)/ && do { print "URL: ", $1 || $none, "\n";
			                next; };
		/^country=(.*)/ && do { print "Country: ", $1 || $none, "\n";
			                    next; };
		/^postal_code=(.*)/ && do { print "Postal code: ", $1 || $none, "\n";
			                        next; };
		/^register_time=.*\((.*)\)/ && do { 
		                          print "Register date: ", $1 || $none, "\n";
								  next; };
		/^last_result_time=.*\((.*)\)/ && do { 
		                          print "Last result date: ", $1 || $none, "\n";
								  next; };
		/^nresults=(.*)/ && do { print "Data units: ", $1 || $none, "\n";
			                     next; };
		/^total_cpu=(.*)/ && do { ($h, $m, $s) = SecsToHMS($1);
		                          print "Total CPU time: ";
		                          printf "%d hs. %d min %.2f sec.\n",$h, $m, $s;
								  next; };
	}
}

#############  StartSeti  #####################################################
sub StartSeti(){
	print "Starting SETI\@Home\n";
	$ENV{'PATH'} = "";
    exec( $SetiCMDLine );
}

#############  StopSeti   #####################################################
sub StopSeti(){
    my @process;
	
	GetFile( $pid_file, \@process );
	foreach my $process ( @process ) {
		chomp $process;
		print "Killing SETI\@Home with PID $process...\n";
		kill 9, $process    unless  ! GoodPID( \$process );
	};
}

#############  GoodPID  ######################################################
sub GoodPID($){
	my $ref_PID = shift;
	
	return 0   unless ( $$ref_PID =~ /^(\d)+$/ );
    $$ref_PID = $&;
	return 1;
}

#############  PrintSeparator  ################################################
sub PrintSeparator(){
	print <<EOT
===============================================================================
EOT
}

#############  PrintHelp  #####################################################
sub PrintHelp(){
	print <<EOT
    --help:   This help screen         --progress: Progress statistics
    --start:  starts SETI\@Home         --stop:     Stops SETI\@Home
EOT
}	
	
#############  PrintHeader  ###################################################
sub PrintHeader(){
	PrintSeparator();
    print <<EOH;
$NAME - $VERSION
$AUTHOR
-------------------------------------------------------------------------------
SETI\@home version v$SETIversion
EOH
}

#############  Progress  #####################################################
sub Progress(){
    my @file;
    my $progress;
    my $time;
    my ($h, $m, $s);

    GetFile( $state_file, \@file );
	
    foreach (@file) {
        /^prog=(.*)/ && do { print "Progress: ", $1 * 100, "\%\n"; 
                             $progress = $1;
                             next; };
        /^cpu=(.*)/  && do { ($h, $m, $s) = SecsToHMS($1);
			                 print "CPU time: ";
					         printf "%d hs. %d min %.2f sec.\n",$h, $m, $s;
                             $time = $1;
                             next; };
    }

    ($h, $m, $s) = SecsToHMS( $time * ( 1 - $progress ) / $progress );

    print  "Estimated CPU time to finish:\n";
	printf "      %d hs. %d min %.2f sec.\n",$h, $m, $s;
}

#############  SecToHMS  ######################################################
sub SecsToHMS($) {
    my $secs = shift;

    my $hours = int ( $secs / 3600 );
    $secs -= $hours * 3600;

    my $min = int ( $secs / 60 );
    $secs -= $min * 60;

    return $hours, $min, $secs; 
}


#############  GetSETIversion #################################################
sub GetSETIversion() {
	my @file;
	my ($major, $minor);

    GetFile( $version_file, \@file );
	
    foreach (@file) {
		/^major_version=(.*)/ && do ( $major = $1 );
		/^minor_version=(.*)/ && do ( $minor = $1 );		
	}
	
	return "$major.$minor";
}

#############  GetFile  #######################################################
sub GetFile($@) {
	my $file = shift;
	my $ref_array = shift;
	
    safe_open_read( *FH{IO}, $file ); 
    @$ref_array= <FH>;
    close FH;
}
