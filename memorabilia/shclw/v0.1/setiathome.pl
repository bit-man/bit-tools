#!/usr/bin/perl -wT

use strict;
use aMail::Sec;

my $SETIHome = '/Users/bitman/Download/setiathome-3.03.powerpc-apple-darwin1.2/';
my $SETIArgs = '-nice 19 -email > /dev/null 2> /dev/null';
my $pid_file = $SETIHome . 'pid.sah';
my $state_file = $SETIHome . 'state.sah';
my $info_file = $SETIHome . 'user_info.sah';
my $version_file = $SETIHome . 'version.sah';
my $sas_file = $SETIHome . 'stop_after_send.txt';

# main
##############################################################################
sub PrintHeader();
sub ProcessArgs();
sub PrintSeparator();
sub GetSETIversion();

my $VERSION = 'v0.1.2';
my $NAME = 'SETI@Home command line wrapper';
my $AUTHOR = 'Victor A. Rodriguez (victor@Bit-Man.com.ar)';
my $SETIversion = GetSETIversion();

my $SETIexec = 'setiathome';
my $SHELL = '/bin/sh';           ## Use shell
my $SHELLArgs = '-c';            ## Execute the commands and exit

PrintHeader();
ProcessArgs();
PrintSeparator();
exit;

#############  ProcessArgs  ##################################################
sub ProcessArgs(){
	my $param;
	
	if( $#ARGV == -1 ) {
	    PrintHelp();
		exit;
	};
	
	while ($_ = shift @ARGV) {
		PrintSeparator();
		
		/^--progress$/ &&      do { Progress();   
		                            next; };
		/^--help$/ &&          do { PrintHelp();  
		                            next; };
		/^--stop$/ &&          do { StopSeti();  
		                            next; };
		/^--start$/ &&         do { $param = GetModifier( $SETIArgs ); 
			                        StartSeti( $param );  
		                            next; };	
		/^--info$/ &&          do { ShowInfo();  
		                            next; };
		/^--stopaftersend$/ && do { $param = GetModifier( 'query' );
									if ( ! ($param eq 'start' ||
								            $param eq 'stop'  ||
											$param eq 'query' ) ) {
										 print "Wrong parameters for ".
										       "'stop after send'.\n\n";
									     PrintHelp();
										 next;
								     };

		                             StopAfterSend( $param );     
		                             next; };
		print "Unknown command: $_\n\n";
		PrintHelp();
	}
};

#############  GetModifier  ###################################################
sub GetModifier($) {
	my $default = shift;
	my $param;
	
	if ( ! defined $ARGV[0] ) {    ## Is it defined ??
        $param = $default;
    } else {                       ## Is it a modifier or a new parameter ??
        $param = ( ($ARGV[0] =~ /^--/ ) ? $default : shift @ARGV);
    };
	
	return $param;
};

#############  StopAfterSend  #################################################
sub StopAfterSend($) {
	my $action = shift;
	
	print "Signaling 'stop after send'...\n";
	if ( $action eq 'start' ) {
	    safe_open_create( *FH{IO}, $sas_file );
		print "Activated.\n";
	} elsif ( $action eq 'stop' ) {
		safe_unlink( $sas_file );
		print "Deactivated.\n";
	} elsif ( $action eq 'query' ) {
		print "'Stop after send' is ";
		print ( (-e $sas_file) ? "activated" : "deactivated");
		print "\n";
	} else {
		print "Unknown parameter \'$action\'\n";
	}
	print "Signaling done.\n";
}

#############  ShowInfo  ######################################################
sub ShowInfo() {
    my @info;
	my $none = "- none -";
	my ($h, $m, $s);
	
	GetFile( $info_file, \@info );
	foreach  (@info) {
		/^email_addr=(.*)/ && do { print "E-mail: ", $1 || $none, "\n";
		                           next; };
		/^name=(.*)/ && do { print "User name: ", $1 || $none, "\n";
			                 next; };
		/^url=(.*)/ && do { print "URL: ", $1 || $none, "\n";
			                next; };
		/^country=(.*)/ && do { print "Country: ", $1 || $none, "\n";
			                    next; };
		/^postal_code=(.*)/ && do { print "Postal code: ", $1 || $none, "\n";
			                        next; };
		/^register_time=.*\((.*)\)/ && do { 
		                          print "Register date: ", $1 || $none, "\n";
								  next; };
		/^last_result_time=.*\((.*)\)/ && do { 
		                          print "Last result date: ", $1 || $none, "\n";
								  next; };
		/^nresults=(.*)/ && do { print "Data units: ", $1 || $none, "\n";
			                     next; };
		/^total_cpu=(.*)/ && do { ($h, $m, $s) = SecsToHMS($1);
		                          print "Total CPU time: ";
		                          printf "%d hs. %d min %.2f sec.\n",$h, $m, $s;
								  next; };
	}
}

#############  StartSeti  #####################################################
sub StartSeti($){
	my $args = shift;
	print "Starting SETI\@Home...\n";
	$ENV{'PATH'} = "";
	system $SHELL, $SHELLArgs, "cd $SETIHome; ./$SETIexec $args \&";
    print "SETI\@Home started !!!\n" unless $? != 0;
}

#############  StopSeti   #####################################################
sub StopSeti(){
    my @process;
	
	GetFile( $pid_file, \@process );
	foreach my $process ( @process ) {
		chomp $process;
		print "Killing SETI\@Home with PID $process...\n";
		kill 9, $process    unless  ! GoodPID( \$process );
	};
}

#############  GoodPID  ######################################################
sub GoodPID($){
	my $ref_PID = shift;
	
	return 0   unless ( $$ref_PID =~ /^(\d)+$/ );
    $$ref_PID = $&;
	return 1;
}

#############  PrintSeparator  ################################################
sub PrintSeparator(){
	print <<EOT
===============================================================================
EOT
}

#############  PrintHelp  #####################################################
sub PrintHelp(){
	print <<EOT
    --help:       This help screen         
    --progress:   Progress statistics
    --start:      Starts SETI\@Home
        options: new parameters
        default: '$SETIArgs'

    --stop:       Stops SETI\@Home
    --stopaftersend:  Stop processing after sending the results
        options: [start|stop|query]
        default: query
EOT
}	
	
#############  PrintHeader  ###################################################
sub PrintHeader(){
	PrintSeparator();
    print <<EOH;
$NAME - $VERSION
$AUTHOR
-------------------------------------------------------------------------------
SETI\@home version v$SETIversion
EOH
}

#############  Progress  #####################################################
sub Progress(){
    my @file;
    my $progress;
    my $time;
    my ($h, $m, $s);

    GetFile( $state_file, \@file );
	
    foreach (@file) {
        /^prog=(.*)/ && do { printf "Progress: %.2f%%\n", $1 * 100; 
                             $progress = $1;
                             next; };
        /^cpu=(.*)/  && do { ($h, $m, $s) = SecsToHMS($1);
			                 print "CPU time: ";
					         printf "%d hs. %d min %.2f sec.\n",$h, $m, $s;
                             $time = $1;
                             next; };
    }

    ($h, $m, $s) = SecsToHMS( $time * ( 1 - $progress ) / $progress );

	if ( $SETIversion < 3 ) {
        print  "Estimated CPU time to finish:\n";
	    printf "      %d hs. %d min %.2f sec.\n",$h, $m, $s;
	}
}

#############  SecToHMS  ######################################################
sub SecsToHMS($) {
    my $secs = shift;

    my $hours = int ( $secs / 3600 );
    $secs -= $hours * 3600;

    my $min = int ( $secs / 60 );
    $secs -= $min * 60;

    return $hours, $min, $secs; 
}


#############  GetSETIversion #################################################
sub GetSETIversion() {
	my @file;
	my ($major, $minor);

    GetFile( $version_file, \@file );
	
    foreach (@file) {
		/^major_version=(.*)/ && do ( $major = $1 );
		/^minor_version=(.*)/ && do ( $minor = $1 );		
	}
	
	return "$major.$minor";
}

#############  GetFile  #######################################################
sub GetFile($@) {
	my $file = shift;
	my $ref_array = shift;
	
    safe_open_read( *FH{IO}, $file ); 
    @$ref_array= <FH>;
    close FH;
}
