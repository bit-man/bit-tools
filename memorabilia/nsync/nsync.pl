#!/usr/bin/perl

use strict;
use File::NCopy qw(copy);
use File::Remove qw(remove);
use Archive::Tar; ## uses IO::Zlib for compressed tar.
use LWP;
use HTTP::Request;
use HTTP::Headers;
use HTTP::Response;
use HTTP::Date;
use Cwd;
use Getopt::Long;
use XML::Parser;
use Tie::File;
use Time::Local;
use Log::Log4perl;
use Fcntl 'SEEK_SET';
use IO::File;

my $VERSION = "0.beta.2";

#### Default Parameter Values

my $workAreaDir = '/nonExistent/home/bit-man/Bit-Software/nsync/workArea';
my $trashDir = '/nonExistent/home/bit-man/Bit-Software/nsync/trash';
my $machineName = 'nsync_default_machine_name';
my $configFileName = 'nsync.xml';
my $repositoryURL = 'http://my.site.com.ar/repository';
my $repositoryMTU = 1024 * 1024;         ## Repository Maximum Transfer Units (in bytes)
my $repositoryTries = 3;
my $repositoryUser = 'nsyncUser';
my $repositoryPassword = 'nsyncPassword';
my $userAgent = "nsync/v" . $VERSION;    ## Browser identification
my $ctrlChannelName = '#ctrlChannel';   ## Control channel name (nsync use channel);
my $gpgPass = 'gnupg_password';
my $secMgrName = "gnupg";                   ## Secrecy manager name

## Data Definition area

my ($inSync, $outSync);    ## Is it an incoming (update) 
                           ## or outgoing (commit) sync ?
my $fullSync;            ## A full sync perhaps ??
my $dirList;             ## channel name conversion table (Hash reference)

my $nsyncConfig;        ## nsync client config (hash reference)

my $nsyncPacket;         ## nsync packet files (hash reference)


my $secMgr;              ## Secrecy manager routines
   $secMgr->{'none'}   =  \&secMgrNone;
   $secMgr->{'gnupg'}  =  \&secMgrGnuPG;

my $lastSyncEpoch = 0;   ## Last sync time (epoch)
my $lastSync;            ## Last sync time
my $prevSync;            ## Previous sync time
my @fileToSync;          ## Files to sync (absolute path)
my @fileToSyncWA;        ## Files to sync (path relative to $workAreaDir)
my @fileList;            ## All files listing (absolute path)
my @fileListWA;          ## All files listing (path relative to $workAreaDir)
my $inRepository = 0;    ## Flag indicating if has been added to the repository       
my $configNsync = 0;
my $configMachine = 0;
my $dieError;
my $inDir = 0;           ## Inside XML dir entity
my $inRepository = 0;    ## Inside XML repository entity
my $inLog = 0;           ## Inside XML logging entity
my $in;                  ## Inside XML entity (has ref)
my $inXMLnsync = 0;      ## Inside XML nsync entity
my $inMachine = 0;       ## Inside XML machine entity
my $inSec = 0;           ## Inside XML secrecy entity
my $inRecip = 0;         ## Inside XML recipients entity
my $inSet = 0;           ## Inside XML settings entity
my $syncFileName;
my $logger;              ## Log4perl handler
my $logConfig;           ## Log4perl config file contents
my $fileContentFH;       ## FH for sub getFileContent
my %tried;               ## canUse() memoization cache
my $configServer;     ## Server config file structure (hash reference)
## main section (abstract level)

about();

getParameters();
showParameters();

$logger->logdie( usage($logger) ) unless  ( $machineName 
                 && ((! $inSync && $outSync) || ($inSync && ! $outSync)));
init();
commit()   if $outSync;
update()   if $inSync;


###########################################################
###################    Level 0     ########################
################   High level coding   ####################
###########################################################

sub showParameters() {
    $logger->info( "Machine name: '$machineName'\n" .
          "      Work area : '$workAreaDir'\n" .
          "          Trash : '$trashDir'\n" .
          "      Last Sync : '$lastSync'\n" .
          "      Last Sync : '$lastSyncEpoch' (epoch)\n" .
          "Secrecy manager : '$secMgrName'\n\n" .
	      "Repository:  URL : '$repositoryURL'\n" .
          "             MTU : '$repositoryMTU'\n" .
          "           tries : '$repositoryTries'\n" .
	      "             user: '$repositoryUser'\n\n" .
          "Directories to sync :\n" );

    foreach ( keys %{ $dirList } ) {
        $logger->info( "    '$_' : '$dirList->{$_}'\n");
    };

    $logger->info( "-" x 60 . "\n" );
};

sub getParameters() {
    
    ## no_pass_through: stops command processing if unknown options are present
    Getopt::Long::Configure( qw(no_pass_through) ); 
    GetOptions ("config=s"   => \$configFileName,
                "in"         => \$inSync,
	            "out"        => \$outSync,
                "full"       => \$fullSync,
                "gpg-pass=s" => \$gpgPass) || do { usage();
                                                   die "Unknown config options"; };

    my $parseXML =  new XML::Parser( Handlers => { Start => \&configTagOpen,
                                                   End => \&configTagClose } );

    my $tree = $parseXML->parsefile( $configFileName );
    
    Log::Log4perl->init( \$logConfig );
    $logger = Log::Log4perl->get_logger('nsync') || die "Can't get logger 'nsync' !\n\n";;
    
    $logger->info("Starting nsync v$VERSION\n");
};

sub init() {
    $dirList->{ $ctrlChannelName } = "$workAreaDir/$ctrlChannelName";  ## Channel for control data
    
    $nsyncPacket->{'fileName'}->{'XML'} = 'nsyncPacket.xml';
    $nsyncPacket->{'fileName'}->{'fileList'} = 'fileList';
    $nsyncPacket->{'XML'} = $dirList->{ $ctrlChannelName }  . "/" . $nsyncPacket->{'fileName'}->{'XML'};
    $nsyncPacket->{'fileList'} = $dirList->{ $ctrlChannelName }  . "/" . $nsyncPacket->{'fileName'}->{'fileList'};
   
    initDirAreas();
};

sub initDirAreas() {
    eraseWorkArea();   ### just in case
    
    ## Create work area
    mkdir $workAreaDir    if ( ! -d $workAreaDir );
    $logger->logdie("Can't make work area ($workAreaDir) \nOS Error: $!")
        unless ( -d $workAreaDir );

    ## Create control files folder
    my $ctrlFilesPath = $dirList->{ $ctrlChannelName };
    mkdir $ctrlFilesPath    if ( ! -d $ctrlFilesPath );
    $logger->logdie("Can't make control channel ($ctrlFilesPath) \nOS Error: $!")
        unless ( -d $ctrlFilesPath );

    ## Create trash can
    mkdir $trashDir    if ( ! -d $trashDir );
    $logger->logdie("Can't make trash can ($trashDir) \nOS Error: $!")
        unless ( -d $trashDir );
};

sub usage($) {
    my $msg = "nsync.pl  [--config=new_config.xml] [--in | --out] [--full] \n"
            . "          [--gpg-pass=password]\n"
            . "\n"
        	. "            config : path to alternative config file (default: $configFileName)\n"
	        . "            in : in sync (update)\n"
	        . "            out : out sync (commit)\n"
            . "            full : full sync (commit or update)\n"
            . "            gpg-pass : password to access GnuPG secret key\n";
    
    if ( @_ ) { 
        $_[0]->info( $msg );
    } else {
        print $msg;
    }
}

##
## Commits the changes made in this machine to the repository
##

sub commit() {
    $logger->info("You want to perform an outSync\n" );
    $logger->info("... and it is a full one !!!\n" )      if $fullSync;
    adjustCommitParams();
    my $file = getFileList();
    $logger->info("Obtaining config from repository ...\n");
    $logger->logdie( "Can't get config from repository !!!!\n Error: $dieError\n" )
        if ( ! getRepositoryConfigFile() );
    lookForMyselfInConfig();
    $logger->info("Copying files to nsync to the work area ...\n");
    my $fileWA = copyFilesToWorkArea( $file );
    
    ### Add "File list" and "XML" file generation here
    
    $logger->info("Building nsync packet ('".@$fileWA ."' file(s) changed from last nsync) ...\n");
    my $syncFileName = makeSyncPacket( $fileWA );
    $logger->info("Storing it into repository ...\n");
    storeIntoExchangeArea( $syncFileName );
    $logger->info("Cleaning work area\n");
    eraseWorkArea();
    $logger->info( "outSync performed !!!\n" );
}

##
## Update this machine with changes made in other ones
##

sub update() {
    $logger->info("You want to perform an inSync\n" );
    $logger->info("... and it is a full one !!!\n" )      if $fullSync;

    $logger->info("Obtaining config from 