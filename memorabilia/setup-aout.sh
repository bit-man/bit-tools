#!/bin/sh

e=/emul/a.out
b="ftp://ftp.openbsd.org/pub/OpenBSD/3.3/`uname -m`/base33.tgz"
d="."
f="base33.tgz comp33.tgz xbase33.tgz"
t=/tmp/aout


usage() {
        echo "usage: $0 -t | -f [-x [/my/path/to/base33.tgz] ]"
        echo "  -f = setup the a.out emulation"
        echo "  -x = and use tarballs in this folder (default='$f')"
        echo "  -t = calculate how much space the emulation requires"
        echo "       (valid only if you are running OpenBSD 3.0 trough 3.3)
}

set -e

if [ "x$1" = x-t ]; then
        size="$(du -sch /sbin/ldconfig /usr/bin/ldd /usr/libexec/ld.so \
                /usr/lib/lib* /usr/local/lib/lib* \
                /usr/X11R6/lib/lib* 2>/dev/null | tail -1 | \
                awk '{print $1}')"
        echo "You will need $size for setting up a.out emulation"
        exit 0
fi

if [ "x$1" != "x-f" ]; then
        usage
        exit 0
fi


if [ "x$2" = "x-x" ]; then
   if [ "x$3" != "x" ]; then
       ## OK, yo gave me your *.tgz file location
       d="$3"
   fi
   ## Let's test if the directory exists
   if [ ! -d $d ]; then
       echo "The directory '$d' doesn't exists"
       exit 0
   fi
   echo "Creating the temporary directory '$t'"
   if [ ! -d $t ]; then
       if ! mkdir $t 2>/dev/null; then
           echo "Problems creating the temporary directory '$t'"
           echo "Please make sure you have the rights to do so"
           exit 0
       fi
   fi
   for file in $f; do
       echo "Checking for file '$d/$file' ..."
       if [ ! -e "$d/$file" ]; then
           echo "FTPing from '$b'"
           ftp -o "$d/$file" $b
           if [ ! -f $f ]; then
               echo "The file '$f' STILL doesn't exists"
               echo "Perhaps the FTP transfer was aborted or an error ocurred :-("
               exit 0
           fi
       fi
       echo "Extracting '$file' to '$t' ..."
       if [ `tar -xvzf "$d/$file" -C $t ./sbin/ldconfig ./usr/bin/ldd ./usr/libexec/ld.so \
                                        ./usr/lib/lib* ./usr/local/lib/lib* ./usr/X11R6/lib/lib* \
                                        2>/dev/null` 2>/dev/null  ]; then
           echo "Problems ocurred extracting files from '$f' into '$t'"
           exit 0
       fi
   done
else
   ## There's no extract (-x) option, so this is a not yet upgraded OpenBSD
   f=""
   t=""
fi

if [ -d $e ]; then
        echo "$e already exists - is a.out emulation already setup?"
        exit 1
fi

if ! mkdir $e 2>/dev/null; then
        echo "Problems creating /emul/a.out - please create /emul and ensure there"
        echo "is enough space to hold the libraries"
        echo
        echo "If there is not enough space, create /usr/emul (or somewhere else with"
        echo "space) and then symlink it to /emul (ln -s /usr/emul /)"
        exit 1
fi

echo "Copying files..."

mkdir -p $e/var/run $e/usr/lib $e/usr/libexec $e/usr/local/lib
mkdir -p $e/usr/X11R6/lib $e/usr/bin $e/sbin

cp $t/sbin/ldconfig $e/sbin
cp $t/usr/bin/ldd $e/usr/bin
cp $t/usr/libexec/ld.so $e/usr/libexec

cp $t/usr/lib/lib* $e/usr/lib
cp $t/usr/local/lib/lib* $e/usr/local/lib 2>/dev/null || true
cp $t/usr/X11R6/lib/lib* $e/usr/X11R6/lib 2>/dev/null || true

chroot $e /sbin/ldconfig /usr/local/lib

echo "Setting kernel a.out emulation ..." 
if [ -z "$(grep "^kern.emul.aout=1" /etc/sysctl.conf)" ]; then
        echo "kern.emul.aout=1" >> /etc/sysctl.conf
        sysctl -w kern.emul.aout=1
fi

echo "Cleaning up '$t'"
rm -Rf $t

echo "Finished setting up a.out emulation"


