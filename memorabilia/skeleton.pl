#!/usr/bin/perl -T

use warnings;
use strict;
use Getopt::Long; 


### Default settings
my %input;         ## input parameters passed to each sub in $cmdSub, to avoid
                   ## global variables use
my %debug;         ## debug vector
my $debugAll = 0;
my %validDebug = ( 'setDebugFlags' => 1,
                   'run'           => 1 );
my $debugList;

### internal settings and structure
### Non Plus Ultra !!

my $NAME = "Skeleton";
my $VERSION = "0.0";
my $AUTHOR = "Victor A. Rodriguez (Bit-Man)";
my $CONTACT = 'http://www.bit-man.com.ar';
my $SCREENWIDTH = 80;

my $cmdSub = [ \&printInfo, \&printHelp, \&run];
my $cmd = [ 0, 0, 1 ];    ## 0: disabled, 1: enabled
my $errStr;    ## Extended error string

#############  setDebugFlags  ################################################
## sets accordingly the debug vector @debug 
sub setDebugFlags($$) {
    my ($debugList, $debugAll) = @_;
    my @list;
    
    return if ( ! $debugList && ! $debugAll);

    if ($debugAll) { 
        @list = keys %validDebug;
    } else {
        @list = split /,/, $debugList;
    };
    
    foreach my $thisDebug (@list) {
        dieSoft( "Debugging element '$thisDebug' isn't a valid one\n", 1)
            if ( ! $debugAll && ! $validDebug{ $thisDebug } );
        $debug{ $thisDebug } = 1;
    };

};

#############  ProcessArgs  ##################################################
sub ProcessArgs(){
    
    ## no_pass_through: stops command processing if unknown options are present
    Getopt::Long::Configure( qw(no_pass_through) );  
    GetOptions( 'in=s' => \$input{'infile'},
                'out=s' => \$input{'outfile'},
                'debugAll' => \$debugAll,
                'debug=s' => \$debugList,
                'help' => \($cmd->[1]),
                'info' => \($cmd->[0]) )
       || dieSoft("Unknown arguments were passed", 1);
    
    setDebugFlags( $debugList, $debugAll );
    $cmd->[1] = ( ! $input{'infile'} || ! $input{'outfile'} );
    $cmd->[2] = ! $cmd->[0] &&  ! $cmd->[1];

    ## The options are loaded and the command processing can begin ...
    ## in case no commands are needed, just leave $cmdSub and $cmd empty

    my ($i, $cmdExec) = (0, 0);
    foreach (@$cmdSub) {
        if ($cmd->[$i++]) {
		    PrintSeparator2();
		    &$_( \%input );
            $cmdExec++;
        };
    }
    
    if (! $cmdExec) {
        PrintSeparator2();
        printHelp();
    };
};

#############  Born to run !!  ################################################
sub run($) {
    my $env = shift;
    my $debug = $debug{'run'};
    
    print "DEBUG: Entering run()\n" if $debug;
	print "RUNNING !!!\n";

    foreach my $key ( keys %$env ) {
        print "DEBUG: $key => " . $env->{$key} . "\n"
            if $debug;
    }
};

#############  Separators  ####################################################
sub PrintSeparator(){
	print '=' x $SCREENWIDTH . "\n";
}

sub PrintSeparator2(){
	print '-' x $SCREENWIDTH . "\n";
}

#############  printHelp  #####################################################
sub printHelp($){
	print <<EOT
    
    usage:
    $0 [{--debug | --deubgAll}] [--help] [--info]

    --help:       This help screen
    --info:       Author and version info
    --in:         Input file
    --out:        Output file
    
    --debug:      comma separated value of subs to debug
    --debugAll:   debug all subs

EOT
}	
	
#############  PrintHeader  ###################################################
sub PrintHeader(){
	PrintSeparator();
    printInfo();
};

sub printInfo($) {
    print "$NAME - v$VERSION\n" .
          "$AUTHOR\n" .
          "Contact: $CONTACT\n"; 
}

sub dieSoft($$) {
    my $msg = shift;
    my $die = shift;

    my $dieMsg = "error !!!!!\n    $msg\n";
    $dieMsg .= "    $errStr\n"    if $errStr;
    $dieMsg .= "OS Error: $!\n\n"  if $!;
    die "\n\nFatal $dieMsg" if $die;
    print $dieMsg;
}

# main
##############################################################################


PrintHeader();
ProcessArgs();
PrintSeparator2();
exit;

