#!/usr/bin/perl

use strict;
use warnings;

use constant SCREEN_WIDTH => 80;     ## Screen width
use constant SIDE_BLANKS => 2;       ## number of spaces left between title and asterisks

use Net::SSH qw(ssh ssh_cmd sshopen3);
use Test::Harness;
use Error qw(:try);

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst);
my $penDir = $ENV{'PENDRIVE_DIR'};
my $home = '/Users/bitman';
my ($user, $host) = ('root', 'goliath');
my $remote = "$user\@$host";

# subs
################################################################################

sub getLengths($) {
    my $msg = shift;
    
    my $asteriskLen = int( (SCREEN_WIDTH - length ($msg) - SIDE_BLANKS * 2) / 2 );
    my $asteriskLen_R = $asteriskLen;
    $asteriskLen_R++
        if ( ( $asteriskLen * 2 + SIDE_BLANKS * 2 + length ($msg) ) < SCREEN_WIDTH );

    return ($asteriskLen, $asteriskLen_R);
};

################################################################################
sub getBanner($) {
    my $msg = shift;
    
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    $year += 1900;
    $mon++;
    my $dateTime = "$mday/$mon/$year $hour:$min:$sec";

    my ($asteriskLen,$asteriskLen_R) = getLengths( $msg );
    my ($asteriskLen2,$asteriskLen2_R) = getLengths( $dateTime );
    
    return "*" x SCREEN_WIDTH . "\n" .
           "*" x $asteriskLen . " " x SIDE_BLANKS . $msg . " " x SIDE_BLANKS . "*" x $asteriskLen_R . "\n" .
           "*" x $asteriskLen2 . " " x SIDE_BLANKS . $dateTime . " " x SIDE_BLANKS . "*" x $asteriskLen2_R . "\n" .
           "*" x SCREEN_WIDTH . "\n";
};

################################################################################
sub execTest($$$) {
    my ($title, $dir, $test) = @_;
    
    print "\n\n============================ $title ============================\n\n";

    my $cmd = "cd /home/bit-man/Bit-Software/$dir\nperl -MTest::Harness -e \"runtests('$test')\"";
    print "$cmd\n";

    sshopen3($remote, *WRITER, *READER, *ERROR, "$cmd") 
        || do {     print "Can't execute test '$title' : $!";
                    return;      };

    print "\n------------------------ Output -----------------------\n";

    while (<READER>) {
        chomp();
        print "$_\n";
    };

    my @error;
    while (<ERROR>) {
        chomp();
        push @error, $_;
    };

    print "\n-------------------- Error Output --------------------\n"    if @error;
    print "$_\n"    foreach ( @error );
};


################################################################################
sub localTest($$) {
    my ($title, $test) = @_;
    eval {
        print "\n\n============================ $title ============================\n\n";
        runtests($test);
    };
    print "\n\n***** FATAL error testing $test \n***** $@ \n\n" if $@;
};


# main
################################################################################

BEGIN { 
    print "\n\n" . getBanner('Starting Bit-Software maintenance');
};

print "\n\n**********************************************************************\n"
    . "**********************         MAC OS X         **********************\n"
    . "**********************************************************************\n";

my $newDir = "$home/$penDir/Bit-Software/easyWork";
chdir  $newDir ||
    do {  print "\n\n===> FATAL ERROR : Can't change to '$newDir' : $!\n";
          goto LINUX;  };

localTest( 'BAM testing', 'BAM.t' );
localTest( 'BLM testing', 'BLM.t' );

$newDir = "$home/$penDir/Bit-Software/modules";
chdir  $newDir ||
    do {  print "\n\n===> FATAL ERROR : Can't change to '$newDir' : $!\n";
          goto LINUX;  };
          
localTest( 'WWW::Yahoo::Groups::Pending testing','WWW/Yahoo/Groups/Pending.t' );
chdir( $ENV{HOME} ) ||
    do {  print "\n\n===> FATAL ERROR : Can't change to '{$ENV{HOME}}' : $!\n";
          goto LINUX;  };


LINUX:

(system ("scp", "-r", "-q", "$home/$penDir/Bit-Software/", "$remote:/home/bit-man/" ) == 0)
    || die("Can't copy $penDir/Bit-Software to $remote:/home/bit-man/Bit-Software: $!");

print "\n\n**********************************************************************\n"
    . "**********************           Linux          **********************\n"
    . "**********************************************************************\n";


execTest( 'BAM testing', 'easyWork', 'BAM.t');
execTest( 'BLM testing', 'easyWork', 'BLM.t');
execTest( 'WWW::Yahoo::Groups::Pending testing', 'modules', 'WWW/Yahoo/Groups/Pending.t');
print "\n\n" . getBanner("Execution FINISHED !!!!");


