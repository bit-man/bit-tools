#!/usr/bin/perl

use strict;
use warnings;

my $testFile = shift;

if ( ! $testFile ) {
    print <<USAGE;

    [Missed arguments]

usage:

$0  folder test_file

Todos los tests est�n hecho para ejecutarse desde Bit-Software :-D

USAGE

    exit;
};

##system ('cover', '-delete');

$ENV{'HARNESS_PERL_SWITCHES'} = '-MDevel::Cover';
use Test::Harness;
runtests( $testFile );

system "cover";
exit;

