#!/usr/bin/perl -wT

## 
## Hack script to get the WAN IP address from a Cisco Linksys WRT54G router
## and pass it to Mldonkey
##

use strict;
use Getopt::Long; 
use LWP::UserAgent;
use Tie::File;

### Default settings
my $NAME = "mldonkey IP";
my $VERSION = "0.0";
my $AUTHOR = "Victor A. Rodriguez (Bit-Man)";
my $CONTACT = 'http://www.bit-man.com.ar';
my $SCREENWIDTH = 80;

my $urlIP = 'http://10.1.1.1/Status_Router.asp';   ## WRT54G Status page
my $password = "admin";                            ## WRT54G: Default admin password
my $ini = '/home/mldonkey/downloads.ini';          ## mlDonkey INI path

my $cmdSub = [ \&printInfo, \&printHelp, \&changeIP ];
my $cmd = [ 0, 0, 1 ];    ## 0: disabled, 1: enabled

# main
##############################################################################
sub PrintHeader();
sub ProcessArgs();
sub PrintSeparator2();


PrintHeader();
ProcessArgs();
PrintSeparator2();
exit;


#############  ProcessArgs  ##################################################
sub ProcessArgs(){
    
    ## no_pass_through: stops command processing if unknown options are present
    Getopt::Long::Configure( qw(no_pass_through) );  
    GetOptions( 'pwd=s', \$password,
                'ini=s', \$ini,
                'help' => \($cmd->[1]),
                'info' => \($cmd->[0]) )
       || dieHard( "Unknown commands were passed");
    
    ## The options are loaded and the command processing can begin ...
    ## in case no commands are needed, just leave $cmdSub and $cmd empty

    my ($i, $cmdExec) = (0, 0);
	foreach (@$cmdSub) {
        if ($cmd->[$i++]) {
		    PrintSeparator2();
		    &$_();
            $cmdExec++;
        };
	}
    
    if (! $cmdExec) {
        PrintSeparator2();
        printHelp();
    };
};

#############  changeIP  ################################################
sub changeIP() {
        my $response = getURL();

        if ($response->is_success) {
            my $wanIP = getIP( $response );
            dieHard( "Can't find WAN IP address" )
                if ( ! defined $wanIP );
            writeToDonkey( $wanIP );
        } else {
            dieHard( $response->status_line );
        }
};

sub writeToDonkey($) {
    my $ip = shift;
    
    my @iniLine;
    
    tie @iniLine, 'Tie::File', $ini
        || dieHard( "Can't open file '$ini'");

    ## mlDonkey hack: the line with the line ' client_ip ='
    ##                is the one to change

    for( my $i=0; $i < @iniLine; $i++ ) {
        if ($iniLine[$i] =~ /^ client_ip \= /) {
            $iniLine[$i] = " client_ip = \"$ip\"";
            untie @iniLine;
            return;
        };
    };

    untie @iniLine;
    dieHard( "Can't find 'client_ip' setting in file '$ini'");
};

sub getURL() {
        my $ua = LWP::UserAgent->new;
        my $h = HTTP::Headers->new;

        ## WRT54G: No user is needed, just password
        $h->authorization_basic('', $password);
    
        my $request = HTTP::Request->new( 'GET', $urlIP, $h);
        my $response = $ua->request($request);
        
        return $response;
}

sub getIP($) {
    my $responseObj = shift;

    my @line = split /\n/, $responseObj->content;
    
    ## WRT54G Hack
    ##
    ## the admin URL for status in Cisco Linksys WRT54G shows the WAN IP address
    ## through HTML in s TABLE object, but also it's stored in the wan_ip JavaScript
    ## variable shown as
    ##                        var wan_ip = "24.232.95.56";
    ## Looking at this line tha IP is obtained

    foreach ( @line ) {
        $_ =~ /var wan_ip = \"(.*)\"/;
        return $1  if ( defined $1 );
    };
    
    return undef;
}

#############  Separators  ################################################
sub PrintSeparator(){
	print '=' x $SCREENWIDTH . "\n";
}

sub PrintSeparator2(){
	print '-' x $SCREENWIDTH . "\n";
}

#############  printHelp  #####################################################
sub printHelp(){
	print <<EOT

    --help:       This help screen
    --info:       Author and version info
    
    --pwd=admin_password_for_WRT54G
    --ini=mldonkey_download.ini_path

EOT
}	
	
#############  PrintHeader  ###################################################
sub PrintHeader(){
	PrintSeparator();
    printInfo();
};

sub printInfo() {
    print "$NAME - v$VERSION\n" .
          "$AUTHOR\n" .
          "Contact: $CONTACT\n"; 
}

sub dieHard($) {
    my $msg = shift;

    die "\nFatal error !!!!!\n $msg\n\n";
}
