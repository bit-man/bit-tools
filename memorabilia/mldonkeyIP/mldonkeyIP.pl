#!/usr/bin/perl -wT

## MLdonkey IP
## 
## Hack script to get the WAN IP address from a Cisco Linksys WRT54G router
## and pass it to Mldonkey
##

use strict;
use Getopt::Long; 
use LWP::UserAgent;

### Default settings
my $NAME = "MLdonkey IP";
my $VERSION = "0.1";
my $AUTHOR = "Victor A. Rodriguez (Bit-Man)";
my $CONTACT = 'http://www.bit-man.com.ar';
my $SCREENWIDTH = 80;

my $urlIP = 'http://yoda/Status_Router.asp';   ## WRT54G Status page
my $password = "admin";                        ## WRT54G: Default admin password
my $mldonkeyURL = 'http://localhost:4080/';                      ## MLdonkey URL
my $mldonkeyQRY = 'submit?setoption=q&option=client_ip&value=';  ## MLdonkey paarmeters

my $cmdSub = [ \&printInfo, \&printHelp, \&changeIP ];
my $cmd = [ 0, 0, 1 ];    ## 0: disabled, 1: enabled

# main
##############################################################################
sub PrintHeader();
sub ProcessArgs();
sub PrintSeparator2();


PrintHeader();
ProcessArgs();
PrintSeparator2();
exit;


#############  ProcessArgs  ##################################################
sub ProcessArgs(){
    
    ## no_pass_through: stops command processing if unknown options are present
    Getopt::Long::Configure( qw(no_pass_through) );  
    GetOptions( 'pwd=s', \$password,
                'mlURL=s', \$mldonkeyURL,
                'mlQRY=s', \$mldonkeyQRY,
                'rsURL=s', \$urlIP,
                'help' => \($cmd->[1]),
                'info' => \($cmd->[0]) )
       || dieHard( "Unknown commands were passed");
    
    ## The options are loaded and the command processing can begin ...
    ## in case no commands are needed, just leave $cmdSub and $cmd empty
    
    $cmd->[2] = 0      ## Disble command execution if HELP was requested
        if ( $cmd->[1] );

    my ($i, $cmdExec) = (0, 0);
	foreach (@$cmdSub) {
        if ($cmd->[$i++]) {
		    PrintSeparator2();
		    &$_();
            $cmdExec++;
        };
	}
    
    if (! $cmdExec) {
        PrintSeparator2();
        printHelp();
    };
};

#############  changeIP  ################################################
sub changeIP() {
        my $response = getURL();

        dieHard( $response->status_line . " in getting IP from WRT54G" )
            unless ($response->is_success);

        my $wanIP = getIP( $response );
        writeToDonkey( $wanIP );
        print "MLdonkey client IP changed to '$wanIP' !!!\n";
};

sub writeToDonkey($) {
    my $ip = shift;
    
    print "WAN IP address: '$ip'\n";
    my $ua = LWP::UserAgent->new;
    my $request = HTTP::Request->new( 'POST' => $mldonkeyURL . $mldonkeyQRY . $ip );
    my $response = $ua->request($request);
    dieHard( $response->status_line . "in setting IP to MLdonkey")
        unless ($response->is_success);
};

sub getURL() {
        my $ua = LWP::UserAgent->new;
        my $h = HTTP::Headers->new;

        ## WRT54G: No user is needed, just password
        $h->authorization_basic('', $password);
    
        my $request = HTTP::Request->new( 'GET', $urlIP, $h);
        my $response = $ua->request($request);
        
        return $response;
}

sub getIP($) {
    my $responseObj = shift;

    my @line = split /\n/, $responseObj->content;
    
    ## WRT54G Hack
    ##
    ## the admin URL for status in Cisco Linksys WRT54G shows the WAN IP address
    ## through HTML in s TABLE object, but also it's stored in the wan_ip JavaScript
    ## variable shown as
    ##                        var wan_ip = "24.232.95.56";
    ## Looking at this line tha IP is obtained

    foreach ( @line ) {
        $_ =~ /var wan_ip = \"(.*)\"/;
        return $1  if ( defined $1 );
    };
    
    dieHard( "Can't find WAN IP address" )
}

#############  Separators  ################################################
sub PrintSeparator(){
	print '=' x $SCREENWIDTH . "\n";
}

sub PrintSeparator2(){
	print '-' x $SCREENWIDTH . "\n";
}

#############  printHelp  #####################################################
sub printHelp(){
	print <<EOT

    --help:       This help screen
    --info:       Author and version info
    
    --pwd=admin_password_for_WRT54G (default: '$password')
    --rsURL=WRT54G_status_URL       (default: '$urlIP')
    --mlURL=MLdonkey_URL            (default: '$mldonkeyURL')
    --mlQRY=MLdonkey_QRY            (default: '$mldonkeyQRY')

EOT
}	
	
#############  PrintHeader  ###################################################
sub PrintHeader(){
	PrintSeparator();
    printInfo();
};

sub printInfo() {
    print "$NAME - v$VERSION\n" .
          "$AUTHOR\n" .
          "Contact: $CONTACT\n"; 
}

sub dieHard($) {
    my $msg = shift;

    die "\nFatal error !!!!!\n $msg\n\n";
}
