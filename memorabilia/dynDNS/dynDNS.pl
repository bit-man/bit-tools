#!/usr/bin/perl -wT

## dynamic IP
## 
## Hack script to get the WAN IP address from a Cisco Linksys WRT54G router
## and pass it to resolv.conf
##

use strict;
use Getopt::Long; 
use LWP::UserAgent;
use Tie::File;

### Default settings
my $NAME = "dynamic DNS";
my $VERSION = "0.0";
my $AUTHOR = "Victor A. Rodriguez (Bit-Man)";
my $CONTACT = 'http://www.bit-man.com.ar';
my $SCREENWIDTH = 80;

my $urlIP = 'http://yoda/Status_Router.asp';   ## WRT54G Status page
my $password = "admin";                        ## WRT54G: Default admin password
my $resolvPath = '/etc/resolv.conf';

my $cmdSub = [ \&printInfo, \&printHelp, \&changeIP ];
my $cmd = [ 0, 0, 1 ];    ## 0: disabled, 1: enabled

# main
##############################################################################
sub PrintHeader();
sub ProcessArgs();
sub PrintSeparator2();


PrintHeader();
ProcessArgs();
PrintSeparator2();
exit;


#############  ProcessArgs  ##################################################
sub ProcessArgs(){
    
    ## no_pass_through: stops command processing if unknown options are present
    Getopt::Long::Configure( qw(no_pass_through) );  
    GetOptions( 'pwd=s', \$password,
                'rsURL=s', \$urlIP,
                'resolv=s', \$resolvPath,
                'help' => \($cmd->[1]),
                'info' => \($cmd->[0]) )
       || dieHard( "Unknown commands were passed");
    
    ## The options are loaded and the command processing can begin ...
    ## in case no commands are needed, just leave $cmdSub and $cmd empty
    
    $cmd->[2] = 0      ## Disble command execution if HELP was requested
        if ( $cmd->[1] );

    my ($i, $cmdExec) = (0, 0);
	foreach (@$cmdSub) {
        if ($cmd->[$i++]) {
		    PrintSeparator2();
		    &$_();
            $cmdExec++;
        };
	}
    
    if (! $cmdExec) {
        PrintSeparator2();
        printHelp();
    };
};

#############  changeIP  ################################################
sub changeIP() {
        my $response = getURL();

        dieHard( $response->status_line . " in getting IP from WRT54G" )
            unless ($response->is_success);

        my @dns = getDNS( $response );
        changeResolvConf( \@dns );
        print "DNS IP address(es) added to '$resolvPath' !!!\n";
};


sub changeResolvConf($) {
    my $dns = shift;
    my $found = 0;

    my $namserver = 'nameserver';
    foreach( @$dns ) {
        $namserver .= " $_";
    };

    my @resolvLine;
    
    tie @resolvLine, 'Tie::File', $resolvPath
        || dieHard( "Can't open file '$resolvPath'");

    ## resolv.conf contains one entry for nameservers

    for( my $i=0; $i < @resolvLine; $i++ ) {
        if ($resolvLine[$i] =~ /nameserver/) {
            if ( $found ) {
                undef $resolvLine[$i];
            } else {
                $resolvLine[$i] = $namserver;
                $found = 1;
            }
        };
    };

    push @resolvLine, $namserver
        if ( ! $found );

    untie @resolvLine;
}

sub getURL() {
        my $ua = LWP::UserAgent->new;
        my $h = HTTP::Headers->new;

        ## WRT54G: No user is needed, just password
        $h->authorization_basic('', $password);
    
        my $request = HTTP::Request->new( 'GET', $urlIP, $h);
        my $response = $ua->request($request);
        
        return $response;
}

sub getDNS($) {
    my $responseObj = shift;
    my $foundDNS = 0;
    my @dns;

    my @line = split /\n/, $responseObj->content;
    
    ## WRT54G Hack
    ##
    ## the admin URL for status in Cisco Linksys WRT54G shows the WAN IP address
    ## through HTML in a TABLE object

    foreach ( @line ) {
        if ( $foundDNS ) {
            /<TD(.*)><FONT style="FONT-SIZE: 8pt"><B>(.*)<\/B><\/FONT><\/TD>/;
            push @dns, $2;
            $foundDNS = 0;
            next;
        };
        /<TD(.*)><FONT style="FONT-SIZE: 8pt">DNS(.*)/;
        $foundDNS = 1  if ( defined $2 );
    };
    
    dieHard( "Can't find DNS addresses" )    if ( ! @dns );
    
    return @dns;
}

#############  Separators  ################################################
sub PrintSeparator(){
	print '=' x $SCREENWIDTH . "\n";
}

sub PrintSeparator2(){
	print '-' x $SCREENWIDTH . "\n";
}

#############  printHelp  #####################################################
sub printHelp(){
	print <<EOT

    --help:       This help screen
    --info:       Author and version info
    
    --pwd=admin_password_for_WRT54G (default: '$password')
    --rsURL=WRT54G_status_URL       (default: '$urlIP')
    --resolv=resolv.conf_path       (default: '$resolvPath')
EOT
}	
	
#############  PrintHeader  ###################################################
sub PrintHeader(){
	PrintSeparator();
    printInfo();
};

sub printInfo() {
    print "$NAME - v$VERSION\n" .
          "$AUTHOR\n" .
          "Contact: $CONTACT\n"; 
}

sub dieHard($) {
    my $msg = shift;

    die "\nFatal error !!!!!\n $msg\n\n";
}
