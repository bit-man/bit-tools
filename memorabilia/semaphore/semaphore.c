
#include "stdio.c"          /* Archivo de Procedimientos standard */

#define VERDE-DEFAULT   100 /* Duración de luz verde (default) */
#define ROJO-DEFAULT    100 /* Duración de luz roja (default) */
#define AMARILLO        100 /* Duración de luz amarilla */
#define IDENTIFICATIVO  'A'
#define CONVERSION      100
#define FLAG            0x7e
#define ACK             'A'
#define NAK             'N'

unsigned int verde, rojo, amarillo, p_buffer, device, blink, comienzo;
unsigned int tpo_on, light_on, light_peaton, p_out, more;
char buffer[9], shift_reg, c, ack_nak, estado, out_buf[9];



main() {
    reset();
    enable_interrupt();
    while() {
        elegir_luz();
        encender_luz();
    }
    
/***** rutina de interrupcion *****/
       
        if (scan_inter ()  != 1) {
            if (shift_reg == FLAG) {
                switch (p_buffer) {
                    case (0) : comienzo = 1;
                               break;
                    case (1) : if (comienzo==1) {
                                    p_buffer = 0;
                                    device = gchar();
                                    if ( device == IDENTIFICATIVO) {
                                        acknowledge();
                                        p_out = 0;
                                        wserial( gout () );
                                        more = 1;
                                    }
                               }
                               break;
                     default : if ( device == IDENTIFICATIVO ) {
                                    if (verificar() == NAK)  no_acknowledge ();
                                    else {
                                        ack_nak = interpretar ();
                                        if ( ack_nak == ACK ) acknowledge ();
                                        else no_acknowledge ();
                                    }
                                    p_out = 0;
                                    wserial( gout () );
                                    more = 1;
                               }
                               break;
                }
                p_buffer = 0;
            }
            else pchar(shift_reg);
        }
        else {
            if (more == 0) goto RTI;
            wserial( gout () );
            if (shift_reg == 'f') {
                p_out = 1;
                more = 0;
            }
        }
        RTI ();
}
