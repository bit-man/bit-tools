/**
 * 
 */
package bitman.algo2.arbol;

import bitman.algo2.excepciones.ExcepcionOrdenIncorecto;

/**
 * @author bit-man
 * 
 *         Árbol AVL : los hijos de la derecha de un nodo son mayores, y los de
 *         la izquierda son menores
 */

public class AVL<V extends Comparable<V>> extends ArbolBinario<V>
{

	@SuppressWarnings("unchecked")
	public AVL(AVL<V> izq, V valor, AVL<V> der) throws Exception
	{

		/***
		 * En este punto debería simplemente heredar el constructor de
		 * ArbolBinario (llamano a super() ) pero en su lugar copio lo que hace
		 * arbol binario en su constructor porque el nodo que se necesita usar
		 * es distinto al de un árbol binario ordinario (puntero al padre, etc.)
		 * entonces para usar un tipo de nodo diferente debo poder hacer que
		 * ArbolBinario pueda pasársele el tipo de nodo en lugar de usar un tipo
		 * de nodo prefijado
		 * 
		 * TODO : hacer que ArbolBinario pueda manejar cualquier tipo de nodo y
		 * evitar tener que copiar el código como hice acá
		 */

		raiz = (Nodo) new NodoAVL<V, NodoAVL>(valor);
		raiz.izq = izq == null ? null : izq.raiz;
		raiz.der = der == null ? null : der.raiz;

		if (izq != null)
			((NodoAVL) izq.raiz).padre = raiz;

		if (der != null)
			((NodoAVL) der.raiz).padre = raiz;

		verificacionAVL();
	}

	public AVL() throws Exception
	{
		super();
		verificacionAVL();
	}

	private void verificacionAVLaltura() throws Exception
	{
		if (raiz == null)
			return;

		ArbolBinario<V> izq = this.izq();
		ArbolBinario<V> der = this.der();
		int alturaIzq = izq == null ? 0 : izq.altura();
		int alturaDer = der == null ? 0 : der.altura();

		boolean inv = Math.abs((long) (alturaIzq - alturaDer)) < 2;

		if (!inv)
			throw new Exception("La diferencia de altura de los árboles AVL es mayor a 2");

	}

	private void verificacionAVL() throws Exception
	{
		verificacionAVLaltura();
		verificacionAVLorden();
	}

	private void verificacionAVLorden() throws Exception
	{
		if (raiz == null)
			return;

		if ((this.der() != null) && (raiz.valor.compareTo(this.der().raiz.valor) > 0))
			throw new ExcepcionOrdenIncorecto("La raiz es mayor que sus hijos derechos");

		if ((this.izq() != null) && (raiz.valor.compareTo(this.izq().raiz.valor) < 0))
			throw new ExcepcionOrdenIncorecto("La raiz es menor que sus hijos izquierdos. "
					+ raiz.valor + " es mayor que " + this.izq().raiz.valor);
	}

	@SuppressWarnings("unchecked")
	public void agregar(V valor) throws Exception
	{
		boolean fin = false;

		NodoAVL<V, NodoAVL> actual = (NodoAVL) this.raiz;

		while (!fin)
		{
			if (actual == null)
			{
				actual = new NodoAVL<V, NodoAVL>(valor);
				actual.izq = null;
				actual.der = null;
				fin = true;
			}
		}
	}

}
