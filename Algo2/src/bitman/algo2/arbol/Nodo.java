package bitman.algo2.arbol;

import bitman.algo2.Utils;

public class Nodo<V, H>
{

	public V valor;
	public H izq;
	public H der;

	public Nodo(V valor)
	{
		this.valor = valor;
		izq = null;
		der = null;
	}

	public Nodo()
	{
		this(null);
	}

	@SuppressWarnings("unchecked")
	public boolean equals(Object o) {
		if (o instanceof Nodo)
			return this.equals((Nodo<V,H>) o);

		return false;
	}
	
	
	private boolean equals(Nodo<V, H> that)
	{
		/***
		 * Este objeto no es null, por lo tanto si el otro lo es ya los nodos
		 * son distintos
		 */
		if (that == null)
			return false;
		/**
		 * Desde el punto de vista de la función de abstracción cuál es el valor
		 * de los hijos si el valor que el nodo contiene es null. Por otro lado
		 * ambos deberían ser null para dejar de referenciar a sus objetos hijos
		 */
		if (this.valor == null && that.valor == null)
			return true;

		return Utils.sonIguales(this.valor, that.valor) && Utils.sonIguales(this.der, that.der)
				&& Utils.sonIguales(this.izq, that.izq);
	}
	



	public boolean esHoja()
	{
		return izq == null && der == null;
	}

	public String toString()
	{
		StringBuilder s = new StringBuilder("");
		s.append("Nodo : [").append(valor).append("]");

		if (izq == null)
			s.append(" NoIzq");
		if (der == null)
			s.append(" NoDer");
		s.append("\n");

		return s.toString();
	}
}
