/**
 * 
 */
package bitman.algo2.arbol;

/**
 * @author bit-man
 * 
 */

// TODO : cómo encaja, en el diseño, un ABB. AVL es un ABB específico
// pero por ahora no lo modelo, voy a seguir con AVL para continuar con el orden
// del programa de estudio

public class ABB<V> extends ArbolBinario<V>
{

	public ABB()
	{
		super();
	}

	public ABB(ABB<V> izq, V a, ABB<V> der)
	{
		super(izq, a, der);
	}

}
