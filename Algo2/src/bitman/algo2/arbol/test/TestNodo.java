/**
 * 
 */
package bitman.algo2.arbol.test;

import junit.framework.TestCase;
import bitman.algo2.arbol.Nodo;

/**
 * @author bit-man
 * 
 */

// TODO convertir a test de JUnit 4 (ver TestAVL)
public class TestNodo extends TestCase
{

	public void testCreacion()
	{
		Nodo<Integer, Object> n = new Nodo<Integer, Object>();
		assertEquals("Valor no nulo", null, n.valor);
		assertEquals("Hijo izquierdo no nulo: " + n, null, n.izq);
		assertEquals("Hijo derecho no nulo: " + n, null, n.der);
		assertTrue("No es hoja :-P: " + n, n.esHoja());

		Integer valor = new Integer(10);
		Nodo<Integer, Object> n2 = new Nodo<Integer, Object>(valor);
		assertEquals("Valor erroneo", valor, n2.valor);
		assertEquals("Hijo izquierdo no nulo : " + n2, null, n2.izq);
		assertEquals("Hijo derecho no nulo: " + n2, null, n2.der);
		assertTrue("No es hoja :-P : " + n2, n2.esHoja());
	}

	@SuppressWarnings("unchecked")
	public void testCombinacion()
	{

		Integer valor = new Integer(10);
		Nodo<Integer, Nodo> n = new Nodo<Integer, Nodo>(valor);
		Nodo<Integer, Nodo> n2 = new Nodo<Integer, Nodo>(valor);
		assertTrue("No es hoja :-P", n.esHoja());
		assertTrue("No es hoja :-P", n2.esHoja());

		n.der = n2;
		assertFalse("Es hoja y no debería", n.esHoja());
		assertTrue("No es hoja :-P", n2.esHoja());
	}

	@SuppressWarnings("unchecked")
	public void testEqualsConUnHijo()
	{
		Nodo<Integer, Nodo> n1 = new Nodo<Integer, Nodo>(new Integer(8));
		Nodo<Integer, Nodo> n2 = new Nodo<Integer, Nodo>(new Integer(8));

		assertTrue("No son iguales, y deberían", n2.equals(n1));
		assertTrue("No son iguales, y deberían", n1.equals(n2));
		assertFalse("El nodo n1 no es nulo", n1.equals(null));

		Nodo<Integer, Nodo> n3 = new Nodo<Integer, Nodo>(new Integer(4));
		Nodo<Integer, Nodo> n4 = new Nodo<Integer, Nodo>(new Integer(4));
		assertTrue("No son iguales, y deberían", n3.equals(n4));
		assertTrue("No son iguales, y deberían", n4.equals(n3));

		assertFalse("Deben ser distintos", n4.equals(n1));

		n3.der = n1;
		assertFalse("Deben ser distintos", n4.equals(n3));

		n4.der = n2;
		assertTrue("No son iguales, y deberían", n3.equals(n4));
		assertTrue("No son iguales, y deberían", n4.equals(n3));

		// Pruebo con aliasing
		n2 = n1;
		assertTrue("No son iguales, y deberían", n3.equals(n4));
		assertTrue("No son iguales, y deberían", n4.equals(n3));
	}

	@SuppressWarnings("unchecked")
	public void testEqualsConDosHijos()
	{
		Nodo<String, Nodo> raiz1 = new Nodo<String, Nodo>("Padre");
		Nodo<String, Nodo> der1 = new Nodo<String, Nodo>("Derecho");
		Nodo<String, Nodo> izq1 = new Nodo<String, Nodo>("Izquierdo");
		raiz1.der = der1;
		raiz1.izq = izq1;

		Nodo<String, Nodo> raiz2 = new Nodo<String, Nodo>("Padre");
		Nodo<String, Nodo> der2 = new Nodo<String, Nodo>("Derecho");
		Nodo<String, Nodo> izq2 = new Nodo<String, Nodo>("Izquierdo");
		raiz2.der = der2;
		raiz2.izq = izq2;

		assertTrue("Deben ser iguales", raiz1.equals(raiz2));

		// Pruebo con aliasing
		raiz2.der = der1;
		raiz2.izq = izq1;

		assertTrue("Deben seguir ser iguales", raiz1.equals(raiz2));

	}

}
