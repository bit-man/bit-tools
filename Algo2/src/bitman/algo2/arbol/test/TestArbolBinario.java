/**
 * 
 */
package bitman.algo2.arbol.test;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import bitman.algo2.arbol.ArbolBinario;

/**
 * @author bit-man
 * 
 */

// TODO convertir a test de JUnit 4 (ver TestAVL)
public class TestArbolBinario extends TestCase
{

	public void testCreacionVacio() throws Exception
	{
		ArbolBinario<Integer> a = new ArbolBinario<Integer>();
		assertTrue("El arbol no está vacio", a.nil());

		boolean ex = false;
		try
		{
			a.der();
		} catch (Exception e)
		{
			ex = true;
		}

		assertTrue("No hubo error al intentar acceder al arbol derecho inexistente", ex);

		ex = false;
		try
		{
			a.izq();
		} catch (Exception e)
		{
			ex = true;
		}

		assertTrue("No hubo error al intentar acceder al arbol izquierdo inexistente", ex);
	}

	public void testCreacion() throws Exception
	{
		Integer valor = new Integer(10);

		ArbolBinario<Integer> ab = new ArbolBinario<Integer>(null, valor, null);

		assertEquals("Raices distintas", valor, ab.raiz());
		assertFalse("Arbol vacío :-P", ab.nil());
		assertEquals("Arbol derecho incorrecto", null, ab.der());
		assertEquals("Arbol izquierdo incorrecto", null, ab.izq());
	}

	public void testAltura() throws Exception
	{
		ArbolBinario<Integer> ab = new ArbolBinario<Integer>();
		assertEquals("Altura inorrecta", 0, ab.altura());

		ArbolBinario<Integer> ab2 = new ArbolBinario<Integer>(null, new Integer(10), null);
		assertEquals("Altura inorrecta", 1, ab2.altura());

		ArbolBinario<Integer> ab3 = new ArbolBinario<Integer>(null, new Integer(20), ab2);
		assertEquals("Altura inorrecta", 2, ab3.altura());

		ArbolBinario<Integer> ab4 = new ArbolBinario<Integer>(ab2, new Integer(20), ab2);
		assertEquals("Altura inorrecta", 2, ab4.altura());
	}

	public void testTamanio() throws Exception
	{
		ArbolBinario<Integer> ab = new ArbolBinario<Integer>();
		assertEquals("Tamaño inorrecto", 0, ab.tamanio());

		ArbolBinario<Integer> ab2 = new ArbolBinario<Integer>(null, new Integer(10), null);
		assertEquals("Tamaño inorrecto", 1, ab2.tamanio());

		ArbolBinario<Integer> ab3 = new ArbolBinario<Integer>(null, new Integer(20), ab2);
		assertEquals("Tamaño inorrecto", 2, ab3.tamanio());

		ArbolBinario<Integer> ab4 = new ArbolBinario<Integer>(ab2, new Integer(20), ab2);
		assertEquals("Tamaño inorrecto", 3, ab4.tamanio());
	}

	public void testInorder() throws Exception
	{
		Integer valor10 = new Integer(10);
		Integer valor20 = new Integer(20);
		Integer valor30 = new Integer(30);

		ArbolBinario<Integer> ab = new ArbolBinario<Integer>();
		List<Object> vacia = new ArrayList<Object>();
		assertEquals("Inorder inorrecto", vacia, ab.inorder());

		ArbolBinario<Integer> ab1 = new ArbolBinario<Integer>(null, valor10, null);
		List<Integer> in1 = new ArrayList<Integer>();
		in1.add(valor10);
		assertEquals("Inorder inorrecto", in1, ab1.inorder());

		ArbolBinario<Integer> ab2 = new ArbolBinario<Integer>(ab1, valor20, null);
		List<Integer> in2 = new ArrayList<Integer>();
		in2.add(valor10);
		in2.add(valor20);
		assertEquals("Inorder inorrecto", in2, ab2.inorder());

		ArbolBinario<Integer> ab3 = new ArbolBinario<Integer>(ab1, valor20,
				new ArbolBinario<Integer>(null, valor30, null));
		List<Integer> in3 = new ArrayList<Integer>();
		in3.add(valor10);
		in3.add(valor20);
		in3.add(valor30);
		assertEquals("Inorder inorrecto", in3, ab3.inorder());
	}

	// public void testInsertarAlaDerecha() throws Exception
	// {
	// Integer valor10 = new Integer(10);
	// ArbolBinario ab = new ArbolBinario(null, valor10, null);
	//
	// Integer valor20 = new Integer(20);
	// ab.agregarNodoAlaDerDe(valor20, valor10);
	// assertEquals("No se agregó el valor en el lugar esperado", valor20,
	// ab.der().raiz());
	// assertEquals("El úlitmo nodo no debería tener hijo derecho", null,
	// ab.der().der());
	// assertEquals("El úlitmo nodo no debería tener hijo izquierdo", null,
	// ab.der().izq());
	// assertEquals("No debería haber arbol izquierdo", null, ab.izq());
	// }

	// public void testInsertarAlaIzquierda() throws Exception
	// {
	// Integer valor10 = new Integer(10);
	// ArbolBinario ab = new ArbolBinario(null, valor10, null);
	//
	// Integer valor20 = new Integer(20);
	// ab.agregarNodoAlaIzqDe(valor20, valor10);
	// assertEquals("No se agregó el valor en el lugar esperado", valor20,
	// ab.izq().raiz());
	// assertEquals("El úlitmo nodo no debería tener hijo derecho", null,
	// ab.izq().der());
	// assertEquals("El úlitmo nodo no debería tener hijo izquierdo", null,
	// ab.izq().izq());
	// assertEquals("No debería haber arbol izquierdo", null, ab.der());
	// }

	public void testEquals()
	{
		ArbolBinario<Integer> ab1 = new ArbolBinario<Integer>();
		ArbolBinario<Integer> ab2 = new ArbolBinario<Integer>();

		assertTrue("Todo arbol es igual a si mismo", ab1.equals(ab1));
		assertTrue("Deben ser iguales", ab1.equals(ab2));
		assertTrue("Deben valer la propiedad transitiva", ab2.equals(ab1));

		ArbolBinario<Integer> ab3 = new ArbolBinario<Integer>(null, new Integer(1), ab1);
		ArbolBinario<Integer> ab4 = new ArbolBinario<Integer>(null, new Integer(1), ab2);

		assertTrue("Todo arbol es igual a si mismo", ab3.equals(ab3));
		assertTrue("Deben ser iguales", ab3.equals(ab4));
		assertTrue("Deben valer la propiedad transitiva", ab4.equals(ab3));

		ab3 = new ArbolBinario<Integer>(ab2, new Integer(1), ab1);
		ab4 = new ArbolBinario<Integer>(ab2, new Integer(1), ab1);

		assertTrue("Todo arbol es igual a si mismo", ab3.equals(ab3));
		assertTrue("Deben ser iguales", ab3.equals(ab4));
		assertTrue("Deben valer la propiedad transitiva", ab4.equals(ab3));
	}
}
