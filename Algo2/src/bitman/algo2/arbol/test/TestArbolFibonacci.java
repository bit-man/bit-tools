/**
 * 
 */
package bitman.algo2.arbol.test;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import bitman.algo2.arbol.ArbolFibonacci;

/**
 * @author bit-man
 * 
 */

// TODO convertir a test de JUnit 4 (ver TestAVL)
public class TestArbolFibonacci extends TestCase
{

	/**
	 * 
	 */
	private static final Integer CUATRO = new Integer(4);
	private static final Integer OCHO   = new Integer(8);

	public void test0() throws Exception
	{
		ArbolFibonacci<Integer> a = new ArbolFibonacci<Integer>();
		List<Integer> in = new ArrayList<Integer>();

		boolean ex = false;
		try
		{
			// Al tratar de acceder la raiz debe darme una excepción, porque la
			// raiz es nula
			a.raiz();
		} catch (Exception e)
		{
			ex = true;
		}

		assertTrue("El arbol 0 no debe tener raiz", ex);
		assertEquals("Altura incorrecta", 0, a.altura());
		assertEquals("Inroder incorrecto", in, a.inorder());
	}

	public void test1() throws Exception
	{
		ArbolFibonacci<Integer> a = new ArbolFibonacci<Integer>(null, CUATRO, null);

		assertEquals("Altura incorrecta", 1, a.altura());
		assertEquals("Arbol izquierdo incorrecto", null, a.izq());
		assertEquals("Arbol derecho incorrecto", null, a.der());

		List<Integer> in = new ArrayList<Integer>();
		in.add(CUATRO);
		assertEquals("Inroder incorrecto", in, a.inorder());

		boolean ex = false;
		try
		{
			// Debe fallar porque la altura de los árboles no difire en 1

			ArbolFibonacci<Integer> a1 = new ArbolFibonacci<Integer>();
			ArbolFibonacci<Integer> a2 = new ArbolFibonacci<Integer>(a1, CUATRO, a1);
		} catch (Exception e)
		{
			ex = true;
		}

		assertTrue("No puedo generar un árbol Fibonacci que sea balanceado", ex);
	}

	public void test2() throws Exception
	{
		ArbolFibonacci<Integer> a = new ArbolFibonacci<Integer>(null, CUATRO, null);
		ArbolFibonacci<Integer> a2 = new ArbolFibonacci<Integer>(null, OCHO, a);

		assertEquals("Altura incorrecta", 2, a2.altura());
		assertEquals("Arbol izquierdo incorrecto", null, a2.izq());
		assertEquals("Arbol derecho incorrecto", a, a2.der());

		List<Integer> in = new ArrayList<Integer>();
		in.add(OCHO);
		in.add(CUATRO);
		assertEquals("Inroder incorrecto", in, a2.inorder());
	}
}
