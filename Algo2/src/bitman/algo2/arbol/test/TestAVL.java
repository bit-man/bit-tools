/**
 * 
 */
package bitman.algo2.arbol.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import bitman.algo2.arbol.AVL;
import bitman.algo2.excepciones.ExcepcionOrdenIncorecto;

/**
 * @author bit-man
 * 
 */
public class TestAVL
{

	@Test
	public void testCreacion0() throws Exception
	{
		AVL<Integer> a = new AVL<Integer>();
		assertEquals("Altura errónea", 0, a.altura());

		testAccederRaizDebeFallar(a);
		testAccederAlNodoDerechoDebeFallar(a);
		testAccederAlArbolIzquierdoDebeFallar(a);

		List<Integer> list = new ArrayList<Integer>();
		assertEquals("Inorder incorrecto", list, a.inorder());
	}

	@Test
	public void testCreacion1() throws Exception
	{
		AVL<Integer> aDer = new AVL<Integer>(null, new Integer(6), null);
		AVL<Integer> aIzq = new AVL<Integer>(null, new Integer(2), null);
		// Árbol bien formado, no debe arrojar ninguna excepción durate la
		// creación
		AVL<Integer> a = new AVL<Integer>(aIzq, new Integer(4), aDer);
		assertEquals("Altura incorrecta", 2, a.altura());
		assertEquals("Árbol derecho incorrecto", aDer, a.der());
		assertEquals("Árbol derecho incorrecto", aIzq, a.izq());
		List<Integer> lista = new ArrayList<Integer>();
		lista.add(new Integer(2));
		lista.add(new Integer(4));
		lista.add(new Integer(6));

		assertEquals("Inorder incorrecto", lista, a.inorder());
		assertFalse("El árbol no es vacío", a.nil());
		assertEquals("Raíz errónea", new Integer(4), a.raiz());
		assertEquals("Tamaño erróneo", 3, a.tamanio());
	}

	@Test
	public void testArbolMalformadoAderecha() throws Exception
	{
		boolean ex = false;
		try
		{

			AVL<Integer> a3 = new AVL<Integer>(null, new Integer(2), null);
			@SuppressWarnings("unused")
			AVL<Integer> a = new AVL<Integer>(null, new Integer(4), a3);
		} catch (ExcepcionOrdenIncorecto e)
		{
			ex = true;
		}

		assertTrue("Árbol mal formado no detectado", ex);
	}

	@Test
	public void testArbolMalformadoAizquierda() throws Exception
	{
		boolean ex = false;

		try
		{
			AVL<Integer> a2 = new AVL<Integer>(null, new Integer(6), null);
			@SuppressWarnings("unused")
			AVL<Integer> a = new AVL<Integer>(a2, new Integer(4), null);
		} catch (ExcepcionOrdenIncorecto e)
		{
			ex = true;
		}

		assertTrue("Árbol mal formado no detectado", ex);
	}

	private void testAccederRaizDebeFallar(AVL<Integer> a)
	{
		boolean ex = false;
		try
		{
			@SuppressWarnings("unused")
			int n = a.raiz();
		} catch (Exception e)
		{
			ex = true;
		}
		assertTrue("Debe arrojar una excepción al acceder un nodo vacío", ex);
	}

	private void testAccederAlNodoDerechoDebeFallar(AVL<Integer> a)
	{
		boolean ex;
		ex = false;
		try
		{
			@SuppressWarnings("unused")
			AVL<Integer> der = (AVL<Integer>) a.der();
		} catch (Exception e)
		{
			ex = true;
		}
		assertTrue("Debe arrojar una excepción al acceder un nodo vacío", ex);
	}

	private void testAccederAlArbolIzquierdoDebeFallar(AVL<Integer> a)
	{
		boolean ex;
		ex = false;
		try
		{
			@SuppressWarnings("unused")
			AVL<Integer> izq = (AVL<Integer>) a.izq();
		} catch (Exception e)
		{
			ex = true;
		}
		assertTrue("Debe arrojar una excepción al acceder un nodo vacío", ex);
	}
}
