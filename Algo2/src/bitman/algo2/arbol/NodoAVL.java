/**
 * 
 */
package bitman.algo2.arbol;

/**
 * @author bit-man
 * 
 */
public class NodoAVL<V, H> extends Nodo<V, H>
{

	public H padre;

	public NodoAVL(V a)
	{
		super(a);
	}

}
