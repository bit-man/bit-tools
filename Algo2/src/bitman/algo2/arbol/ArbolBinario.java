package bitman.algo2.arbol;

import java.util.ArrayList;
import java.util.List;

import bitman.algo2.Utils;

/**
 * Arbol binario
 * 
 * Este implementación típica arbol binario me presentó un primer desafío de
 * diseño interesante, el poder separar la interfaz de la implementación.
 * Básicamente cuando hice la primer implementación había un nodo, y de cada
 * nodo podían colgar 2 objetos ArbolBonario. El problema que esto me trae es
 * que la implementación en realidad no es un árbol puro sino que es heterogéneo
 * donde cada nodo no tiene dos nodos como hijos sino dos ArbolBinario (y cuando
 * se trate de un AVL on un Fibonacci pasaría lo propio) No se que tan mal esté
 * esto desde el punto de vista de implementación, pero tratándose de una
 * implementación no comercial preferí mantenerla la más pura posible, así como
 * mantenerme atento a las opciones de diseño sin que esto me complique la vida
 * o me desvíe del objetivo
 * 
 * @author bit-man
 * 
 */
public class ArbolBinario<V>
{
	private static final String ESPACIO = " ";
	// Cada hijo del nodo tiene una referencia a un arbol binario
	protected Nodo<V, Nodo>	 raiz;

	public ArbolBinario()
	{
		raiz = null;
	}

	public ArbolBinario(ArbolBinario<V> izq, V a, ArbolBinario<V> der)
	{
		raiz = new Nodo<V, Nodo>(a);
		raiz.izq = izq == null ? null : izq.raiz;
		raiz.der = der == null ? null : der.raiz;
	}

	private ArbolBinario(Nodo raiz)
	{
		this((Nodo) raiz.izq, (V) raiz.valor, (Nodo) raiz.der);
	}

	private ArbolBinario(Nodo izq, V raiz, Nodo der)
	{
		this.raiz = new Nodo<V, Nodo>(raiz);
		this.raiz.izq = izq;
		this.raiz.der = der;
	}

	public boolean nil()
	{
		return raiz == null;
	}

	public V raiz() throws Exception
	{
		if (nil())
			throw new Exception("El arbol no tiene raiz");
		return raiz.valor;
	}

	public ArbolBinario<V> izq() throws Exception
	{
		if (nil())
			throw new Exception("El arbol no tiene raiz");

		return raiz.izq == null ? null : new ArbolBinario<V>(raiz.izq);
	}

	public ArbolBinario<V> der() throws Exception
	{
		if (nil())
			throw new Exception("El arbol no tiene raiz");

		return raiz.der == null ? null : new ArbolBinario<V>(raiz.der);
	}

	public int altura() throws Exception
	{
		if (this.nil())
			return 0;

		int derecho = this.der() == null ? 0 : this.der().altura();
		int izquierdo = this.izq() == null ? 0 : this.izq().altura();
		return 1 + (izquierdo > derecho ? izquierdo : derecho);
	}

	public int tamanio() throws Exception
	{

		if (this.nil())
			return 0;

		int derecho = this.der() == null ? 0 : this.der().tamanio();
		int izquierdo = this.izq() == null ? 0 : this.izq().tamanio();
		return 1 + izquierdo + derecho;

	}

	public List<V> inorder() throws Exception
	{
		if (this.nil())
			return new ArrayList<V>();

		ArbolBinario<V> miIzq = this.izq();
		List<V> listIzq = miIzq == null ? new ArrayList<V>() : new ArrayList<V>(miIzq.inorder());

		ArbolBinario<V> miDer = this.der();
		List<V> listDer = miDer == null ? new ArrayList<V>() : new ArrayList<V>(miDer.inorder());

		if (!this.nil())
			listIzq.add(this.raiz.valor);

		listIzq.addAll(listDer);
		return listIzq;
	}

	// --------------------------------------------------------------------------------------

	/***
	 * Los métodos desde aquí hacia abajo no pertenecen a la especificación
	 */
	public String toString()
	{
		return this.toString(0);
	}

	private String toString(int i)
	{
		StringBuilder s = new StringBuilder("");
		String offset = dameEspacios(i * 4);

		// Me permite acer que el oString sea genérico, tomando el tipo de árbol
		// de nombre de la clase
		String tipoArbol = this.getClass().getSimpleName();
		s.append(offset).append(tipoArbol + " : [").append(raiz == null ? null : raiz.valor)
				.append("]\n");

		if (raiz != null)
		{
			s.append(offset).append("   ");
			s.append(raiz.der == null ? null : (new ArbolBinario<V>(raiz.der)).toString(i + 1));
			s.append("\n");
			s.append(offset).append("   ");
			s.append(raiz.izq == null ? null : (new ArbolBinario<V>(raiz.izq)).toString(i + 1));
		}
		return s.toString();
	}

	private String dameEspacios(int n)
	{
		String ret = new String("");
		for (int i = 0; i < n; i++)
			ret += ESPACIO;
		return ret;
	}

	/***
	 * Agrega un nodo con valor n a la izquierda del nodo cuyo valor es v Este
	 * nodo debe existir en el árbol
	 * 
	 * @param n
	 *            : Nodo a agregar
	 * @param v
	 *            : valor existente en el árbol a cuya izquierda se agregará n
	 * @throws Exception
	 *             cuando no existe el nodo con valor v o el arbol izquierdo ya
	 *             existe
	 */
	// public void agregarNodoAlaIzqDe(Object n, Object v) throws Exception
	// {
	// Nodo este = buscarNodo(v, this);
	// if (este == null)
	// throw new Exception("No exsite el nodo " + v);
	// if (este.izq != null)
	// throw new Exception("El nodo izquierdo de '" + v + "' ya está asignado");
	//
	// este.izq = new ArbolBinario(null, n, null);
	// }

	/***
	 * Agrega el nodo n a la derecha del nodo cuyo valor es v Este nodo debe
	 * existir en el árbol
	 * 
	 * @param n
	 *            : Nodo a agregar
	 * @param v
	 *            : valor existente en el árbol a cuya derecha se agregará n
	 * @throws Exception
	 */
	// public void agregarNodoAlaDerDe(Object n, Object v) throws Exception
	// {
	// Nodo este = buscarNodo(v, this);
	// if (este == null)
	// throw new Exception("No exsite el nodo " + v);
	// if (este.der != null)
	// throw new Exception("El nodo derecho de '" + v + "' ya está asignado");
	//
	// este.der = new ArbolBinario(null, n, null);
	//
	// }

	/**
	 * Busca un nodo que contenga el valor v a usando esteNodo como raiz
	 * 
	 * @param v
	 *            valor a buscar
	 * @param esteArbol
	 *            nodo raiz
	 * @return devuelve el nodo cuyo valor coincide con v, o null si no lo
	 *         encuentra
	 * @throws Exception
	 */
	private Nodo buscarNodo(Object v, ArbolBinario esteArbol) throws Exception
	{
		if (v.equals(esteArbol.raiz.valor))
			return esteArbol.raiz;

		if (esteArbol.izq() != null)
		{
			Nodo ret = buscarNodo(v, esteArbol.izq());
			if (ret != null)
				return ret;
		}

		if (esteArbol.der() != null)
		{
			Nodo ret = buscarNodo(v, esteArbol.der());
			if (ret != null)
				return ret;
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public boolean equals(Object o)
	{
		if (o instanceof ArbolBinario)
			return this.equals((ArbolBinario<V>) o);

		return false;
	}

	private boolean equals(ArbolBinario<?> that)
	{
		// este objeto no es null, si el otro no lo es ya son distintos
		if (that == null)
			return false;

		/***
		 * Si ambas raices son null, no importa el contenido de los hijos (de
		 * hecho no los hay)
		 */
		if (that.raiz == null && this.raiz == null)
			return true;

		return sonIguales(this.raiz, that.raiz) && Utils.sonIguales(this.raiz.der, that.raiz.der)
				&& Utils.sonIguales(this.raiz.izq, that.raiz.izq);
	}

	private boolean sonIguales(Nodo a, Nodo b)
	{
		if (a == null && b == null)
			return true;

		if (((a == null) && (b != null)) || ((a != null) && (b == null)))
			return false;

		/***
		 * Si null fuera un objeto yo podría comparar null.equals(null) y que no
		 * arroje una NullPointerException, pero al no serlo tengo que hacer
		 * todas estas verificaciones "sucias" para que al comparar 'a' con 'b'
		 * ninguno de los dos sean null
		 */
		return a.equals(b);
	}
}
