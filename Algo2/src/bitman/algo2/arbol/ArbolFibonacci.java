/**
 * 
 */
package bitman.algo2.arbol;

/**
 * @author bit-man
 * 
 *         Árbol de Fibonacci. Donald E. Knuth, The Art of Computer
 *         Programminng, Vol. 3
 * 
 */

public class ArbolFibonacci<V> extends ArbolBinario<V>
{
	/**
	 * Construye un árbol de Fibonacci de orden 0
	 */
	public ArbolFibonacci()
	{
		super();
	}

	@SuppressWarnings("unchecked")
	public ArbolFibonacci(ArbolFibonacci<V> izq, V a, ArbolFibonacci<V> der) throws Exception
	{
		/***
		 * Para no duplicar código podría haber hecho super(izq, a, der) y luego
		 * verificar que la altura de ambos sea incorrecta pero esto me llega a
		 * crear un ábol que después puedo no llegar a usar. Prefería darle
		 * importancia al uso de recursos y no al diseño (que sentido tiene
		 * generar un árbol potencialmente enorme para luego trarlo al tacho de
		 * la basura ??)
		 */
		int altIzq = izq == null ? -1 : izq.altura();
		int altDer = der == null ? -1 : der.altura();

		// Pongo la expresión como 2 if en lugar de una sola porque es mucho más
		// fácil de leer. Adicionalmente si ambos son null se trata de un nodo
		// de altura 1 ue, el pobrecito no puede hacer otra cosa que tener dos
		// hijos nulos, loq ue equivale a decir de igual altura :-P
		if (izq != null && der != null)
			if (Math.abs(altIzq - altDer) != 1)
				throw new Exception("La diferencia de altura de ambos árboles es distinta de 1."
						+ " No se puede generar un nuevo árbol de Fibonacci");

		raiz = new Nodo<V, Nodo>(a);
		raiz.izq = izq == null ? null : izq.raiz;
		raiz.der = der == null ? null : der.raiz;
	}

	public boolean equals(ArbolFibonacci<V> that)
	{
		return super.equals(that);
	}

	/***
	 * El código original sólo llamaba a super.izq() con un cast :
	 * 
	 * return (ArbolFibonacci<V>) super.izq()
	 * 
	 * El problema es que en runtime este cast no se puede hacer, por eso opté
	 * por cambiar el código al actual
	 */
	@SuppressWarnings("unchecked")
	public ArbolFibonacci<V> izq() throws Exception
	{
		if (nil())
			throw new Exception("El arbol no tiene raiz");

		Nodo raizIzq = raiz.izq;
		return raizIzq == null ? null : new ArbolFibonacci<V>((ArbolFibonacci<V>) raizIzq.izq,
				(V) raizIzq.valor, (ArbolFibonacci<V>) raizIzq.der);
	}

	@SuppressWarnings("unchecked")
	public ArbolBinario<V> der() throws Exception
	{
		if (nil())
			throw new Exception("El arbol no tiene raiz");

		Nodo raizDer = raiz.der;
		return raizDer == null ? null : new ArbolFibonacci<V>((ArbolFibonacci<V>) raizDer.izq,
				(V) raizDer.valor, (ArbolFibonacci<V>) raizDer.der);

	}

}