/**
 * 
 */
package bitman.algo2.tipos;

/**
 * @author bit-man
 * 
 *         La idea detrás de este tipo de datos es poder ofrecer un elemento
 *         nulo que al compararlo con otro objeto no de NullPointerException.
 * 
 *         Integer.equals(null); // NullPointerException null.equals(Object); //
 *         NullPointerException
 * 
 *         Digamos el problema no es la excepción sino que, por ejemplo, en los
 *         árboles si un Nodo no tiene hijos entonces cada hijo es asignado un
 *         valor null, con lo cual al comparar dos nodos debi ver que sus raices
 *         sean iguales pero no null y lo mismo con sus hijos. Esto lleva a un
 *         código bastante ilegible y engorroso en la comparación de cada objeto
 *         que cree.
 * 
 *         Entonces con este objeto puedo hacer cualquier comparación, incluso
 *         Null.equals(null) y el código queda mucho más simple
 */
public class ObjetoBase
{

	private static ObjetoBase   singleton	   = new ObjetoBase();
	private static final String NOMBRE_DE_CLASE = "bitman.algo2.types.ObjetoBase";

	public static ObjetoBase nulo()
	{
		return singleton;
	}

	/***
	 * Protejo el constructor para que nadie lo acceda, y sólo obtener una única
	 * instancia de Null, de esta forma se comporta igual que null y puedo
	 * comparar con == o equals()
	 * 
	 * @return
	 */
	protected ObjetoBase()
	{
	};

	public boolean equals(ObjetoBase that)
	{
		/***
		 * La única forma de llegara este método es si no se definió un método
		 * equals() dentro de las otras clases que compara el objeto con uno del
		 * tipo ObjetoBase (cosa que no debe hacerse). Por lo tanto cualquier
		 * objeto comparado con NULL es falso a menos que el objeto sea NULL
		 */
		return this.getClass().getName().equals(that.getClass().getName());
	}

	public String toString()
	{
		return "NULL";
	}

}
