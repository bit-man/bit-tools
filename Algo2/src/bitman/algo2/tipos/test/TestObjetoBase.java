/**
 * 
 */
package bitman.algo2.tipos.test;

import junit.framework.TestCase;
import bitman.algo2.tipos.ObjetoBase;

/**
 * @author bit-man
 * 
 */
public class TestObjetoBase extends TestCase
{

    public void testConstructor()
    {
	ObjetoBase n1 = ObjetoBase.nulo();
	ObjetoBase n2 = ObjetoBase.nulo();

	assertTrue("No se comporta como un singleton", n1 == n2);
    }

    public void testEquals()
    {
	ObjetoBase n1 = ObjetoBase.nulo();
	ObjetoBase n2 = ObjetoBase.nulo();

	assertTrue("Deben ser guales", n1.equals(n1));
	assertTrue("Deben ser guales", n1.equals(n2));
	/**
	 * La verdad es que no puedo comparar peras con manzanas, pero al menos
	 * puedo hacer que si las comparo me den falso (porque no son iguales
	 * !!)
	 */
	assertFalse("No puedo comparar 2 con null, deben ser incomparables", n1
		.equals(2));
	assertFalse(
		"No puedo comparar un Integer() con Null(), deben ser incomparables",
		n1.equals(new Integer(5)));

    }

    public void testEqualsClasesHijas()
    {
	ObjetoBase n1 = ObjetoBase.nulo();
	HijaDeObjetoBase n2 = new HijaDeObjetoBase();

	assertFalse("No deberían ser iguales", n2.equals(n1));
	assertFalse("No deberían ser iguales", n1.equals(n2));

	HijaDeObjetoBase n3 = new HijaDeObjetoBase();
	assertTrue(
		"Deben ser iguales, si no lo son es porque no estoy usando el equals correcto",
		n3.equals(n2));
    }

    public void testToString()
    {
	ObjetoBase n1 = ObjetoBase.nulo();

	assertEquals("mal toString()", "NULL", n1.toString());
    }

    private class HijaDeObjetoBase extends ObjetoBase
    {

	public boolean equals(HijaDeObjetoBase that)
	{
	    return true;
	}
    }
}
