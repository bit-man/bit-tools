/**
 * 
 */
package bitman.algo2.sorting;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bit-man
 * 
 */
public class Sort
{
    /**
     * Para agregar cierta pedagogía al momento de aprender, no alcanza con
     * saber que el array se ordenó sino que se saben cuáles pasos va a seguir
     * el algoritmo. Para ello en cada paso que cada algortimo de ordenamieno
     * usa deja un registro (o camino) que luego puede ser consultado y
     * comparado con el camino esperado
     */
    private List<Migaja> camino;

    private Sort()
    {
        camino = new ArrayList<Migaja>();
    }

    /***
     * Ordena el array 'a' de menor a mayor, usand Selection Sort
     * 
     * @param a
     */
    public static Sort selection(Integer[] a)
    {
        Sort s = new Sort();

        for (int i = 0; i < a.length - 1; i++)
        {
            int menor = s.seleccionarMenor(a, i);
            s.swap(a, i, menor);
        }

        return s;
    }

    /***
     * Ordena el array 'a' de menor a mayor, usando Insertion Sort
     * 
     * @param a
     */
    public static Sort inserton(Integer[] a)
    {
        Sort s = new Sort();
        for (int ultimo = 1; ultimo < a.length; ultimo++)
        {
            s.insertarUltimoElementoEnOrden(a, ultimo);
        }
        return s;
    }

    /***
     * Convierte el array 'a' en un heap, siendo el elemento menor el que está
     * en la punta del heap. El heap generado es completo a izquierda, significa
     * que en el último nivel los nodos hoja se colocarán comenzando desde el
     * padre de la izquierda y completando hacia la derecha
     * 
     * @param a
     * @throws ArrayLongitudIncorrectaException
     */
    public static Sort array2heap(Integer[] a) throws ArrayLongitudIncorrectaException
    {
        if (!esPotenciade2(a.length + 1))
            throw new ArrayLongitudIncorrectaException("Debe ser de longitud 2**n - 1");

        Sort s = new Sort();
        // Empiezo desde el nivel inmediato superior, porque el úlitmo nivel
        // contiene sólo hojas (son heaps de 1 elemento => ya están ordenados)
        for (int nivel = maxNivel(a.length) - 1; nivel >= 0; nivel--)
        {
            s.agregarNivelAlCamino(nivel);
            int numNodos = nodosEnElNivel(nivel);
            int primerElem = primerElementoDelNivel(nivel);
            for (int padre = primerElem; padre < numNodos + primerElem; padre++)
            {
                int ix1 = padre;
                int ix2 = (2 * padre) + 1;
                int ix3 = (2 * padre) + 2;
                s.hacerHeap3(a, ix1, ix2, ix3);
            }
        }

        return s;
    }

    /***
     * Ordenación por el método merge sort. La longtiud del array debe ser
     * potencia de 2
     * 
     * @param a
     * @throws ArrayLongitudIncorrectaException
     * @throws Exception
     */
    public static Sort mergeSort(Integer[] a) throws ArrayLongitudIncorrectaException
    {
        if (!esPotenciade2(a.length))
            throw new ArrayLongitudIncorrectaException();

        Sort s = new Sort();
        s.mergeSortIndices(a, 0, a.length - 1);
        return s;
    }

    /**
     * @param a
     * @param i
     * @param length
     */
    private void mergeSortIndices(Integer[] a, int ixInicial, int ixFinal)
    {
        if (ixFinal - ixInicial + 1 == 2)
        {
            mergeSort2elementos(a, ixInicial);
        } else
        {
            int medio = (int) Math.floor((ixFinal + ixInicial) / 2);
            mergeSortIndices(a, ixInicial, medio);
            mergeSortIndices(a, medio + 1, ixFinal);
            mergeSortCombine(a, ixInicial, ixFinal);
        }
    }

    /**
     * @param a
     * @param ixInicial
     * @param ixFinal
     */
    private void mergeSortCombine(Integer[] a, int ixInicial, int ixFinal)
    {
        /**
         * Considero que tengo 2 arrays que debo ordenar en uno solo, uno va
         * desde ix0Inicial a ix0Final y el otro de ix1Inicial a ix1Final. Al
         * terminar el algoritmo deben estar ordenados en el array 'ordenado'
         */
        int medio = (int) Math.floor((ixFinal + ixInicial) / 2);
        int ix0Inicial = ixInicial;
        int ix0Final = medio;
        int ix1Inicial = medio + 1;
        int ix1Final = ixFinal;
        Integer[] ordenado = new Integer[(int) Math.floor(ixFinal - ixInicial) + 1];
        int i = 0;

        // Tomo el menor de cada subarray y lo coloco en 'ordenado'
        while (ix0Inicial <= ix0Final && ix1Inicial <= ix1Final)
        {
            if (a[ix0Inicial] < a[ix1Inicial])
            {
                ordenado[i++] = a[ix0Inicial++];
            } else
            {
                ordenado[i++] = a[ix1Inicial++];
            }
        }

        // En este punto uno de los dos subarrays ya está vacío, con lo cual de
        // estos for sólo debería ejecutarse uno

        for (int j = ix0Inicial; j <= ix0Final; j++)
        {
            ordenado[i++] = a[j];
        }

        for (int j = ix1Inicial; j <= ix1Final; j++)
        {
            ordenado[i++] = a[j];
        }

        // Copiar ordenado sobre el array 'a'
        for (int j = 0; j < ordenado.length; j++)
        {
            a[ixInicial + j] = ordenado[j];
        }

    }

    /**
     * @param a
     * @param ixInicial
     * @param ixFinal
     */
    private void mergeSort2elementos(Integer[] a, int i)
    {
        if (a[i] > a[i + 1])
        {
            swap(a, i, i + 1);
        }

    }

    /**
     * @param length
     * @return
     */
    private static boolean esPotenciade2(int length)
    {
        for (int p = 0; Math.pow(2, p) <= length; p++)
        {
            if (length == (int) Math.pow(2, p))
                return true;
        }
        return false;
    }

    /**
     * @param nivel
     */
    private void agregarNivelAlCamino(int nivel)
    {
        Migaja acc = new Migaja(Accion.NIVEL);
        acc.param(String.valueOf(nivel));
        acc.finParametros();
        camino.add(acc);
    }

    /**
     * @param nivel
     * @return
     */
    private static int primerElementoDelNivel(int nivel)
    {
        return (int) Math.pow(2, nivel) - 1;
    }

    /**
     * @param nivel
     * @return
     */
    private static int nodosEnElNivel(int nivel)
    {
        return (int) Math.pow(2, nivel);
    }

    /**
     * Número de nivel del árbol que tiene 'numElementos', empezando de cero
     * 
     * @param a
     * @return
     */
    private static int maxNivel(int numElementos)
    {
        return (int) Math.ceil(log(2, (long) numElementos)) - 1;
    }

    /**
     * @param base
     * @param length
     * @return
     */
    private static double log(int base, long length)
    {
        return Math.log10(length) / Math.log10(base);
    }

    /**
     * Hace un heap de los 3 elementos indicados por ix1, ix2 e ix3
     * 
     * @param a
     * @param cabeza
     */
    private void hacerHeap3(Integer[] a, int ix1, int ix2, int ix3)
    {
        // TODO supongo que ix1,2y3 son válidos. Arreglar para cuando caen fuera
        // del array
        Integer[] aux = new Integer[] { a[ix1], a[ix2], a[ix3] };

        // TODO medio choto es ordenarlos porque alcanza con que a[ix1] sea
        // menos que los otros dos
        inserton(aux);

        a[ix1] = aux[0];
        a[ix2] = aux[1];
        a[ix3] = aux[2];

        agregarHeapifyAlCamino(ix1, ix2, ix3);

    }

    /**
     * @param ix1
     * @param ix2
     * @param ix3
     */
    private void agregarHeapifyAlCamino(int ix1, int ix2, int ix3)
    {
        Migaja acc = new Migaja(Accion.HEAPIFY);
        acc.param(String.valueOf(ix1));
        acc.param(String.valueOf(ix2));
        acc.param(String.valueOf(ix3));
        acc.finParametros();
        camino.add(acc);
    }

    /**
     * Los elementos del array desde 0 hasta ultimo-1 están ordenados. Hay que
     * insertar el elemento a[ultimo] de forma que el subarray desde 0 hasta
     * ultimo quede ordenado
     * 
     * @param a
     * @param i
     */
    private void insertarUltimoElementoEnOrden(Integer[] a, int ultimo)
    {
        for (int i = 0; i < ultimo; i++)
        {
            if (a[i] > a[ultimo])
                swap(a, i, ultimo);
        }

    }

    /**
     * Devuelve el índice del menor elemento del array 'a', entre los elementos
     * 'i' y el final del array
     * 
     * @param a
     * @param i
     * @return
     */
    private int seleccionarMenor(Integer[] a, int i)
    {
        int menor = i;
        for (int j = i + 1; j < a.length; j++)
        {
            if (a[j] < a[menor])
            {
                menor = j;
            }
        }

        agregarMenorAlCamino(menor);

        return menor;
    }

    /**
     * @param menor
     */
    private void agregarMenorAlCamino(int menor)
    {
        Migaja acc = new Migaja(Accion.MENOR);
        acc.param(String.valueOf(menor));
        acc.finParametros();
        camino.add(acc);
    }

    /**
     * intercambia el valor de los elementos i-ésimo y j-ésimo del array 'a'
     * 
     * @param a
     * @param i
     * @param j
     */
    private void swap(Integer[] a, int i, int j)
    {
        Integer aux = a[i];
        a[i] = a[j];
        a[j] = aux;

        agregarSwapAlCamino(i, j);

    }

    /**
     * @param i
     * @param j
     */
    private void agregarSwapAlCamino(int i, int j)
    {
        Migaja acc = new Migaja(Accion.SWAP);
        acc.param(String.valueOf(i));
        acc.param(String.valueOf(j));
        acc.finParametros();
        camino.add(acc);
    }

    public List<Migaja> camino()
    {
        return camino;
    }
}
