/**
 * 
 */
package bitman.algo2.sorting;

/**
 * @author bit-man
 * 
 */
public enum Accion
{
    // SIGNIFICADO DE LA ACCIÓN : EXPLICACIÓN DE LOS PARÁMETROS
    SWAP(2), // swap de elementos : indices que se hacen swap
    MENOR(1), // menor elemento: índice del elemento menor
    HEAPIFY(3), // convertir 3 elementos en un heap : índice de los 3 elementos
    NIVEL(1); // nivel actual : nro. de nivel

    private int numParams;

    Accion(int numParams)
    {
        this.numParams = numParams;
    }

    public int numParams()
    {
        return numParams;

    }

}
