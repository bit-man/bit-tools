/**
 * 
 */
package bitman.algo2.sorting.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import bitman.algo2.sorting.Migaja;
import bitman.algo2.sorting.Sort;

/**
 * @author bit-man
 * 
 */
public class TestSelectionSort
{

    @Test
    public void test1()
    {
        // Ordeno el array y pruebo que quede ordenado
        Integer[] a = new Integer[] { 2, 1 };
        Integer[] ordenado = new Integer[] { 1, 2 };
        Sort s = Sort.selection(a);

        assertEquals("Error de ordenamiento", ordenado, a);

        // Verifico que las operaciones sean las esperadas
        List<Migaja> caminoEsperado = new ArrayList<Migaja>();
        HelperTest.agregarMenor(caminoEsperado, "1");
        HelperTest.agregarSwap(caminoEsperado, "0", "1");

        List<Migaja> caminoEjecutado = s.camino();

        assertEquals("Camino erroneo", caminoEsperado, caminoEjecutado);
    }

    @Test
    public void test2()
    {
        // Ordeno el array y pruebo que quede ordenado
        Integer[] a = new Integer[] { 2, 1, 1000, 345, 989 };
        Integer[] ordenado = new Integer[] { 1, 2, 345, 989, 1000 };
        Sort s = Sort.selection(a);

        assertEquals("Error de ordenamiento", ordenado, a);

        // Verifico que las operaciones sean las esperadas
        List<Migaja> caminoEsperado = new ArrayList<Migaja>();
        HelperTest.agregarMenor(caminoEsperado, "1");
        HelperTest.agregarSwap(caminoEsperado, "0", "1"); // { 1, 2, 1000, 345,
        // 989 }
        HelperTest.agregarMenor(caminoEsperado, "1");
        HelperTest.agregarSwap(caminoEsperado, "1", "1"); // { 1, 2, 1000, 345,
        // 989 }
        HelperTest.agregarMenor(caminoEsperado, "3");
        HelperTest.agregarSwap(caminoEsperado, "2", "3"); // { 1, 2, 345, 1000,
        // 989 }
        HelperTest.agregarMenor(caminoEsperado, "4");
        HelperTest.agregarSwap(caminoEsperado, "3", "4"); // { 1, 2, 345, 989,
        // 1000 }

        List<Migaja> caminoEjecutado = s.camino();

        assertEquals("Camino erroneo", caminoEsperado, caminoEjecutado);
    }

}
