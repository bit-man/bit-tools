/**
 * 
 */
package bitman.algo2.sorting.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import bitman.algo2.sorting.ArrayLongitudIncorrectaException;
import bitman.algo2.sorting.Sort;

/**
 * @author bit-man
 * 
 *         TODO hacer los caminos
 */
public class TestMergeSort
{
    @Test
    public void testNoEsPotenciaDe2()
    {
        Integer[] a = new Integer[] { 1, 2, 3 };

        boolean longIncorrecta = false;
        try
        {
            Sort.mergeSort(a);
        } catch (ArrayLongitudIncorrectaException e)
        {
            longIncorrecta = true;
        }

        assertTrue("Debe fallar por no ser la long. del array potencia de 2", longIncorrecta);
    }

    @Test
    public void test2Elementos() throws ArrayLongitudIncorrectaException
    {
        Integer[] a = new Integer[] { 1, 2 };
        Integer[] aOrdenado = new Integer[] { 1, 2 };
        Sort.mergeSort(a);

        assertEquals(aOrdenado, a);

        a = new Integer[] { 2, 1 };
        aOrdenado = new Integer[] { 1, 2 };
        Sort.mergeSort(a);

        assertEquals(aOrdenado, a);
    }

    @Test
    public void test4Elementos() throws ArrayLongitudIncorrectaException
    {
        Integer[] a = new Integer[] { 1, 2, 3, 4 };
        Integer[] aOrdenado = new Integer[] { 1, 2, 3, 4 };
        Sort.mergeSort(a);
        assertEquals(aOrdenado, a);

        a = new Integer[] { 4, 3, 2, 1 };
        Sort.mergeSort(a);
        assertEquals(aOrdenado, a);

        a = new Integer[] { 4, 1, 2, 3 };
        Sort.mergeSort(a);
        assertEquals(aOrdenado, a);
    }

    @Test
    public void test8Elementos() throws ArrayLongitudIncorrectaException
    {
        Integer[] a = new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8 };
        Integer[] aOrdenado = new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8 };
        Sort.mergeSort(a);
        assertEquals(aOrdenado, a);

        a = new Integer[] { 4, 3, 2, 1, 5, 6, 7, 8 };
        Sort.mergeSort(a);
        assertEquals(aOrdenado, a);

        a = new Integer[] { 1, 6, 7, 3, 4, 5, 2, 8 };
        Sort.mergeSort(a);
        assertEquals(aOrdenado, a);
    }
}
