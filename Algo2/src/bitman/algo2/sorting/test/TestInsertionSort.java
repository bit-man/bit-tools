/**
 * 
 */
package bitman.algo2.sorting.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import bitman.algo2.sorting.Migaja;
import bitman.algo2.sorting.Sort;

/**
 * @author bit-man
 * 
 */
public class TestInsertionSort
{
    @Test
    public void testUnElemento()
    {
        // Ordeno el array y pruebo que quede ordenado
        Integer[] a = new Integer[] { 1 };
        Integer[] ordenado = new Integer[] { 1 };
        Sort s = Sort.inserton(a);

        assertEquals("Error de ordenamiento", ordenado, a);

        // Verifico que las operaciones sean las esperadas
        List<Migaja> caminoEsperado = new ArrayList<Migaja>();
        List<Migaja> caminoEjecutado = s.camino();

        assertEquals("Camino erroneo", caminoEsperado, caminoEjecutado);
    }

    @Test
    public void testArrayOrdenado()
    {
        // Ordeno el array y pruebo que quede ordenado
        Integer[] a = new Integer[] { 1, 2, 3, 4 };
        Integer[] ordenado = new Integer[] { 1, 2, 3, 4 };
        Sort s = Sort.inserton(a);

        assertEquals("Error de ordenamiento", ordenado, a);

        // Verifico que las operaciones sean las esperadas
        List<Migaja> caminoEsperado = new ArrayList<Migaja>();
        List<Migaja> caminoEjecutado = s.camino();

        assertEquals("Camino erroneo", caminoEsperado, caminoEjecutado);
    }

    @Test
    public void testElementosDesordenados()
    {
        // Ordeno el array y pruebo que quede ordenado
        Integer[] a = new Integer[] { 2, 1 };
        Integer[] ordenado = new Integer[] { 1, 2 };
        Sort s = Sort.inserton(a);

        assertEquals("Error de ordenamiento", ordenado, a);

        // Verifico que las operaciones sean las esperadas
        List<Migaja> caminoEsperado = new ArrayList<Migaja>();
        HelperTest.agregarSwap(caminoEsperado, "0", "1");
        List<Migaja> caminoEjecutado = s.camino();

        assertEquals("Camino erroneo", caminoEsperado, caminoEjecutado);
    }

    @Test
    public void testElementosDesordenados2()
    {
        // Ordeno el array y pruebo que quede ordenado
        Integer[] a = new Integer[] { 1, 2, 3, 5, 4 };
        Integer[] ordenado = new Integer[] { 1, 2, 3, 4, 5 };
        Sort s = Sort.inserton(a);

        assertEquals("Error de ordenamiento", ordenado, a);

        // Verifico que las operaciones sean las esperadas
        List<Migaja> caminoEsperado = new ArrayList<Migaja>();
        HelperTest.agregarSwap(caminoEsperado, "3", "4");
        List<Migaja> caminoEjecutado = s.camino();

        assertEquals("Camino erroneo", caminoEsperado, caminoEjecutado);
    }
}
