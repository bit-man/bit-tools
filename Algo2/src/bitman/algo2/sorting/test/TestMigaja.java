/**
 * 
 */
package bitman.algo2.sorting.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import bitman.algo2.sorting.Accion;
import bitman.algo2.sorting.Migaja;

/**
 * @author bit-man
 * 
 */
public class TestMigaja
{

	@Test
	public void testParam()
	{
		Migaja a = new Migaja(Accion.SWAP);
		a.param("a");
		a.param("b");
		a.finParametros();

		assertEquals("Primer parámetro erróneo", a.param(), "a");
		assertEquals("Segundo parámetro erróneo", a.param(), "b");
	}

	@Test
	public void testEquals()
	{
		Migaja a = new Migaja(Accion.SWAP);
		a.param("a");
		a.param("b");
		a.finParametros();

		Migaja b = new Migaja(Accion.SWAP);
		b.param("a");
		b.param("b");
		b.finParametros();

		assertEquals("Deben ser iguales", a, b);

		Migaja c = new Migaja(Accion.SWAP);
		c.param("a");
		c.param("c");
		c.finParametros();

		assertFalse("Las migajas 'a' y 'c' son iguales pero no se detecta que lo sean", a.equals(c));

	}
}
