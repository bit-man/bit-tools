/**
 * 
 */
package bitman.algo2.sorting.test;

import java.util.List;

import bitman.algo2.sorting.Accion;
import bitman.algo2.sorting.Migaja;

/**
 * @author bit-man
 * 
 */
public class HelperTest
{
    /**
     * @param caminoEsperado
     * @param string
     * @param string2
     */
    public static void agregarSwap(List<Migaja> caminoEsperado, String indice1, String indice2)
    {
        Migaja swap = new Migaja(Accion.SWAP);
        swap.param(indice1);
        swap.param(indice2);
        swap.finParametros();
        caminoEsperado.add(swap);
    }

    /**
     * @param caminoEsperado
     * @param string
     * @param string2
     */
    public static void agregarHeapify(List<Migaja> caminoEsperado, String indice1, String indice2,
            String indice3)
    {
        Migaja swap = new Migaja(Accion.HEAPIFY);
        swap.param(indice1);
        swap.param(indice2);
        swap.param(indice3);
        swap.finParametros();
        caminoEsperado.add(swap);
    }

    /**
     * @param caminoEsperado
     */
    public static void agregarMenor(List<Migaja> caminoEsperado, String indice)
    {
        Migaja menor = new Migaja(Accion.MENOR);
        menor.param(indice);
        menor.finParametros();
        caminoEsperado.add(menor);
    }

    /**
     * @param caminoEsperado
     */
    public static void agregarNivel(List<Migaja> caminoEsperado, String nivel)
    {
        Migaja miNivel = new Migaja(Accion.NIVEL);
        miNivel.param(nivel);
        miNivel.finParametros();
        caminoEsperado.add(miNivel);
    }
}
