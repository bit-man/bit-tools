/**
 * 
 */
package bitman.algo2.sorting.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import bitman.algo2.sorting.ArrayLongitudIncorrectaException;
import bitman.algo2.sorting.Migaja;
import bitman.algo2.sorting.Sort;

/**
 * @author bit-man
 * 
 *         El test prueba sólo el array2heap
 */
public class TestArray2Heap
{
    @Test
    public void testNoEsPotenciaDe2()
    {
        Integer[] a = new Integer[] { 1, 2 };

        boolean longIncorrecta = false;
        try
        {
            Sort.array2heap(a);
        } catch (ArrayLongitudIncorrectaException e)
        {
            longIncorrecta = true;
        }

        assertTrue("Debe fallar por no ser la long. del array potencia de 2", longIncorrecta);
    }

    @Test
    public void testUnElemento() throws ArrayLongitudIncorrectaException
    {
        Integer[] a = new Integer[] { 1 };
        Integer menor = 1;
        Sort s = Sort.array2heap(a);

        assertEquals(menor, a[0]);

        List<Migaja> camino = s.camino();
        List<Migaja> caminoEsperado = new ArrayList<Migaja>();

        assertEquals(camino, caminoEsperado);
    }

    @Test
    public void testTresElementosOrdenados() throws ArrayLongitudIncorrectaException
    {
        Integer[] a = new Integer[] { 1, 2, 3 };
        Integer menor = 1;
        Sort s = Sort.array2heap(a);

        assertEquals(menor, a[0]);

        List<Migaja> camino = s.camino();
        List<Migaja> caminoEsperado = new ArrayList<Migaja>();
        HelperTest.agregarNivel(caminoEsperado, "0");
        HelperTest.agregarHeapify(caminoEsperado, "0", "1", "2");

        assertEquals(caminoEsperado, camino);
    }

    @Test
    public void testSieteElementosOrdenados() throws ArrayLongitudIncorrectaException
    {
        Integer[] a = new Integer[] { 1, 2, 3, 4, 5, 6, 7 };
        Integer menor = 1;
        Sort s = Sort.array2heap(a);

        assertEquals(menor, a[0]);

        List<Migaja> camino = s.camino();
        List<Migaja> caminoEsperado = new ArrayList<Migaja>();
        HelperTest.agregarNivel(caminoEsperado, "1");
        HelperTest.agregarHeapify(caminoEsperado, "1", "3", "4");
        HelperTest.agregarHeapify(caminoEsperado, "2", "5", "6");
        HelperTest.agregarNivel(caminoEsperado, "0");
        HelperTest.agregarHeapify(caminoEsperado, "0", "1", "2");

        assertEquals(caminoEsperado, camino);
    }

    @Test
    public void testTresElementosDesordenados() throws ArrayLongitudIncorrectaException
    {
        Integer[] a = new Integer[] { 2, 1, 3 };
        Integer menor = 1;
        Sort s = Sort.array2heap(a);

        assertEquals(menor, a[0]);

        List<Migaja> camino = s.camino();
        List<Migaja> caminoEsperado = new ArrayList<Migaja>();
        HelperTest.agregarNivel(caminoEsperado, "0");
        HelperTest.agregarHeapify(caminoEsperado, "0", "1", "2");

        assertEquals(caminoEsperado, camino);
    }

    @Test
    public void testSieteElementosDesordenados() throws ArrayLongitudIncorrectaException
    {
        Integer[] a = new Integer[] { 7, 2, 4, 3, 1, 6, 5 };
        Integer menor = 1;
        Sort s = Sort.array2heap(a);

        assertEquals(menor, a[0]);

        List<Migaja> camino = s.camino();
        List<Migaja> caminoEsperado = new ArrayList<Migaja>();
        HelperTest.agregarNivel(caminoEsperado, "1");
        HelperTest.agregarHeapify(caminoEsperado, "1", "3", "4");
        HelperTest.agregarHeapify(caminoEsperado, "2", "5", "6");
        HelperTest.agregarNivel(caminoEsperado, "0");
        HelperTest.agregarHeapify(caminoEsperado, "0", "1", "2");

        assertEquals(caminoEsperado, camino);
    }

}
