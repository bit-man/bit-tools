/**
 * 
 */
package bitman.algo2.sorting;

/**
 * @author bit-man
 * 
 */
public class ArrayLongitudIncorrectaException extends Exception
{

    private static final long serialVersionUID = -4965258697921131059L;

    public ArrayLongitudIncorrectaException()
    {
        super();
    }

    public ArrayLongitudIncorrectaException(String mensaje)
    {
        super(mensaje);
    }

}
