/**
 * 
 */
package bitman.algo2.sorting;

/**
 * @author bit-man
 * 
 */
public class Migaja
{
	private Accion   accion;
	private String[] param;
	private int	  i;

	public Migaja(Accion accion)
	{
		this.accion = accion;
		param = new String[accion.numParams()];
		i = 0;
	}

	public void param(String valor)
	{
		param[i++] = valor;
	}

	public String param()
	{
		return param[i++];
	}

	public void finParametros()
	{
		i = 0;
	}

	public boolean equals(Object that)
	{
		if (that instanceof Migaja)
		{

			boolean acciones = accion.equals(((Migaja) that).accion);
			boolean parametros = paramsIguales(((Migaja) that).param);
			return acciones && parametros;
		} else
		{
			return false;
		}
	}

	/**
	 * @param that
	 * @return
	 */
	private boolean paramsIguales(String[] thatParam)
	{
		if (thatParam.length != param.length)
			return false;

		for (int ix = 0; ix < param.length; ix++)
		{
			if (!thatParam[ix].equals(param[ix]))
				return false;
		}

		return true;
	}

	public String toString()
	{
		String ret = accion.name() + "(" + paramToString() + ")";
		return ret;
	}

	private String paramToString()
	{
		String ret = " ";
		for (int ix = 0; ix < param.length; ix++)
		{
			ret += param[ix];
			if (ix != param.length - 1)
				ret += ", ";
		}

		return ret + " ";
	}
}
