/**
 * 
 */
package bitman.algo2.excepciones;

/**
 * @author bit-man
 * 
 */
public class ExcepcionOrdenIncorecto extends Exception
{

	public ExcepcionOrdenIncorecto(String string)
	{
		super(string);
	}

	private static final long serialVersionUID = 1L;

}
