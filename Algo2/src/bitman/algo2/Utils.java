/**
 * 
 */
package bitman.algo2;

import bitman.algo2.tipos.ObjetoBase;

/**
 * @author bit-man
 * 
 */
public class Utils {
	public static ObjetoBase NULL = ObjetoBase.nulo();

	public static boolean sonIguales(Object a, Object b) {
		if (a == null && b == null)
			return true;

		if (((a == null) && (b != null)) || ((a != null) && (b == null)))
			return false;

		/***
		 * Si null fuera un objeto yo podría comparar null.equals(null) y que no
		 * arroje una NullPointerException, pero al no serlo tengo que hacer
		 * todas estas verificaciones "sucias" para que al comparar 'a' con 'b'
		 * ninguno de los dos sean null
		 */
		return a.equals(b);
	}
	
}
