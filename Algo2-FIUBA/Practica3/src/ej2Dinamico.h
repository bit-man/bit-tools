/*
 * ej2a.h
 *
 *  Created on: Apr 24, 2014
 *      Author: vicrodri
 */

#ifndef EJ2A_H_
#define EJ2A_H_

#include <cpptest.h>

#include <iostream>
using namespace std;


template<typename T>
class Lista {
private:
	class Nodo {
	public:
		T value;
		Nodo *next;
		Nodo *prev;
	};

	Nodo *first;
	Nodo *last;
	unsigned int length;

public:
	Lista();
	~Lista();
	bool vacia();
	bool llena();
	void insertar(const T &);
	void agregar(const T &);
	T *buscar(const T&);
	unsigned int tam();
	bool borrar(unsigned int);
	bool swap();
	T & operator[]( unsigned int);
};



template<typename T>
T & Lista<T>::operator[](unsigned int p) {
	if ( p > ( tam() - 1 ) ) {
		T *ret = NULL;
		return *ret;
	}

	Nodo *current = first;
	for (int i = 0; i < p; i++ ) {
		current = current->next;
	}

	return current->value;
}

template<typename T>
bool Lista<T>::swap() {
	if (tam() < 2) {
		return false;
	}

	Nodo *primero = first;
	Nodo *segundo = first->next;

	first = segundo;

	primero->next = segundo->next;
	primero->prev = segundo;

	if ( segundo->next != NULL) {
		segundo->next->prev = primero; // tercero ahora apunta a primero
	} else {
		last = primero;
	}

	segundo->prev = NULL;
	segundo->next = primero;

	return true;
}

template<typename T>
bool Lista<T>::borrar(unsigned int p) {
	if ( (p+1) > tam() ) {
		return false;
	}

	Nodo *current = first;
	for (int i = 0; i < p; i++ ) {
		current = current->next;
	}

	if ( current != first) {
		current->prev->next = current->next;
	} else {
		first = current->next;
	}

	if (current->next == NULL) {
		last = current->prev;
	} else {
		current->next->prev = current->prev;
	}

	delete current;

	length--;

	return true;

}

template<typename T>
Lista<T>::Lista() {
	first = NULL;
	last = NULL;
	length = 0;
};


template<typename T>
Lista<T>::~Lista() {
	Nodo *del = first;
	while( del != NULL) {
		Nodo *next = del->next;
		delete del;
		del = next;
	}
};


template<typename T>
bool Lista<T>::vacia() {
	return (length == 0);
}


template<typename T>
bool Lista<T>::llena() {
	return false;
}


template<typename T>
void Lista<T>::insertar(const T &e) {
	Nodo *n = new Nodo();
	n->value = e;
	n->next = first;
	n->prev = NULL;

	if ( first != NULL ) { // lista vacía
		first->prev = n;
	}

	first = n;

	if ( last == NULL ) { // lista vacía
		last = n;
	}

	length++;
};


template<typename T>
void Lista<T>::agregar(const T &e) {
	Nodo *n = new Nodo();
	n->value = e;
	n->next = NULL;
	n->prev = last;

	if ( last != NULL ) { // lista vacía
		last->next = n;
	}

	last = n;

	if ( first == NULL ) { // lista vacía
		first = n;
	}
	length++;
};


template<typename T>
T *Lista<T>::buscar(const T& e) {
	Nodo *current;

	current = first;
	bool continueSearch = true;
	while( continueSearch ) {
		if (current == NULL ) {
			continueSearch = false;
		} else {
			continueSearch = (current->value != e);

			if ( continueSearch)
				current = current->next;
		}
	}

	return (current == NULL ? NULL : &(current->value) );
};


template<typename T>
unsigned int Lista<T>::tam() {
	return length;
};

class ListaTestSuite: public Test::Suite
{
public:
    ListaTestSuite() {
    	TEST_ADD(ListaTestSuite::nuevaLista)
		TEST_ADD(ListaTestSuite::nuevaListaBorrar)
		TEST_ADD(ListaTestSuite::nuevaListaSwap)
		TEST_ADD(ListaTestSuite::unElemPrincipio)
		TEST_ADD(ListaTestSuite::unElemPrincipioBorrar)
		TEST_ADD(ListaTestSuite::unElemPrincipioSwap)
		TEST_ADD(ListaTestSuite::unElemFinal)
		TEST_ADD(ListaTestSuite::unElemFinalBorrar)
		TEST_ADD(ListaTestSuite::unElemFinalSwap)
		TEST_ADD(ListaTestSuite::dosElemPrincipio)
		TEST_ADD(ListaTestSuite::dosElemPrincipioBorrar)
		TEST_ADD(ListaTestSuite::dosElemPrincipioSwap)
		TEST_ADD(ListaTestSuite::dosElemFinal)
		TEST_ADD(ListaTestSuite::dosElemFinalBorrar)
		TEST_ADD(ListaTestSuite::dosElemFinalSwap)
		TEST_ADD(ListaTestSuite::buscarVacia)
		TEST_ADD(ListaTestSuite::buscarLlena)
		TEST_ADD(ListaTestSuite::buscarLlenaYnoHalar)
    }
private:
    void nuevaLista()
    {
    	Lista<int> l;
        TEST_ASSERT(l.vacia())
        TEST_ASSERT(! l.llena())
        TEST_ASSERT(l.tam() == 0)
    }

    void nuevaListaBorrar()
    {
    	Lista<int> l;
        TEST_ASSERT(! l.borrar(0))
        TEST_ASSERT(l.vacia())
        TEST_ASSERT(! l.llena())
        TEST_ASSERT(l.tam() == 0)
    }

    void nuevaListaSwap()
    {
    	Lista<int> l;
        TEST_ASSERT(! l.swap())
    	TEST_ASSERT_EQUALS(0, l.tam())
    }

    void unElemPrincipio()
    {
    	Lista<int> l;
    	l.insertar(2);
        TEST_ASSERT(!l.vacia())
        TEST_ASSERT(!l.llena())
        TEST_ASSERT(l.tam() == 1)
    }

    void unElemPrincipioBorrar()
    {
    	Lista<int> l;
    	l.insertar(2);
        TEST_ASSERT(l.tam() == 1)

    	TEST_ASSERT(l.borrar(0))
        TEST_ASSERT(l.tam() == 0)

    	TEST_ASSERT(! l.borrar(0))
        TEST_ASSERT(l.tam() == 0)
    }

    void unElemPrincipioSwap()
    {
    	Lista<int> l;
    	l.insertar(2);
        TEST_ASSERT(! l.swap())
    	TEST_ASSERT_EQUALS(1, l.tam())
    }

    void unElemFinal()
    {
    	Lista<int> l;
    	l.agregar(2);
        TEST_ASSERT(!l.vacia())
        TEST_ASSERT(!l.llena())
        TEST_ASSERT(l.tam() == 1)
    }


    void unElemFinalBorrar()
    {
    	Lista<int> l;
    	l.agregar(2);
        TEST_ASSERT(l.tam() == 1)

    	TEST_ASSERT(l.borrar(0))
        TEST_ASSERT(l.tam() == 0)

    	TEST_ASSERT(! l.borrar(0))
        TEST_ASSERT(l.tam() == 0)
    }

    void unElemFinalSwap()
    {
    	Lista<int> l;
    	l.agregar(2);
    	TEST_ASSERT(! l.swap())
    	TEST_ASSERT_EQUALS(1, l.tam())
    }


    void dosElemPrincipio()
    {
    	Lista<int> l;
    	l.insertar(2);
    	l.insertar(2);
        TEST_ASSERT(!l.vacia())
        TEST_ASSERT(!l.llena())
        TEST_ASSERT(l.tam() == 2)
    }


    void dosElemPrincipioBorrar()
    {
    	Lista<int> l;
    	l.insertar(2);
    	l.insertar(2);
        TEST_ASSERT(l.tam() == 2)

    	TEST_ASSERT(l.borrar(1))
        TEST_ASSERT(l.tam() == 1)
    }

    void dosElemPrincipioSwap()
    {
    	Lista<int> l;
    	l.insertar(2);
    	l.insertar(3);
        TEST_ASSERT_EQUALS(2, l.tam())

    	TEST_ASSERT_EQUALS(3, l[0] )
    	TEST_ASSERT_EQUALS(2, l[1] )

    	TEST_ASSERT(l.swap())
    	TEST_ASSERT_EQUALS(3, l[1] )
    	TEST_ASSERT_EQUALS(2, l[0] )
        TEST_ASSERT_EQUALS(2, l.tam())
    }


    void dosElemFinal()
    {
    	Lista<int> l;
    	l.agregar(2);
    	l.agregar(2);
        TEST_ASSERT(!l.vacia())
        TEST_ASSERT(!l.llena())
        TEST_ASSERT(l.tam() == 2)
    }

    void dosElemFinalBorrar()
    {
    	Lista<int> l;
    	l.agregar(2);
    	l.agregar(2);
        TEST_ASSERT(l.tam() == 2)

    	TEST_ASSERT(l.borrar(1))
        TEST_ASSERT(l.tam() == 1)
    }


    void dosElemFinalSwap()
    {
    	Lista<int> l;
    	l.agregar(2);
    	l.agregar(3);
        TEST_ASSERT_EQUALS(2, l.tam())

    	TEST_ASSERT_EQUALS(3, l[1] )
    	TEST_ASSERT_EQUALS(2, l[0] )

    	TEST_ASSERT(l.swap())
    	TEST_ASSERT_EQUALS(3, l[0] )
    	TEST_ASSERT_EQUALS(2, l[1] )
        TEST_ASSERT_EQUALS(2, l.tam())
    }


    void buscarVacia()
    {
    	Lista<int> l;
    	TEST_ASSERT(l.buscar(2) == NULL)
    }

    void buscarLlena()
    {
    	Lista<int> l;
    	l.agregar(2);
    	l.agregar(3);

    	int *value = l.buscar(2);
    	TEST_ASSERT( *value == 2)
    }

    void buscarLlenaYnoHalar()
    {
    	Lista<int> l;
    	l.agregar(2);
    	l.agregar(3);
    	int *value = l.buscar(4);
    	TEST_ASSERT( value == NULL)
    }
};

void ej2Dinamico() {
    ListaTestSuite sts;
    Test::TextOutput output(Test::TextOutput::Verbose);
    sts.run(output, false);
}

#endif /* EJ2A_H_ */
