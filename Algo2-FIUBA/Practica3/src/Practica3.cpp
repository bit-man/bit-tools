//============================================================================
// Name        : Practica3.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

#include "ej1a.h"
#include "ej1b.h"
#include "ej2Dinamico.h"

#include <unordered_map>

class MyClass {
public:
	MyClass() {};
	~MyClass() {};
};

class MyMap {
private:
	unordered_map<string,string> moneda;
	unordered_map<string,MyClass *> xxx;

public:
	MyMap() {}
	void run() {
		// Asignar valores
		moneda["Argentina"] = "Peso";
		moneda["US"] = "dolar";

		cout << endl << "Acceso a un elemento " << endl;
		cout << "Argentina : " << moneda["Argentina"] << endl;

		cout << endl << "Acceso todos los elementos" << endl;

		cout << endl << "Contar elementos" << endl;
		unordered_map<string,string>::iterator it = moneda.begin();
		for( ; it != moneda.end(); it++) {
			cout << it->first << ":" << it->second << endl;
		}

		string existeArgentina = moneda.count("Argentina") > 0 ? "Existe moneda" : "No existe moneda";
		cout << endl << "Argentina : " << existeArgentina << endl;

		xxx["A"] = new MyClass();
		xxx["B"] = new MyClass();

		cout << endl << "Contar elementos" << endl;
		unordered_map<string,MyClass *>::iterator it2 = xxx.begin();
		for( ; it2 != xxx.end(); it2++) {
			cout << it2->first << ":" << it2->second << endl;
		}
	}
};

int main() {
	ej1a();
	ej1b();
	ej2Dinamico();
	MyMap mm;
	mm.run();
	return 0;
}

