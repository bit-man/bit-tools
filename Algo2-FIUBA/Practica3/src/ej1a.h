/*
 * ej1.cpp
 *
 *  Created on: Apr 23, 2014
 *      Author: vicrodri
 */

#ifndef _EJ1_H_
#define _EJ1_H_

#include <cpptest.h>

#include <iostream>
using namespace std;


template<typename T>
class ListaEj1a {
private:
	class Nodo {
	public:
		T value;
		Nodo *next;
		Nodo *prev;
	};

	Nodo *first;
	Nodo *last;
	unsigned int length;

public:
	ListaEj1a();
	~ListaEj1a();
	bool vacia();
	bool llena();
	void insertar(const T &);
	void agregar(const T &);
	T *buscar(const T&);
	unsigned int tam();
};


template<typename T>
ListaEj1a<T>::ListaEj1a() {
	first = NULL;
	last = NULL;
	length = 0;
};


template<typename T>
ListaEj1a<T>::~ListaEj1a() {
	Nodo *del = first;
	while( del != NULL) {
		Nodo *next = del->next;
		delete del;
		del = next;
	}
};


template<typename T>
bool ListaEj1a<T>::vacia() {
	return (length == 0);
}


template<typename T>
bool ListaEj1a<T>::llena() {
	return false;
}


template<typename T>
void ListaEj1a<T>::insertar(const T &e) {
	Nodo *n = new Nodo();
	n->value = e;
	n->next = first;
	n->prev = NULL;

	if ( first != NULL ) { // lista vacía
		first->prev = n;
	}

	first = n;

	if ( last == NULL ) { // lista vacía
		last = n;
	}

	length++;
};


template<typename T>
void ListaEj1a<T>::agregar(const T &e) {
	Nodo *n = new Nodo();
	n->value = e;
	n->next = NULL;
	n->prev = last;

	if ( last != NULL ) { // lista vacía
		last->next = n;
	}

	last = n;

	if ( first == NULL ) { // lista vacía
		first = n;
	}

	length++;
};


template<typename T>
T *ListaEj1a<T>::buscar(const T& e) {
	Nodo *current;

	current = first;
	bool continueSearch = true;
	while( continueSearch ) {
		if (current == NULL ) {
			continueSearch = false;
		} else {
			continueSearch = (current->value != e);

			if ( continueSearch)
				current = current->next;
		}
	}

	return (current == NULL ? NULL : &(current->value) );
};


template<typename T>
unsigned int ListaEj1a<T>::tam() {
	return length;
};

class Ej1aTestSuite: public Test::Suite
{
public:
    Ej1aTestSuite() {
    	TEST_ADD(Ej1aTestSuite::nuevaLista)
		TEST_ADD(Ej1aTestSuite::unElemPrincipio)
		TEST_ADD(Ej1aTestSuite::unElemFinal)
		TEST_ADD(Ej1aTestSuite::dosElemPrincipio)
		TEST_ADD(Ej1aTestSuite::dosElemFinal)
		TEST_ADD(Ej1aTestSuite::buscarVacia)
		TEST_ADD(Ej1aTestSuite::buscarLlena)
		TEST_ADD(Ej1aTestSuite::buscarLlenaYnoHalar)
    }
private:
    void nuevaLista()
    {
    	ListaEj1a<int> l;
        TEST_ASSERT(l.vacia())
        TEST_ASSERT(! l.llena())
        TEST_ASSERT(l.tam() == 0)
    }

    void unElemPrincipio()
    {
    	ListaEj1a<int> l;
    	l.insertar(2);
        TEST_ASSERT(!l.vacia())
        TEST_ASSERT(!l.llena())
        TEST_ASSERT(l.tam() == 1)
    }

    void unElemFinal()
    {
    	ListaEj1a<int> l;
    	l.agregar(2);
        TEST_ASSERT(!l.vacia())
        TEST_ASSERT(!l.llena())
        TEST_ASSERT(l.tam() == 1)
    }


    void dosElemPrincipio()
    {
    	ListaEj1a<int> l;
    	l.insertar(2);
    	l.insertar(2);
        TEST_ASSERT(!l.vacia())
        TEST_ASSERT(!l.llena())
        TEST_ASSERT(l.tam() == 2)
    }


    void dosElemFinal()
    {
    	ListaEj1a<int> l;
    	l.agregar(2);
    	l.agregar(2);
        TEST_ASSERT(!l.vacia())
        TEST_ASSERT(!l.llena())
        TEST_ASSERT(l.tam() == 2)
    }


    void buscarVacia()
    {
    	ListaEj1a<int> l;
    	TEST_ASSERT(l.buscar(2) == NULL)
    }

    void buscarLlena()
    {
    	ListaEj1a<int> l;
    	l.agregar(2);
    	l.agregar(3);

    	int *value = l.buscar(2);
    	TEST_ASSERT( *value == 2)
    }

    void buscarLlenaYnoHalar()
    {
    	ListaEj1a<int> l;
    	l.agregar(2);
    	l.agregar(3);
    	int *value = l.buscar(4);
    	TEST_ASSERT( value == NULL)
    }
};

void ej1a() {
    Ej1aTestSuite sts;
    Test::TextOutput output(Test::TextOutput::Verbose);
    sts.run(output, false);
}

#endif
