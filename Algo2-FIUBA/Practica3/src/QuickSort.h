/*
 * QuickSort.h
 *
 *  Created on: May 31, 2014
 *      Author: vicrodri
 */

#ifndef QUICKSORT_H_
#define QUICKSORT_H_

void QuickSortI(int *a, int l, int r);
int partition(int *a, int l, int r);

#endif /* QUICKSORT_H_ */
