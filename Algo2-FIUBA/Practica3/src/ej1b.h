/*
 * ej1b.h
 *
 *  Created on: Apr 23, 2014
 *      Author: vicrodri
 */

#ifndef EJ1B_H_
#define EJ1B_H_

#include <cpptest.h>

#include <iostream>
using namespace std;


template<typename T,int N>
class ListaEj1b {
private:
	T a [N];

	int first;  // Primer elemento
	int last;   // Último elemento
	// El tamaño pùede calcularse a partir de first y last
	// pero como es engorroso => factible de error es mejor
	// incrementar el tamaño cada vez que se agrega un elemento
	unsigned int size;

	void inc(int &);
	void dec(int &);
	unsigned int indexInicio();
	unsigned int indexFin();
	bool brandNew();

public:
	ListaEj1b();
	~ListaEj1b();
	bool vacia();
	bool llena();
	void insertar(const T &);
	void agregar(const T &);
	T *buscar(const T&);
	unsigned int tam();
};

template<typename T,int N>
ListaEj1b<T,N>::ListaEj1b() {
	first = -1;
	last = -1;
	size = 0;
}

template<typename T,int N>
ListaEj1b<T,N>::~ListaEj1b() {}


template<typename T,int N>
unsigned int ListaEj1b<T,N>::tam() {
	return size;
}

template<typename T,int N>
bool ListaEj1b<T,N>::vacia() {
	return (tam() == 0);
}

template<typename T,int N>
bool ListaEj1b<T,N>::llena() {
	return (tam() == N);
}


template<typename T,int N>
void ListaEj1b<T,N>::inc(int &n) {
	n = (n + 1) % N;
}

template<typename T,int N>
void ListaEj1b<T,N>::dec(int &n) {
	n == 0 ?
		n = N-1 :
		n--;
}

template<typename T,int N>
void ListaEj1b<T,N>::agregar(const T& e) {
	if (llena()) {
		return;
	}

	if ( vacia() ) {
		last = 0;
		first = 0;
	} else {
		inc(last);
	}

	size++;
	a[last] = e;
}

template<typename T,int N>
void ListaEj1b<T,N>::insertar(const T& e) {
	if (llena()) {
		return;
	}

	if ( vacia() ) {
		last = 0;
		first = 0;
	} else {
		dec(first);
	}

	size++;
	a[first] = e;
}


template<typename T,int N>
bool ListaEj1b<T,N>::brandNew() {
	return (first == -1 && last == -1) ;
}

template<typename T,int N>
unsigned int ListaEj1b<T,N>::indexInicio() {
	if ( brandNew() ) {
		return 0;
	} else {
		return ( first < last ? first : last);
	}
}

template<typename T,int N>
unsigned int ListaEj1b<T,N>::indexFin() {
	if ( brandNew() ) {
		return 0;
	} else {
		return ( first > last ? first : last);
	}
}

template<typename T,int N>
T* ListaEj1b<T,N>::buscar(const T& e) {
	bool continueSearch = true;
	T *value;
	value = NULL;
	for ( int i = indexInicio();
			i <= indexFin() && continueSearch;
			i++) {
		continueSearch = (a[i] != e);
		if ( ! continueSearch) {
			value = a+i;
		}
	}

	return value;
}


class Ej1bTestSuite: public Test::Suite
{
public:
    Ej1bTestSuite() {
    	TEST_ADD(Ej1bTestSuite::nuevaLista)
		TEST_ADD(Ej1bTestSuite::unElemPrincipio)
		TEST_ADD(Ej1bTestSuite::unElemFinal)
		TEST_ADD(Ej1bTestSuite::dosElemPrincipio)
		TEST_ADD(Ej1bTestSuite::dosElemFinal)
		TEST_ADD(Ej1bTestSuite::buscarVacia)
		TEST_ADD(Ej1bTestSuite::buscarLlena)
		TEST_ADD(Ej1bTestSuite::buscarLlenaYnoHallar)
		TEST_ADD(Ej1bTestSuite::llenar)
    }
private:
    void nuevaLista()
    {
    	ListaEj1b<int,10> l;
        TEST_ASSERT(l.vacia())
        TEST_ASSERT(! l.llena())
        TEST_ASSERT(l.tam() == 0)
    }

    void unElemPrincipio()
    {
    	ListaEj1b<int,10> l;
    	l.insertar(2);
        TEST_ASSERT(!l.vacia())
        TEST_ASSERT(!l.llena())
        TEST_ASSERT_EQUALS(1, l.tam())
    }

    void unElemFinal()
    {
    	ListaEj1b<int,10> l;
    	l.agregar(2);
        TEST_ASSERT(!l.vacia())
        TEST_ASSERT(!l.llena())
        TEST_ASSERT_EQUALS(1, l.tam())
    }


    void dosElemPrincipio()
    {
    	ListaEj1b<int,10> l;
    	l.insertar(2);
    	l.insertar(2);
        TEST_ASSERT(!l.vacia())
        TEST_ASSERT(!l.llena())
        TEST_ASSERT_EQUALS(2, l.tam())
    }


    void dosElemFinal()
    {
    	ListaEj1b<int,10> l;
    	l.agregar(2);
    	l.agregar(2);
        TEST_ASSERT(!l.vacia())
        TEST_ASSERT(!l.llena())
        TEST_ASSERT_EQUALS(2, l.tam())
    }

    void buscarVacia()
    {
    	ListaEj1b<int,10> l;
    	TEST_ASSERT(l.buscar(2) == NULL)
    }

    void buscarLlena()
    {
    	ListaEj1b<int,10> l;
    	l.agregar(2);
    	l.agregar(3);

    	int *value = l.buscar(2);
    	TEST_ASSERT( *value == 2)
    }

    void buscarLlenaYnoHallar()
    {
    	ListaEj1b<int,10> l;
    	l.agregar(2);
    	l.agregar(3);
    	int *value = l.buscar(4);
    	TEST_ASSERT( value == NULL)
    }

    void llenar()
    {
    	ListaEj1b<int,3> l;
    	TEST_ASSERT( l.vacia())
    	TEST_ASSERT(! l.llena())
    	TEST_ASSERT_EQUALS(0, l.tam())

    	l.agregar(2);
    	TEST_ASSERT(! l.vacia())
		TEST_ASSERT(! l.llena())
    	TEST_ASSERT_EQUALS(1, l.tam())

		l.agregar(3);
    	TEST_ASSERT(! l.vacia())
		TEST_ASSERT(! l.llena())
    	TEST_ASSERT_EQUALS(2, l.tam())

		l.agregar(4);
    	TEST_ASSERT(! l.vacia())
		TEST_ASSERT(l.llena())
    	TEST_ASSERT_EQUALS(3, l.tam())


    	// Se agrega un elemento malvado, que no va a ser agregado
    	// porque la cola ya está llena
		l.agregar(666);
    	TEST_ASSERT(! l.vacia())
		TEST_ASSERT(l.llena())
    	TEST_ASSERT_EQUALS(3, l.tam())

    }
};

void ej1b() {
    Ej1bTestSuite sts;
    Test::TextOutput output(Test::TextOutput::Verbose);
    sts.run(output, false);
}

#endif /* EJ1B_H_ */
