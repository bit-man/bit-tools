/*
 * QuickSort.cpp
 *
 *  Created on: May 31, 2014
 *      Author: vicrodri
 */

#include "QuickSort.h"

void QuickSortI(int *a, int l, int r) {
	if (l < r) {
		int q = partition(a, l, r);
		QuickSortI(a, l, q);
		QuickSortI(a, q+1, r);
	}
}


int partition(int *a, int l, int r) {
	int x = a[l];
	int i = l - 1;
	int j = r + 1;

	while(1) {
		while( a[j] <= x) {
					j--;
				}
		while( a[i] >= x) {
					i--;
				}
		if ( i < j) {
			int w = a[i];
			a[i] = a[j];
			a[j] = w;
		} else {
			return j;
		}
	}
}
