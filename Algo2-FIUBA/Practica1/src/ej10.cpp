/*
 * complejo.cpp
 *
 *  Created on: Mar 14, 2014
 *      Author: bit-man
 */

#include <iostream>
#include <math.h>
using namespace std;


class complejo {
private:
	/**
	 *  Se usan coordenadas cartesianas como representación interna
	 *  porque sabemos que la implementación se va a usar para sumar
	 */
	double x;
	double y;

public:
	/**
	 * Constructor usando coordenadas polares
	 */
	complejo(double radio = 0, double angulo = 0) {
		this->x = radio * cos(angulo);
		this->y = radio * sin(angulo);
	}

	complejo & operator+(const complejo &b) {
		complejo *ret = new complejo();

		ret->x = x + b.x;
		ret->y = y + b.y;

		return *ret;
	}

	 friend ostream& operator<<(ostream&, complejo &);

};

ostream & operator<<( ostream & out, complejo & c){
	   out << c.x << " + j " << c.y;
	   return out;
 }



void ej10() {
	// -------- Test mínimo
	complejo c1;
	complejo *c2 = new complejo(1,0.785); // ángulo : pi/4
	complejo c3 = c1 + *c2;
	cout << "resultado del test : " <<  c3 << endl;

	// -------- Ejercicio
	int n;


	cout << "n : ";
	cin >> n;
	cout << endl;


	complejo *c = new complejo[n];

	for (int i = 0; i < n; i++) {
		double radio;
		double angulo;

		cout << "Radio (" << i << ") : ";
		cin >> radio;
		cout << "Ángulo (" << i << ") : ";
		cin >> angulo;
		// c[i] = new complejo(radio,angulo);
	}

}
