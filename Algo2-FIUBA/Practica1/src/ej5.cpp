/*
 * ej5.cpp
 *
 *  Created on: Mar 14, 2014
 *      Author: bit_man
 */


#include <iostream>
using namespace std;


char encriptar_desencriptar(char c, const string &key, int i) {
	return c ^ key[i];
}


void ej5() {

	string key;
	char c, d, e;

	cout << "Clave de encripción ? ";
	cin >> key;

	unsigned int i = 0;
	while ( true ) {
		cout << "Ingrese el caracter a encriptar : ";
		cin >> c;
		cout << endl;

		if ( c == '0') {
			cout << "Fin de la sesión de encripción" <<endl;
			break;
		}

		e = encriptar_desencriptar(c, key, i);
		cout << "caracter encriptado : " << e << endl;
		cout << "Clave : " << key[i] << endl;

		d = encriptar_desencriptar( e, key, i++);

		if ( i == key.length())
			i = 0;

		if ( d != c ) {
			cout << "char : " << d << " :-(" << endl;
		} else {
			cout << "Desencriptado correcto!!" << endl;
		};
	}

}
