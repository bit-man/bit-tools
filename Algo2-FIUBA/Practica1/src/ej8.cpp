/*
 * ej8.cpp
 *
 *  Created on: Mar 14, 2014
 *      Author: bit-man
 */

#include <iostream>
using namespace std;

class arreglo {
private:
	unsigned int tam;
	int *v;

public:
	arreglo(unsigned int n) {
		cout << "arreglo(unsigned int)" << endl;
		tam = n;
		v = new int[n];
	}

	// Constructor por copia
	arreglo(const arreglo & that) {

		cout << "arreglo( const arreglo &)" << endl;
		this->tam = that.tam;
		cout << "Tamaño : " << that.tam << endl;

		this->v = new int[this->tam];
		unsigned int i;
		for (i = 0; i < this->tam; i++) {
			cout << "i = " << i << endl;
			*(this->v + i) = *(that.v + i);
		}

		cout << "arreglo( const arreglo &)  FIN" << endl;
	}

	int & operator[](unsigned int i) {
		// verificar que está dentro del rango
		return v[i];
	}

	~arreglo() {
		cout << "~arreglo()" << endl;
		if (v != NULL)
			delete[] v;
	}
};

void ej8() {

	// Necesito implementar el constructor con un parámetro (tamaño del arreglo)
	cout << "a(7)" << endl;
	arreglo a(7);

	// Implementar operator[]
	cout << "Sumas" << endl;
	a[0] = 3;
	a[1] = 6;
	a[2] = a[0] + a[1];
	// Invoca el constructor por copia
	cout << "arreglo b = a;" << endl;
	arreglo b = a;

	if (b[2] == 9 && a[0] == 3 && a[1] == 6)
		cout << "Éxito !!" << endl;
	else {
		cout << "Error !!! " << endl;
		cout << "b[2] : " << b[2] << endl;
		cout << "b[1] : " << b[1] << endl;
		cout << "b[0] : " << b[0] << endl;
		cout << "a[2] : " << a[2] << endl;
		cout << "a[1] : " << a[1] << endl;
		cout << "a[0] : " << a[0] << endl;
	}
}
