/*
 * ej1.cpp
 *
 *  Created on: Mar 14, 2014
 *      Author: bit-man
 */

#include <iostream>
using namespace std;



void ej1() {
	int i;
	char c;
	float f;
	int* i2;
	char* c2;
	float* f2;
	cout << "int : " << sizeof i << endl;
	cout << "char : " << sizeof c << endl;
	cout << "float : " << sizeof f << endl;
	cout << "char * : " << sizeof c2 << endl;
	cout << "int * : " << sizeof i2 << endl;
	cout << "float * : " << sizeof f2 << endl;
}
