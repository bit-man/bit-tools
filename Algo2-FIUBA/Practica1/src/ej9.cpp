/*
 * ej9.cpp
 *
 *  Created on: Apr 6, 2014
 *      Author: vicrodri
 */

#include <iostream>
using namespace std;

class foo {
public:
	foo() {
		cout << "foo:foo()" << endl;
	}

	foo(const foo & ) {
		cout << "foo:foo(const foo & )" << endl;
	}

	~foo() {
		cout << "~foo:foo()" << endl;
	}
};

foo bar(foo A) {
	cout << "foo bar(foo A)" << endl;
	return A;
}

void ej9() {
	foo A;
	bar(A);
}
