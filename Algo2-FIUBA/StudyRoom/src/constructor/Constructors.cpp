/*
 * main.cpp
 *
 *  Created on: Apr 2, 2014
 *      Author: Bit-Man
 */

#include <iostream>
#include "Constructors.h"

using namespace std;

ConstructorMain::ConstructorMain() {}

/**
 * ToDo
 * 		- Probar que pasa con constructors/destructors cuando paso una variable por
 * 	      valor, puntero o referencia y combinado con const
 *
 * 	      f( MyType a)
 * 	      f( MyType *a)
 * 	      f( MyType &a)
 * 	      f( const MyType a)
 * 	      f( const MyType *a)
 * 	      f( const MyType &a)
 *
 *		- ver código Pág 335, muy representativo del uso de constructor por copia
 *
 */
void ConstructorMain::run() {
	cout << "\tMyClass mc" << endl;
	MyClass mc;


	cout << "\tMyClass *mc2 = new MyClass(1)" << endl;
	MyClass *mc1 = new MyClass(1);

	cout << "\tMyClass mc3(3)" << endl;
	MyClass mc3(3);

	cout << "\tmc.run()" << endl;
	mc.run();
	cout << "\tmc1->run()" << endl;
	mc1->run();

	cout << endl << "\tmc1 is not cleaned up, only mc is !!!" << endl;
	cout << "\tdelete mc1" << endl;
	delete mc1;

	cout << "\tBye !!!" << endl;
}

MyClass::MyClass() {
	this->id = 0;
	cout << "\t\tMyClass::MyClass()" << endl;
}

MyClass::MyClass(unsigned int id) {
	this->id = id;
	cout << "\t\tMyClass::MyClass(" << id << ")" << endl;
}

MyClass::~MyClass() {
	cout << "\t\tdestructor MyClass(" << id << ")" << endl;
}

void MyClass::run() {
	cout << "\t\tMyClass::run()" << endl;
}
