/*
 * Constructors.h
 *
 *  Created on: Apr 2, 2014
 *      Author: vicrodri
 */

#ifndef CONSTRUCTORS_H_
#define CONSTRUCTORS_H_


class ConstructorMain {
	public:
		ConstructorMain();
		void run();
};


class MyClass {
private:
	unsigned int id;
public:
	MyClass();
	MyClass(unsigned int);
	~MyClass();
	void run();
};


#endif /* CONSTRUCTORS_H_ */
