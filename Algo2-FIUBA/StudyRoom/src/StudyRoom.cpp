//============================================================================
// Name        : StudyRoom.cpp
// Author      : 
// Version     :
// Copyright   : Free Software
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "constructor/Constructors.h"

using namespace std;

int main() {

	cout << "------------------ Constructor" << endl;
	ConstructorMain *cm = new ConstructorMain();
	cm->run();

	// Thanks Valgrind for noticing it !!!
	delete cm;
	cout << "------------------ Constructor ENDS" << endl;

	return 0;
}
