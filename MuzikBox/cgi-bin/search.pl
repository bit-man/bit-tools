#!/usr/bin/perl

use strict;
use warnings;

use CGI;
use Error qw(:try);
use MuzikBox::Repository;

## settings
################################################################################

my $contentType = 'text/html; charset=ISO-8859-1';
my $pageTitle = 'Busc� en MuzikBox ... tu cajita de m�sica';
my $lang = 'es-AR';
my $URLprefix = '/cgi-bin/MuzikBox';
my $searchScript = $URLprefix . '/search.pl';
my $playScript = $URLprefix . '/play.pl';
my $searchName = 'searchForm';
my $queryFieldName = 'q';
my $queryFieldSize = 50;
my $submitValue = 'Buscar';
my $submitName = 'search';
my $maxCGIpost = 1000;
my $CGIuploadsDisable = 1;
my $errorColour = 'red';
my $resultTableHAlign = 'CENTER';
my $resultTableVAlign = 'CENTER';

my $JSCode = "function play(volume,id) {\n" 
           . "  alert( \"Coloque el CD marcado como '\" + volume + \"' y presione ENTER\");\n"
           . "  window.open( \"$playScript?id=\" + id);\n"
           . "}";

## Internal variables and settings
## NON PLUS ULTRA !!!!
################################################################################

my $cgi = new CGI;

## subroutines
################################################################################

sub init() {
    $CGI::POST_MAX = $maxCGIpost;
    $CGI::DISABLE_UPLOADS = $CGIuploadsDisable;
};

#----------------------------------------------------------------- showHeader()
sub showHeader() {
    print $cgi->header( -type => $contentType );
    print $cgi->start_html( -title => $pageTitle, 
                            -author => 'victor@bit-man.com.ar',
                            -lang => $lang,
                            -script => $JSCode ),
          $cgi->h1( $pageTitle );
};

#----------------------------------------------------------------- showFooter()
sub showFooter() {
    print $cgi->end_html;
};

#------------------------------------------------------------- showSearchForm()
sub showSearchForm() {
    print $cgi->start_form( -name => $searchName, -action => $searchScript );
    print $cgi->textfield( -name => $queryFieldName, -size => $queryFieldSize );
    print $cgi->submit( -name=> $submitName,   -value => $submitValue );
    print $cgi->endform;
};

#----------------------------------------------------------------- emptyQuery()
sub emptyQuery() {
    return ( ! $cgi->param('q') );
};

#---------------------------------------------------------------- showResults()
sub showResults() {
    my $hRef;

    return if emptyQuery();

    print $cgi->hr,
          $cgi->h2('Archivos encontrados');

    try {
        my $h = MuzikBox::Repository->new();
        $h->open();
        $hRef = $h->search( 'text' => $cgi->param('q') );
        $h->close();
        showFoundFile( $hRef );
    }
    catch Error::Simple with {
        my $E = shift;
        showError( $E );
    };
    

};

#--------------------------------------------------------------- showFoundFile()
sub showFoundFile($) {
    my $hRef = shift;
    
    print "\n";   ## just for HTML reading pleasure
    print $cgi->start_table( { -border => 1 } );
    print "\n";   ## just for HTML reading pleasure
    print $cgi->Tr( { -align => $resultTableHAlign, -valign => $resultTableVAlign },
                      $cgi->td( [ 'Nombre del archivo',
                                  'Categor�as',
                                  'Comentarios' 
                                ] )
                  );
    print "\n";   ## just for HTML reading pleasure

    foreach my $id ( keys %$hRef ) {
            print $cgi->Tr( { -align => $resultTableHAlign, -valign => $resultTableVAlign },
                      $cgi->td( [ $cgi->a( { -href => "javascript:play(\"". $hRef->{$id}->{'Label'} ."\",$id)" },  $hRef->{$id}->{'Filename'} ),
                                  $hRef->{$id}->{'Categories'},
                                  $hRef->{$id}->{'Comment'}
                                ] )
                  );
            print "\n";   ## just for HTML reading pleasure
    };
    print $cgi->end_table();
};

sub showError($) {
    my $E = shift;

    print $cgi->h2( { -style => "Color: $errorColour;" }, "Se encontr� un error !!! :-(" ), "\n",
          $cgi->h3( { -style => "Color: $errorColour;" }, $E->{-text} );
};

## main
################################################################################

init();
showHeader();
showSearchForm();
showResults();
showFooter();


