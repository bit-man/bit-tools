#!/usr/bin/perl

use strict;
use warnings;

use CGI;
use Error qw(:try);
use MuzikBox::Repository;

## settings
################################################################################

my $contentType = 'text/html; charset=ISO-8859-1';
my $pageTitle = 'Busc� en MuzikBox ... tu cajita de m�sica';
my $lang = 'es-AR';
my $URLprefix = '/cgi-bin/MuzikBox';
my $searchScript = $URLprefix . '/search.pl';
my $playScript = $URLprefix . '/play.pl';
my $queryFieldName = 'q';
my $queryFieldSize = 50;
my $submitValue = 'Buscar';
my $submitName = 'search';
my $maxCGIpost = 1000;
my $CGIuploadsDisable = 1;
my $errorColour = 'red';

## play settings
################################################################################

my $defaultContentType = 'application/octet-stream';

## Internal variables and settings
## NON PLUS ULTRA !!!!
################################################################################

my $cgi = new CGI;

## subroutines
################################################################################

sub init() {
    $CGI::POST_MAX = $maxCGIpost;
    $CGI::DISABLE_UPLOADS = $CGIuploadsDisable;
};

sub sendFile() {
    try {
        my $h = MuzikBox::Repository->new();
        $h->open();
        my $fo =  $h->getFileObject( 'id' => $cgi->param('id') );
        my $fileContentType = $fo->getAttribute('Content_Type') || $defaultContentType;
        my $file = $h->getFile( 'id' => $cgi->param('id') );

        print $cgi->header( -type => $fileContentType,
                            -Content_Diposition => 'attachment; filename="' . $fo->getAttribute('Filename') . '"',
                            -Content_Length => $fo->getAttribute('Size') );
        print $file;
        $h->close();
        showFoundFile( $h );
    }
    catch Error::Simple with {
        my $E = shift;
        showError( $E );
    };
};

sub showError($) {
    my $E = shift;
    
    print $cgi->header( -type => $contentType );
    print $cgi->start_html( -title => $pageTitle, 
                            -author => 'victor@bit-man.com.ar',
                            -lang => $lang );
    print $cgi->h2( { -style => "Color: $errorColour;" }, "Se encontr� un error !!! :-(" ),
          $cgi->h3( { -style => "Color: $errorColour;" }, $E->{-text} );
    print $cgi->end_html;
};

## main
################################################################################

init();
sendFile();


