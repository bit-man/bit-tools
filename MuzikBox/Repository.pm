package MuzikBox::Repository;

use strict;
use warnings;

use Error;
use DBI;

use MuzikBox::File;
 
use Exporter();
our (@ISA, @EXPORT);
@ISA = qw(Exporter);
@EXPORT = qw(new open search getFile close);

use constant DEBUG_STR => "debug (" . __PACKAGE__ . ") => ";

use constant CANT_OPEN_DB => 1;
use constant CANT_OPEN_DB_T => "Can't open database";
use constant CANT_PREPARE => 2;
use constant CANT_PREPARE_T => "Can't prepare database search";
use constant NO_TEXT => 3;
use constant NO_TEXT_T => "There's no text to search for";
use constant CANT_EXECUTE => 4;
use constant CANT_EXECUTE_T => "Can't execute query";
use constant CANT_FETCH => 5;
use constant CANT_FETCH_T => "Can't fetch records from database";
use constant NO_ID => 6;
use constant NO_ID_T => "There's no file id";


=pod

=head1 NAME

MuzikBox::Repository - Manage the data repository for MuzikBox

=head1 SYNOPSIS

 use Error qw(:try);
 
 try {
     my $h = MuzikBox::Repository->new();
     $h->open();
     $hRef = $h->search( 'text' => 'mp3' );
     $h->close();
 }
 catch Error::Simple with {
     my $E = shift;
     print "An error has occurred !!! :-(\n"
         . "    " . $E->{-text} . "\n\n";
 };

=head1 DESCRIPTION 

This module manages the relationship between the UI and the storages that comprise
the media repositories (CDs, DVDs, HDs, etc.) and it's metadata (database).

=head1 TO DO

=over 1

=item * UNWIRE the settings inside new(). XML ??

=item * Handle DBI errors, adding an extra attribute to Error::Simple to retrive db errors

=back


=head1 METHODS

All methods return an exception in case of error. It's generated and handled
using the Error module, and the exception object is Error::Simple.

=head2 new

Creates a new MuzikBox::Repository object

=cut

sub new {
    my $class = shift;
    my %option = @_;

    my $self;
    $self->{'db'} = 'MuzikBox';
    $self->{'host'} = '127.0.0.1';
    $self->{'user'} = 'root';
    $self->{'pwd'} = 'piringundin';
    $self->{'port'} = 3306;
    $self->{'mount'} = '/mnt/cdrom';
    $self->{'mountWait'} = 10;

    ## Database column to use when retrieving records from database
    $self->{'key_field'} = 'id';

    return bless $self, $class;
};

=pod 

=head2 open

Opens a connection to  MuzikBox the MuzikBox repository

=cut

sub open {
    my ($self) = @_;

    $self->{'dsn'} = 'DBI:mysql:database=' . $self->{'db'} 
                   . ';host=' . $self->{'host'}
                   . ';port=' . $self->{'port'};

    $self->{'dbh'} = DBI->connect( $self->{'dsn'}, $self->{'user'}, $self->{'pwd'},
                                   { RaiseError => 1, 
                                     PrintError => 1,
                                     AutoCommit => 0 } 
                                 )
                   || throw  Error::Simple ( CANT_OPEN_DB_T, CANT_OPEN_DB );

};

=pod 

=head2 search

Searches the repository for the passed text. Returns a reference to a hash 
containing a key for each distinct value of the 'id' column that was fetched. 
For each key the corresponding value is a reference to a hash containing all the
selected columns and their values.

Take a look at method "fetchall_hashref" from module DBI for further info about
the returned value.

=cut

sub search {
    my $self = shift;
    my %option = @_;

    throw  Error::Simple ( NO_TEXT_T, NO_TEXT )
        unless $option{'text'};
    my $text = '%' . $option{'text'} . '%';

    _prepare($self);

    $self->{'sth'}->execute($text,$text,$text)
        || throw Error::Simple ( CANT_EXECUTE_T, CANT_EXECUTE );

    $self->{'result'} = $self->{'sth'}->fetchall_hashref( $self->{'key_field'} )
                                        || throw Error::Simple ( CANT_FETCH_T, CANT_FETCH );

    return $self->{'result'};
};

=pod 

=head2 getFile

retrieves a Repository::File object from the MuzikBox repository

=cut

sub getFile {
    my $self = shift;
    my %option = @_;

    my $fileObject = _buildAndStoreFileObject( $self, $option{'id'} );

    _waitForCDMount( $self );
    
    return $fileObject->read();
};


=pod 

=head2 getFileObject

retrieves a Repository::File object from the MuzikBox repository

=cut

sub getFileObject {
    my $self = shift;
    my %option = @_;

    return _buildAndStoreFileObject( $self, $option{'id'} );
};



=pod 

=head2 close

Closes the connection with the MuzikBox repository

=cut

sub close {
    my ($self) = @_;

    $self->{'dbh'}->disconnect()
        if defined $self->{'dbh'};
};

################################################################################
######################      PRIVATED (??) METHODS       ########################
################################################################################
sub _prepare($) {
    my ($self) = @_;

    ## B�squeda de archivos por nombre, nombre de media (CD, DVD, etc.) o categor�a.

    return if defined $self->{'sth'};

    my $statement = "SELECT files.id, media.Label, Filename, Comment, Categories "
    		      . "FROM files, media WHERE ("
                  . "media.Label LIKE ? OR "
                  . "Filename LIKE ? OR "
                  . "Categories LIKE ?) "
                  . "AND files.media_id = media.id";

    $self->{'sth'} = $self->{'dbh'}->prepare( $statement )
                      || throw  Error::Simple ( CANT_PREPARE_T, CANT_PREPARE );

};


sub _buildAndStoreFileObject($$) {
    my ($self, $id) = @_;

    throw Error::Simple ( NO_ID_T, NO_ID )
        unless $id;

    return $self->{'fileObject'}->{$id}
        if defined $self->{'fileObject'}->{$id};

    my $fileObject = MuzikBox::File->new( 'id' => $id, 
                                          'dbh' => $self->{'dbh'},
                                          'mount' => $self->{'mount'}
                                        );

    $self->{'fileObject'}->{$id} = $fileObject;
    return $fileObject;
};

sub _waitForCDMount($) {
    my $self = shift;
    
    my $done = 0;
    while( ! $done ) {
        my $dh;
        opendir( $dh, $self->{'mount'} )
            || do { sleep( $self->{'mountWait'} );
                    next; };

        $done = 1;
    };
};

1;
