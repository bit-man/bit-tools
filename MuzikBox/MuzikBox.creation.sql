CREATE DATABASE MuzikBox;
GRANT ALL ON MuzikBox.* TO root@localhost;
USE MuzikBox

-- Create 'media' table

CREATE TABLE media (  id VARCHAR(30)  NOT NULL,
                      Label VARCHAR(100)  NOT NULL,
                      PRIMARY KEY (id)
) ENGINE=INNODB;

INSERT INTO media SET
    id="MEDIALIB002", Label="Media Library #02";
    
-- Create 'file' table

CREATE TABLE files (  id BIGINT UNSIGNED      NOT NULL AUTO_INCREMENT,
                      media_id VARCHAR(30)  NOT NULL,
                      Filepath VARCHAR(500)   NOT NULL,
                      Filename VARCHAR(80)    NOT NULL, 
                      Size INT UNSIGNED       NOT NULL,
                      Date_Added DATETIME     NOT NULL,
                      Categories VARCHAR(200),
                      Content_Type VARCHAR(50),
                      Comment VARCHAR(1000),
                      PRIMARY KEY (id)
) ENGINE=INNODB;

ALTER TABLE files
    ADD FOREIGN  KEY (media_id) REFERENCES media(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

LOAD DATA LOCAL INFILE   
    'E:\\_Data\\Bit-Software\\MuzikBox\\catalog\\MEDIALIB002.CSV_NEW.CSV' 
    INTO TABLE files
    FIELDS TERMINATED BY ',';

-- Create 'tracks' table (STILL IN DEVELOPMENT STAGE)

CREATE TABLE tracks ( file_id  BIGINT UNSIGNED  NOT NULL,
                      Time SMALLINT UNSIGNED,
                      Artist VARCHAR(100),
                      Album VARCHAR(200),
                      Last_Played DATETIME,
                      Play_Count INT UNSIGNED  NOT NULL
) ENGINE=INNODB;

ALTER TABLE tracks
    ADD FOREIGN  KEY (file_id) REFERENCES files(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

-- This insert should work
INSERT INTO tracks SET
    file_id=100,Play_Count=0;

-- This insert SHOULDN'T work
INSERT INTO tracks SET
    file_id=1000000,Play_Count=0;



