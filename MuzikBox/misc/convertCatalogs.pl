#!/usr/bin/perl

use strict;
use warnings;

use MIME::Types;

my $rightCommas = 4;
my $null = '\N';
my $fileNameIn = $ARGV[0];
my $fileNameOut = $ARGV[1];
my $fhIn;
my $fhOut;
my $mimetypes = MIME::Types->new;

sub checkFilenames() {
    die("Both filename must be different\n")
        if ( ! $ARGV[0] || ! $ARGV[1] 
             || ($ARGV[0] eq $ARGV[1]) );
};

sub now() {
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday) = localtime(time);
    $year += 1900;
    $mon++;
    return "$year-$mon-$mday $hour:$min:$sec";
};

sub getContentType($) {
    my $fileName = shift;

    $fileName =~ /\.(\w*)$/; ## file extension in $1
    return $mimetypes->mimeTypeOf($1) || $null;
};

sub getFileName($) {
    my $line = shift;
    
    return (split /,/, $line)[2];
};

sub addNulls($) {
    my $line = shift;
    
    $$line =~ s/,,/,$null,/;
};

sub commas($) {
    my $line = shift;
    my $commas =  (split /,/, $line) - (split /\\,/, $line) + 1;
    return $commas;
};

checkFilenames();
open( $fhIn, $fileNameIn ) || die "Can't open file $fileNameIn:\n $!";
open( $fhOut, "> $fileNameOut" ) || die "Can't open file $fileNameOut:\n $!";

my $line;

while ($line = <$fhIn>) {
    chomp $line;
    my $now = now();
    
    die( "The next line has too many (or to few) commas:\n$line\n")
        if (commas( $line ) != $rightCommas);
    addNulls( \$line );
    my $fileName = getFileName( $line );
    my $contentType = getContentType( $fileName );
    print $fhOut (",$line" . "$now,$null,$contentType,$null\n");
}

close $fhIn;
close $fhOut;

print "Done. !!!\n";
