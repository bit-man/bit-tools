package MuzikBox::File;

use strict;
use warnings;

use Error;
use DBI;

use Exporter();
our (@ISA, @EXPORT);
@ISA = qw(Exporter);
@EXPORT = qw(new read getAttribute);

my @validAttr = ( 'id', 'Media_Name', 'Categories', 'Filename', 'Filepath',
                  'Size', 'Content_Type', 'Comment' );

use constant DEBUG_STR => "debug (" . __PACKAGE__ . ") => ";

use constant TRUE => 1;
use constant FALSE => 0;

use constant NO_ID => 101;
use constant NO_ID_T => "There's no file id";
use constant NO_DBH => 102;
use constant NO_DBH_T => "There's no database handler";
use constant CANT_GET_FILE_ATTR => 103;
use constant CANT_GET_FILE_ATTR_T => "Can't retrieve attributes for the desired file id";
use constant NO_MOUNT => 104;
use constant NO_MOUNT_T => "No mount point was specified";
use constant NO_FOPEN => 105;
use constant NO_FOPEN_T => "Can't open file";
use constant NO_VALID_ATTR => 106;
use constant NO_VALID_ATTR_T => "Invalid attribute";




=pod

=head1 NAME

WWW::Yahoo - Yahoo! infrastructure access methods and base class

=head1 SYNOPSIS

 my $yahoo = WWW::Yahoo->new( errStr => \$errorString )
    || die ( "Can't instantiate WWW::Yahoo : $errorString" );
 $yahoo->login( user => $user, password => $pwd )
    || die("Can't login to Yahoo! : $errorString");

=head1 DESCRIPTION 

This one is a package whose purpose is to contain every single access method for
common task to the Yahoo! infrastructure, and serve as base class for all packages
that have to deal with Yahoo!

=head1 USAGE

Mainly this package is used as a base class for any interaction with Yahoo! through
the Internet, and is derived from WWW::Mechanize, that means that you can access
its methods through WWW::Yahoo

=head1 METHODS

All methods return 1 on success or 0 in failure, and in such case the variable
'errStr' passed to the constructor new() will be filled with an error description.

=head2 new

Create a new WWW::Yahoo::Groups::Pendings robot.

 my $yahoo = WWW::Yahoo->new(
            errStr => \$errorString, 
            debug => 1
        );

=head3 options

=over 1

=item * errStr

mandatory option (not an optiona at all ;-D ) which establishes
the refrence toa variable which will contain the error messages in case of error.

=item * debug

set the flag to allow printing of debug messages 

=back

=cut

sub new {
    my $class = shift;
    my %option = @_;

    my $self;

    throw Error::Simple ( NO_ID_T, NO_ID )
        unless $option{'id'};
    throw Error::Simple ( NO_DBH_T, NO_DBH )
        unless $option{'dbh'};
    throw Error::Simple ( NO_MOUNT_T, NO_MOUNT )
        unless $option{'mount'};

    $self->{'dbh'} =  $option{'dbh'};
    $self->{'id'} =  $option{'id'};
    $self->{'mount'} = $option{'mount'};

    my $attr = _getAttributes( $self );
    _storeAttrInObject( $self, $attr );

    return bless $self, $class;
};

sub read {
    my ($self) = @_;
    
    my $fh;
    my $fileName = $self->{'mount'} . $self->{'Filepath'} . '/' . $self->{'Filename'};

    open( $fh, $fileName ) ||
        throw Error::Simple ( NO_FOPEN_T, NO_FOPEN );

    
    binmode($fh); ## Just in case, for old systems.
    local $/;     ## Slurp mode enabled
    return <$fh>;
};

sub getAttribute {
    my ($self, $attr) = @_;

    throw Error::Simple ( NO_VALID_ATTR_T, NO_VALID_ATTR )
        unless _validAttribute( $attr );
    
    return $self->{ $attr };
};

################################################################################
######################      PRIVATED (??) METHODS       ########################
################################################################################

sub _getAttributes {
    my $self = shift;

    my $statement = "SELECT * FROM files where id=?";
    my %driverAttributes;
    my @bindValues = ( $self->{'id'} );
    my $res = $self->{'dbh'}->selectall_hashref( $statement, 'id', \%driverAttributes, @bindValues )
            || throw Error::Simple ( CANT_GET_FILE_ATTR_T, CANT_GET_FILE_ATTR );
    
    return $res;
};

sub _storeAttrInObject {
    my $self = shift;
    my $attr = shift;

    my $id = $self->{'id'};

    foreach my $col ( keys %{ $attr->{$id} } ) {
            next if ($col eq "id");
            $self->{$col} = $attr->{$id}->{$col};
    };
};

sub _validAttribute($) {
    my $attr = shift;
    
    foreach (@validAttr) {
        return TRUE
            if ($_ eq $attr);
    };
    
    return FALSE;
};


1;
