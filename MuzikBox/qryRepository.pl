#!/usr/bin/perl

use lib '..';

use strict;
use warnings;

## Settings
##################################################################

## main
##################################################################

my $hash_ref;
my $h;
##################################################################

use Error qw(:try);
use MuzikBox::Repository;

die ( "usage:\n        $0 text_to_search_for\n" )
    if ! $ARGV[0];

try {
     $h = MuzikBox::Repository->new();
     $h->open();
     $hash_ref = $h->search( 'text' => $ARGV[0] );
     $h->close();
}
catch Error::Simple with {
     my $E = shift;
     $h->close();
     print "An error has occurred !!! :-(\n"
         . "    " . $E->{-text} . "\n\n";
};

foreach my $id ( keys %$hash_ref ) {
    print "($id) ". $hash_ref->{$id}->{'Filename'}  ."\n";
    next;
    
    foreach my $col ( keys %{ $hash_ref->{$id} } ) {
        next if ($col eq "id");
        my $value = $hash_ref->{$id}->{$col};
        print "    $col: $value\n";
    };
};

