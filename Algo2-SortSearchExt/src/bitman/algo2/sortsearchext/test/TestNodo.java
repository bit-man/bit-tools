/**
 * 
 */
package bitman.algo2.sortsearchext.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import bitman.algo2.sortsearchext.Nodo;

/**
 * @author bit-man
 *
 */
public class TestNodo {

	private static final int	VALOR_3	= 3;
	private static final int	VALOR_2	= 2;
	private static final int	VALOR_1	= 1;
	private static final int	VALOR_0	= 0;
	private Nodo n;

	@Before
	public void setup() {
		n = new Nodo();
	}
	/**
	 * Test method for {@link bitman.algo2.sortsearchext.Nodo#Nodo()}.
	 */
	@Test
	public void testNodo() {
		assertTrue( n.getTipo() == 1);
		assertTrue( todosNull(n.hijo) );
	}


	/**
	 * Test method for {@link bitman.algo2.sortsearchext.Nodo#getTipo()}.
	 */
	@Test
	public void testGetTipo() {
		assertTrue( n.getTipo() == 1);
		n.agregarValor(VALOR_1);
		assertTrue( n.getTipo() == 2);
		n.agregarValor(VALOR_1);
		assertTrue( n.getTipo() == 3);
		n.agregarValor(VALOR_1);
		assertTrue( n.getTipo() == 4);	
	}

	/**
	 * Test method for {@link bitman.algo2.sortsearchext.Nodo#agregarValor(int)}.
	 */
	@Test
	public void testAgregarValor() {
		assertTrue( n.getTipo() == 1);
		
		n.agregarValor(VALOR_1);
		assertTrue( n.getTipo() == 2);
		assertTrue( arrayValoresEnOrdenAscendente(n) );
		
		n.agregarValor(VALOR_2);
		assertTrue( n.getTipo() == 3);
		assertTrue( arrayValoresEnOrdenAscendente(n) );
		
		n.agregarValor(VALOR_3);
		assertTrue( n.getTipo() == 4);	
		assertTrue( arrayValoresEnOrdenAscendente(n) );
	}

	@Test
	public void testAgregarHijoMayor() {
		n.agregarValor(VALOR_2);
		n.agregarValor(VALOR_3);
		
		Nodo n2Mayor = new Nodo();
		Nodo n2Menor = new Nodo();
		Nodo n3Mayor = new Nodo();

		n.agregarHijoMayor(n2Mayor, VALOR_2);
		assertTrue( n.hijo[0] == null );
		assertTrue( n.hijo[1] == n2Mayor );
		assertTrue( n.hijo[2] == null );
		assertTrue( n.hijo[3] == null );
		
		n.agregarHijoMenor(n2Menor, VALOR_2);
		assertTrue( n.hijo[0] == n2Menor );
		assertTrue( n.hijo[1] == n2Mayor );
		assertTrue( n.hijo[2] == null );
		assertTrue( n.hijo[3] == null );
		
		n.agregarHijoMayor(n3Mayor, VALOR_3);
		assertTrue( n.hijo[0] == n2Menor );
		assertTrue( n.hijo[1] == n2Mayor );
		assertTrue( n.hijo[2] == n3Mayor );
		assertTrue( n.hijo[3] == null );
		

		n.agregarValor(VALOR_1);
		assertTrue( n.hijo[0] == null );
		assertTrue( n.hijo[1] == n2Menor );
		assertTrue( n.hijo[2] == n2Mayor );
		assertTrue( n.hijo[3] == n3Mayor );
		
		Nodo n1Menor = new Nodo();
		n.agregarHijoMenor(n1Menor, VALOR_1);
		assertTrue( n.hijo[0] == n1Menor );
		assertTrue( n.hijo[1] == n2Menor );
		assertTrue( n.hijo[2] == n2Mayor );
		assertTrue( n.hijo[3] == n3Mayor );
	}
	
	/**
	 * @param n
	 */
	private void mostrarValores(Nodo n, String msg) {
		System.out.print(msg + " - Valores : ");
		for(int i=0; i < n.proxValor; i++)
			System.out.print(n.valor[i] + " ");
		System.out.println();
	}

	/**
	 * @param n
	 * @return
	 */
	private boolean arrayValoresEnOrdenAscendente(Nodo n) {
		for( int i = 0; i < n.proxValor - 1; i ++ )
			if ( n.valor[i] > n.valor[i+1] )
				return false;

		return true;
	}
	
	/**
	 * @param hijo
	 * @return
	 */
	private boolean todosNull(Nodo[] hijo) {
		for( int i = 0; i < hijo.length; i++)
			if ( hijo[i] != null )
				return false;

		return true;
	}


}
