/**
 * 
 */
package bitman.algo2.sortsearchext;

/**
 * @author bit-man
 *
 */
public class Nodo {
	/**
	 * Máxima cantidad de valores que puede albergar el nodo
	 */
	public static final int	MAX_CANT_VALORES	= 3;
	
	private int tipo;
	public int proxValor;
	public int[] valor;
	public Nodo[] hijo;

	public Nodo() {
		tipo = 1;
		proxValor = 0;
		valor = new int[MAX_CANT_VALORES];
		hijo = new Nodo[MAX_CANT_VALORES+1];
		java.util.Arrays.fill(hijo, null);
	}
	
	public int getTipo()  {
		return tipo;
	}
	
	public void agregarValor( int v ) {
		valor[proxValor] = v;
		tipo++;
		proxValor++;
		ordenarEnFormaAscendente();
		return;
	}
	
	/***
	 * Agrega el nodo 'h' como hijo mayor del valor 'v'
	 * @param h: hijo mayor del valor 'v'
	 * @param v: valor al que se le va a gregar un hijo
	 */
	public void agregarHijoMayor( Nodo h, int v) {
		int i = indiceDelValor(v);
		hijo[i+1] = h;
	}
	
	/***
	 * Agrega el nodo 'h' como hijo menor del valor 'v'
	 * @param h: hijo mayor del valor 'v'
	 * @param v: valor al que se le va a gregar un hijo
	 */
	public void agregarHijoMenor( Nodo h, int v) {
		int i = indiceDelValor(v);
		hijo[i] = h;
	}

	/**	
	 * Busca en que posición del array valores se encuentra el valor 'v'.
	 * Este valor debe estar en el array. 
	 * @param v: valor a buscar
	 * @return devuelve el índice en el que se encuentra el valor 'v'
	 */
	private int indiceDelValor(int v) {
		int ret = 0;
		for(int i=0; i < proxValor; i++)
			if ( valor[i] == v)
				ret = i;

		return ret;
	}

	/**
	 * 
	 */
	private void ordenarEnFormaAscendente() {
		int i = proxValor-1; // donde se insertó el último valor ??
		while( i > 0 && valor[i-1] > valor[i]) {
			swapValores(i);
			swapHijos(i+1);
			
			if (i == 1)
				swapHijos(i);
			
			i--;
		}
	}

	/**
	 * @param i
	 */
	private void swapValores(int i) {
		int aux = valor[i];
		valor[i] = valor[i-1];
		valor[i-1] = aux;
	}
	
	/**
	 * @param i
	 */
	private void swapHijos(int i) {
		Nodo aux = hijo[i];
		hijo[i] = hijo[i-1];
		hijo[i-1] = aux;
	}

	// ----------------------------------------------------------------------------
	


}
