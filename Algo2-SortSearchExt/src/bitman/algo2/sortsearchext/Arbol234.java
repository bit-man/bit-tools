/**
 * 
 */
package bitman.algo2.sortsearchext;

/**
 * Implementación básica de un árbol 234 para enteros (int)
 * @author bit-man
 * Nota : hay muchos métodos que deberían ser privados pero son públicos 
 * para poder hacer un testing progresivo de la clase a medida que se va 
 * construyendo. Estos métodos comienzan con el símbolo '_'
 */
public class Arbol234 {

	private Nodo raiz;

	public Arbol234() {
		raiz = null;
	}
	
	public void agregar( int v ) {
		if (raiz == null) {
			raiz = new Nodo();
			raiz.agregarValor(v);
			return;
		}
		
		agregarNodo(v);
	}
	
	/**
	 * @param v
	 */
	private void agregarNodo(int v) {
		Nodo nodo = raiz;
		Nodo padre = null;
		boolean fueAgregado = false;

		while( ! fueAgregado ) {
			if (nodo.getTipo() == 4) {
				/***
				 * Al partir el nodo, uno de los valores (el del medio)
				 * se debe colocar en el padre, por lo tanto ya no se
				 * por cuál de las dos partes del nodo 'este' buscar.
				 * Lo soluciono buscando nuevamente desde el padre.
				 * Esto no agrega complejidad ya que se hace en O(1) 
				 */
				_partirNodo4(nodo, padre);
				nodo = _decimePorDondeSigoBuscando(v,padre);
			}
			
			if ( _perteneceAlNodo(nodo, v)) {
				/**
				 * Al agregar 'v' al nodo 'este' ocurre que puede convertirse
				 * en un nodo 4, pero no es problema porque la próxima vez que
				 * agregue algo, cuando llegue a este nodo lo voy a partir.
				 * No es necesario hacerlo esta vez
				 */
				_agregarValorAlNodo(v, nodo);
				fueAgregado = true;
			} else {
				padre = nodo;
				nodo = _decimePorDondeSigoBuscando(v,nodo);
			}
		}
	}

	/**
	 * Dado un valor 'v', me devuelve cuál nodo hijo es raiz del árbol en el
	 * cual es posible que esté 'v', y del cual 'nodo' es el padre
	 * padre, 
	 * @param v: valor a buscar
	 * @param nodo: padre del árbol
	 * @return
	 */
	private Nodo _decimePorDondeSigoBuscando(int v, Nodo nodo) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Agrega el valor 'v' al nodo 'nodo'.
	 * @param v: valor a insertar
	 * @param nodo: nodo en el cual va a insertarse el valor
	 */
	private void _agregarValorAlNodo(int v, Nodo nodo) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Busca si el valor 'v' prtenece al nodo 'nodo'
	 * @param nodo: nodo a usar para la búsqueda
	 * @param v: valor buscar
	 * @return Devuelve si 'v' pertenece o no a 'nodo'
	 */
	private boolean _perteneceAlNodo(Nodo nodo, int v) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Dado que 'nodo' es un nodo con 4 hijos, lo parte en 2 nodos
	 * de 2 hijos
	 * @param nodo: nodo a partir en dos 
	 * @param padre: padre de 'nodo'
	 */
	private void _partirNodo4(Nodo nodo, Nodo padre) {
		if (padre == null) { // signifique que nodo == raiz !!
			Nodo menor = new Nodo();
		//	menor.agregarValor( nodo.getValorMenor() );
		//	menor.setHijoMenor( nodo.getHijoMenorMenor() );
		//	menor.setHijoMayor( nodo.getHijoMenor() );
			
			Nodo mayor = new Nodo();
		//	mayor.agregarValor( nodo.getValorMayor() );
		//	mayor.setHijoMenor( nodo.getHijoMayor );
		//	mayor.setHijoMayor( nodo.getHijoMayorMayor );
			
		//	raiz = new Nodo( nodo.getValorMedio() );
		//	raiz.setHijoMenor( menor );
		//	raiz.setHijoMayor( mayor );
		} else {
			/***
			 * En este caso ocurre que el nodo padre ya sea
			 * un nodo con 4 hijos, debido a que ya se partió en dos 
			 * en caso que hubiera sido un nodo 4
			 */
		}
		
	}

	public String toString() {
		String ret = new String();
		
		if (raiz == null)
			return ret;
		
		return ret;
	}
}
