/**
 * 
 */
package micar.entities.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import micar.entities.Util;

import org.junit.Test;

/**
 * @author bit-man
 * 
 */
public class UtilEstimatedKm
{
    @Test
    public void minimal()
    {
        List<Integer> kmL = new ArrayList<Integer>();
        kmL.add(new Integer(100));
        kmL.add(new Integer(200));
        kmL.add(new Integer(300));

        long now = System.currentTimeMillis();
        List<Date> dateL = new ArrayList<Date>();
        dateL.add(new Date(now - 300));
        dateL.add(new Date(now - 200));
        dateL.add(new Date(now - 100));

        /***
         * the meaning of kmL and dateL is that each 100 msecs the car runs for
         * 100 km, meaning that 100 msecs from the last timestamp it will run
         * for 100 more kms => the total ksm run should be 400
         */

        Date nowDate = new Date(now);
        assertEquals(new Integer(400), Util.estimatedKm(kmL, dateL, nowDate));
    }
}
