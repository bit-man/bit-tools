/**
 * 
 */
package micar.entities.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import micar.entities.Util;

import org.junit.Test;

/**
 * @author bit-man
 * 
 */
public class UtilAverage
{
    @Test
    public void same()
    {
        List<Integer> kmL = new ArrayList<Integer>();
        kmL.add(new Integer(10));
        kmL.add(new Integer(10));
        kmL.add(new Integer(10));

        assertEquals(new Integer(10), Util.calcAverage(kmL));
    }

    @Test
    public void different()
    {
        List<Integer> kmL = new ArrayList<Integer>();
        kmL.add(new Integer(10));
        kmL.add(new Integer(20));
        kmL.add(new Integer(30));

        assertEquals(new Integer(20), Util.calcAverage(kmL));
    }

    @Test
    public void rounded()
    {
        List<Integer> kmL = new ArrayList<Integer>();
        kmL.add(new Integer(10));
        kmL.add(new Integer(20));
        kmL.add(new Integer(31));

        assertEquals(new Integer(20), Util.calcAverage(kmL));
    }

    @Test
    public void rounded2()
    {
        List<Integer> kmL = new ArrayList<Integer>();
        kmL.add(new Integer(10));
        kmL.add(new Integer(20));
        kmL.add(new Integer(34));

        assertEquals(new Integer(21), Util.calcAverage(kmL));
    }
}
