/**
 * 
 */
package micar.entities.test.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author bit-man
 * 
 */
@Entity
@Table(name = "ITEM")
public class Item implements Serializable
{
    @Id
    @Column(name = "ID")
    private int    id;

    @Column(name = "DESCRIPTION")
    private String descripton;

    public Item()
    {
    };

    /**
     * @param descripton
     *            the descripton to set
     */
    public void setDescripton(String descripton)
    {
        this.descripton = descripton;
    }

    /**
     * @return the descripton
     */
    public String getDescripton()
    {
        return descripton;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * @return the id
     */
    public int getId()
    {
        return id;
    }
}
