/**
 * 
 */
package micar.entities.test.infrastructure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

/**
 * @author bit-man
 * 
 */
public class SQLite
{

    @Test
    public void gettingStarted() throws ClassNotFoundException, SQLException
    {
        String[] name = new String[] { "Gandhi", "Turing", "Wittgenstein" };
        String[] occupation = new String[] { "politics", "computers", "smartypants" };

        // TODO add property to set JDBC class
        Class.forName("org.sqlite.JDBC");

        // TODO add property to set JDBC URL
        Connection conn = DriverManager.getConnection("jdbc:sqlite:/tmp/test.db");
        assertNotNull(conn);

        Statement stat = conn.createStatement();
        assertNotNull(stat);

        stat.executeUpdate("drop table if exists people;");
        stat.executeUpdate("create table people (name, occupation);");

        PreparedStatement prep = conn.prepareStatement("insert into people values (?, ?);");
        assertNotNull(prep);

        for (int i = 0; i < 3; i++)
        {
            prep.setString(1, name[i]);
            prep.setString(2, occupation[i]);
            prep.addBatch();
        }

        conn.setAutoCommit(false);
        int[] result = prep.executeBatch();
        // TODO assert on results value too
        assertEquals("There was more executiopns that planned", 3, result.length);

        conn.setAutoCommit(true);

        ResultSet rs = stat.executeQuery("select * from people;");
        assertNotNull(rs);

        int nRecord = 0;
        while (rs.next())
        {
            assertEquals("Names doesn't match", name[nRecord], rs.getString("name"));
            assertEquals("Occupations doesn't match", occupation[nRecord++], rs.getString("occupation"));
        }
        rs.close();
        conn.close();
    }
}
