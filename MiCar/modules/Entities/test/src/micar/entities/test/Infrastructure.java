/**
 * 
 */
package micar.entities.test;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author bit-man
 * 
 */
public class Infrastructure
{

    public static Test suite()
    {
        TestSuite suite = new TestSuite("Test for micar.entities");
        // $JUnit-BEGIN$
        //
        // suite.addTest(Hibernate.class);

        // $JUnit-END$
        return suite;
    }

}
