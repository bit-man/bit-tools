/**
 * 
 */
package micar.entities.test.infrastructure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import micar.entities.test.entities.Item;

import org.junit.Test;

/**
 * @author bit-man
 * 
 */
public class Hibernate
{
    private static final String YES      = "true";
    private static final String SHOW_SQL = "hibernate.show_sql";
    private static final String PU       = "micarPU";
    private static final int    ITEM_ID  = 1;

    @Test
    public void basic()
    {

        Map<String, String> props = new HashMap<String, String>();
        props.put(SHOW_SQL, YES); // shows the SQL queries being executed
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(PU, props);

        EntityManager em = emf.createEntityManager(); // Retrieve an application
        // managed entity manager

        safeDeleteItem(em, ITEM_ID);
        Item it = addItem(em, ITEM_ID);
        Item it2 = em.find(Item.class, new Integer(ITEM_ID));

        assertNotNull("No se encontró", it2);
        assertEquals("Son diferentes", it, it2);

        em.close();
        emf.close(); // close at application end

    }

    /**
     * @param em
     * @param i
     * @return
     */
    private Item addItem(EntityManager em, int i)
    {
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        Item it = new Item();
        it.setId(i);
        it.setDescripton("TEST DESCRIPTION");

        em.persist(it);

        tx.commit();
        return it;
    }

    /**
     * @param em
     * @param i
     * 
     */
    private void safeDeleteItem(EntityManager em, int i)
    {
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        Item it = em.find(Item.class, new Integer(i));

        if (it != null)
        {
            em.remove(it);
        }

        tx.commit();

    }
}
