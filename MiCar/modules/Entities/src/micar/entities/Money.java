/**
 * 
 */
package micar.entities;

/**
 * @author bit-man
 * 
 */
public class Money
{
    private int money;
    private int cents;

    public Money(int money, int cents)
    {
        this.setMoney(money);
        this.setCents(cents);
    }

    public Money(int money)
    {
        this(money, 0);
    }

    public Money()
    {
        this(0, 0);
    }

    /**
     * @param money the money to set
     */
    public void setMoney(int money)
    {
        this.money = money;
    }

    /**
     * @return the money
     */
    public int getMoney()
    {
        return money;
    }

    /**
     * @param cents the cents to set
     */
    public void setCents(int cents)
    {
        this.cents = cents;
    }

    /**
     * @return the cents
     */
    public int getCents()
    {
        return cents;
    }

}
