/**
 * 
 */
package micar.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author bit-man
 * 
 */
public class Maint
{

    private boolean    scheduled;
    private Date       date;
    private int        odometer;
    private Money      price;
    private String     comment;
    private String     name;
    private List<Task> tasks;

    public Maint(boolean scheduled, Date date, int odometer, Money price, String comment, String name)
    {
        this.scheduled = scheduled;
        this.date = date;
        this.odometer = odometer;
        this.price = price;
        this.comment = comment;
        this.name = name;
        this.tasks = new ArrayList<Task>();
    }

    /**
     * @param scheduled
     *            the scheduled to set
     */
    public void setScheduled(boolean scheduled)
    {
        this.scheduled = scheduled;
    }

    /**
     * @return the scheduled
     */
    public boolean isScheduled()
    {
        return scheduled;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setDate(Date date)
    {
        this.date = date;
    }

    /**
     * @return the date
     */
    public Date getDate()
    {
        return date;
    }

    /**
     * @param odometer
     *            the odometer to set
     */
    public void setOdometer(int odometer)
    {
        this.odometer = odometer;
    }

    /**
     * @return the odometer
     */
    public int getOdometer()
    {
        return odometer;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(Money price)
    {
        this.price = price;
    }

    /**
     * @return the price
     */
    public Money getPrice()
    {
        return price;
    }

    /**
     * @param comment
     *            the comment to set
     */
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    /**
     * @return the comment
     */
    public String getComment()
    {
        return comment;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param tasks
     *            the tasks to set
     */
    public void setTasks(List<Task> tasks)
    {
        this.tasks = tasks;
    }

    /**
     * @return the tasks
     */
    public List<Task> getTasks()
    {
        return tasks;
    }

}
