/**
 * 
 */
package micar.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author bit-man
 * 
 */
public class Util
{

    public static Integer estimatedCurrentKm(List<Integer> kmL, List<Date> dateL)
    {
        Date now = new Date(System.currentTimeMillis());
        return estimatedKm(kmL, dateL, now);
    }

    /***
     * Calculates estimated current kilometers
     * 
     * @param kmL
     *            : kilometer mark. Must be ordered from lower to higher km
     *            mark, meaning kmL.get(i) < kmL.get(i+1)
     * @param dateL
     *            : date of each corresponding kilometer mark. Must be ordered
     *            from older date to new date, meaning dateL.get(i) <
     *            dateL.get(i+1)
     * @param timeStamp
     *            : time to calculate kms for
     * @return the estimated kilometer mark at the timeStamp time
     */
    public static Integer estimatedKm(List<Integer> kmL, List<Date> dateL, Date timeStamp)
    {
        Date dateLastElem = dateL.get(dateL.size() - 1);

        assert (kmL.size() == dateL.size());
        assert (timeStamp.getTime() > dateLastElem.getTime());

        List<Integer> kmAvgL = averageKmOverTime(kmL, dateL);

        Integer avgKmOverTime = calcAverage(kmAvgL);
        long deltaTime = timeStamp.getTime() - dateLastElem.getTime();
        long lastKm = kmL.get(kmL.size() - 1);
        return new Integer((int) (lastKm + avgKmOverTime * deltaTime));
    }

    /**
     * Calculate the average for each time period between marks
     * 
     * @param kmL
     * @param dateL
     * @return average kms per time period, meaning that if each list contains
     *         'n' elements there will be 'n-1' time periods
     */
    private static List<Integer> averageKmOverTime(List<Integer> kmL, List<Date> dateL)
    {
        int i = 0;
        Date lastDate = dateL.get(0);
        Integer lastKm = kmL.get(0);
        List<Integer> kmp = new ArrayList<Integer>(kmL.size());
        for (Integer km : kmL)
        {
            if (i != 0)
            {
                long deltaTime = dateL.get(i).getTime() - lastDate.getTime();
                Integer deltaKm = km - lastKm;
                kmp.add(new Integer((int) (deltaKm / deltaTime)));
                lastDate = dateL.get(i);
                lastKm = km;
            }
            i++;
        }
        return kmp;
    };

    // ToDo research on usage of Integer/int vs Km
    public static Integer calcAverage(List<Integer> kmL)
    {
        assert (kmL.size() != 0);

        Integer prom = 0;

        for (Integer km : kmL)
            prom += km;

        assert (prom > 0);

        return prom / kmL.size();
    }
}
