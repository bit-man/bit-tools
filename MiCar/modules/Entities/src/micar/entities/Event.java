/**
 * 
 */
package micar.entities;

import java.util.Date;

/**
 * @author bit-man
 * 
 */
public class Event
{
    private boolean scheduled;
    private Date    date;
    private Maint   maint;
    /**
     * @param maint the maint to set
     */
    public void setMaint(Maint maint)
    {
        this.maint = maint;
    }
    /**
     * @return the maint
     */
    public Maint getMaint()
    {
        return maint;
    }
    /**
     * @param date the date to set
     */
    public void setDate(Date date)
    {
        this.date = date;
    }
    /**
     * @return the date
     */
    public Date getDate()
    {
        return date;
    }
    /**
     * @param scheduled the scheduled to set
     */
    public void setScheduled(boolean scheduled)
    {
        this.scheduled = scheduled;
    }
    /**
     * @return the scheduled
     */
    public boolean isScheduled()
    {
        return scheduled;
    }
}
