﻿using NSubstitute;
using NUnit.Framework;

namespace Testing101
{
    public class TestSubTest
    {
        [Test]
        public void SpecShipShootsAtLEastOneShot_WhenFunctionalWeaponIsEquiped_Manual()
        {
            ISpaceShip ship = new SpaceShipWithSingleWeapoinSlot();
            ship.Equip(new FunctionalWeaponStub());

            var round = ship.Shoot();

            var roundContsainsOneShoot = round.Length == 1;
            Assert.That(roundContsainsOneShoot);
        }

        [Test]
        public void SpecShipShootsAtLEastOneShot_WhenFunctionalWeaponIsEquiped_NSubstitute()
        {
            ISpaceShip ship = new SpaceShipWithSingleWeapoinSlot();

            var weapon = Substitute.For<IWeapon>();
            weapon.Shoot().Returns(new[] {new Shoot()});
            ship.Equip(weapon);

            var round = ship.Shoot();

            Assert.AreEqual(1, round.Length);
        }

        private class FunctionalWeaponStub : IWeapon
        {
            public Shoot[] Shoot()
            {
                return new[] {new Shoot()};
            }
        }
    }


}