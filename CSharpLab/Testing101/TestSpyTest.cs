﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using NSubstitute;
using NSubstitute.Core;
using NUnit.Framework;

namespace Testing101
{
    [TestFixture]
    public class TestSpyTest
    {
        [Test]
        public void OpponentGetHitInEncounter_OnAttack()
        {
            int hitCount = 0;

            var opponent = Substitute.For<ISpaceShip>();
            opponent.AcceptIncomingShots( Arg.Do<IEnumerable<Shoot>>( x => hitCount += x.Count())) ;

            var player = Substitute.For<ISpaceShip>();
            player.FunctionalWeapons.Returns(2);

            var encounter = new Encounter(player, opponent);

            encounter.Attack();

//          ToDo Make opponent mock method HitsCount() return hitCount
//            Assert.That(opponent.HitsCount(), Is.EqualTo(2));
            Assert.That(hitCount, Is.EqualTo(2));
        }


        private class Encounter
        {
            private ISpaceShip opponent;
            private ISpaceShip player;

            public Encounter(ISpaceShip player, ISpaceShip opponent)
            {
                this.player = player;
                this.opponent = opponent;
            }

            public void Attack()
            {
                var shoots = new Shoot[player.FunctionalWeapons];
                for (int i = 0; i < player.FunctionalWeapons; i++)
                {
                    shoots[i] = new Shoot();
                }
                opponent.AcceptIncomingShots(shoots);
            }
        }
    }
}