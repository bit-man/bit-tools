﻿using System.Collections.Generic;

namespace Testing101
{
    public interface IWeapon
    {
        Shoot[] Shoot();
    }

    public interface ISpaceShip
    {
        void Equip(IWeapon weapon);
        int AvailableWeaponsSlots { get; }
        int FunctionalWeapons { get; set; }
        Shoot[] Shoot();
        void AcceptIncomingShots(IEnumerable < Shoot >  shots);

        int HitsCount();
    }


    internal class SpaceShipWithSingleWeapoinSlot : ISpaceShip
    {
        private IWeapon weapon;

        public void Equip(IWeapon weapon)
        {
            this.weapon = weapon;
        }

        public int AvailableWeaponsSlots { get; }
        public int FunctionalWeapons { get; set; }

        public Shoot[] Shoot()
        {
            return weapon.Shoot();
        }

        public void AcceptIncomingShots(IEnumerable<Shoot> shots)
        {
            throw new System.NotImplementedException();
        }

        public int HitsCount()
        {
            throw new System.NotImplementedException();
        }
    }

    public class Shoot
    {
    }
}