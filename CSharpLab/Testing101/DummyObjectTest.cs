﻿using NUnit.Framework;

namespace Testing101
{
    [TestFixture]
    public class DummyObject
    {
        [Test]
        public void NonWeaponSlotsAvailable_AfterWeaponIsEquiped_DummyObjectManual()
        {
            ISpaceShip spaceship = new SpaceShipWithSingleWeapoinSlot();
            IWeapon weapon = new DummyWeapon();
            spaceship.Equip(weapon);
            var noWeaponSlotsAvailable = spaceship.AvailableWeaponsSlots == 0;
            Assert.That(noWeaponSlotsAvailable);
        }

        [Test]
        public void NonWeaponSlotsAvailable_AfterWeaponIsEquiped_DummyObjectNSubstitute()
        {
            ISpaceShip spaceship = new SpaceShipWithSingleWeapoinSlot();
            IWeapon weapon = NSubstitute.Substitute.For<IWeapon>();
            spaceship.Equip(weapon);
            var noWeaponSlotsAvailable = spaceship.AvailableWeaponsSlots == 0;
            Assert.That(noWeaponSlotsAvailable);
        }



        private class DummyWeapon : IWeapon
        {
            public Shoot[] Shoot()
            {
                throw new System.NotImplementedException();
            }
        }

    }
}