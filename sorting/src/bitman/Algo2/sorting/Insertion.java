package bitman.Algo2.sorting;

/**
 * @author bit-man
 *
 * Implements Selection sort method
 */

public class Insertion {

	public static void sort( int[] a, boolean learningFlag ) {
		sortParcial( a, 0, 0, a.length - 1);
	}
	
	private static void putItInPlace( int hereYouGo, int butYouAreIn, int[] a ) {
		int backup = a[butYouAreIn];
		for( int i = butYouAreIn; i > hereYouGo; i-- )
			a[i] = a[i-1];
		a[hereYouGo] = backup;
	};
	
	/**
	 * 
	 * Desde los índices inicio hasta medio-1 el array se encuentra ordenado, pero desde 
	 * medio hasta fin no lo está. Al finalizar el método este debe devolver el array a
	 * ordenado entre sus índices inicio y fin 
	 * 
	 * La necesidad de este método surge debido a que en el método de Divide and conquer,
	 * al momento de hacer el conquer, se tiene un array que entre dos índices se encuentra
	 * ordenado, y entre el siguiente y un tercer índice también y lo que debe obtenerse es
	 * el ordenamiento de todos los números entre el primer y tercer índice. Ideal para 
	 * aplicar Insertion sort !!
	 */
	public static void sortParcial( int[] a, int inicio, int medio, int fin ) {
		for (int i = medio; i <= fin; i++) {
			Tools.setStartingArray(a);
			int thisIsYourPlace = veamosDondeEncaja( i, a, inicio );
			putItInPlace( thisIsYourPlace, i, a );
			Tools.showArrayDiffs(a);
		}		
	}
	
	private static int veamosDondeEncaja( int hastaAqui, int[] a, int desdeAqui ) {
		int debeIrAqui = hastaAqui;
		boolean loEncontre = false;
		
		for (int i = desdeAqui; (i <= hastaAqui) && (! loEncontre); i++) {
			if (a [i] > a[debeIrAqui] ) {
				debeIrAqui = i;
				loEncontre = true;
			};
		}
		return debeIrAqui;
	};
}
