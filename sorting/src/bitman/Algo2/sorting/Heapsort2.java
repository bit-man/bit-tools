package bitman.Algo2.sorting;

/**
 * 
 * @author bit-man
 *
 * Esta implementación del método de Heapsort usa un sólo array (el original) siendo
 * este usado como una estructura de heap-máximo en un array desde el elemento 0 al
 * elemento ultimoElemDelHeap y desde este elemento hasta le último el array original 
 * ordenado de menor a mayor.
 * 
 * Al final del sorting el heap habrá desaparecido (ultimoElemDelHeap == 0) y el array
 * completo estará ordenadpo de menor a mayor
 */
public class Heapsort2 {
	private static int[] heapArray;
    private static int ultimoElemDelHeap;
    private static boolean estoyAprendiendo;
    
	public static void sort( int[] a, boolean learningFlag ) {
		estoyAprendiendo = learningFlag;
		heapArray = a;
		
		if (estoyAprendiendo)
			System.out.println("Comienza el array2heap()");
		array2heapMayor();

		if (estoyAprendiendo)
			System.out.println("Comienza a llenarse el array ordenado");
		/** 
		 * Paro cuando faltan 2 elementos para procesar, porque los elementos 0 y 1
		 * de heapArray forman no solo un heap sino una array ordenado de menor a mayor
		 **/
		for (int i = heapArray.length - 1; i > 1; i--) {
			sacarMayorDelHeapYPonerloAlFinal();
			if (estoyAprendiendo) {
				Tools.showArray( heapArray );
			}
		}
	}
	
	private static void sacarMayorDelHeapYPonerloAlFinal() {
		Tools.intercambiar(ultimoElemDelHeap, 0, heapArray);
		ultimoElemDelHeap--;
		
		if (ultimoElemDelHeap > 0)
			heapifyDesdeLaRaiz();
	};
	
	private static void heapifyDesdeLaRaiz() {
		int i = 0;
		while ( padreMenorQueLosHijos(i) )
			i = intercambiarConElHijoMayor(i);
	}
	
	private static boolean padreMenorQueLosHijos( int padre ) {
		if ( unHijo(padre) >= ultimoElemDelHeap &&
		     elOtroHijo(padre) >= ultimoElemDelHeap) {
			return false;
		} else if ( unHijo(padre) >= ultimoElemDelHeap ) {
			return heapArray[padre] < heapArray[elOtroHijo(padre)];
		} else if ( elOtroHijo(padre) >= ultimoElemDelHeap ) {
			return heapArray[padre] < heapArray[unHijo(padre)];
		} else {
			return heapArray[padre] < heapArray[unHijo(padre)] ||
				   heapArray[padre] < heapArray[elOtroHijo(padre)];
	    }
	}
	
	private static int unHijo( int padre ) {
		return 2 * padre + 1;
	}
	
	private static int elOtroHijo( int padre ) {
		return 2 * padre + 2;
	}
	
	// Precondición : el padre es mayor que al menos uno de los hijos
	//                padreMayorQueLosHijos( padre )
	private static int intercambiarConElHijoMayor( int padre ) {
		if ( heapArray[unHijo(padre)] < heapArray[elOtroHijo(padre)] ) {
		    Tools.intercambiar(padre, elOtroHijo(padre), heapArray);
		    return elOtroHijo(padre);
		} else {
			Tools.intercambiar(padre, unHijo(padre), heapArray);
			return unHijo(padre);
		}
	}
	
	private static void array2heapMayor() {
		for (int i = 1; i < heapArray.length; i++) {
			Tools.setStartingArray(heapArray);
			heapify(i);
			Tools.showArrayDiffs(heapArray);
		};
		ultimoElemDelHeap = heapArray.length - 1;
	}
	
	private static void heapify( int nuevoNodo ) {
		while ( ! esRaiz(nuevoNodo) && heapArray[nuevoNodo] > padre(nuevoNodo)  ) {
			Tools.intercambiar( nuevoNodo, indicePadre(nuevoNodo), heapArray );
			nuevoNodo = indicePadre(nuevoNodo);
		}
	}
	
	/**
	 *	Los hijos del nodo i son los nodos 2i+1 y 2i+2
	 *  En el ejemplo que sigue, el primer número es el 
	 *  índice del padre padre y los dos siguientes son 
	 *  los índices hijos
	 *  
	 *	0 1 2
	 *	  1    3 4
	 *      2       5 6
	 *         3         7 8
	 *           4           9 10	          
	 */
	private static int indicePadre( int i ) {
		return (i - 1)/2;
	}
	
	private static boolean esRaiz( int i ) {
		return (i == 0);
	}
	

	private static int padre( int i ) {
		return heapArray[indicePadre(i)];  
	}

}
