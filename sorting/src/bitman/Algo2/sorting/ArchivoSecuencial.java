/**
 * 
 */
package bitman.Algo2.sorting;

/**
 * @author bit-man
 *
 * Se pretende ofrecer el manejo de archivos externos de una
 * forma simple y orientada al uso en problemas de sorting :
 * - lectura en bloques
 * - cuantificación de la cantidad y tipo de accesos
 */

import java.io.*;

public class ArchivoSecuencial {
	
	private final static String DIR_TEMP = "/tmp/";
	private final static String ARCHI_NOMBRE_PREFIJO = "sort";
	private final static String ARCHI_NOMBRE_SUFIJO = ".txt";
	// TODO hacer que el tamaño del bloque se grabe en el archivo !!!
	private final static int TAM_BLOQUE_DEFAULT = 5;

	private File f;
	private FileInputStream fis = null;
	private DataInputStream dis = null;
	private FileOutputStream fos = null;
	private DataOutputStream dos = null;
	private boolean archiDeLectura;
	private int tamBloque = TAM_BLOQUE_DEFAULT;

	private int escrituras = 0;
	private int lecturas = 0;

	public ArchivoSecuencial() {
		try {
			setParams( File.createTempFile( ARCHI_NOMBRE_PREFIJO, 
											ARCHI_NOMBRE_SUFIJO, 
											new File(DIR_TEMP) ),
					   true );
		} catch ( Exception e ) {
			System.out.println( "ERROR : " + e.getMessage());
			e.printStackTrace();
		}
		
	}	
	
	public ArchivoSecuencial ( String filePath, boolean archiNuevo ) {
		setParams( new File( filePath ),
				   archiNuevo );
		if ( archiNuevo )
			crearArchiNuevo();
	};
	
	private void crearArchiNuevo() {		
		try {
			f.createNewFile();
		} catch (Exception e ) {
			System.out.println( "ERROR : " + e.getMessage());
			e.printStackTrace();
		}
	}

	private void setParams( File f, boolean archiNuevo ) {
		this.f = f;
		archiDeLectura = ! archiNuevo;

		try {
			if ( archiDeLectura ) {
				fis = new FileInputStream( f );
				dis = new DataInputStream( fis );
			} else {
				fos = new FileOutputStream( f );
				dos = new DataOutputStream( fos );
			}
		}
		catch(Exception e ) {
			System.out.println( "ERROR : " + e.getMessage());
			e.printStackTrace();
		}
	}
	

	public int getTamBloque() {
		return this.tamBloque;
	};
	
	public void setTamBloque( int tam ) {
		this.tamBloque = tam;
	};

	public void grabarBloque( int[] bloque ) {
		assert( bloque.length <= this.tamBloque && ! this.archiDeLectura );
		
		try {
			for (int i=0; i < bloque.length; i++)
				dos.writeInt(bloque[i]);
			escrituras++;
		}
		catch(Exception e ) {
			System.out.println( "ERROR : " + e.getMessage());
			e.printStackTrace();
		}
	}

	// Lee bloques de datos en forma secuencial
	public int[] leerBloque() throws EOFException {
		assert( this.archiDeLectura );

		int[] bloque = new int[tamBloque];
		int i = 0;

		try {
			while( i < tamBloque ) {
				bloque[i] = dis.readInt();
				i++;
			};
		}
		catch (EOFException e) {
			// No hay más datos !!
			if ( i == 0 )
				throw e;
			int[] bloqueAux = new int[i];
			for ( int j=0; j < i; j++)
				bloqueAux[j] = bloque[j];
			bloque = bloqueAux;
		}
		catch(Exception e ) {
			System.out.println( "ERROR : " + e.getMessage());
			e.printStackTrace();
		}

		lecturas++;
		return bloque;
		
	}

	public String getNombreArchi() {
		return f.getName();
	}
	
	public String getAbsNombreArchi() {
		return f.getAbsolutePath();
	}
	
	public int statLecturas() {
		return lecturas;
	}
	
	public int statEscrituras() {
		return escrituras;
	}
	
	public void close() {
		try {
			if ( archiDeLectura ) {
				dis.close();
			} else {
				dos.close();
			}
		} catch( Exception e ) {
			System.out.println( "ERROR : " + e.getMessage());
			e.printStackTrace();
		}
	}

}
