package bitman.Algo2.sorting;

import java.io.EOFException;

public class MergeSortExternal {
	private static final int TAM_BLOQUE = 3;
	private static final int CANT_ARCHIVOS = 2;
	private static final String ARCHI_NOMBRE_INICIAL = "inicial";
	private static final boolean NUEVO = true;
	private static boolean aprendiendo;
	
	private static String[] archivoIntermedio;
	private static int ultimoTamBloque;
	
	public static void sort( int[] a, boolean learningFlag ) {
		aprendiendo = learningFlag;
		String archivoInicial = guardar(a);
		mostrar(archivoInicial);
		archivoIntermedio = splitSortArray(archivoInicial);
		mostrarVarios( archivoIntermedio );
		
		boolean fin = false;
		while( fin ) {
			ultimoTamBloque *= 2;
			sortExt();
		};
	};
	
	/**
	 * Toma archivoIntermedio[], los combina en dos archivos nuevos con el doble
	 * de tamaño de bloque, los ordena y guarda los nombres de archivos en archivoIntermedio[]
	 * @return
	 */
	private static boolean sortExt() {
		// TODO Inspirarme en el código de splitSortArray()
		return false;
	}
	
	private static void mostrarVarios( String nombreArchivo[] ) {
		for (int i=0; i< CANT_ARCHIVOS; i++)
			mostrar(nombreArchivo[i]);
	};
	
	private static void mostrar( String nombreArchivo ) {
		if ( ! aprendiendo )
			return;

		ArchivoSecuencial a2 = new ArchivoSecuencial( nombreArchivo, ! NUEVO );
		a2.setTamBloque(TAM_BLOQUE);
		System.out.println( "Tam. bloque : " + a2.getTamBloque() );
		boolean finDeArchivo = false;
		while( ! finDeArchivo ) {	
			try {
				int[] bloque2 = a2.leerBloque();
				System.out.print( " * " );
				for( int i=0; i < bloque2.length; i++)
					System.out.print( bloque2[i] + " " );
			} catch (EOFException e) {
				finDeArchivo = true;
				System.out.println();
			}
		}

		showStats(a2);
		a2.close();
	}
	
	private static String guardar( int[] a ) {
		String nombreArchivo = ARCHI_NOMBRE_INICIAL;
		ArchivoSecuencial archi = new ArchivoSecuencial( nombreArchivo, NUEVO );
		int inicio;
		for( inicio = 0; inicio < a.length; inicio += TAM_BLOQUE) {
			int tamBloque = TAM_BLOQUE;
			if ( inicio + TAM_BLOQUE >= a.length)
				tamBloque = a.length - inicio;
			int[] bloque = new int[tamBloque];
			for (int i = 0; i < tamBloque; i++)
				bloque[i] = a[i + inicio];
			archi.grabarBloque(bloque);
		};
		showStats(archi);
		archi.close();
		return nombreArchivo;
	};
	
	private static String[] splitSortArray( String nombreArchivo ) {
		ArchivoSecuencial archi = new ArchivoSecuencial( nombreArchivo, ! NUEVO );
		ArchivoSecuencial[] archiSalida = new ArchivoSecuencial[CANT_ARCHIVOS];
		
		archi.setTamBloque(TAM_BLOQUE);
		for(int i = 0; i < CANT_ARCHIVOS; i ++) {
			archiSalida[i] = new ArchivoSecuencial();
			archiSalida[i].setTamBloque(TAM_BLOQUE);
		}
		archiSalida[1].setTamBloque(TAM_BLOQUE);
		
		boolean finDeArchivo = false;
		int i = 0;
		
		while( ! finDeArchivo ) {	
			try {
				int[] a = archi.leerBloque();
				int inicio = 0;
				int fin = inicio + a.length - 1;
				int medio = (inicio + fin) / 2;
				Insertion.sortParcial(a, inicio, medio, fin);
				
				if ( i == CANT_ARCHIVOS )
					i = 0;

				archiSalida[i++].grabarBloque(a);
			} catch (EOFException e) {
				finDeArchivo = true;
				System.out.println("EOF");
			}
		};
		String[] nombreSalida = new String[CANT_ARCHIVOS];

		for(int j = 0; j < CANT_ARCHIVOS; j++) {
			nombreSalida[j] = archiSalida[j].getAbsNombreArchi();
			archiSalida[j].close();
		};
		archi.close();
		ultimoTamBloque = TAM_BLOQUE;
		return nombreSalida;
	};
	
	private static void showStats( ArchivoSecuencial a ) {
		System.out.println( "Archivo : " + a.getAbsNombreArchi() );
		System.out.println( "Lecturas : " + a.statLecturas() );
		System.out.println( "Escrituras : " + a.statEscrituras() );
	}
}
