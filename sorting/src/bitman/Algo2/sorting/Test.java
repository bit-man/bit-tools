package bitman.Algo2.sorting;

public class Test {

	/**
	 * @param args: list of numbers to order
	 */
	
	// TODO: poner las pre-condiciones de todos los métodos
	// TODO: a todas las propiedades privadas que se llaman 'array' llamarlas 'a'
	//       y a los par�metros de los m�todos sort() llamarlos 'array'
	public static void main(String[] args) {
		int[] a = Tools.intArrayFromArgs(args, 1);

		Tools.showArray( a, "**** Before sorting : " );
		
		// TODO: implementar el flag de estoyAprendiendo en
		//       todos los métodos
		switch ( Tools.toIntSortMethod( args[0] ) ) {
		case Tools.INSERTION_SORT:
			Insertion.sort(a, false);
			break;

		case Tools.SELECTION_SORT:
			Selection.sort(a, false);
			break;

		case Tools.HEAPSORT_SORT:
			Heapsort.sort(a, true);
			break;

		case Tools.HEAPSORT2_SORT:
			Heapsort2.sort(a, true);
			break;

		case Tools.DIVIDEANDCONQUER_SORT:
			DivideAndConquer.sort(a, true);
			break;

		case Tools.QUICKSORT_SORT:
			QuickSort.sort(a, true);
			break;

		case Tools.MERGESORT_EXT_SORT:
			MergeSortExternal.sort(a, true);
			break;
			
		default: // Tools.UNKNOWN_SORT
			System.out.println("Unknown sorting method : " + args[0]);
			break; 

		};

		Tools.showArray( a, "**** After sorting : " );

	}
}
