/**
 * 
 */
package bitman.Algo2.sorting;

/**
 * @author bit-man
 *
 * Implements Selection sort method
 */
public class Selection {
	private static boolean inLearningMode = true;
	
	public static void sort( int[] a, boolean learningFlag ) {
		inLearningMode = learningFlag;
		
		for ( int i = 0; i < a.length - 1; i++) {
			Tools.setStartingArray(a);
			if (inLearningMode)
				System.out.println ("-----------------------------------------------------------");
			int smallest = chooseTheSmallestOne( a, i );
			exchangePositions( a, i, smallest );
			Tools.showArrayDiffs(a);
		}
		
		if (inLearningMode)
			System.out.println ("-----------------------------------------------------------");
	}
	
	public static int chooseTheSmallestOne( int[] a, int start ) {
		int smallest = start;
		
		if (inLearningMode)
			System.out.println ("Choosing the smallest starting from index " + start + " (" + a[start] + ")" );
		
		for ( int i = start; i < a.length; i++ ) {
			if ( a[i] < a[smallest] )
				smallest = i;
		};
		
		if (inLearningMode)
			System.out.println ("The smallest ones is at index " + smallest + " (" + a[smallest] + ")" );
		
		return smallest;
	}
	
	public static void exchangePositions( int [] a, int x, int y ) {
		if (inLearningMode)
			System.out.println ("Exchanging the element at index " + x + " (" + a[x] + ")\n" +
							    "          with the one at index " + y + " (" + a[y] + ")" );

		int aux = a[x];
		a[x] = a[y];
		a[y] = aux;
	}
}
