package bitman.Algo2.sorting;

public class Heapsort {
	private static int[] heapArray;
    private static int ultimoElemDelHeap;
    private static boolean estoyAprendiendo;
    
	public static void sort( int[] a, boolean learningFlag ) {
		estoyAprendiendo = learningFlag;
		heapArray = new int[a.length];
		System.arraycopy(a, 0, heapArray, 0, a.length);
		
		if (estoyAprendiendo)
			System.out.println("Comienza el array2heap()");
		array2heap();

		if (estoyAprendiendo)
			System.out.println("Comienza a llenarse el array ordenado");
		for (int i = 0; i < heapArray.length; i++) {
			a[i] = sacarMenorDelHeap();
			if (estoyAprendiendo) {
				Tools.showArray(heapArray, "heap : ");
				Tools.showArray(a, i, "array : ");
			}
		}
	}
	
	private static int sacarMenorDelHeap() {
		int menor = heapArray[0];
		heapArray[0] = heapArray[ultimoElemDelHeap];
		
		// Hack para evitar que siga tratando de heapify más allá
		// del final del heap (ver heapiFyDesdeLaRaiz() y padreMayorQueLosHijos(int) )
		heapArray[ultimoElemDelHeap] = Integer.MAX_VALUE;
		
		ultimoElemDelHeap--;
		if (ultimoElemDelHeap > 0)
			heapifyDesdeLaRaiz();
		
		return menor;
	};
	
	private static void heapifyDesdeLaRaiz() {
		int i = 0;
		while ( padreMayorQueLosHijos(i) )
			i = intercambiarConElHijoMenor(i);
	}
	
	private static boolean padreMayorQueLosHijos( int padre ) {
		if ( unHijo(padre) >= ultimoElemDelHeap &&
		     elOtroHijo(padre) >= ultimoElemDelHeap) {
			return false;
		} else if ( unHijo(padre) >= ultimoElemDelHeap ) {
			return heapArray[padre] > heapArray[elOtroHijo(padre)];
		} else if ( elOtroHijo(padre) >= ultimoElemDelHeap ) {
			return heapArray[padre] > heapArray[unHijo(padre)];
		} else {
			return heapArray[padre] > heapArray[unHijo(padre)] ||
				   heapArray[padre] > heapArray[elOtroHijo(padre)];
	    }
	}
	
	private static int unHijo( int padre ) {
		return 2 * padre + 1;
	}
	
	private static int elOtroHijo( int padre ) {
		return 2 * padre + 2;
	}
	
	// Precondición : el padre es mayor que al menos uno de los hijos
	//                padreMayorQueLosHijos( padre )
	private static int intercambiarConElHijoMenor( int padre ) {
		if ( heapArray[unHijo(padre)] > heapArray[elOtroHijo(padre)] ) {
		    Tools.intercambiar(padre, elOtroHijo(padre), heapArray);
		    return elOtroHijo(padre);
		} else {
			Tools.intercambiar(padre, unHijo(padre), heapArray);
			return unHijo(padre);
		}
	}
	
	private static void array2heap() {
		for (int i = 0; i < heapArray.length; i++) {
			Tools.setStartingArray(heapArray);
			heapify(i);
			Tools.showArrayDiffs(heapArray);
		};
		ultimoElemDelHeap = heapArray.length - 1;
	}
	
	private static void heapify( int i ) {
		while ( ! esRaiz(i) && heapArray[i] < padre(i)  ) {
			Tools.intercambiar( i, indicePadre(i), heapArray );
			i = indicePadre(i);
		}
	}
	
	/**
	 *	Los hijos del nodo i son los nodos 2i+1 y 2i+2
	 *  En el ejemplo que sigue, el primer número es el 
	 *  índice del padre padre y los dos siguientes son 
	 *  los índices hijos
	 *  
	 *	0 1 2
	 *	  1    3 4
	 *      2       5 6
	 *         3         7 8
	 *           4           9 10	          
	 */
	private static int indicePadre( int i ) {
		return (i - 1)/2;
	}
	
	private static boolean esRaiz( int i ) {
		return (i == 0);
	}
	

	private static int padre( int i ) {
		return heapArray[indicePadre(i)];  
	}

}
