package bitman.Algo2.sorting;

public class QuickSort {
	private static final int TAM_ARRAY_ORDENADO = 1;
	private static int[] a;
	private static boolean estoyAprendiendo = false;

	public static void sort( int[] array, boolean learningFlag ) {
		a = array;
		estoyAprendiendo = learningFlag;
		quickSort( 0, a.length - 1);
	};
	
	private static void quickSort( int inicio, int fin ) {
		if ( ( fin - inicio ) <= TAM_ARRAY_ORDENADO  )
			return;

		int media = elegirLaMedia( inicio, fin );
		/**
		 *  Aqu� hay que poner el m�todo de poner todos los menores que 
		 *  elElegido en los �ndices menores y todos los mayores en los
		 *  �ndices mayores
		 */

		int izq = inicio;
		int der = fin;
		while ( izq < der ) { 
			while ( izq < der && a[izq] <= media )
				izq++;
			
			while( der > izq && a[der] > media )
				der--;
			
			if ( a[izq] > a[der] ) {
				Tools.intercambiar(izq, der, a);
				izq++;
				der--;
			}
		};
		
		quickSort( inicio, izq -1);
		quickSort( izq, fin );
		
	}
	
	/**
	 * En este m�todo pongo la l�gica de elecci�n del valor medio.
	 * Como lo desconozco puedo alterar s�lo este m�todo y ver c�mo
	 * funciona ante distintas situaciones, sin cambiar el resto del 
	 * c�digo
	 */
	private static int elegirLaMedia( int inicio, int fin ) {
		int laMedia = ( a[inicio] + a[fin] ) / 2;
		return laMedia;
	}
}
