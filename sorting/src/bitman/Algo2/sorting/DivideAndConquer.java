package bitman.Algo2.sorting;


public class DivideAndConquer {
	private static final int TAM_ARRAY_RESUELTO = 1;
	private static boolean estoyAprendiendo = false;
	private static int[] array;

	public static void sort( int[] a, boolean learningFlag ) {
		estoyAprendiendo = learningFlag;
		array = a;
		sort(  0, a.length - 1 );
	}
	
	private static void sort ( int inicio, int fin) {
		if ( (fin - inicio ) < TAM_ARRAY_RESUELTO )
			return;
		
		// Divide
		int masOmenosLaMitad = (fin-inicio) / 2;
		sort( inicio, inicio + masOmenosLaMitad );
		sort( inicio + masOmenosLaMitad + 1, fin );
		
		// and conquer !!!
		conquer( inicio, fin, inicio + masOmenosLaMitad );

	};

	private static void conquer( int inicio, int fin, int medio ) {
		Insertion.sortParcial( array, inicio, inicio + medio + 1, fin );
				
	}
}
