package bitman.Algo2.sorting;

public class Tools {
	public final static int UNKNOWN_SORT   = 0; 
	public final static int INSERTION_SORT = 1; 
	public final static int SELECTION_SORT = 2;
	public final static int HEAPSORT_SORT  = 3;
	public final static int HEAPSORT2_SORT = 4;
	public final static int DIVIDEANDCONQUER_SORT = 5;
	public final static int QUICKSORT_SORT = 6;
	public final static int MERGESORT_EXT_SORT = 7;
	
	
	private static int [] previous;
	
	public static void showArray( int[] a, int maxIndex, String title ) {
		System.out.print( title );
		for( int i=0; i <= maxIndex; i++ ) {
			System.out.print(a[i] + " ");
		};
		System.out.println();
	}

	public static void showArray( int[] a ) {
		showArray(a, a.length - 1, "");
	}
	
	public static void showArray( int[] a, String title ) {
		showArray(a, a.length - 1, title);
	}
	
	public static void showArrayDiffs ( int[] a ) {
		for( int i=0; i < a.length; i++ ) {
			if (a[i] != previous[i]) {
				System.out.print("*");
			} else {
				System.out.print(" ");
			};
			
			System.out.print(a[i] + " ");
		};
		System.out.println();
	}
	
	public static void setStartingArray ( int[] a ) {
		previous = new int[a.length];
		System.arraycopy(a, 0, previous, 0, a.length);
	}
	
	public static int[] intArrayFromArgs( String[] args, int start ) {
		int[] a = new int[args.length - start];
		for ( int i = start; i < args.length; i++) 
			a[i - start] = Integer.parseInt(args[i]);
	    return a;
	}
	
	public static int toIntSortMethod( String sortMethod ) {
		String sM = sortMethod.toLowerCase();
		if ( sM.equals("insertion") ) {
			return INSERTION_SORT;
		} else if (sM.equals("selection")) {
			return SELECTION_SORT;
		} else if (sM.equals("heapsort")) {
			return HEAPSORT_SORT;
		} else if (sM.equals("heapsort2")) {
			return HEAPSORT2_SORT;
		} else if (sM.equals("dac")) {
			return DIVIDEANDCONQUER_SORT;
		} else if (sM.equals("quick")) {
			return QUICKSORT_SORT;
		} else if (sM.equals("mergesortext")) {
			return MERGESORT_EXT_SORT;
		} else {
			return UNKNOWN_SORT;
		}
	}

	public static void intercambiar( int i, int j, int[] a ) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
} 

