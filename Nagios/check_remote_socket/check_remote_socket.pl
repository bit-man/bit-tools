#!/usr/bin/perl -w

use strict;
use Getopt::Long;

# internal settings, please don't touch them
###################################################################################################

## ToDo use Nagios::Plugin for easy and more confident plug-in develpoment (and soon deprecation)
## use Nagios::Plugin;

use utils qw(%ERRORS);
use Socket; 

## ToDo use the _try_and_use() mechanism to trap the error if a module is not
##		available

use vars qw($PRGNAME $VERSION $opt_host $opt_port $opt_help);

$PRGNAME="check_remote_socket.pl";

$opt_host = '';
$opt_port  = '';
$opt_help = '';

my $build = '$Rev$';
$build =~ /(\d+)/;
$VERSION="0.0.beta.$1";

sub show_ThisHelp() {
	print   "\n" .
            "$PRGNAME - version : $VERSION\n" .
            "brought to you by Victor A. Rodriguez (http://www.bit-man.com/.ar)\n" . 
            "GNU General Public License v2.0\n\n";

	print_ThisUsage();
	print   "	-H = remote server name\n" .
			"	-p = remote server socket\n\n";
}

sub print_ThisUsage(){
	print "Usage : $PRGNAME -H <hostname> -p <socket_nunmer>\n\n";
}

sub tryToConnect() {

    my $iaddr = inet_aton( $opt_host );
    if ( ! $iaddr ) {
    	print "CRITICAL: the host '$opt_host' doesn't exists";
		return $ERRORS{"CRITICAL"};
    }
    my $paddr   = sockaddr_in($opt_port, $iaddr);

    ## ToDo allow UDP also
    my $proto  = getprotobyname('tcp');
    socket(SOCK, PF_INET, SOCK_STREAM, $proto)  || do  {
    	print "CRITICAL: socket error $!";
		return $ERRORS{"CRITICAL"};
    };
    connect(SOCK, $paddr)  || do  {
    	print "CRITICAL: can't connect: $!";
		return $ERRORS{"CRITICAL"};
    };

    close (SOCK)            || die "close: $!";

	print "OK: connection successful to server '$opt_host', port '$opt_port'";
	return $ERRORS{'OK'};
};

sub exitWithStyle {
	print ("CRITICAL: $_[0]");
	exit $ERRORS{"CRITICAL"};
};

# main
################################################################################

local $SIG{'__WARN__'} = \&exitWithStyle;

Getopt::Long::Configure('bundling');
GetOptions ("H=s"   => \$opt_host,
			"p=i"   => \$opt_port,
	 		"h"   	=> \$opt_help
	 );

if ( $opt_help ) {
	show_ThisHelp();
	exit $ERRORS{"UNKNOWN"};
};

unless ($opt_host &&  $opt_port) {
	print_ThisUsage();
	exit $ERRORS{"UNKNOWN"};
};


my $rc = tryToConnect();
exit $rc;
