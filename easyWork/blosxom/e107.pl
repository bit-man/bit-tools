#!/usr/bin/perl

use Net::FTP;
$ftp = Net::FTP->new("bit-man.com.ar", Debug => 0)
             or die "Cannot connect to bit-man.com.ar: $@";

$ftp->login($ARGV[0],$ARGV[1])
             or die "Cannot login ", $ftp->message;

$ftp->cwd("blosxom/entries")
             or die "Cannot change working directory ", $ftp->message;

my @folder = $ftp->dir();

for my $line ( @folder ) {
    ## retrieve only files with numeric name and ending in txt
    next if ( $line !~ /([0-9]+\.txt)$/);
    my $file=$1;
    print "Retrieving file $file\n";
    $ftp->get($file);
    print "Deleting file $file\n";
    $ftp->delete($file);
}

$ftp->quit;

