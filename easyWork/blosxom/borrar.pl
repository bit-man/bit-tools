#!/usr/bin/perl

use strict;
use warnings;

use WWW::Mechanize;


my $pageList = 'paraBorrar.txt';

sub login($$) {
    my ($user, $pass) = @_;
    
    print "Creating object\n";
    my $mech = WWW::Mechanize->new() ||
        die("Can't obtain mechanize object");
    
    print "Logging in $user-$pass\n";
    my $url = 'http://www.bit-man.com.ar/cgi-bin/wiki.pl/action=editprefs';
    my $response = $mech->get( $url );
    die "Cannot get login url : " . $response->status_line . "\n"
        if (! $response->is_success);
    
    print "Sending login\n";
    $response = $mech->submit_form(
       form_number => 1,
       fields      => {
           p_username    => $user,
           p_password    => $pass,
           p_adminpw     => $pass
       }
    );
    die "Cannot login" 
        if (! $response->is_success);

    return $mech;
}

sub deletePage($$) {
    my $mech = shift;
    my $page = shift;

    my $url = 'http://www.bit-man.com.ar/cgi-bin/wiki.pl/';

    my $response = $mech->get( $url . $page );
    die "Cannot delete page $page (can't get page)" 
        if (! defined $response or ! $response->is_success);
    
    die "Cannot delete page $page (no link to delete it)" if (! $response->is_success);
    $response = $mech->follow_link( text_regex => qr/Delete this page/i );
    
    $mech->follow_link( text_regex => qr/Confirm Delete/i );
    die "Cannot delete page $page (no link to confirm delete)" if (! $response->is_success);
};

# main
############################################################################

#my $browser = login($ARGV[0], $ARGV[1]);

my $fh;
open ( $fh, "<", $pageList ) || die("No puedo abrir $pageList\n");

while( my $page = <$fh> ) {
    chomp $page;
    print "$page\n";
}

#deletePage($browser, $testPage);


close( $fh );
