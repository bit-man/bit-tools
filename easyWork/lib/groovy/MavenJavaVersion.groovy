//<?xml version="1.0" encoding="UTF-8"?>
//<project xmlns="http://maven.apache.org/POM/4.0.0"
//xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
//xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
//    <parent>
//        <artifactId>YASQT</artifactId>
//        <groupId>YASQT</groupId>
//        <version>1.0-SNAPSHOT</version>
//    </parent>
//
//    <modelVersion>4.0.0</modelVersion>
//
//    <artifactId>Main</artifactId>
//
//    <dependencies>
//        <dependency>
//            <groupId>junit</groupId>
//            <artifactId>junit</artifactId>
//            <version>4.11</version>
//            <scope>test</scope>
//        </dependency>
//    </dependencies>
//
//    <build>
//        <plugins>
//            <plugin>
//                <groupId>org.apache.maven.plugins</groupId>
//                <artifactId>maven-compiler-plugin</artifactId>
//                <version>3.1</version>
//                <configuration>
//                    <encoding>UTF-8</encoding>
//                    <source>1.7</source>
//                    <target>1.7</target>
//                    <compilerArgument>-Werror</compilerArgument>
//                </configuration>
//            </plugin>
//            <plugin>
//                <groupId>org.apache.maven.plugins</groupId>
//                <artifactId>maven-jar-plugin</artifactId>
//                <version>2.5</version>
//                <configuration>
//                    <finalName>YASQT-${project.version}</finalName>
//                </configuration>
//            </plugin>
//        </plugins>
//    </build>
//
//</project>

// ToDo use Groovy CLI (and add HELP)
def pomFilePath = args[0]
String outFormat = args[1]
def project = new XmlSlurper().parse(pomFilePath);

def plugin = project.build.plugins.plugin

plugin.each {
    p ->
        if (p.groupId == "org.apache.maven.plugins" && p.artifactId == "maven-compiler-plugin") {
            String target = p.configuration.target
            int dot = target.indexOf('.')
            int minor = target[dot + 1..target.size() - 1] as Integer;
            int major = target[0..dot - 1] as Integer

            int majorIdx, minorIdx
            (majorIdx, minorIdx, outFormat) = replaceMsWithS(outFormat)
            def outParams = createOutputParams(majorIdx, minorIdx, major, minor)

            printf outFormat, outParams
            System.exit(-1)
        }
}

private java.util.List createOutputParams(int majorIdx, int minorIdx, int major, int minor) {
    def outParams = []
    if (majorIdx > -1 && minorIdx > -1) {
        if (majorIdx > minorIdx) {
            outParams.add(minor)
            outParams.add(major)
        } else {
            outParams.add(major)
            outParams.add(minor)
        }
    } else if (majorIdx > -1) {
        outParams.add(major)
    } else {
        outParams.add(minor)
    }
    outParams
}

private java.util.List replaceMsWithS(String outFormat) {

    int majorIdx = outFormat.indexOf("%M")

    if (majorIdx != -1) {
        outFormat = outFormat.replaceAll("\\%M", "\\%s")
    }

    int minorIdx = outFormat.indexOf("%m")
    if (minorIdx != -1) {
        outFormat = outFormat.replaceAll("\\%m", "\\%s")
    }

    [majorIdx, minorIdx, outFormat]
}