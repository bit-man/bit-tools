package Output::Text;

use strict;
use warnings;


use constant DEFAULT_WIDTH => 72;

use Exporter ();

no warnings 'redefine'; ## because of the Output::Base is a fallback for no
                        ## implemented methods then the new redefined ones
                        ## will issue a warning, that is silenced this way
use Output::Base;

our @ISA = qw(Exporter Output::Base);


=pod

=head1 NAME

Output::Base - Base class for output management

=head1 SYNOPSIS

  use Output::Text;
  
  my $out = Output::Text->new();
  $out->begin();
  
  $out->progress("Starting program ... \n");
  . . .
  $out->endAndDie()   if ($error);
  . . . 
  $out->end();


=head1 DESCRIPTION

This class manage program output for a CLI user oriented interface, 
where messages are interpreted by a human user. 
  
=cut

=pod

=head1 METHODS

=head2 new

Generates a new object reference for this class.

 my $out = Output::Base->new( width => 80 );
 
The parameter width sets the maximum line width
   
=cut

sub new {
    my $class = shift;
    my $option = shift;

    my $self;
    
    no strict 'refs';  ## avoids option access error
    
    $self->{'width'} = $option->{'width'} || DEFAULT_WIDTH;

    use strict 'refs';
    return bless $self, $class;
};

=pod

=head2 header

Header used at the program beginning (shows author, version, etc.) 

 $out->header( "Easy program v" . $VERSION . "\n"
             . "Author : Bit-Man \n"  );
   
=cut

sub header {
    my $self = shift;
    
    print STDOUT '=' x $self->{'width'} . "\n"; 
    
    $self->SUPER::header($_[0]);
}

=pod

=head2 newTask

Commonly used to show a separator between two different tasks.
Can accept a parameter with the task name taht will be used 
as separator line between tasks.

 $out->newTask(  $taskName   );
   
=cut

sub newTask {
    my $self = shift;

    if ( $_[0] ) {
        my $width = (  $self->{'width'} - length($_[0]) - 2) / 2;
	
		print STDOUT  '-' x $width
	                . " $_[0] "
	                . '-' x $width . "\n" ;
    } else {
        print STDOUT '-' x $self->{'width'} . "\n";
    }
}

1;
