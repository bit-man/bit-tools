package Output::TAP;

use strict;
use warnings;


use Exporter ();

no warnings 'redefine'; ## because of the Output::Base is a fallback for no
                        ## implemented methods then the new redefined ones
                        ## will issue a warning, that is silenced this way
use Output::Base;
use Test::More;

our @ISA = qw(Exporter Output::Base);

our @EXPORT = qw( 	 new begin end
                    info help header
                    newTask debug progress
                    error endAndDie );

use constant NO_MESSAGE => "";

use constant LAST_MSG => 'lastMsg';
use constant TESTNAME => 'testName';

=pod

=head1 NAME

Output::TAP - Base class for output management

=head1 SYNOPSIS

  use Output::TAP;
  
  my $out = Output::TAP->new();
  $out->begin();
  
  $out->progress("Starting program ... \n");
  . . .
  $out->endAndDie()   if ($error);
  . . . 
  $out->end();


=head1 DESCRIPTION

This is a barebones class to manage program output and make it format independent.
The basic usage in mind when this class was generated is to be used as a base class
for output management.
  
=cut

=pod

=head1 METHODS

=head2 new

Generates a new object reference for this class.

 my $out = Output::TAP->new();
   
=cut

sub new {
    my $class = shift;
    my $option = shift;
    my $self;
    
    $self->{ TESTNAME } = "First test";
    $self->{ LAST_MSG } = NO_MESSAGE;
    
    return bless $self, $class;
};


=pod

=head2 begin

Signals that the test starts here .
No output is generated previous to this method call

 $out->begin();
   
=cut

sub begin {
    my $self = shift;
}


=pod

=head2 end

Signals that the test ends here

 $out->end();
   
=cut

sub end {
    my $self = shift;
    done_testing();
}

=pod

=head2 info

Shows information related output (parameters, etc.)

 $out->info( "Parameters\n"
            . "User : $user\n"
            . "Host : $host->{name}"  );
   
=cut

sub info {
    my $self = shift;
    $self->{ LAST_MSG } = shift;
    
    note $self->{ LAST_MSG };
}

=pod

=head2 help

Shows help /usage information 

 $out->help( "usage\n"
            . "    program --param=search"  );
   
=cut

sub help {
    my $self = shift;
    $self->{ LAST_MSG } = shift;
    
   fail( "Help requested");
   diag( $self->{ LAST_MSG } );
}

=pod

=head2 header

Header used at the program beginning (shows author, version, etc.) 

 $out->header( "Easy program v" . $VERSION . "\n"
             . "Author : Bit-Man \n"  );
   
=cut

sub header {
    my $self = shift;
    $self->{ LAST_MSG } = shift;

    note( $self->{ LAST_MSG } );
}

=pod

=head2 newTask

Signals a new task to be carried out.
Has no effect on this implementation.

 $out->newTask( "New task : " . $taskName . "\n"  );
   
=cut

sub newTask {
    my $self = shift;
}

=pod

=head2 debug

Prints debugging information (variable values, internal data structires, etc. ) 

 $out->debug( "Connection hash : " . Debug::output( %myConnectionHash ) . "\n"  );
   
=cut


sub debug {
    my $self = shift;
    $self->{ LAST_MSG } = shift;

    note( $self->{ LAST_MSG } );
}

=head2 progress

Prints progress information, but on the TAP implementation
it uses the passed message as a base for the test name.

 $out->progress( "Connecting to $host\n"  );
 ...
 $out->progress( "Logging as user $use"  );
   
=cut

sub progress {
    my $self = shift;
    $self->{ LAST_MSG } = shift;
    
    note(  $self->{ LAST_MSG } );
        
    ## It's chomped because it will be used as a test name
    ## Let's call it a sanity check
    $self->{ TESTNAME } =  $self->{ LAST_MSG };
    chomp $self->{ TESTNAME }; 
}

sub progressOK() {
    my $self = shift;

    pass( $self->{ TESTNAME } );
    
    $self->{ LAST_MSG } = NO_MESSAGE;
}

=head2 error

Shows error messages 

 $out->error( "Error connecting to $host: $!\n"  );
   
=cut

sub error {
    my $self = shift;
    $self->{ LAST_MSG } = shift;
    
    fail( $self->{ TESTNAME } );
    diag ( $self->{ LAST_MSG } );
}

=head2 endAndDie

Shows an error message, sognals output end and dies using passed 
error message  

 $out->endAndDie( "Error connecting to $host: $!\n"  );
 ## code below this line won't be reached

=cut

sub endAndDie {
    my $self = shift;
    $self->{ LAST_MSG } = shift;

    $self->error( $self->{ LAST_MSG } );
    $self->end();
    exit;
}

