package Output::Base;

use strict;
use warnings;


use Exporter ();

our @ISA = qw(Exporter);
our @EXPORT = qw( 	 new begin end
                    info help header
                    newTask debug progress
                    error endAndDie );

=pod

=head1 NAME

Output::Base - Base class for output management

=head1 SYNOPSIS

  use Output::Base;
  
  my $out = Output::Base->new();
  $out->begin();
  
  $out->progress("Starting program ... \n");
  . . .
  $out->endAndDie()   if ($error);
  . . . 
  $out->end();


=head1 DESCRIPTION

This is a barebones class to manage program output and make it format independent.
The basic usage in mind when this class was generated is to be used as a base class
for output management.
  
=cut

=pod

=head1 METHODS

=head2 new

Generates a new object reference for this class.

 my $out = Output::Base->new();
   
=cut

sub new {
    my $class = shift;
    my $option = shift;
    my $self;
    
    return bless $self, $class;
};


=pod

=head2 begin

Signals that the output will start from this point onwards, no 
output is generated previous to thsi method call

 $out->begin();
   
=cut

sub begin {
    my $self = shift;
}


=pod

=head2 end

Signals that the output will stop from this point onwards, no 
output is generated after to thsi method call.

Very useful for Test modules (e.g. Test::More that) where a 
finalize method must be called at the test ending 

 $out->end();
   
=cut

sub end {
    my $self = shift;
}

=pod

=head2 info

Shows information related output (parameters, etc.)

 $out->info( "Parameters\n"
            . "User : $user\n"
            . "Host : $host->{name}"  );
   
=cut

sub info {
    my $self = shift;
    
    print STDOUT $_[0];
}

=pod

=head2 help

Shows help /usage information 

 $out->help( "usage\n"
            . "    program --param=search"  );
   
=cut

## ToDo WOULD BE NICE TO USE A CALLBACK

sub help {
    my $self = shift;
    
   print STDOUT $_[0];
}

=pod

=head2 header

Header used at the program beginning (shows author, version, etc.) 

 $out->header( "Easy program v" . $VERSION . "\n"
             . "Author : Bit-Man \n"  );
   
=cut

sub header {
    my $self = shift;
    
    print STDOUT $_[0];
}

=pod

=head2 newTask

Commonly used to show a separator between two different tasks 

 $out->newTask( "New task : " . $taskName . "\n"  );
   
=cut

sub newTask {
    my $self = shift;
}

=pod

=head2 debug

Prints debugging information (variable values, internal data structires, etc. ) 

 $out->debug( "Connection hash : " . Debug::output( %myConnectionHash ) . "\n"  );
   
=cut


sub debug {
    my $self = shift;
    
    print STDOUT $_[0];
}

=head2 progress

Prints progress information 

 $out->progress( "Connecting to $host\n"  );
 ...
 $out->progress( "Logging as user $use"  );
   
=cut

sub progress {
    my $self = shift;
    
    print STDOUT $_[0];
}

sub progressOK {
    my $self = shift;
}


=head2 error

Shows error messages 

 $out->error( "Error connecting to $host: $!\n"  );
   
=cut

sub error {
    my $self = shift;
    
    print STDERR $_[0];
}

=head2 endAndDie

Shows an error message, sognals output end and dies using passed 
error message  

 $out->endAndDie( "Error connecting to $host: $!\n"  );
 ## code below this line won't be reached

=cut

sub endAndDie {
    my $self = shift;

    $self->end();
    die $_[0];
}

