#!/usr/bin/perl -T

use warnings;
use strict;
use Getopt::Long; 


### Default settings
my %input;         ## input parameters passed to each sub in $cmdSub, to avoid
                   ## global variables use
my %debug;         ## debug vector
my $debugAll = 0;
my %validDebug = ( 'setDebugFlags' => 1,
                   'run'           => 1,
                   'compareBoth'   => 1 );
my $debugList;

### internal settings and structure
### Non Plus Ultra !!

my $NAME = "Skeleton";
my $VERSION = "0.0";
my $AUTHOR = "Victor A. Rodriguez (Bit-Man)";
my $CONTACT = 'http://www.bit-man.com.ar';
my $SCREENWIDTH = 80;

my $cmdSub = [ \&printInfo, \&printHelp, \&run];
my $cmd = [ 0, 0, 1 ];    ## 0: disabled, 1: enabled
my $errStr;    ## Extended error string

#############  setDebugFlags  ################################################
## sets accordingly the debug vector @debug 
sub setDebugFlags($$) {
    my ($debugList, $debugAll) = @_;
    my @list;
    
    return if ( ! $debugList && ! $debugAll);

    if ($debugAll) { 
        @list = keys %validDebug;
    } else {
        @list = split /,/, $debugList;
    };
    
    foreach my $thisDebug (@list) {
        dieSoft( "Debugging element '$thisDebug' isn't a valid one\n", 1)
            if ( ! $debugAll && ! $validDebug{ $thisDebug } );
        $debug{ $thisDebug } = 1;
    };

};

#############  ProcessArgs  ##################################################
sub ProcessArgs(){
    
    ## no_pass_through: stops command processing if unknown options are present
    Getopt::Long::Configure( qw(no_pass_through) );  
    GetOptions( 'dir1=s' => \$input{'dir1'},
                'dir2=s' => \$input{'dir2'},
                'debugAll' => \$debugAll,
                'debug=s' => \$debugList,
                'help' => \($cmd->[1]),
                'info' => \($cmd->[0]) )
       || dieSoft("Unknown arguments were passed", 1);
    
    setDebugFlags( $debugList, $debugAll );
    $cmd->[1] = ( ! $input{'dir1'} || ! $input{'dir2'} );
    $cmd->[2] = ! $cmd->[0] &&  ! $cmd->[1];

    ## The options are loaded and the command processing can begin ...
    ## in case no commands are needed, just leave $cmdSub and $cmd empty

    my ($i, $cmdExec) = (0, 0);
    foreach (@$cmdSub) {
        if ($cmd->[$i++]) {
		    PrintSeparator2();
		    &$_( \%input );
            $cmdExec++;
        };
    }
    
    if (! $cmdExec) {
        PrintSeparator2();
        printHelp();
    };
};

#############  Born to run !!  ################################################
sub run($) {
    my $env = shift;
    my $debug = $debug{'run'};
    
    if ( $debug ) {
        print "DEBUG: Entering run()\n" ;

        foreach my $key ( keys %$env ) {
            print "DEBUG: $key => " . $env->{$key} . "\n";
        };
    };
    
    dieSoft( "Either '" . $env->{'dir1'} . "' or '" . $env->{'dir2'} ."' doesn't exist :-(", 1 )
        unless ( -d $env->{'dir1'} && -d $env->{'dir2'} );

    print "Files structures are equal !!!\n"
        if compareBoth( $env, $env->{'dir1'}, $env->{'dir2'} );
};

sub compareBoth($$$) {
    my ($env, $dir1, $dir2) = @_;
    my $debug = $debug{'compareBoth'};
    my $ret = 1;
    
    print "DEBUG: entering 'compareBoth'\n"
            if $debug;
            
    my @dir1 = getDirEntries( $dir1 );
    my %dir2 = map { $_ => 1 } getDirEntries( $dir2 );
    
    print "DEBUG: dir1 @dir1\n"
            if $debug;
    print "DEBUG: dir2 " . %dir2 ."\n"
            if $debug;
            
    foreach my $file1 ( @dir1 ) {
        my $path1 = "$dir1/$file1";
        my $path2 = "$dir2/$file1";
        
        print "$path1 - $path2\n"
            if $debug;

        if ( $dir2{ $file1 } ) {
            delete $dir2{ $file1 };
        } else {
            dieSoft( "The file '$path2' doesn't exists", 0 );
            $ret = 0;
        };

        compareBoth( $env, $path1, $path2 )
            if (-d $path1);
            
        unless ( filesAreEqual( $path1, $path2 ) ) {
            $ret = 0;
        };
    };
    
    foreach ( keys %dir2 ) {
        dieSoft( "The file '$dir1/". $_ ."' doesn't exists", 0 );
        $ret = 0;
    };

    return $ret;
};

sub filesAreEqual($$) {
    my ($file1, $file2) = @_;

    return( (-s $file1) == (-s $file2) );
};

sub fileExists($$) {
    my ( $file, $dir ) = @_;
    
    foreach ( @$dir ) {
        return 1
            if ($file eq $_);
    };
    
    return 0;
};

sub getDirEntries($) {
    my $dir = shift;
    
    opendir ( my $fh, $dir ) 
        || dieSoft( "Can't open folder '$dir'", 1);
    my @file = readdir( $fh );
    closedir( $fh );

    @file = cleanupEntries( @file );
    
    return @file;
};

sub cleanupEntries {
    my @file;
    
    foreach ( @_ ) {
        push @file, $_
            if ! ( $_ eq '.' || $_ eq '..' );
    };
    
    return @file
};

#############  Separators  ####################################################
sub PrintSeparator(){
	print '=' x $SCREENWIDTH . "\n";
}

sub PrintSeparator2(){
	print '-' x $SCREENWIDTH . "\n";
}

#############  printHelp  #####################################################
sub printHelp($){
	print <<EOT
    
    usage:
    $0 [{--debug | --deubgAll}] [--help] [--info]
       --dir1=/first/folder/to/compare --dir2=/second/folder/to/compare

    --help:       This help screen
    --info:       Author and version info
    --in:         Input file
    --out:        Output file
    
    --debug:      comma separated value of subs to debug
    --debugAll:   debug all subs
    
    --dir1        Folder to compare against dir2
    --dir2        Folder to compare against dir1 X-D

EOT
}	
	
#############  PrintHeader  ###################################################
sub PrintHeader(){
	PrintSeparator();
    printInfo();
};

sub printInfo($) {
    print "$NAME - v$VERSION\n" .
          "$AUTHOR\n" .
          "Contact: $CONTACT\n"; 
}

sub dieSoft($$) {
    my $msg = shift;
    my $die = shift;

    my $dieMsg = "error !!!!!\n    $msg\n";
    $dieMsg .= "    $errStr\n"    if $errStr;
    $dieMsg .= "OS Error: $!\n\n"  if $!;
    die "\n\nFatal $dieMsg" if $die;
    print $dieMsg;
}

# main
##############################################################################


PrintHeader();
ProcessArgs();
PrintSeparator2();
exit;

