#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long; 

use Net::FTP;


use constant TEXT_MODE => 'text';
use constant TAP_MODE => 'tap';

use constant SCREENWIDTH => 72;

### Default settings
my %input;         ## input parameters passed to each sub in $cmdSub, to avoid
                   ## global variables use
my @debug;         ## debug vector 
my $debugMax = 1;  ## number of subs that allow debugging
my $debugAll = 0;
my $debugList;
my $out;

### internal settings and structure
### Non Plus Ultra !!

my $NAME = "Pollxn SPAM clean";
my $VERSION = "0.3.0";
my $AUTHOR = "Victor A. Rodriguez (Bit-Man)";
my $CONTACT = 'http://www.bit-man.com.ar';


my $cmdSub = [ \&printInfo, \&printHelp, \&run];
my $cmd = [ 0, 0, 1 ];    ## 0: disabled, 1: enabled
my $errStr;    ## Extended error string


#############  Separators  ####################################################


sub PrintSeparator2(){
	$out->newTask();
}

sub PrintSeparator2Titled($){
	my $title = shift;
	$out->newTask( $title  );
}

#############  printHelp  #####################################################
sub printHelp(){
    PrintHeader();
    PrintSeparator2();

	my $helpMsg = <<EOT;
    
    usage:
    $0 [--debug] [--help] [--info]
       --host=hostname  --user=username  --pwd=password  
       --dir=/path/to/blosxom/entries  
       [--text="spam offending text" | --textfile=/this/file/has/offending/words]
       [--passive | --no-passive] [--output={ tap | text }]

    --help:        This help screen
    --info:        Author and version info
    --host:        hostname
    --user:        username to login with
    --pwd :        password
    --dir :        folder where the pollxn comments are located
    --passive:     connects using FTP passive mode

    --text:        spam text to search for
    --textfile:    file containing a list of words identifying spam

    --output:      allows output format 
             tap:  output in TAP format, to be used with Test::Harness
             text: output in free form

    --debug:       comma separated list of debg subs (--debug=ftp debugs FTP)

EOT

    $out->help( $helpMsg );
}  
	
#############  PrintHeader  ###################################################
sub PrintHeader(){
	    $out->header( "$NAME - v$VERSION\n" .
		  "$AUTHOR\n" .
		  "Contact: $CONTACT\n" ); 
}

#############  setDebugFlags  ################################################
## sets accordingly the debug vector @debug 
sub setDebugFlags($$) {
    my ($debugList, $debugAll) = @_;
    
    ## inits the debug vector
    for my $i (0..($debugMax -1) ) {
        $debug[$i] = 0
    }

    return if ( ! $debugList && ! $debugAll);

    if ($debugAll) {  ## sets all debugging flags
        for my $i (0..($debugMax -1) ) {
            $debug[$i] = 1;
        };
    } else {  ## just set the passed ones
        foreach my $i (split /,/, $debugList) {
            if ( lc($i) eq 'ftp' ) {
                $input{'debugFTP'} = 1;
                next;
            };
            
            dieSoft( "Debugging element '$i' is greater than the maximum allowed of '".($debugMax -1)."'\n", 1)
                if ($i > ($debugMax -1));

            $debug[$i] = 1;
        };
    };
};

sub dieSoft($$) {
    my $msg = shift;
    my $die = shift;

    my $dieMsg = "error !!!!!\n    $msg\n";
    $dieMsg .= "    $errStr\n"    if $errStr;
    $dieMsg .= "OS Error: $!\n\n"  if $!;
    
    if ( $die ) {
        $out->endAndDie( "\n\nFatal $dieMsg" );
    } else {
        $out->error($dieMsg);
    }

}

sub getOutputMode($) {
	my $mode = shift;
	
	$mode = lc $mode;
	
	return ( $mode eq TEXT_MODE || $mode eq TAP_MODE ) ?
    			$mode : TEXT_MODE;
}


sub obtainOutput($) {
    my $outputMode = shift;
    my $outP;
    
    ($outputMode eq TEXT_MODE) && do {
        use Output::Text;
        $outP = Output::Text->new( width => SCREENWIDTH );
    };
    
    ($outputMode eq TAP_MODE) && do {
        use Output::TAP;
        $outP = Output::TAP->new();
    };
    
    return $outP;
}

sub initOutput() {
	$input{'output'} = getOutputMode( $input{'output'}  );
	
	$out = obtainOutput( $input{'output'} );
    $out->begin();
}

sub manageBadArgumentParsing() {
    ## Output still not set 
    initOutput();
	dieSoft("Unknown arguments were passed", 1);
}

#############  ProcessArgs  ##################################################
sub ProcessArgs(){

    ## no_pass_through: stops command processing if unknown options are present
    Getopt::Long::Configure( qw(no_pass_through) );  
    GetOptions( 'host=s' => \$input{'host'},
                'user=s' => \$input{'user'},
                'pwd=s' => \$input{'pwd'},
                'dir=s' => \$input{'dir'},
                'text=s' => \$input{'text'},
                'textfile=s' => \$input{'textfile'},
                'passive!' => \$input{'passiveFTP'},
                'output=s' => \$input{'output'},
                'debugAll' => \$debugAll,
                'debug=s' => \$debugList,
                'help' => \($cmd->[1]),
                'info' => \($cmd->[0]) )
       || manageBadArgumentParsing();
    
    ## Output still not set 
    initOutput();
    
    setDebugFlags( $debugList, $debugAll );
    $cmd->[1] = ( ! $input{'host'} || ! $input{'user'}  || ! $input{'pwd'}
                  || ! $input{'dir'} 
                  || ! ( $input{'text'} || $input{'textfile'} )
                );
    $cmd->[2] = ! $cmd->[0] &&  ! $cmd->[1];

    
    ## The options are loaded and the command processing can begin ...
    ## in case no commands are needed, just leave $cmdSub and $cmd empty

    my ($i, $cmdExec) = (0, 0);
    foreach (@$cmdSub) {
        if ($cmd->[$i++]) {
		    &$_( \%input );
		    PrintSeparator2();
            $cmdExec++;
        };
    }
    
    if (! $cmdExec) {
        PrintSeparator2();
        printHelp();
    };
    
    $out->end();
};

#############  FTP related functions  ##########################################
sub FTPconnect($) {
    my $env = shift;
    $out->progress( "Connecting to " . $env->{'host'} . "... \n");
    	
    $env->{'ftp'} = Net::FTP->new( $env->{'host'}, 
                                   Debug => $env->{'debugFTP'},
                                   Passive => $env->{'passiveFTP'} )
        || dieSoft ("Can't connect to host: '$@'", 1 );
    $env->{'ftp'}->login( $env->{'user'}, $env->{'pwd'} )
        || dieSoft ("Can't login : " . $env->{'ftp'}->message, 1 );
    $env->{'ftp'}->cwd( $env->{'dir'} )
        || dieSoft ("Can't change to selected dir : " . $env->{'ftp'}->message, 1 );
    $out->progressOK();
};

sub FTPdelete($$) {
    my ($file, $env) = @_;
    
    $out->progress(  "Deleting SPAM file '$file' ... \n" );

    $env->{'ftp'}->delete( $file )
        || dieSoft ("Can't delete file '$file' : " . $env->{'ftp'}->message, 0);
        
    $out->progressOK();
};

sub FTPbye($) {
    my $env = shift;
    $out->progress( "Bye, bye !!!\n" );
    $env->{'ftp'}->quit;
    $out->progressOK();
};

################################################################################

sub cleanListBySuffix($) {
    my $file = shift;
    my @clean;
    foreach my $f (@$file) {
        push (@clean, $f)
            if ($f =~ /\.txt.pollxn/);
    };
    return @clean;
};


sub rewind($) {
    seek( $_[0], 0, 0 );
};

sub fileContainsSPAM($$) {
    my ($file, $env) = @_;
    my $fh;

    open( $fh, $file )
        || do { dieSoft ("Can't open local file '$file' : " . $env->{'ftp'}->message, 0);
                return 0;
              };
    foreach my $text ( @{ $env->{'text'} } ) {
        rewind( $fh );
        foreach my $line (<$fh>) {
            if ($line =~ qr/$text/) {
                $out->info( "  Offending word: '$text'\n  Offending line :\n  $line\n" );
                showFile($fh);
                close $fh;
                return 1;
            }
        };
    };
    close $fh;
    return 0;
};

sub showFile($) {
	my $fh = shift;
	rewind($fh);
	
	PrintSeparator2Titled("Comment");
	while( my $line = <$fh> ) {
		$out->info( $line );
	}
	$out->info( "\n" ); 

	PrintSeparator2();
}

sub isSPAM($$) {
    my ($file, $env) = @_;
    
    ## filename cleanup to avoid taint mode croaking in Net::FTP
    $file =~ //;
    $file = $';
    
    $env->{'ftp'}->get( $file )
        || do { dieSoft ("Can't retrieve file '$file' : " . $env->{'ftp'}->message, 0);
                return 0;
              };
    my $isSPAM =  fileContainsSPAM( $file, $env );
    unlink ( $file )
        || dieSoft ("Can't delete local file '$file'", 0 );
    
    return $isSPAM;
};
#############  cleanSPAM  ################################################
sub cleanSPAM($) {
    my $env = shift;

    my @file = $env->{'ftp'}->ls();
    @file = cleanListBySuffix( \@file );
    foreach my $file ( @file ) {
        $out->progress("Processing $file\n");
        if ( isSPAM( $file, $env ) ) {
            ## ToDo when the program fails can do it in more than
            ## one place, meaning that the same test will fail more than
            ## once (in TAP mode). Is this OK ??
            FTPdelete( $file, $env );
        } else {
            $out->progressOK();
        }
    };
};

sub getFile($) {
    my $file = shift;
    my $fh;
    my @file;
    
    open ( $fh, $file ) 
        || dieSoft ("Can't open file '$file'", 1);
    
    foreach my $line( <$fh> ) {
        chomp $line;
        push @file, $line;
    };
    close $fh;
    return \@file;
};

sub textSearchParameters($) {
    my $env = shift;

    if ( $env->{'text'} ) {
        $env->{'text'} = [ $env->{'text'} ];
    } else {
        $env->{'text'} = getFile( $env->{'textfile'} );
        delete $env->{'textfile'};
    };
};

#############  Born to run !!  ################################################
sub run($) {
    my $env = shift;

    PrintHeader();
    PrintSeparator2();

    if( $debug[0] ) {
        foreach my $key ( keys %$env ) {
            $out->debug( "$key => " . $env->{$key} . "\n" );
        };
    };
    
    printParams($env);
    textSearchParameters( $env );
    FTPconnect( $env );
    cleanSPAM( $env );
    FTPbye( $env );
    
};


sub printParams($) {
	my $param = shift;
	
	my $text = $param->{'text'} ? $param->{'text'} : $param->{'textfile'};
	my $conn = $param->{'passiveFTP'} ? "FTP, passive" : "FTP, active";
	
    $out->info(
		      "Host       : " . $param->{'host'} . "\n" 
		.     "User       : " . $param->{'user'} . "\n" 
		.     "Folder     : " . $param->{'dir'} . "\n"
		.	  "Filter     : " . $text . "\n"
		.	  "Connection : " . $conn . "\n" );

}



# main
##############################################################################

ProcessArgs();

exit;

