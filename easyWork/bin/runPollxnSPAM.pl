#!/usr/bin/perl

use TAP::Parser::Aggregator;
use TAP::Parser;
use Getopt::Long;

my ($sFolder, $sBaseDir, $host, $user, $pwd);
my ($text, $textfile);

sub showHelp() {
	my $helpMsg = <<EOT;   
        
    usage:
    $0 --folders=folder1,folder2,... --basedir=/blosxom/entries/
      --host=hostname --user=username --pwd=password
      [--text="spam offending text" | --textfile=/this/file/has/offending/words]

    --folders  : comma separated list of folders.
    --basedir  : folder where the pollxn comments are located
    
    --host     : hostname
    --user     : username to login with
    --pwd      : password
    
    --text     : spam text to search for
    --textfile : file containing a list of words identifying spam

EOT

    print $helpMsg;

}

my  $easyworkHome = $ENV{'EW_HOME'} 
    || die ("runPollxnSPAM.pl: EW_HOME environment variable no set !!\n");
                           
my $optsOK =    GetOptions (
                    'folders=s' => \$sFolder,
                    'basedir=s' => \$sBaseDir,
                    'host=s' => \$host,
                    'user=s' => \$user,
                    'text=s' => \$text,
                    'textfile=s' => \$textfile,
                    'pwd=s' => \$pwd
                );
do { showHelp();
        exit(-1);
    }
    if ( ! $optsOK || 
         ! $sFolder || ! $sBaseDir || ! $host || ! $user || ! $pwd ||
         ! ( $text || $textfile ) );

my @pFolder =  split /,/, $sFolder;                               
    
my $aggregate = TAP::Parser::Aggregator->new;
    

for my $pollxnFolder ( @pFolder ) {

    print "**** Scanning folder $pollxnFolder\n";

    my @pParams = (
            '--host=' . $host, '--user=' . $user, '--pwd=' . $pwd,
            '--passive', '--output=tap',
            ($textfile ?
                '--textfile='. $textfile :
                '--text=' . $text),
            '--dir='.  $sBaseDir . '/' . $pollxnFolder
             );
    
    my %pNewParams = (
        'source' => $easyworkHome . 'bin/pollxnSPAM.pl',
        'test_args' => \@pParams
    );  
        
    $parser = TAP::Parser->new( \%pNewParams );

    ## actually run the tests and show the output
    ## if the test failed
    while ( my $result = $parser->next ) {
        if ( $result->is_test() && ! $result->is_ok() ) { 
            print $result->as_string . "\n";
        }
    }
    
    $aggregate->add( $pollxnFolder, $parser );

}

my $summary = <<'END_SUMMARY';
SPAM / Errors          : %s
Unexpectedly succeeded : %s
END_SUMMARY

printf $summary,
      scalar $aggregate->failed,
      scalar $aggregate->todo_passed;


exit;

