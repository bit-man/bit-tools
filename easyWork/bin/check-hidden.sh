#!/usr/bin/env bash

if [ `iwlist $1 scanning | grep -c $2` -eq 0 ] ; then
  iwlist $1 scanning essid $2 > /dev/null ;
fi
