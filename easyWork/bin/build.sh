#!/bin/bash

tPGM_NAME=build

usage() {
    echo -e "\nusage:  $tPGM_NAME --lang {parrot|rakudo} [--update]" 
    echo -e "                  [--build] [--test] [--doc]\n"
}

checkReturnStatus() {
    if [[ "$?" != "0"  ]]; then
        echo -e "\n********* $tPGM_NAME: Error: $1\n"
        exit 1
    fi
}

while [[ ! -z $1 ]]; do
    if [[ "$1" == "--update" ]]; then
       tUPDATE=1
    elif [[ "$1" == "--build" ]]; then
       tBUILD=1
    elif [[ "$1" == "--test" ]]; then
       tTEST=1
    elif [[ "$1" == "--doc" ]]; then
       tDOC=1
    elif [[ "$1" == "--lang" ]]; then
       shift
       tLANG=$1
    else
       echo -n "\n** $tPRG_NAME: ERROR : Unknown option $1\n"
       usage
       exit 0
    fi
    shift
done

if [[ -z $tLANG ]]; then
    usage
    exit 0
fi

if [[ ! -d parrot ]]; then
    echo -e "\nThere's no parrot folder below this one\n"
    exit 0
fi

cd parrot
if [[ "$tLANG" == "rakudo" ]]; then
    cd languages/rakudo
fi

if [[ ! -z $tUPDATE ]]; then
        if [[ "$tLANG" == "rakudo" ]]; then
            cd .. &&  git clone git://github.com/rakudo/rakudo.git \
            && cd rakudo
        else
	    svn up
        fi
        checkReturnStatus "UPDATE FAILED"
fi


if [[ ! -z $tBUILD ]]; then
        make clean && perl Configure.pl && make
        checkReturnStatus "BUILD FAILED"
        if [[ "$tLANG" == "parrot" ]]; then
            ./parrot --version
        else
            make spectest
            checkReturnStatus "SPECTEST FAILED"
        fi

fi

if [[ ! -z $tTEST ]]; then
        make test
        checkReturnStatus "TEST FAILED"
fi

if [[ ! -z $tDOC ]]; then
        make html
        checkReturnStatus "DOC FAILED"
fi

