SECONDS_IN_ONE_DAY = 86400


class FileWrapper:
    def __init__(self, os, time):
        self.os = os
        self.time = time

    def should_delete(self, path, days):
        file_timestamp = self.os.timestamp(path)
        deletion_limit_timestamp = self.deletion_timestamp(days)
        return file_timestamp < deletion_limit_timestamp

    def deletion_timestamp(self, days):
        now_timestamp = self.time.now()
        return now_timestamp - days * SECONDS_IN_ONE_DAY

    def is_file(self, path):
        return self.os.is_file(path)

    def list_dir(self, path):
        return self.os.listdir(path)

    def remove(self, path):
        self.os.remove(path)
