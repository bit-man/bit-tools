import os, time
from functional import seq
from foldercleaner.file_wrapper import FileWrapper


class OS:
    def timestamp(self, path):
        return os.stat(path).st_mtime

    def is_file(self, path):
        return os.path.isfile(path)

    def listdir(self, path):
        return os.listdir(path)

    def remove(self, path):
        os.remove(path)


class Time:
    def now(self):
        return time.now


class FolderCleaner:
    def __init__(self, path, days, file=FileWrapper(OS(), Time())):
        self.days = days
        self.path = path
        self.file = file

    def run(self):
        seq(self.file.list_dir(self.path)) \
            .map(lambda file_name: os.path.join(self.path, file_name)) \
            .filter(lambda path: self.file.should_delete(path, self.days)) \
            .filter(lambda path: self.file.is_file(path)) \
            .for_each(lambda path: self.file.remove(path))
