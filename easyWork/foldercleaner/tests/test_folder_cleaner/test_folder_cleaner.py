#!/usr/bin/env python3

from typing import Dict
from foldercleaner.folder_cleaner import FolderCleaner
from foldercleaner.file_wrapper import FileWrapper


class FileAttributes:
    def __init__(self, is_file, timestamp):
        self.is_file = is_file
        self.timestamp = timestamp
        self.removed = False
        self.timestamp_path = ""
        self.is_file_path = ""
        self.remove_path = ""


def test_no_files_no_deletion():
    os = OS()
    cleaner = FolderCleaner("", 0, FileWrapper(os, Time()))
    cleaner.run()
    assert os.removed_count == 0


def test_no_deletion_if_none_is_file():
    file1 = FileAttributes(False, 0)
    file2 = FileAttributes(False, 0)
    file = {"file1": file1, "file2": file2}
    os = OS(file, ["file1", "file2"])
    cleaner = FolderCleaner("", 0, FileWrapper(os, Time()))
    cleaner.run()
    assert os.removed_count == 0
    assert not file1.removed
    assert not file2.removed


def test_no_deletion_if_none_should_be_deleted():
    file1 = FileAttributes(True, 0)
    file2 = FileAttributes(True, 0)
    file = {"file1": file1, "file2": file2}
    os = OS(file, ["file1", "file2"])
    cleaner = FolderCleaner("", 100, FileWrapper(os, Time()))
    cleaner.run()
    assert os.removed_count == 0
    assert not file1.removed
    assert not file2.removed


def test_one_file_in_list_is_deleted():
    file1 = FileAttributes(True, 0)
    file = {"/pepe/file1": file1}
    os = OS(file, ["file1"])
    cleaner = FolderCleaner("/pepe", 0, FileWrapper(os, Time()))
    cleaner.run()
    assert os.removed_count == 1
    assert file1.removed


def test_two_file_in_list_are_deleted():
    file1 = FileAttributes(True, 0)
    file2 = FileAttributes(True, 0)
    file = {"file1": file1, "file2": file2}
    os = OS(file, ["file1", "file2"])
    cleaner = FolderCleaner("", 0, FileWrapper(os, Time()))
    cleaner.run()
    assert os.removed_count == 2
    assert file1.removed
    assert file2.removed


def test_all_attribute_combination():
    file1 = FileAttributes(True, 0)
    file2 = FileAttributes(False, 0)
    file3 = FileAttributes(True, 10000000)
    file = {"/pepe/file1": file1, "/pepe/file2": file2, "/pepe/file3": file3}
    os = OS(file, ["file1", "file2", "file3"])
    cleaner = FolderCleaner("/pepe", 0, FileWrapper(os, Time()))
    cleaner.run()
    assert os.removed_count == 1
    assert file1.removed
    assert not file2.removed
    assert not file3.removed


class OS:
    def __init__(self, file: Dict[str, FileAttributes] = {}, listdir=[]):
        self.removed_count = 0
        self.file = dict(file)
        self.listdir_property = listdir

    def timestamp(self, path):
        return self.file[path].timestamp

    def is_file(self, path):
        return self.file[path].is_file

    def listdir(self, path):
        return self.listdir_property

    def remove(self, path):
        self.removed_count += 1
        self.file[path].removed = True


class Time:
    def now(self):
        return 1
