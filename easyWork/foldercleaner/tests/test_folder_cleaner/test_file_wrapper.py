#!/usr/bin/env python3

from foldercleaner.file_wrapper import FileWrapper


def test_should_not_delete():
    file = FileWrapper(OS(), Time())
    assert not file.should_delete("", 100)


def test_should_delete():
    file = FileWrapper(OS(), Time())
    assert file.should_delete("", 0)


def test_is_file():
    file = FileWrapper(OS(True), Time())
    assert file.is_file("")


def test_is_not_file():
    file = FileWrapper(OS(False), Time())
    assert not file.is_file("")


def test_list_dir_empty():
    file = FileWrapper(OS(), Time())
    assert file.list_dir("") == []


def test_list_dir_not_empty():
    file = FileWrapper(OS(True, [1, 2, 3]), Time())
    assert file.list_dir("") == [1, 2, 3]


def test_file_not_removed():
    os = OS()
    file = FileWrapper(os, Time())
    assert not os.is_removed


def test_file_removed():
    os = OS()
    file = FileWrapper(os, Time())
    file.remove("")
    assert os.is_removed


class OS:
    def __init__(self, is_file=True, listdir=[]):
        self.is_removed = False
        self.is_file_property = is_file
        self.listdir_property = listdir

    def timestamp(self, path):
        return 0

    def is_file(self, path):
        return self.is_file_property

    def listdir(self, path):
        return self.listdir_property

    def remove(self, path):
        self.is_removed = True


class Time:
    def now(self):
        return 1
