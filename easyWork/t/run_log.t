#!/usr/bin/perl

use 5.006;
use strict;
use warnings;
use Test::ShellScript;

use constant TEST_LINE => "^run: TEST:";
use constant RUN_PATH => 'bin/run';

## Log file testing
run_ok(RUN_PATH . ' --mute ls', TEST_LINE);
variable_ocurrences("LOG_FILE", 1);
variable_ok("LOG_FILE", '/dev/null');
variable_ocurrences("LOG_FILE", 0);