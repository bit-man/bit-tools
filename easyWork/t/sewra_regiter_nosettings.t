#!/usr/bin/perl

use 5.006;
use strict;
use warnings;
use Test::ShellScript;
use Test::More;

use File::Path qw(remove_tree);
use Git::Wrapper;

use constant TEST_LINE => "^sewra: TEST:";
use constant RUN_PATH => 'bin/sewra';

use constant SETTINGS_FOLDER => '/tmp/.test';
use constant ID => "true1";

use constant TRUE => 1;
use constant FALSE => 0;

use constant STATUS => "STATUS";

sub containsString($@) {
    my $str = shift;
    my @out = @_;
    
    foreach my $line (@out) {
    	return TRUE
    	   if ( $line =~ /$str/ );
    }

    return FALSE;
}

SKIP: {
	skip "Folder '". SETTINGS_FOLDER."' shouldn't exist" , 14
	   unless  (! -d SETTINGS_FOLDER);
	
	
	$ENV{'__EW_TEST'} = 1;
    run_ok(RUN_PATH . ' register '. ID .' /bin/true ' . SETTINGS_FOLDER, TEST_LINE);
    
    
    ok( (-d SETTINGS_FOLDER), "The settings folder '".SETTINGS_FOLDER."' should be created" );
    isCurrentVariable(STATUS);
    isCurrentValue("register");
    nextSlot();
    isCurrentVariable(STATUS);
    isCurrentValue("init_settings");
    nextSlot();
    isCurrentVariable(STATUS);
    isCurrentValue("init_settings_fromScratch");
    nextSlot();
    isCurrentVariable(STATUS);
    isCurrentValue("END");
    nextSlot();
    
    my $git = Git::Wrapper->new(SETTINGS_FOLDER);
    ok( $git, "Git wrapper must be generated" );
    
    ## ToDo add try block to catch Git::Wrapper errors
    my @out = $git->branch();
    ok( containsString(ID, @out), "Branch '".ID."' must be created" );
    
    ### Cleanup
    remove_tree( SETTINGS_FOLDER );
    ok( (! -d SETTINGS_FOLDER), "The settings folder '".SETTINGS_FOLDER."' should not exist anymore." );
    run_ok(RUN_PATH . ' unregister true1', TEST_LINE);
};
