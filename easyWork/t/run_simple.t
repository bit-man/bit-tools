#!/usr/bin/perl

use 5.006;
use strict;
use warnings;
use Test::ShellScript;

use constant TEST_LINE => "^run: TEST:";
use constant RUN_PATH => 'bin/run';

run_ok(RUN_PATH, TEST_LINE);
variable_ok("RUNNING", "false");
variable_ocurrences("RUNNING", 0);

## Using mute to avoid time command output interference wit
run_ok(RUN_PATH . ' --mute ls', TEST_LINE);
variable_ok("RUNNING", "true");
variable_ocurrences("RUNNING", 0);
