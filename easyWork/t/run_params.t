#!/usr/bin/perl

use 5.006;
use strict;
use warnings;
use Test::ShellScript;

use constant TEST_LINE => "^run: TEST:";
use constant RUN_PATH => 'bin/run';

my $mail = ! $ENV{'__EW_MAIL'} ?  $ENV{'USER'} . '@localhost' : $ENV{'__EW_MAIL'};

## allow mail sending and spawn
run_ok( RUN_PATH . " --mail $mail --spawn ls", TEST_LINE);
variable_ok("PARAMS_OK", "true");
variable_ocurrences("PARAMS_OK", 0);

run_ok(RUN_PATH, TEST_LINE);
variable_ok("PARAMS_OK", "true");
variable_ocurrences("PARAMS_OK", 0);

## Won't send e-mail on mute and mail
run_ok( RUN_PATH . " --mail $mail --mute  ls", TEST_LINE);
variable_ok("PARAMS_OK", "true");
variable_ocurrences("PARAMS_OK", 0);
variable_ok("RUNNING", "false");
variable_ocurrences("RUNNING", 0);

