#!/usr/bin/perl

use 5.006;
use strict;
use warnings;
use Test::ShellScript;

use constant TEST_LINE => "^openFile: TEST:";
use constant RUN_PATH => 'bin/openFile';

## no file to open was passed as parameter
run_ok(RUN_PATH, TEST_LINE);
isCurrentVariable("STATE");
isCurrentValue("start");
nextSlot();
isCurrentVariable("STATE");
isCurrentValue("end_noFile");

