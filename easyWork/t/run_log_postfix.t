#!/usr/bin/perl

use 5.006;
use strict;
use warnings;
use Test::ShellScript;

use constant TEST_LINE => "^run: TEST:";
use constant RUN_PATH => 'bin/run';

## Log file testing
run_ok(RUN_PATH . ' --mute ls', TEST_LINE);
variable_ocurrences("LOG_POSTFIX", 0);


run_ok(RUN_PATH . ' --mute --log-postfix XX ls', TEST_LINE);
variable_ocurrences("LOG_POSTFIX", 1);
