#!/usr/bin/perl

use 5.006;
use strict;
use warnings;
use Test::ShellScript;

use constant TEST_LINE => "^openFile: TEST:";
use constant RUN_PATH => 'bin/openFile';

## no file to open was passed as parameter
run_ok(RUN_PATH . ' nonexistentFile.txt', TEST_LINE);
isCurrentVariable("STATE");
isCurrentValue("start");
nextSlot();
isCurrentVariable("STATE");
isCurrentValue("validated");
nextSlot();
isCurrentVariable("STATE");
isCurrentValue("got_mimetype");
nextSlot();
isCurrentVariable("HAS_ASSOC");
isCurrentValue("0");
nextSlot();
isCurrentVariable("STATE");
isCurrentValue("parsed_mimetype");
nextSlot();
isCurrentVariable("HAS_ASSOC");
isCurrentValue("0");

## here shows the mime-type but because this file doesn't exists
## the output captured can be dofferent from system to system
nextSlot();
isCurrentVariable("MIME_TYPE");

nextSlot();
isCurrentVariable("STATE");
isCurrentValue("parsed_ending");

nextSlot();
isCurrentVariable("HAS_ASSOC_END");
isCurrentValue("1");

nextSlot();
isCurrentVariable("TERMINATION");
isCurrentValue("txt");

nextSlot();
isCurrentVariable("STATE");
isCurrentValue("program_acquired");

nextSlot();
isCurrentVariable("PROGRAM");
isCurrentValue("less");

