#!/usr/bin/perl

use 5.006;
use strict;
use warnings;
use Test::ShellScript;

use constant TEST_LINE => "^run: TEST:";
use constant RUN_PATH => 'bin/run';

my $mail = ! $ENV{'__EW_MAIL'} ? $ENV{'USER'} . '@localhost' : $ENV{'__EW_MAIL'};

## allow mail sending and spawn
run_ok( RUN_PATH . " --mail $mail --spawn ls", TEST_LINE);
variable_ok("PARAMS_OK", "true");
reset_timeline();
variable_ok("RUNNING", "true");
reset_timeline();
variable_ok("SPAWN", "1");
reset_timeline();
variable_ok("MUTE", "0");
reset_timeline();
variable_ok("MAIL", "1");
reset_timeline();
variable_ok("MAIL_DEST", $mail);
