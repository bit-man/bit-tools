
import http.client
import json
import sys

sentry_token = sys.argv[1]
HEADERS_ = {"Authorization": "Bearer %s" % sentry_token,
            "Accept" : "application/json"}
ISSUES_EVENTS_ = "/api/0/issues/275129671/events/"

ids = set()
conn = http.client.HTTPSConnection("sentry.io")

def parse_link(response):
    link_header = response.getheader('Link')
    link_header_array = link_header.split(";")
    ## print("Link : " + "\n - ".join(link_header_array))

    next_part = 0
    next_cursor = ""
    has_next = False
    for part in link_header_array:
        if "rel=\"next\"" in part:
            next_cursor = link_header_array[next_part + 2].replace("\"", "").strip()
            has_next = link_header_array[next_part + 1].strip() == "results=\"true\""
        next_part = next_part + 1

    ## print("Has next : " + str(has_next) + ", cursor : " + next_cursor)
    return has_next, next_cursor

def store_msg_ids(response):
    json_string = response.read().decode('UTF-8')  # This will return entire content.
    json_data = json.loads(json_string)

    for event in json_data:
        entry_message = event['entries'][0]['data']['message']

        message = json.loads(entry_message)
        try:
            msg_id = message['id']
            ## if id in ids:
                ## print("Ya estaba!!!")

            ids.add(msg_id)
            ## print(msg_id)
        except KeyError:
            sys.stderr.write("Error al buscar Facebook ID en : %s\n" % entry_message)


def perform_request(has_next, next_cursor):

    uri = ISSUES_EVENTS_ if  has_next == False else ISSUES_EVENTS_ + "?&" + next_cursor
    # print("uri : "+ uri)
    conn.request("GET", uri, {}, HEADERS_)
    return conn.getresponse()


response = perform_request(False, None)
(has_next, next_cursor) = parse_link(response)
store_msg_ids(response)

while has_next:
    response = perform_request(has_next, next_cursor)
    (has_next, next_cursor) = parse_link(response)
    store_msg_ids(response)

# print("------------------------------------------------------------------------------------")

for id in ids:
    print(id)