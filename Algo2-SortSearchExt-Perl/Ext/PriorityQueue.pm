package Ext::PriorityQueue;

use strict;
use warnings;

# WARNING : this package just mimics a priority queue, but it doesn't
#           exhibits its execution order (it's implemented with an array
#           and the search for the lowest one is O(n) )

sub new() {
	my $class = shift;
	
	my $this;
	$this->{'queue'} = [];
	
	bless $this, $class;
}

sub add($$) {
	my $this = shift;
	my $value = shift;
	
	push( @{$this->{'queue'}}, $value);
}


sub _topIndex($) {
	my $this = shift;
	
	my $ret = 0;
	for( my $i=1; $i < @{$this->{'queue'}}; $i++ ) {
		if ( $this->{'queue'}->[$i] <  $this->{'queue'}->[$ret] ) {
			$ret = $i;
		}
	}
	
	return $ret;
}

## Obtains the top element. The queue should not be empty
sub top($) {
	my $this = shift;
	
	return $this->{'queue'}->[ _topIndex($this) ];
}

## Obtains the top element and deletes it from the queue.
## The queue should not be empty
sub get() {
	my $this = shift;
	
	my $top = _topIndex($this);
	my $last = size($this) - 1;
	my $ret = $this->{'queue'}->[ $top ];
	$this->{'queue'}->[ $top ] = $this->{'queue'}->[ $last ];
	delete $this->{'queue'}->[ $last ];
	
	return $ret;
}

sub isEmpty($) {
	my $this = shift;
	
	return @{$this->{'queue'}} == 0;
}

sub size($) {
	my $this = shift;
	
	return @{$this->{'queue'}};
}

1;