package Ext::TapeLib;

use strict;
use warnings;
use lib '..';

use Ext::Tape;

use constant DEFAULT_TAPES => 4;

use constant READ => 0;
use constant WRITE => 1;

our @EXPORT = qw(READ WRITE);

sub new($) {
	my $class = shift;
	my $tapeNum = shift || DEFAULT_TAPES;
	
	my $this;
	$this->{'numTapes'} = $tapeNum;
	 _createTapes($this, $tapeNum);
	 _setTapesForRead($this);
	
	bless $this, $class;
};

sub _setTapesForRead($) {
	my $this = shift;
	
	foreach my $i (0 .. $this->{'numTapes'}-1) {
		$this->{'tapeFlag'}->[$i] = READ;
	}
}

sub _createTapes() {
	my $this = shift;
	my $numTapes = shift;
	
	foreach my $i (1 .. $numTapes) {
		push(@{ $this->{'tape'} }, Ext::Tape->new("Tape$i") );
	};
	
	closeTapes($this);
}

sub closeTapes($) {
	my $this = shift;
	
	foreach my $tape (@{ $this->{'tape'} }) {
		$tape->close();
	}
}

sub openTapes($) {
	my $this = shift;
	
	foreach my $tape (@{ $this->{'tape'} }) {
		$tape->open();
	}
}

sub eraseTapes($) {
	my $this = shift;
	
	foreach my $tape (@{ $this->{'tape'} }) {
		$tape->erase();
	}
}

sub getTape($$) {
	my $this = shift;
	my $i = shift;
	
	return $this->{'tape'}->[$i];
} 

sub getStats($) {
	my $this = shift;
	
	my $ret;
	
	foreach my $tape (@{ $this->{'tape'} }) {
		my $stat = $tape->getStats();
		$ret->{'reads'} += $stat->{'reads'};
		$ret->{'writes'} += $stat->{'writes'};
	};
	
	return $ret;
}

sub getTapeFlag($$) {
	my $this = shift;
	my $tape = shift;
	
	return $this->{'tapeFlag'}->[$tape]
}

sub setTapeFlag($$$) {
	my $this = shift;
	my $tape = shift;
	my $value = shift;
	
	$this->{'tapeFlag'}->[$tape] = $value;
}

sub resetStats($) {
	my $this = shift;
	
	foreach my $tape (@{ $this->{'tape'} }) {
		$tape->resetStats();
	}
}


sub saveToTape($$$) {
	my $this = shift;
	my $tape = shift;
	my $value = shift;
	
	$this->{'tape'}->[$tape]->save($value);
} 


sub readFromTape($$) {
	my $this = shift;
	my $tape = shift;
	
	return $this->{'tape'}->[$tape]->read();
}

sub closeRecordOnTape($$) {
	my $this = shift;
	my $tape = shift;
	
	return $this->{'tape'}->[$tape]->closeRecord();
}

sub tapeForWrite() {
	my $this = shift;
	my $tape = shift;
	
	$this->{'tapeFlag'}->[$tape] = WRITE;
}

sub _getTapeForWrite($) {
	my $this = shift;
	
	my $ret = 0;
	
	foreach my $i (0 .. $this->{'numTapes'}-1) {
		if ( $this->{'tapeFlag'}->[$i] == WRITE ) {
			$ret = $i;
		}	
	}
	
	return $ret;
}

sub save($$) {
	my $this = shift;
	my $value = shift;
	
	my $tape = _getTapeForWrite($this);
	$this->{'tape'}->[$tape]->save($value);
};

## All the tapes must be closed (aka the tape lib. is closed)
sub getNumTapesEmpty($) {
	my $this = shift;
	
	my $emptyCount = 0;
	foreach my $tape (@{ $this->{'tape'} }) {
		if ( $tape->isEmpty() ) {
			$emptyCount++;
		}
	}
	
	return $emptyCount;
}

## All the tapes must be open (aka the tape lib. is closed)
sub getNumTapesAtEOT($) {
	my $this = shift;
	
	my $count = 0;
	foreach my $tape (@{ $this->{'tape'} }) {
		if ( $tape->isEOT() ) {
			$count++;
		}
	}
	
	return $count;

}

## advance all tapes with flag READ to the next record;
sub nextRecordForOpenTapes($) {
	my $this = shift;

	foreach my $i (0 .. $this->{'numTapes'}-1) {
		if ( $this->{'tapeFlag'}->[$i] == READ ) {
			$this->{'tape'}->[$i]->nextRecord();
		}	
	}
}

1;
