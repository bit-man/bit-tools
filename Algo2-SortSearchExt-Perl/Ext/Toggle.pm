package Ext::Toggle;

use strict;
use warnings;

use constant RESET_VALUE => 0;
use constant SET_VALUE => 1;

sub new() {
	my $class = shift;
	my $value = shift || RESET_VALUE;
	
	my $this;
	$this->{'value'} = $value;
	
	bless $this, $class;
}

sub set($) {
	my $this = shift;
	
	$this->{'value'} = SET_VALUE;
}

sub reset($) {
	my $this = shift;
	
	$this->{'value'} = RESET_VALUE;
}


sub isSet($) {
	my $this = shift;
	
	return ( $this->{'value'} == SET_VALUE );
}

sub isReset($) {
	my $this = shift;
	
	return ! isSet($this);
}


sub toggle() {
	my $this = shift;
	
	if ( isSet($this)) {
		$this->reset();
	} else {
		set($this);
	}
}

1;