package Ext::Registers;

use strict;
use warnings;

use constant DEFAULT_VALUE => 0;

sub _initRegistries($$) {
	my $array = shift;
	my $numReg = shift;
	
	for( my $i=0; $i < $numReg; $i++) {
		$array->[$i] = DEFAULT_VALUE;
	}
}

sub new($) {
	my $class = shift;
	my $numReg = shift;
	my $this;
	$this->{'numReg'} = $numReg;
	$this->{'reg'} = [];
	
	_initRegistries($this->{'reg'},$numReg);

	bless $this, $class;
}

sub getReg($$) {
	my $this = shift;
	my $regNum = shift;

	return $this->{'reg'}->[$regNum];
}

sub setReg($$$) {
	my $this = shift;
	my $regNum = shift;
	my $value = shift;
	
	$this->{'reg'}->[$regNum] = $value;
}

sub sortString($) {
	my $this = shift;

	my @aux = sort( @{ $this->{'reg'} } );
	@{ $this->{'reg'} } = @aux;
}

sub getRegNum($) {
	my $this = shift;
	return $this->{'numReg'};
}

sub showReg($) {
	my $this = shift;

	for(my $i=0; $i < $this->{'numReg'}; $i++) {
		print "Register #".$i." : '".getReg($this,$i)."'\n";
	}
}

sub getLowerRegString($) {
	my $this = shift;
	
	my $lower = 0;
	for( my $i=0; $i < $this->{'numReg'}; $i++) {
		if ( $this->{'reg'}->[$i] le $this->{'reg'}->[$lower] ) {
			$lower = $i;
		}
	}
	
	return $lower;
}
1;
