package Ext::Tape;

use strict;
use warnings;

use constant EOR => '***EOR***';
use constant EMPTY_SIZE => 0;
use constant DEFAULT_NAME => "Tape";

use constant OPEN => 1;
use constant CLOSED => 2;

sub new() {
	my $class = shift;
	my $name = shift || DEFAULT_NAME;
	
	my $this;
	$this->{'status'} = OPEN;
	resetStats($this);
	_initParams($this,$name);
	
	bless $this, $class;
};

sub _initParams($$) {
	my $this = shift;
	my $name = shift;
	
	$this->{'tape'} = []; ## empty tape
	$this->{'current'} = 0;
	$this->{'name'} = $name;
}

sub isEmpty($) {
	my $this = shift;
	
	if ( $this->{'status'} == OPEN ) {
		print STDERR "isEmpty() : The tape ".getName($this)." is OPEN\n";
		return 0;
	}
	return (@{ $this->{'tape'} } == EMPTY_SIZE);
}

sub save($$) {
	my $this = shift;
	my $value = shift;

	if ( $this->{'status'} == CLOSED ) {
		print STDERR "save() : The tape ".getName($this)." is CLOSED\n";
		return;
	}
	
	$this->{'tape'}->[ $this->{'current'}++ ] = $value;
	$this->{'stats'}->{'writes'}++;
};

sub read($) {
	my $this = shift;
	my $ret;
	
	if ( $this->{'status'} == CLOSED ) {
		print STDERR "read() : The tape ".getName($this)." is CLOSED\n";
		return;
	};
	
	if ( isEOR($this) ) {
		$ret = 0;  ## meaningless value, beyond end of tape
	} else {
		$ret = $this->{'tape'}->[ $this->{'current'}++ ];
	}
	
	$this->{'stats'}->{'reads'}++;
	return $ret;
};

sub isEOR($) {
	my $this = shift;
	
	if ( $this->{'status'} == CLOSED ) {
		print STDERR "isEOR() : The tape ".getName($this)." is CLOSED\n";
		return;
	}
	
	my $endPlus = @{$this->{'tape'}};
	return ( ! defined $this->{'tape'}->[ $this->{'current'} ] )
		   || ( $this->{'tape'}->[ $this->{'current'} ] eq EOR)
		   || ( $this->{'current'} == $endPlus );
}

sub isEOT($) {
	my $this = shift;	
	
	if ( $this->{'status'} == CLOSED ) {
		print STDERR "isEOT() : The tape ".getName($this)." is CLOSED\n";
		return;
	}
	
	return ( @{  $this->{'tape'} } == $this->{'current'} );
}

sub close($) {
	my $this = shift;
	
	if ( $this->{'status'} == CLOSED ) {
		print STDERR "close() : The tape ".getName($this)." is already CLOSED\n";
		return;
	}
	
	my $end = @{$this->{'tape'}} - 1;
	delete @{$this->{'tape'}}[$this->{'current'} .. $end];
	$this->{'status'} = CLOSED;
}

sub open($) {
	my $this = shift;
	
	if ( $this->{'status'} == OPEN ) {
		print STDERR "open() : The tape ".getName($this)." is already OPEN\n";
		return;
	}
	
	$this->{'current'} = 0;
	$this->{'status'} = OPEN;
}

sub _jumpToEndOfRecord($) {	
	my $this = shift;

	while( ! isEOR($this) ) {
		$this->{'current'}++;
	};
}

sub nextRecord($) {
	my $this = shift;

	if( $this->{'status'} == CLOSED) {
		print STDERR "nextRecord() : The tape ".getName($this)." is CLOSED\n";

		return;
	};
	
	_jumpToEndOfRecord($this);
	$this->{'current'}++
		unless isEOT($this);
}

sub closeRecord() {
	my $this = shift;
	
	if( $this->{'status'} == CLOSED) {
		print STDERR "closeRecord() : The tape ".getName($this)." is CLOSED\n";
		return;
	};
	
	save($this, EOR);
}

sub getCurrent() {
	my $this = shift;
	
	if( $this->{'status'} == CLOSED) {
		print STDERR "getCurrent() : The tape ".getName($this)." is CLOSED\n";
		return;
	};
	
	return $this->{'current'};
}

sub getName($) {
	my $this = shift;
	
	return $this->{'name'};
}

sub erase() {
	my $this = shift;
	
	_initParams($this, getName($this));
	## The status remains unchanged
}

sub getStats($) {
	my $this = shift;
	
	return $this->{'stats'}
}

sub resetStats($) {
	my $this = shift;
	
	
	$this->{'stats'}->{'reads'} = 0;
	$this->{'stats'}->{'writes'} = 0;
}

=pod

data structure

each tape record is represented by an array, meaning that

* if the array is empty the tape also is
* each record is separated by an EOR separator
* a record is delimited by two EOR or an EOR and the end of the array

Each time the tape is open can be read or wrote, but not both.
In case this is done the results can be inconsistent.
=cut

1;