#!/usr/bin/perl

use strict;
use warnings;
use lib '..';

use constant NUM_REG => 10;

use Test::More qw(no_plan);

###################################################### Initialization test

BEGIN { use_ok( 'Ext::PriorityQueue' ); }

my $pq = Ext::PriorityQueue->new();

isnt( $pq, undef, "Must obtain a valid reference");
is ($pq->size(), 0, "Emqty queue means length zero");
ok ($pq->isEmpty(), "New queue means an empty queue");

$pq->add(10);
is ($pq->size(), 1, "One element added");
ok ( ! $pq->isEmpty(), "One element added means no more empty queue");

$pq->add(1);
is ($pq->size(), 2, "One element added");
ok ( ! $pq->isEmpty(), "One element added means no more empty queue");

$pq->add(100);
is ($pq->size(), 3, "One element added");
ok ( ! $pq->isEmpty(), "One element added means no more empty queue");

is ($pq->top(), 1, "Top element must be 1");
is ($pq->size(), 3, "Top doesn't changes queue length");
ok ( ! $pq->isEmpty(), "One element added means no more empty queue");


is ($pq->get(), 1, "Top element must be 1");
is ($pq->size(), 2, "get() does changes queue length");
ok ( ! $pq->isEmpty(), "One element added means no more empty queue");

is ($pq->get(), 10, "Top element must be 1");
is ($pq->size(), 1, "get() does changes queue length");
ok ( ! $pq->isEmpty(), "One element added means no more empty queue");

is ($pq->get(), 100, "Top element must be 1");
is ($pq->size(), 0, "get() does changes queue length");
ok ( $pq->isEmpty(), "One element added means no more empty queue");
