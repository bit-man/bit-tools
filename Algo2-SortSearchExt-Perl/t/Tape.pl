#!/usr/bin/perl

use strict;
use warnings;
use lib '..';

use constant FALSE => "";
use constant TRUE => ! FALSE;

use constant VALUE => "VALUE";
use constant VALUE2 => "VALUE2";

use Test::More qw(no_plan);

###################################################### Initialization test

BEGIN { use_ok( 'Ext::Tape' ); }

my $tape = Ext::Tape->new();

isnt( $tape, undef, "A reference to the module must exist");
$tape->close();
is( $tape->isEmpty(), TRUE, "A new tape must be empty" );
###################################################### Single value test

$tape->open();
$tape->save(VALUE);
$tape->close();


is( $tape->isEmpty(), FALSE, "added some value, cannot be empty" );
$tape->open();
is( $tape->isEOR(), FALSE, "End of record not reached");
is( $tape->isEOT(), FALSE, "End of tape not reached");
is ($tape->read, VALUE, "Read value stored");
is( $tape->isEOR(), TRUE, "End of record reached");
is( $tape->isEOT(), TRUE, "End of tape reached");
$tape->close();

###################################################### Two values in a record

$tape->open();
$tape->save(VALUE2);
$tape->save(VALUE);
$tape->close();

is( $tape->isEmpty(), FALSE, "added some value, cannot be empty" );
$tape->open();
is( $tape->isEOR(), FALSE, "End of record not reached");
is( $tape->isEOT(), FALSE, "End of tape not reached");

is ($tape->read, VALUE2, "Read VALUE2 stored");
is( $tape->isEOR(), FALSE, "End of record reached");
is( $tape->isEOT(), FALSE, "End of tape reached");

is ($tape->read, VALUE, "Read VALUE stored");
is( $tape->isEOR(), TRUE, "End of record reached");
is( $tape->isEOT(), TRUE, "End of tape reached");
$tape->close();

###################################################### Two records, one value each

$tape->open();
$tape->save(VALUE2);
$tape->closeRecord();
$tape->save(VALUE);
$tape->close();


is( $tape->isEmpty(), FALSE, "added some value, cannot be empty" );
$tape->open();
is( $tape->isEOR(), FALSE, "End of record not reached");
is( $tape->isEOT(), FALSE, "End of tape not reached");

is ($tape->read, VALUE2, "Read VALUE2 stored");
is( $tape->isEOR(), TRUE, "End of record reached");
is( $tape->isEOT(), FALSE, "End of tape reached");

is ($tape->read, 0, "End of record, meaningless value (DON'T DO THIS !!)");

$tape->nextRecord();

is ($tape->read, VALUE, "Read VALUE stored");
is( $tape->isEOR(), TRUE, "End of record reached");
is( $tape->isEOT(), TRUE, "End of tape reached");

$tape->close();
