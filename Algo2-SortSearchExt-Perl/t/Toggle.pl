#!/usr/bin/perl

use strict;
use warnings;
use lib '..';

use Test::More qw(no_plan);

###################################################### Initialization test

BEGIN { use_ok( 'Ext::Toggle' ); }

my $t = Ext::Toggle->new();
isnt( $t, undef, "A reference to Ext::Toggle must be obtained");

is ($t->isSet, "", "Initial value : reset");
is ($t->isReset, 1, "Initial value : reset");

$t->set();
is ($t->isSet, 1, "Value set");
is ($t->isReset, "", "Value set");


$t->reset();
is ($t->isSet, "", "Value : reset");
is ($t->isReset, 1, "Value : reset");

$t->toggle();
is ($t->isSet, 1, "Value set after toggle");
is ($t->isReset, "", "Value set after toggle");


$t->toggle();
is ($t->isSet, "", "Value reset after toggle");
is ($t->isReset, 1, "Value reset after toggle");
