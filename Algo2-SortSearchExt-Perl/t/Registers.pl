#!/usr/bin/perl

use strict;
use warnings;
use lib '..';

use constant NUM_REG => 10;

use Test::More qw(no_plan);

###################################################### Initialization test

BEGIN { use_ok( 'Ext::Registers' ); }

my $reg = Ext::Registers->new(NUM_REG);

isnt( $reg, undef, "Must obtain a valid reference");

$reg->setReg(1,1000);
is( $reg->getReg(1), 1000, "Must return the stored value (1000)");

for(my $i=0; $i < NUM_REG; $i++) {
	$reg->setReg($i,1000+$i)
}

for(my $i=0; $i < NUM_REG; $i++) {
	is( $reg->getReg($i), 1000+$i, "Must return the stored value (".(1000+$i).")");
}

is ($reg->getLowerRegString(), 0, "The lowest one is the first register" );
