#!/usr/bin/perl

use constant TAPES => 3;

my $value = "EJEMPLODEORDENACIONFUSION";

# No more settings beyond this point
####################################################################################

use strict;
use warnings;
use lib '..';

use Ext::Tape;
use Ext::Registers;
use Ext::TapeLib qw(READ WRITE);

use constant NUM_REG => TAPES - 1;

use constant VALUE_FILLER => "ZZ-Filler Value"; ## when ordering, will be in the last position

my $tapeLib;
my $tapeStats;
my $reg;

sub	spreadInputOnTapes() {
	my @input = split( //, $value);
	
	$tapeLib->openTapes();
	my $last = shift @input;
	my $tape = 0;
	$tapeLib->saveToTape($tape,$last);

	foreach my $val (@input) {
		
		if ($val lt $last) { ## close the record on this tape and go to the next one
			$tapeLib->closeRecordOnTape($tape);
			$tape++;
			if ($tape == TAPES-1) { ## last tape is saved for write
				$tape = 0;
			}
		}
		
		$last = $val;
		$tapeLib->saveToTape($tape,$val);
	}
	
	$tapeLib->closeRecordOnTape($tape);
	$tapeLib->closeTapes();
}

sub tapesShowCurrentRecord($) {
	my $tape = shift;
	
	while( ! $tape->isEOR() ) {
		print $tape->read() . "-";
	}
	print "\n";
};

sub tapesShow {
	my $display = shift;
	print "*----------------------- $display -----------------------*\n";
	
	my $i = 0;
	foreach my $i (0..TAPES-1) { 
		my $tape = $tapeLib->getTape($i);
		$tape->open();
		print "########################## Tape ".$i." (".$tape->getName().")\n";
		print " is empty \n"
			if ( $tape->isEOT());

		while( ! $tape->isEOT() ) {
			tapesShowCurrentRecord($tape);
			$tape->nextRecord();
		}

		$i++;
		$tape->close();
		print "----------------------------------------\n";
	}
}

sub loadForGetAndSortRecords($) {
	my $tapeForWrite = shift;
	
	my $i=0;
	foreach my $tape (0..TAPES-1) {
		next if ($tape == $tapeForWrite);
		
		my $value;
		
		if ( ! $tapeLib->getTape($tape)->isEOR()) {
			$value = $tapeLib->readFromTape($tape);
		} else {
			$value = VALUE_FILLER;
		};
		
		$reg->setReg( $i, $value );
		print "Read from tape '$tape' and set reg '$i' (value : '$value')\n";
		$i++;
	}
}

sub numOfExhaustedTapes($) {
	my $tapeForWrite = shift;

	my $eotCount = 0;
	foreach my $tape (0..TAPES-1) {
		next if ($tape == $tapeForWrite); ## remember, one of them is for output
		if ( $tapeLib->getTape($tape)->isEOT() ) {
			$eotCount++;
		};
	}
	
	return $eotCount;
}

sub noSourceTapeIsExhausted($) {
	my $tapeForWrite = shift;

	return (numOfExhaustedTapes($tapeForWrite) == 0); 
	
}

sub allRegsAreExhausted() { 
	my $count = 0;
	foreach my $i ( 0..NUM_REG-1) {
		if ( $reg->getReg($i) eq VALUE_FILLER ) {
			$count++;
		}
	}

	return ($count == NUM_REG);
}

sub getTapeNumWhereForThisReg($$) {
	my $regNum = shift;	
	my $tapeForWrite = shift;
	
	my $reg = 0;
	my $add = 0;
	foreach my $tape (0..TAPES-1) {
		if ($tape == $regNum) {
			$reg = $tape + $add;
		};
		
		if ($tape == $tapeForWrite) {
			$add++;
		}
	}
	
	return $reg;
}


sub eraseSourceTapes($) {
	my $tapeForWrite = shift;
	
	foreach my $i (0..TAPES-1) {
		next if ($i == $tapeForWrite);
		
		$tapeLib->getTape($i)->erase();
	}
}

sub sortAndSaveToTapeDest($) {
	my $tapeForWrite = shift;
	
	my $lowest = $reg->getLowerRegString();
	print "Lower : ".$lowest.", '".$reg->getReg($lowest) ."'\n";
	$tapeLib->saveToTape( $tapeForWrite, $reg->getReg($lowest) );
	
	my $tapeToReadFrom = getTapeNumWhereForThisReg($lowest,$tapeForWrite);
	if (  $tapeLib->getTape($tapeToReadFrom)->isEOR() ) {
		$reg->setReg( $lowest, VALUE_FILLER );
	} else {
		$reg->setReg( $lowest, $tapeLib->readFromTape($tapeToReadFrom) );
	}
}

sub nextRecordForOpenTapesNotExhausted($) {
	my $tapeForWrite = shift;

	foreach my $i (0..TAPES-1) {
		next if ($i == $tapeForWrite);
		
		my $tape = $tapeLib->getTape($i);
		if ($tape->isEOT($i)) {
			$tape->nextRecord();
		}
	}
}

sub revertStatusForWriteAndExhaustedTapes() {

	no strict; ## to allow use of READ and WRITE as barewords
	
	foreach my $tape (0..TAPES-1) {
		if ($tapeLib->getTapeFlag($tape) == WRITE) { ## used for write
			$tapeLib->getTape($tape)->close();
			$tapeLib->getTape($tape)->open();
			$tapeLib->setTapeFlag($tape,READ);
			next;
		}
		
		if ($tapeLib->getTape($tape)->isEOT() ) { ## exhausted and in READ mode
			$tapeLib->getTape($tape)->close();
			$tapeLib->getTape($tape)->open();
			$tapeLib->setTapeFlag($tape,WRITE);
			next;
		}
	}

	use strict;
}

sub getAndSortRecords($) {
	my $tapeForWrite = shift;

	$tapeLib->openTapes();
	
	## Initial load
	loadForGetAndSortRecords($tapeForWrite);

	while( noSourceTapeIsExhausted($tapeForWrite) ) {
		while( ! allRegsAreExhausted() ) {
			sortAndSaveToTapeDest($tapeForWrite);
		}

		print "------------> Next record\n";
		nextRecordForOpenTapesNotExhausted($tapeForWrite);
		$tapeLib->getTape($tapeForWrite)->close();
		revertStatusForWriteAndExhaustedTapes();
		
		loadForGetAndSortRecords($tapeForWrite);
	}
	
	eraseSourceTapes($tapeForWrite);

	$tapeLib->closeTapes();
}

sub orderUsingOneOutputTape() {
	my $tapeForWrite = TAPES - 1;
	
	while( $tapeLib->getNumTapesEmpty() != TAPES - 1 ) {
		print "Tape to write to : #$tapeForWrite\n";
	
		getAndSortRecords($tapeForWrite);
		tapesShow("orderUsingOneOutputTape '$tapeForWrite'");
		
		$tapeForWrite++;
		if ($tapeForWrite == TAPES) {
			$tapeForWrite = 0;
		}
		
	}
}

####################################################################################

package main;

	$reg = Ext::Registers->new(TAPES - 1);
	print "Registers : #" . $reg->getRegNum() ."\n";
	$tapeLib = Ext::TapeLib->new(TAPES);
	$tapeStats  = {
		'reads' => 0,
		'writes' => 0
	};

	tapesShow("Empty tapes");
	spreadInputOnTapes();
	
	tapesShow("Initial load");
	orderUsingOneOutputTape();

1;