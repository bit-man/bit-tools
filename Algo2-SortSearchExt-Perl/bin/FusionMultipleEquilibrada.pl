#!/usr/bin/perl

use constant NUM_REG => 3;

my $value = "EJEMPLODEORDENACIONFUSION";

# No more settings beyond this point
####################################################################################

use strict;
use warnings;
use lib '..';

use Ext::Tape;
use Ext::Registers;
use Ext::Toggle;

use constant TAPES => 2 * NUM_REG;

use constant VALUE_FILLER => "ZZZZZ"; ## when ordering, will be in the last position

my $reg; ## Registros

sub tapesShowCurrentRecord($) {
	my $tape = shift;
	
	while( ! $tape->isEOR() ) {
		print $tape->read() . "-";
	}
	print "\n";
};

sub tapesShow {
	my $display = shift;
	print "*----------------------- $display -----------------------*\n";
	
	my $i = 0;
	foreach my $thisTape (@_) { 
		$thisTape->open();
		print "########################## Tape ".$i." (".$thisTape->getName().")\n";
		print " is empty \n"
			if ( $thisTape->isEOT());

		while( ! $thisTape->isEOT() ) {
			tapesShowCurrentRecord($thisTape);
			$thisTape->nextRecord();
		}

		$i++;
		$thisTape->close();
		print "----------------------------------------\n";
	}
}

sub createTapes() {
	my @tape;
	
	for(my $i = 1; $i <= TAPES; $i++ ) {
		push(@tape, Ext::Tape->new("Tape0$i") );
	}
	
	print "Tapes : #".@tape."\n";
	closeTapes(@tape);
	return @tape;
}

sub regLoadValues(@) {
	my @val = @_;

	for(my $i = 0; $i < NUM_REG; $i++) {
		my $value;
		
		if (defined $val[$i]) {
			$value = $val[$i];
		} else {
			$value = VALUE_FILLER;
		}
		$reg->setReg($i,$value);
	}
}

sub regSaveToTape($) {
	my $tape = shift;
	for( my $i = 0; $i < NUM_REG; $i++) {
		if ( defined($reg->getReg($i)) && $reg->getReg($i) ne VALUE_FILLER) {
			$tape->save( $reg->getReg($i) );
		}
	}
	
	$tape->closeRecord();
}

sub openTapes(@) {
	foreach my $tape (@_) {
		$tape->open();
	}
}

sub closeTapes(@) {
	foreach my $tape (@_) {
		$tape->close();
	}
}

sub loadValuesInTapes($@) {
	my $values = shift;
	my @tape = @_;
	
	openTapes(@tape);
	
	my $thisTape = 0;
	for( my $index = 0; $index < length($values); $index += NUM_REG ) {
		my @val;
		for( my $i = 0; $i < NUM_REG && ($index + $i) < length($values); $i++) {	
			push( @val, substr($values, $index + $i, 1) );
		};

		regLoadValues(@val);
		$reg->sortString();

		regSaveToTape( $tape[$thisTape++] );
		
		if ($thisTape == NUM_REG) {
			$thisTape = 0;
		}
	}
	
	closeTapes(@tape);
};

sub onlyOneTapeIsFull(@) {
	my @tape = @_;
	
	my $emptyCount = 0;
	foreach my $tape (@tape) {
		if ( $tape->isEmpty() ) {
			$emptyCount++;
		}
	}
	
	return ($emptyCount + 1) == @tape;
}

sub getTapesLimitsDest($) { 
	my $lower = shift;
	
	
	my ($begin,$end);
	if ($lower->isSet()) {
		$begin = 0;
		$end = NUM_REG - 1;
	} else {
		$begin = NUM_REG;
		$end = TAPES - 1;
	}
	
	return ($begin,$end);
}

sub getTapesLimits($) { 
	my $lower = shift;
	
	
	my ($begin,$end);
	if ($lower->isReset()) {
		$begin = 0;
		$end = NUM_REG - 1;
	} else {
		$begin = NUM_REG;
		$end = TAPES - 1;
	}
	
	return ($begin,$end);
}

 
sub allRegsAreExhausted() { 
	my $count = 0;
	for(my $i=0; $i < NUM_REG; $i++) {
		if ( $reg->getReg($i) eq VALUE_FILLER ) {
			$count++;
		}
	}

	return ($count == NUM_REG);
}


sub advanceToNextRecord($@) { 
	my $lower = shift;
	my @tape = @_;

	my ($begin,$end) = getTapesLimits($lower);

	for(my $i=$begin; $i <= $end; $i++) {
		$tape[$i]->nextRecord();
	}
}

sub allSourceTapesAreExhausted($@) { 
	my $lower = shift;
	my @tape = @_;


	my ($begin,$end) = getTapesLimits($lower);

	my $eotCount = 0;
	for(my $i=$begin; $i <= $end; $i++) {
		if ( $tape[$i]->isEOT() ) {
			$eotCount++;
		}
	}
	
	return ($eotCount == NUM_REG)
}


sub eraseTapes($@) { 
	my $lower = shift;
	my @tape = @_;
	
	my ($begin,$end) = getTapesLimits($lower);
	
	for(my $i=$begin; $i <= $end; $i++) {
		$tape[$i]->erase();
	}
}

sub loadForGetAndSortRecords($$@) {
	my $origBegin = shift;
	my $origEnd = shift;
	my @tape = @_;
	
	my $i = 0;
	for(my $orig = $origBegin; $orig <= $origEnd; $orig++) {
		$reg->setReg( $i, $tape[$orig]->read() );
		$i++
	};
}

sub getTapeNumWhereForThiReg($$) {
	my $lower = shift;
	my $regNum = shift;
	
	if ($lower->isReset()) {
		return $regNum;
	} else {
		return $regNum + NUM_REG;
	}
}

sub readFromTapesSortSaveAndSaveToTapeDest($$@) {
	my $dest = shift;
	my $lower = shift;
	my @tape = @_;
	
	my $lowest = $reg->getLowerRegString();
	print "Lower : ".$lowest.", '".$reg->getReg($lowest) ."'\n";
	$tape[$dest]->save( $reg->getReg($lowest) );
	
	my $tapeToReadFrom = getTapeNumWhereForThiReg($lower,$lowest);

	if (  $tape[$tapeToReadFrom]->isEOR() ) {
		$reg->setReg( $lowest, VALUE_FILLER );
	} else {
		$reg->setReg( $lowest, $tape[$tapeToReadFrom]->read() );
	}
}

sub getAndSortRecords($@) { 
	my $lower = shift;
	my @tape = @_;

	openTapes(@tape);
	
	## Initial load
	my ($origBegin,$origEnd) = getTapesLimits($lower);
	print "Carga inicial : '$origBegin,$origEnd'\n";
	loadForGetAndSortRecords($origBegin,$origEnd,@tape);
	
	my ($destBegin,$destEnd) = getTapesLimitsDest($lower);
	
	my $dest = $destBegin;
	while( ! allSourceTapesAreExhausted($lower, @tape ) ) {
		while( ! allRegsAreExhausted() ) {
			readFromTapesSortSaveAndSaveToTapeDest($dest,$lower,@tape);
		}

		advanceToNextRecord($lower,@tape);
		$tape[$dest]->closeRecord();
		
		loadForGetAndSortRecords($origBegin,$origEnd,@tape);
		
		$dest++;
		if ($dest > $destEnd) {
			$dest = $destBegin;
		}
	}
	
	eraseTapes($lower, @tape);

	closeTapes(@tape);
}

sub sortValuesInTapes(@) {
	my @tape = @_;
	
	my $lower = Ext::Toggle->new();
	
	while( ! onlyOneTapeIsFull(@tape) ) {
		getAndSortRecords($lower, @tape);
		tapesShow("sortValuesInTapes", @tape);
		$lower->toggle();
	}
}

sub resetTapesStats(@) {
	my @tape = @_;
	
	foreach my $tape (@tape) {
		$tape->resetStats();
	}
}

sub updateStatsFromTapes($@) {
	my $tapeStats = shift;
	my @tape = @_;
	
	foreach my $thisTape (@tape) {
		my $stat = $thisTape->getStats();
		$tapeStats->{'reads'} += $stat->{'reads'};
		$tapeStats->{'writes'} += $stat->{'writes'};
	};
}

sub showTapesStats($) {
	my $tapeStats = shift;
	
	print "\n";
	print "* Tape reads : '".$tapeStats->{'reads'}."'\n";
	print "* Tape writes : '".$tapeStats->{'writes'}."'\n\n";
}

##########################################################

package main;

	$reg = Ext::Registers->new(NUM_REG);
	print "Registers : #" . $reg->getRegNum() ."\n";
	my @tape = createTapes();
	my $tapeStats = {
		'reads' => 0,
		'writes' => 0
	};
	
	updateStatsFromTapes($tapeStats,@tape);
	tapesShow("Recently created", @tape);
	resetTapesStats(@tape);
	
	loadValuesInTapes($value, @tape);
	updateStatsFromTapes($tapeStats,@tape);
	tapesShow("Values loaded", @tape);
	resetTapesStats(@tape);
	
	sortValuesInTapes(@tape);
	updateStatsFromTapes($tapeStats,@tape);
	tapesShow("Sorted", @tape);
	resetTapesStats(@tape);
	
	showTapesStats($tapeStats);

1;
