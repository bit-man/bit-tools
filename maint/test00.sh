#!/bin/bash

testPerl6() {
    cd $HOME/test

    echo "**** Actualizando repositorio cafeperl6"
    svn up cafeperl6

    echo "**** Actualizando Rakudo (Perl 6)"
    export MACOSX_DEPLOYMENT_TARGET=10.3
    cafeperl6/bin/actualizarRakudo

    echo "**** Verificar Copyright"
    export PERL6LIB=$HOME/test/cafeperl6/lib
    cd  $HOME/test/cafeperl6  
    bin/ayudaOpendir.pl ejemplos/
    ../rakudo/parrot_install/bin/perl6 bin/verificarCopyright
}

testBitTools() {

    cd $HOME/bit-tools

    if [[ -x runTests ]]; then
        ./runTests
        ./runTests  blosxom_plugins/twitter/daily/
    else
        echo "RUN TESTS SCRIPT NOT FOUND :-("
    fi
}


function waitForUpdate() {
    while [[ ! -f /tmp/bit-tools_UPDATE_OK ]]; do
        echo "Waiting for update..."
        sleep 60
    done
    rm -f /tmp/bit-tools_UPDATE_OK
}

echo
echo "*************************************************************************"
echo "*****************           Software testing           ******************"
echo "*************************************************************************"
echo

waitForUpdate
# testBitTools
# testPerl6

