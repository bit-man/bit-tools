#!/bin/bash

__home=$3
export EW_HOME=${__home}/bit-tools/easyWork/
export PATH="$PATH:/usr/local/bin"
export PERL5LIB="${__home}/lib:$EW_HOME/lib"

POLLXN_SPAM=$EW_HOME/bin/runPollxnSPAM.pl 

function updateSpawnedTools() {
    cd ${__home}/sw
    cd Twitter-Daily && git pull && cd ..
    cd -
}

function updateBitTools() {
    cd ${__home}/bit-tools

    maint/svn.exp cleanup
    maint/svn.exp -N up .
    maint/svn.exp up easyWork
    maint/svn.exp up blosxom_plugins
    /usr/sbin/chown -R bitman .
 
    cd -
}

function pollxnSPAM() {
    echo
    echo "*************************************************************************"
    echo "********************       Blog comments spam        ********************"
    echo "*************************************************************************"
    echo

    $POLLXN_SPAM  --host=bit-man.com.ar --user=$1 --pwd=$2 \
        --basedir=/blosxom/entries \
        --folders=Perl,,Mac,Cinemania,DeTodoUnPoco,Java,Linux,Mate,Twitter,Photos,Music,ElHombreQueCalculaba \
        --textfile="$EW_HOME/data/pollxnSPAM.txt"
}

function finkUpdate() {
    echo "*************************************************************************"
    echo "********************       Updating fink core        ********************"
    echo "*************************************************************************"
    echo
    /sw/bin/fink selfupdate

    echo
    echo "*************************************************************************"
    echo "*****************       Updating other packages        ******************"
    echo "*************************************************************************"
    echo
    /sw/bin/fink --yes update-all
}

rm -f /tmp/bit-tools_UPDATE_OK
df -h
# finkUpdate

echo
echo "*************************************************************************"
echo "********************       Updating Bit-Tools        ********************"
echo "*************************************************************************"
echo


updateBitTools
updateSpawnedTools

pollxnSPAM $1 $2

echo
echo "*************************************************************************"
echo "*****************          Perl 5 Maintenance            ******************"
echo "*************************************************************************"
echo
    
# cpan < cpan.input
# cpan -O | grep Reporter

# ls -la /dev/tty
# cd ${__home}
# cat cpanp.lst | $EW_HOME/bin/cpanBulk


touch /tmp/bit-tools_UPDATE_OK
/usr/sbin/chown bitman /tmp/bit-tools_UPDATE_OK

