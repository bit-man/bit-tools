README

Job Submission
--------------

To submit a job the script submitJob must be used, which generates
and submits a job. Running it without parameters produces the next
output 

    Creates and submits a SGE job 
    
  usage:
  submitJob --program /path/to/program [--job name] [--proc numProc] 
         [--dry-run] -- arg1 arg2 ...
 
     --program : Path to the executable to run
     --job : name fo the job to submit (default : Bit-Man) 
     --proc : number of processes to generate (default: )
     --dry-run : generates the job only, does not submit it
     arg1 args2 ... : program arguments
         
   e.g.
   
   submitJob --program $HOME/bin/myMPIprog  --proc 10 -- 10 36
   
   Will submit a job that runs the program located at $HOME/bin/myMPIprogam
   in a MPI environment (invoked with command line parameters 10 and 36)
   using Bit-Man as job name and  10 processes

The folder where the submittion job is saved can be customized by changing the
__baseDir variable value in the submitJob script

Program compilation and execution
---------------------------------
 
Compiling the first exercise is done using the target tpa, and tpb for the second exercise.
Later submit them with the submitJob script.
 
If debugging output is desired define the __DEBUG flag at compile time. e.g. :
 
#  make DEFFLAGS="-D__DEBUG" tpa
 
The output will become verbose and each line will be preceded with a timestamp
between parentheses (using the format seconds.microseconds) followed by the
MPI process number (aka rank) and a message.
This format allows to follow the whole program execution by ordering the output 
lines using the sort command. e.g. :
 
#  make DEFFLAGS="-D__DEBUG" tpa
#  submitJob --program `pwd`/bin/Trapezoidal_sendReceive
 
 ... wait for the job to run
 
# sort /path/to/output/file
 
Enjoy !
 
