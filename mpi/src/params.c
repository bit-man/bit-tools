/*
 * params.c
 *
 *  Created on: Jul 31, 2011
 *      Author: bit-man
 */


#include <unistd.h>
#include <mpi.h>

#include "params.h"
#include "debug.h"

void getValues(int argc, char **argv, double *a, double *b,
	       int *nTrapezoid, int size)
{
	*a = DEFAULT_A;
	*b = DEFAULT_B;
	*nTrapezoid = DEFAUL_N;
	char msg[100];

	char flag;
	while ((flag = getopt(argc, argv, "a:b:n:")) != -1) {
		switch (flag) {
		case 'a':
			*a = atof(optarg);
			break;
		case 'b':
			*b = atof(optarg);
			break;
		case 'n':
			*nTrapezoid = atoi(optarg);
			break;
		default:
			if (rank == 0) {
				sprintf(msg,
					"\nusage:\n    %s [-a firstXvalue] [-b secondXvalue] [-n numOfTrapezoids]\n\n",
					argv[0]);
				perror(msg);
			}

			MPI_Finalize();
			exit(-1);
		}
	}

	sprintf(msg, "a : %f, b : %f, n : %i", *a, *b, *nTrapezoid);
	DEBUG(msg);

	if (*a >= *b) {
		if (rank == 0) {
			perror
			    (" 'a' must be lower than 'b' (remember your calculus lessons :-) )");
		}

		MPI_Finalize();
		exit(-1);
	}

	if (*nTrapezoid < size) {
		if (rank == 0) {
			perror
			    ("Please use more trapezoids or less processes (don't make me feel sad :-( )");

		}
		MPI_Finalize();
		exit(-1);
	}
	if (rank == 0) {
		printf("a : %f, b : %f, n : %i\n", *a, *b, *nTrapezoid);
	}
}
