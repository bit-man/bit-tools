/***
 * Calculates the area under f(x) = x^2 between values 'a' and 'b' using :
 *      - trapezoid rule
 *      - parallel programming (MPI interface)
 *      - Collective MPI primitives
 *
 * The idea behind this implementation is to divide the x axis into pieces of
 * length 'h', each process evaluates the area under the curve in its own piece
 * only, providing that the x axis piece is comprised between xi = a + (i * h)
 * and xi + h using the formula
 *
 *       Ai = h * ( f(xi) + 2 * f(xi+1) ) /2
 *
 * The process with rank 0 acts as a collector meaning that is the
 * responsible for data spreading and final area calculation.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>

#include "debug.h"
#include "params.h"

#define TAG 		1
#define ROOT		0


int rank, size;

double evalF(double x)
{
	return x * x;
}

double area(double a, double h)
{
	return (evalF(a) + evalF(a + h)) * h / 2;
}

int main(int argc, char **argv)
{
	double a;
	double b;
	int nTrapezoid;


	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	DEBUG("Start");

	getValues(argc, argv, &a, &b, &nTrapezoid, size);
	double h = (b - a) / nTrapezoid;
	char msg[100];
	double integral = 0.0;

	int i;

	/**
	 * Each process calculates the ith piece of the area
	 * under the curve, meaning :
	 *
         * process0 : x0, x0+size, x0+2*size, ...
         * process1 : x1, x1+size, x1+2*size, ...
         *
         * and so on
	 */
	for (i = rank; i < nTrapezoid + rank; i += size) {
		double trapArea;
		double result;

		/***
                   * Might be that the number of process needed near the end
                   * of the interval is less than the number of intervals left
                   * to calculate. To avoid that this process won't do any calculation
                   * (aka suing the null element for addition).
                   * Given that no further calculation will be needed the process
                   * can't be finalized because the process with rank 0 will hang in
                   * the MPI_Reduce primitive call waiting for the finalized process
                   * to answer.
                   */
		if (i < nTrapezoid) {
			DEBUG("Calculating");
			trapArea = area(a + (i * h), h);
		} else {
			DEBUG("Skipping");
			trapArea = 0;
		}
		sprintf(msg, "area = %e", trapArea);
		DEBUG(msg);
		MPI_Reduce(&trapArea, &result, 1, MPI_DOUBLE, MPI_SUM,
			   ROOT, MPI_COMM_WORLD);

		sprintf(msg, "result = %e", result);
		DEBUG(msg);
		if (rank == ROOT) {
			integral += result;
			sprintf(msg, "parcial integral : %e", integral);
			DEBUG(msg);
		}
	}

	DEBUG("End");

	MPI_Finalize();

	if (rank == ROOT) {
		printf("Integral : %e\n", integral);
	}

	return 0;
}
