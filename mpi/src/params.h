/*
 * params.h
 *
 *  Created on: Jul 31, 2011
 *      Author: bit-man
 */

#ifndef PARAMS_H_
#define PARAMS_H_

extern int rank;

#define DEFAULT_A       0.0
#define DEFAULT_B       2.0
#define DEFAUL_N        1000

void getValues(int argc, char **argv, double *a, double *b,
	       int *nTrapezoid, int size);

#endif				/* PARAMS_H_ */
