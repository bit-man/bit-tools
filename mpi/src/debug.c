/*
 * debug.c
 *
 *  Created on: Jul 30, 2011
 *      Author: bit-man
 */


#include "debug.h"

void DEBUG(char *msg)
{
#ifdef __DEBUG
	struct timeval tim;
	gettimeofday(&tim, NULL);
	double secs = tim.tv_sec + (tim.tv_usec / 1000000.0);
	printf("(%f) %d: %s\n", secs, rank, msg);
#endif
}
