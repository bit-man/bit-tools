/*
 * debug.h
 *
 *  Created on: Jul 30, 2011
 *      Author: bit-man
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

extern int rank;

void DEBUG(char *msg);

#endif				/* DEBUG_H_ */
