/***
 * Calculates the area under f(x) = x^2 between values 'a' and 'b' using :
 *      - trapezoid rule
 *      - parallel programming (MPI interface)
 *      - Simple send and receive MPI primitives
 *
 * The idea behind this implementation is to divide the x axis into pieces of
 * length 'h', thus send each process the value xi = a * (i + h) which calculates
 * f(xi) and send the result back, providing that the area under the curve (A) :
 *
 *
 *       A = h * ( f(x0) + 2 * f(x1) + ... + 2* f(xn-1) + f(xn) ) /2
 *
 * The process with rank 0 acts as a dispatcher/collector meaning that is the
 * responsible for x axis partitioning, data sending, result collecting and
 * final area calculation.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include "debug.h"
#include "params.h"

#define TAG 		1
#define DISPATCHER 	0

int rank, size;

struct {
	int terminate;
	double x;
} inF;

double evalF(double x)
{
	return x * x;
}

int main(int argc, char **argv)
{
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int nTrapezoid;
	double a;
	double b;
	int procPoolN = size - 1;

	getValues(argc, argv, &a, &b, &nTrapezoid, procPoolN);

	if (size <= 1) {
		if (rank == 0) {
			perror
			    ("This implementation needs at least 2 processes");
		}
		MPI_Finalize();
		exit(-1);
	}

       /**
         * Complex data type definition : used to define the data to be send to each process
         */
	int inFblocklengths[2] = { 1, 4 };
	MPI_Datatype inFtypes[2] = { MPI_INT, MPI_DOUBLE };
	MPI_Aint displacements[2];
	MPI_Datatype restype;
	MPI_Aint intex, doublex;
	MPI_Type_extent(MPI_INT, &intex);
	MPI_Type_extent(MPI_DOUBLE, &doublex);
	displacements[0] = (MPI_Aint) 0;
	displacements[1] = doublex;

	MPI_Type_struct(2, inFblocklengths, displacements, inFtypes,
			&restype);
	MPI_Type_commit(&restype);

	if (rank == 0) {
		// Dispatcher code
		double h = (b - a) / nTrapezoid;

		char msg[100];
		DEBUG("Start");

		int i;
		double integral = 0.0;
		for (i = 0; i <= nTrapezoid; i++) {

			int process = (i % procPoolN) + 1;

			/***
			 * Might be that the number of process needed near the end
			 * of the interval is less than the number of intervals left
			 * to calculate. To avoid that this process don't receive
			 * data and get blocked in a MPI_Recv then a terminate
			 * flag is passed, meaning that once this calculation is
			 * performed it must end
			 */
			inF.terminate = (nTrapezoid - i + 1) <= procPoolN;
			inF.x = a + (i * h);

			sprintf(msg, "Send to %d (it. %d, x = %e)",
				process, i, inF.x);
			DEBUG(msg);

			MPI_Send(&inF, 1, restype, process, TAG,
				 MPI_COMM_WORLD);

			double response;
			MPI_Recv(&response, 1, MPI_DOUBLE, process, TAG,
				 MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			sprintf(msg, "Receiving from %d, value : %e",
				process, response);
			DEBUG(msg);
			// The first and last result (f(x0) and f(xn))
			// must not be multiplied by 2
			double mult = (i == 0 || i == nTrapezoid) ? 1 : 2;
			integral += mult * response;

			sprintf(msg, "Multiplier : %e", mult);
			DEBUG(msg);
			sprintf(msg, "Integral (parcial) : %e", integral);
			DEBUG(msg);
		}
		integral *= h / 2;

		printf("Integral : %f\n", integral);
	} else {
		// Calculating nodes code
		int terminate = 0;
		DEBUG("Start");

		while (!terminate) {
			MPI_Recv(&inF, 1, restype, DISPATCHER, TAG,
				 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			terminate = inF.terminate;

			DEBUG("Calculate");
			if (terminate) {
				DEBUG("Termination received");
			}

			double response = evalF(inF.x);
			MPI_Send(&response, 1, MPI_DOUBLE, DISPATCHER, TAG,
				 MPI_COMM_WORLD);
		}
		DEBUG("bye");
	}


	MPI_Finalize();

	return 0;
}
