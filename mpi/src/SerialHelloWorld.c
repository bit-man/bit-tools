#include <mpi.h>
#include <stdio.h>

int main(int argc, char **argv)
{
	int rank, size;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (rank % 2 == 0) {
		int b = rank + 10;
		printf("Hello world!, %d\n", rank);
		MPI_Send(&b, 1, MPI_INT, rank + 1, 27, MPI_COMM_WORLD);
	} else {
		int b = 1;
		printf("Bye world!, %d\n", rank);
		MPI_Recv(&b, 1, MPI_INT, rank - 1, 27, MPI_COMM_WORLD,
			 MPI_STATUS_IGNORE);
		printf("B : %d\n", b);
	}

	MPI_Finalize();

	return 0;
}
