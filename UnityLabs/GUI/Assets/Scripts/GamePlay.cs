﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class GamePlay : MonoBehaviour
{


	void Update () {
		if (!Game.IsInProgress())
		{
			return;
		}

		if (transform.position.y > 580)
		{
			Game.Stop();
			return;
		}

		transform.position += Vector3.up;
	}

	
}
