﻿using UnityEngine;

namespace DefaultNamespace
{
    public class Game
    {
        private static bool _inProgress;

        public static void Start()
        {
            _inProgress = true;
        }

        public static void Stop()
        {
            Debug.Log("ENDED");
        }

        public static bool IsInProgress()
        {
            return _inProgress;
        }
    }
}