﻿using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class ButtonClickDetector : MonoBehaviour
    {
        public Button startButton;


        private void OnEnable()
        {
            //Register Button Events
            startButton.onClick.AddListener(StartButtonOnClick);
        }

        private void StartButtonOnClick()
        {
            Debug.Log("Click!");
            Game.Start();

            // TODO hide button
            startButton.enabled = false;
        }
    }
}