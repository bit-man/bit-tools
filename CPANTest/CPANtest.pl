#!/usr/bin/perl

use 5.006;
use strict;
use warnings;

use CPAN;

CPAN::HandleConfig->load;
CPAN::Shell::setup_output;
CPAN::Index->reload;


my @modules = qw/Test::ShellScript Test::More/;

CPAN::Bundle::force('CPAN::Shell','test', @modules);

