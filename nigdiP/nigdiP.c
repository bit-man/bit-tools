/*
 * nigdiP Plugin
 *
 * Copyright (C) 2008, Victor A. Rodriguez (Bit-Man) <victor@bit-man.com.ar>
 * Licensed under GPL v2 terms
 *
 * Based on "Hello World"and "Newline" plugins :-D
 */

#define THIS_PLUGIN_VERSION  "0.0"

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* config.h may define PURPLE_PLUGINS; protect the definition here so that we
 * don't get complaints about redefinition when it's not necessary. */
#ifndef PURPLE_PLUGINS
# define PURPLE_PLUGINS
#endif

#include <glib.h>

/* This will prevent compiler errors in some instances and is better explained in the
 * how-to documents on the wiki */
#ifndef G_GNUC_NULL_TERMINATED
# if __GNUC__ >= 4
#  define G_GNUC_NULL_TERMINATED __attribute__((__sentinel__))
# else
#  define G_GNUC_NULL_TERMINATED
# endif
#endif

#include <string.h>

#include <notify.h>
#include <conversation.h>
#include <debug.h>
#include <plugin.h>
#include <signals.h>
#include <util.h>
#include <version.h>

/* Prototypes */

void reverseString( char *message );

/* we're adding this here and assigning it in plugin_load because we need
 * a valid plugin handle for our call to purple_notify_message() in the
 * plugin_action_test_cb() callback function */
PurplePlugin *nigdip_plugin = NULL;


void reverseString( char *message ) {
    char *from = (char *) message;
    char *to = (char *) message + (int) strlen( message ) - 1;

    while( from < to ) {
        char aux = *from;
        *from = *to;
        *to = aux;
        from++;
        to--;
    };
}

static gboolean
reverse_msg_cb(PurpleAccount *account, char *sender, char **message,
					 PurpleConversation *conv, int *flags, void *data)
{

	if (g_ascii_strncasecmp( *message, "/me ", strlen("/me ") ) ) {
		char *tmp = g_strdup_printf("%s", *message);
        reverseString( tmp );
        g_free(*message);
        *message = tmp;
	}

	return FALSE;
}

static gboolean
plugin_load (PurplePlugin * plugin)
{

    void *conversation = NULL;
    
	nigdip_plugin = plugin; /* assign this here so we have a valid handle later */
    conversation = purple_conversations_get_handle();

	purple_signal_connect(conversation, "sending-im-msg",
						plugin, PURPLE_CALLBACK(reverse_msg_cb), NULL);
	purple_signal_connect(conversation, "sending-chat-msg",
						plugin, PURPLE_CALLBACK(reverse_msg_cb), NULL);
	return TRUE;
}

/* For specific notes on the meanings of each of these members, consult the C Plugin Howto
 * on the website. */
static PurplePluginInfo info = {
	PURPLE_PLUGIN_MAGIC,
	PURPLE_MAJOR_VERSION,
	PURPLE_MINOR_VERSION,
	PURPLE_PLUGIN_STANDARD,
	NULL,
	0,
	NULL,
	PURPLE_PRIORITY_DEFAULT,

	"core-bit-man-nigdiP",
	"nigdiP",
	THIS_PLUGIN_VERSION,
	"Reverses words typed when chatting",
	"Do you believe in satanic messages ?? Discover if you are posessed by Satan reading you messages reversed in Pidgin",
	"Victor A. Rodriguez (Bit-Man) <victor@bit-man.com.ar>",
	"http://www.bit-man.com.ar",


	plugin_load,
	NULL,
	NULL,

	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

static void
init_plugin (PurplePlugin * plugin)
{
}

PURPLE_INIT_PLUGIN (nigdiP, init_plugin, info)

