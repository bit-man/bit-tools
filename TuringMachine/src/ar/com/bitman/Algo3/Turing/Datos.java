/**
 * 
 */
package ar.com.bitman.Algo3.Turing;

import java.io.LineNumberReader;
import java.io.FileReader;
import java.io.FileNotFoundException;

/**
 * @author bit-man
 * 
 * El archivo que contiene los datos es básicamente un formato CSV, 
 * el cual contiene :
 * - la primera línea tiene la posición inicial (estado y casillero inicial)
 * - la segunda línea son los datos
 * 
 */
public class Datos {
	private LineNumberReader r = null;
	
	public Datos ( String filePath ) throws FileNotFoundException {
		r = new LineNumberReader( new FileReader(filePath) );
	}
}
