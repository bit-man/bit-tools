package ar.com.bitman.Algo3.Turing;

import java.io.LineNumberReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author bit-man
 * 
 * Manejo de archivos CSV (valores separados por coma o
 * Comma Separated Values)
 */
public class ArchivoCSV {
	private static final int NO_HAY_MAS_COMAS = -1;
	private static final char COMA_CHAR = ',';
	private LineNumberReader r = null;
	
	public ArchivoCSV( String filePath ) throws FileNotFoundException {
		r = new LineNumberReader( new FileReader( filePath ) );
	}

	public String[] leerLinea() throws IOException {
		String linea = r.readLine();
		if ( linea == null )
			throw new IOException();
		
		String[] o = new String[ cantDeComas(linea) + 1];
		llenarArray(o, linea);
		return o;
	}
	
	public void cerrar() throws IOException {
		r.close();
	}
	
	private int cantDeComas( String s ) {
		int numComas = 0;
		int ultimaComa = 0;
		int i = 0;

		while ( (ultimaComa < s.length() ) &&
				(i = s.indexOf(COMA_CHAR, ultimaComa)) != NO_HAY_MAS_COMAS ) {
			ultimaComa = i+1;
			numComas++;
		}
		
		return numComas;
	};

	private void llenarArray( Object[] o, String linea ) {
		int i = 0;
		int primerComa = 0;
		int ultimaComa = 0;

		while ( (primerComa < linea.length() ) &&
				( (ultimaComa = linea.indexOf(COMA_CHAR, primerComa)) != NO_HAY_MAS_COMAS) ) {
			o[i] = linea.substring(primerComa, ultimaComa);
			primerComa = ultimaComa + 1;
			i++;
		};
		
		// Elemento final, ubicado entre la última coma y el final de la línea
		// que no es capturado por el while() anterior
		o[i] = linea.substring(primerComa, linea.length());
	}
}
