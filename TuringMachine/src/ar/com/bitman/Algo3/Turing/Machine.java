/**
 * 
 */
package ar.com.bitman.Algo3.Turing;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author bit-man
 * 
 *  Máquina de Turing : acceso desde CLI (línea de comandos)
 */
public class Machine {

	private static final int ERROR_EXIT = -1;
	private static final int CANT_ARGUMENTOS = 4;
	private static final String ERROR_MSG = "ERROR : ";

	private static int posInicial;
	private static String estadoInicial;
	private static Programa p;
	private static Datos d;
	
	/**
	 * @param arg
	 * 
	 * arg[0] : estado inicial
	 * arg[1] : posición inicial
	 * arg[2] : archivo de programa
	 * arg[3] : archivo de datos
	 * 
	 * ejemplo: 
	 * java ar.com.bitman.Algo3.Turing.Machine q0 4 /home/bit-man/Programa.turing /home/bit-man/Datos.turing 
	 */
	
	public static void main(String[] arg) {
		
		if (arg.length < CANT_ARGUMENTOS ) {
			mostrarUso();
			System.exit(ERROR_EXIT);
		}
			
		try {
			p = new Programa( arg[2] );
			p.cargar();
		    d = new Datos( arg[3] );
			estadoInicial = arg[0];
			posInicial = Integer.parseInt( arg[1] );
			Maquina m = new Maquina( p, d, estadoInicial, posInicial );
		} catch (FileNotFoundException e) {
			System.err.println( ERROR_MSG + " Archivo de datos o programa no encontrado" );
			e.printStackTrace(System.err);
		} catch (IOException e) {
			System.err.println( ERROR_MSG + " Probable error en la carga del programa o los datos");
			e.printStackTrace(System.err);
		}
	}

	private static void mostrarUso() {
		System.out.println(	 
		    "uso:\n"
		  + "java ar.com.bitman.Algo3.Turing.Machine estado_inicial posición_inicial archivo_programa archivo_datos\n"
		  + "\n"
		  + "ejemplo: \n"
		  + "java ar.com.bitman.Algo3.Turing.Machine q0 4 /home/bit-man/Programa.turing /home/bit-man/Datos.turing"
		);
	}
	
	public static void mostrar( String msg, String[] s ) {
		System.out.print( msg + " : ");
		for( int i = 0; i < s.length; i++) {
			System.out.print(" " + s[i]);
		};
		System.out.println();
	}
}
