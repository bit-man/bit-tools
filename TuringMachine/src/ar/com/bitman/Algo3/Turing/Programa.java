/**
 * 
 */
package ar.com.bitman.Algo3.Turing;

import java.io.LineNumberReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

/***
 * 
 * @author bit-man
 *
 * Encapsula un programa Turing, que contiene una 5-tupla :
 * - Estado inicial
 * - símbolo
 * - estado final
 * - símbolo que reemplaza al anterior
 * - movimiento (una posición hacia adelante o una hacia atrás)
 * 
 * El archivo que contiene al programa es básicamente un formato CSV, donde cada
 * celda se encuentra delimitada por comas, y donde :
 * - La primer línea son los símbolos del programa
 * - el resto de las líneas contiene una tupla por cada una de las instrucciones
 * 
 */
public class Programa {
	private static final int CANT_ELEMS_TUPLA = 5;

	private ArchivoCSV a = null;
	private String[] estados = null;

	public Programa ( String filePath ) throws FileNotFoundException {
		a = new ArchivoCSV( filePath );
	}
	
	public void cargar() throws IOException {
		estados = a.leerLinea();
		Machine.mostrar( "Estados", estados );
		
		String[] pgm = new String[CANT_ELEMS_TUPLA];
		try {
			while(true) {
				 pgm = a.leerLinea();
				 Machine.mostrar( "Programa", pgm );
			}
		} catch ( IOException e ) {
			// Ignorar la excepción, se terminó el archivo
		}
	}
}
