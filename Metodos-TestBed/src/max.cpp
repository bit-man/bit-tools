#include <iostream>

using namespace std; 

int main() {
    float x1 = 1;
    float suma = 1 + x1;

    while( suma != 1 ) {
    	cout <<  x1  << ";" << suma << endl;
        x1 /= 2;
        suma = 1 + x1;
    }
}
