/*
 * PrecisionVariable.h
 *
 *  Created on: 24/03/2009
 *      Author: Claudio Gauna
 *      Modified by : Mart�n Lafont, V�ctor A. Rodr�guez
 */

#ifndef PRECISIONVARIABLE_H_
#define PRECISIONVARIABLE_H_

#include <iostream>
using namespace std;
#include <string>

#define BIG_NUMBER   long double

#define MIN_PRECISION    1
#define MAX_PRECISION    64

class PrecisionVariable {

private:
	unsigned int precision;
	BIG_NUMBER real;
	BIG_NUMBER truncar(BIG_NUMBER num);
public:
	PrecisionVariable();
	PrecisionVariable(unsigned int precision);
	PrecisionVariable& operator + (PrecisionVariable &sumando);
	PrecisionVariable& operator + (BIG_NUMBER num);
	PrecisionVariable& operator - (PrecisionVariable &);
	PrecisionVariable& operator - (BIG_NUMBER num);
	PrecisionVariable& operator = (BIG_NUMBER num);
	PrecisionVariable& operator = (PrecisionVariable& num);
	PrecisionVariable& operator * (PrecisionVariable& num);
	PrecisionVariable& operator * (BIG_NUMBER num);
	PrecisionVariable& operator / (PrecisionVariable& num);
	PrecisionVariable& operator / (BIG_NUMBER num);
	BIG_NUMBER getReal();
	unsigned int getPrecision();
    friend PrecisionVariable& abs(PrecisionVariable& );
	void setPrecision(unsigned int precision);
	friend PrecisionVariable& minimo(PrecisionVariable& num1, PrecisionVariable& num2);
	friend PrecisionVariable& maximo(PrecisionVariable& num1, PrecisionVariable& num2);
	friend PrecisionVariable& maximo(PrecisionVariable& num1, PrecisionVariable& num2, PrecisionVariable& num3);
	friend PrecisionVariable& raiz(PrecisionVariable& num);
    friend ostream& operator<< (ostream& , PrecisionVariable& );
    friend bool operator== (PrecisionVariable&, PrecisionVariable&);
    friend bool operator< (PrecisionVariable&, PrecisionVariable&);
    friend bool operator> (PrecisionVariable&, PrecisionVariable&);
};




#endif /* PRECISIONVARIABLE_H_ */
