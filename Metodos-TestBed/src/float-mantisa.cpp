#include <iostream>

using namespace std; 

#define T    2.0
#define N    20

/**
 * Este programa muestra la convergencia de una mantisa, de base T, calculada en
 * base a N iteraciones
**/

int main() {
    double d = 1 / T;
    double sum = 0;

    for( int i=1; i <= N; i++) {
        sum += d;
        cout << i << " : " << sum << endl;
        d /= T;
    }
}
