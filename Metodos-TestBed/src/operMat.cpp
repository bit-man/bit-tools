#include <iostream>

using namespace std;

int main( int argc, char *argv[] ) {
    int num1 = atoi(argv[1]);
    int den1 = atoi(argv[2]);
    char oper = *argv[3];
    int num2 = atoi(argv[4]);
    int den2 = atoi(argv[5]);

    int den, num;

    den = den1 * den2;

    switch (oper) {
        case '+':
            num = num1 * den2 + num2 * den1;
            break;

        case '-':
            num = num1 * den2 - num2 * den1;
            break;
        
        default:
            cout << endl << "Error: operacion desconocida " << oper << endl;
            return -1;
    }
    
    if (den1 == den2) {
        /***
         * Como den1 == den1 => puede poner den2 donde decia den1
         * num = num1 * den2 + num2 * den2 = (num1 + num2) * den2
         * den = den2 * den2
         * ==> num y den pueden ser divididos por den2 (o den1) sin afectar 
         *     el resultado
         ***/
        
        den /= den2;
        num /= den2;
    };
    
    
    cout << num1 << "/" << den1 << " " << oper << " " << num2 << "/" << den2 ;
    cout << " = " << num << "/" << den << endl;
}
