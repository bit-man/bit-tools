#include <iostream>
#include "superFloat/PrecisionVariable.h"

using namespace std;

int main( int argc, char *argv[] ) {

    int precision = atoi(argv[1]);    

    PrecisionVariable num1 = PrecisionVariable( precision );
    PrecisionVariable num2 = PrecisionVariable( precision );
    PrecisionVariable res = PrecisionVariable( precision );

    num1 = atof(argv[2]);
    char oper = *argv[3];
    num2 = atof(argv[4]);

    
    switch(oper) {
        case '+':
            res = num1 + num2;
            break;

        case '-':
            res = num1 - num2;
            break;

        case 'x':
            res = num1 * num2;
            break;

        case '/':
            res = num1 / num2;
            break;
            
       default:
            cout << "Error: Operacion no reconocida '" << oper << "'" <<endl;
            return -1;
    };
    
    cout << num1 << " " << oper << " " << num2 << " = " << res << endl;
}
