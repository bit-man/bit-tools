/***
 * This belongs to Universidad de Buenos Aires, Facultad de Ciencias Exactas y Naturales, assignment
 * Métodod Numéricos (http://www-2.dc.uba.ar/materias/metnum/homepage.html) and was modified by
 * Víctor A. Rodríguez (http://www.bit-man.com.ar) to allows floating point analysis for big endian
 * and little endian architectures
 */

#include <iostream>

using namespace std;

#define BYTES_FLOAT     sizeof(float)
#define NUMERO_MAGICO   (float) 1.001953125

/**
 * Muestra por STDOUT el valor del signo, exponente y mantisa pasados como
 * parametros, a saber :
 *
 *  - c0 : exponente
 *  - c1 : mantisa, byte 0
 *  - c2 : mantisa, byte 1
 *  - c3 : mantisa, byte 2
 *
 **/
void mostrar(unsigned char c0, unsigned char c1, unsigned char c2, unsigned char c3) {
    char todo[33];

    unsigned char pot = 128;
    for (int i = 0; i < 8; ++i)
    {
        todo[i] = '0' + (c0 / pot) % 2;
        pot /= 2;
    }

    pot = 128;
    for (int i = 8; i < 16; ++i)
    {
        todo[i] = '0' + (c1 / pot) % 2;
        pot /= 2;
    }

    pot = 128;
    for (int i = 16; i < 24; ++i)
    {
        todo[i] = '0' + (c2 / pot) % 2;
        pot /= 2;
    }

    pot = 128;
    for (int i = 24; i < 32; ++i)
    {
        todo[i] = '0' + (c3 / pot) % 2;
        pot /= 2;
    }

    todo[32] = 0;

    char signo = todo[0];
    char exponente[9];
    char mantisa[24];

    int _exponente = 0;
    float _mantisa = 1.0;

    for (int i = 0, pot = 128; i < 8; ++i)
    {
        exponente[i] = todo[i + 1];
        _exponente += (exponente[i] - '0') * pot;
        pot /= 2;
    }

    float _pot = 0.5;
    for (int i = 0; i < 23; ++i)
    {
        mantisa[i] = todo[i + 9];
        _mantisa += (mantisa[i] - '0') * _pot;
        _pot /= 2;
    }

    exponente[8] = 0;
    mantisa[23] = 0;

    printf("signo = %c, ", signo);
    printf("exp = %s (%d), ", exponente, _exponente);
    printf("mant = 1.%s \n", mantisa);
    //  printf( "%3.5f = %s \n", *x, todo );
}

/**
 * Muestra por STDOUT el valor del signo, exponente y mantisa del float pasado
 * parámetro.
 *
 * Vamos a usar mostrar(c0, c1, c2, c3) por lo tanto necesitamos colocar los
 * valores correspondientes en cada parámetro. Como no sabemos en qué posición
 * está la mantisa y cada byte del exponente (básicamente porque esto varía por
 * cada CPU) entonces necesitamos reconocer esto para cada CPU.
 *
 * El truco usado es dado un número y su representación ubicar cada uno de los
 * componentes (mantisa y exponente) de ese número conocido y este deberá tener
 * la misma ubicación en memoria de estos componentes que el número que queremos
 * mostrar.
 * Por ejemplo si usamos el número 1.001953125 sabemos que su representación
 * en formato IEEE 1794 es :
 *
 * Exponente          : 3F
 * Mantisa, 1er. byte : 80
 * Mantisa, 2do. byte : 40
 * Mantisa, 3er. byte : 00
 *
 * Elegimos este número mágico porque cada byte de su representación en formato
 * IEEE 1794 es distinto, y de esta forma si vemos un byte y este posee el valor
 * 40 sabremos inequívocamente que se trata del 2do. byte de la mantisa, por
 * lo tanto el byte correspondiente del float pasado como parámetro también
 * corresponderá al 2do. byte de la mantisa.
 *
 **/
void mostrar(float *x) {
    /**
     *  - c0 : exponente
     *  - c1 : mantisa, byte 0
     *  - c2 : mantisa, byte 1
     *  - c3 : mantisa, byte 2
     **/
    unsigned char c0, c1, c2, c3;
    unsigned char *p_byte_x, *p_byte_y;
    float y = NUMERO_MAGICO;

    for (int i=0; i < BYTES_FLOAT; i++)
    {
        p_byte_y = ((unsigned char*) &y + i);
        p_byte_x = ((unsigned char*) x + i);

        if ( *p_byte_y == 0x00 )
        {
            c3 = *p_byte_x;
        }
        else if ( *p_byte_y == 0x40 )
        {
            c2 = *p_byte_x;
        }
        else if ( *p_byte_y == 0x80 )
        {
            c1 = *p_byte_x;
        }
        else if ( *p_byte_y == 0x3F )
        {
            c0 = *p_byte_x;
        }
        else
        {
            cout << "Formato desconocido :-(" << endl;
        }
    };

    mostrar(c0, c1, c2, c3);
}

/**
 * Convierte el parámetro pasado (unsigned int) que representa un dígito
 * hexadecimal (en el rango 0..15) a un caracter que lo representa:
 *
 *     unsigned int x = 15;
 *     char hex = toHex( x );
 *     // hex tiene el valor 'F'
 */
char toHex(unsigned int num) {
    char convertir[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A',
                           'B', 'C', 'D', 'E', 'F' };

    return convertir[num];
}

/**
 * Convierte a un unsigned int (en el rango 0..1) a un char que representa
 * el valor '0' o '1'
 */
char toBit(unsigned int bit) {
    return '0' + bit;
}

/**
 * Muestra por STDOUT el contenido de un unsigned char (byte)
 * tal cual estÃ¡ almacenado em memoria principal
 */

void mostrarByte(unsigned char mibyte) {
    cout << "        ";
    unsigned char pot = 128;

    for (int i = 0; i < 8; ++i)
    {
        cout << toBit((mibyte / pot) % 2);
        pot /= 2;
    }

    int high = (int) (mibyte / 16);
    int low = mibyte - 16 * high;
    cout << " - " << toHex(high) << toHex(low) << endl;

}

/**
 * Muestra por STDOUT el contenido de un float
 * tal cual está almacenado em memoria principal
 */

void mostrarEnMemoria(float *x) {
    cout << "En memoria :" << endl << endl;
    for (int i = 0; i < BYTES_FLOAT; i++)
    {
        unsigned char mibyte = *((char*) x + i);
        mostrarByte(mibyte);
    }

}

int main(void) {
    float x;

    do
    {
        printf("\nIngrese un numero: ");
        scanf("%f", &x);

        cout << endl;
        mostrar(&x);
        cout << endl;
        mostrarEnMemoria(&x);

    }
    while (x != 0);
}

