#include <iostream>

using namespace std; 

int main() {
    float x1 = 1;
    float resta = 1 - x1;

    while( resta != 1 ) {
    	cout <<  x1  << ";" << resta << endl;
        x1 /= 2;
        resta = 1 - x1;
    }
}
