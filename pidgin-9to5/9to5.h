/*
 * 9to5.h
 *
 *  Created on: Aug 27, 2010
 *      Author: bit-man
 */

#ifndef _9TO5_H_
#define _9TO5_H_

void get9to5Preferences();
void init9to5Preferences();

void setStatusToAway(GList *);

#endif /* _9TO5_H_ */
