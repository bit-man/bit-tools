#include "internal.h"

#include <glib.h>
#include <time.h>

#include "notify.h"
#include "plugin.h"
#include "pluginpref.h"
#include "version.h"
#include "status.h"

#include "9to5.h"

#define PLUGIN_VERSION        "0.0.beta0.268"
// FIXME add revision number to PLUGIN_VERSION using svn Revision tag
#define PLUGIN_REVISION       "$Revision$"

#define PLUGIN_ID            "gtk-9to5"
#define TITLE		         "9 to 5!"
#define PLUGIN_NAME		     "9 to 5!"
#define PLUGIN_SUMMARY       "9 to 5! - Let's you arrive at home on time"
#define PLUGIN_DESCRIPTION   "This plugin let's you now when it's time to go home, and puts your status as Away"
#define PLUGIN_AUTHOR        "Victor A. Rodriguez (Bit-Man) <victor@bit-man.com.ar>"
#define PLUGIN_SITE           "http://www.bit-man.com.ar/"

#define POLLING_SECS          60

#define PREFS_ROOT     "/plugins/gtk/nine-to-five"
#define PREFS_MINUTES  "/plugins/gtk/nine-to-five/min"
#define PREFS_HOUR     "/plugins/gtk/nine-to-five/hour"
#define PREFS_AWAY     "/plugins/gtk/nine-to-five/set-away"

#define HOUR_TO_GO_HOME_DEFAULT        17
#define MINUTE_TO_GO_HOME_DEFAULT      0
#define SETAWAY_DEFAULT                FALSE

// Hour and minute to go home
int hour_9to5;
int min_9to5;
gboolean setAway_9to5;


/***
 * Status setting related code
 */
void setStatusToAway(GList *account_list)
{
	PurplePresence *presence;
	PurpleStatus *status_away;
	PurpleAccount *account;

    account = (PurpleAccount*)account_list->data;
    presence = purple_account_get_presence(account);
    status_away = purple_presence_get_status(presence,
    		             purple_primitive_get_id_from_type(PURPLE_STATUS_AWAY));

    if(status_away != NULL) {
			purple_presence_set_status_active(presence,
											purple_status_get_id(status_away), TRUE);
	}
}

static void markAsWentHome(gpointer data) {

	if (! setAway_9to5)
		return;

	PurplePlugin* plugin = (PurplePlugin*) data;

	GList *account_list;

	// Set status to "Away" for each account
	for (account_list = purple_accounts_get_all_active();
			account_list != NULL;
				account_list = account_list->next) {
		setStatusToAway(account_list);
	}

	purple_notify_message(plugin, PURPLE_NOTIFY_MSG_INFO, TITLE,
						"Your status have been changed to AWAY\nClose piding and start it again to see you new status",
						NULL, NULL, NULL);
}

static gboolean isItTimeToGoHome(gpointer data) {
	PurplePlugin* plugin = (PurplePlugin*) data;

	get9to5Preferences();

	time_t rawtime;
	struct tm * timeinfo;
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );

	/**
	 * FIXME : if the plugin closes before time to go home and is started after that time the alert is missed
	 */

	if ( timeinfo->tm_hour == hour_9to5 && timeinfo->tm_min == min_9to5 ) {
		purple_notify_message(plugin, PURPLE_NOTIFY_MSG_INFO, TITLE,
							"Time to go home", NULL, markAsWentHome, plugin);
	}

	return TRUE;
};

/***
 * Preferences loading and initialization
 */

void get9to5Preferences() {
	hour_9to5 = purple_prefs_get_int(PREFS_HOUR);
	min_9to5 = purple_prefs_get_int(PREFS_MINUTES);
	setAway_9to5 = purple_prefs_get_bool(PREFS_AWAY);
}

void init9to5Preferences() {
	purple_prefs_add_none(PREFS_ROOT);
	purple_prefs_add_int(PREFS_HOUR, HOUR_TO_GO_HOME_DEFAULT);
	purple_prefs_add_int(PREFS_MINUTES, MINUTE_TO_GO_HOME_DEFAULT);
	purple_prefs_add_bool(PREFS_AWAY, SETAWAY_DEFAULT);
}

static gboolean
plugin_load(PurplePlugin *plugin) {

	// Poll me each 60 seconds, and decide if it's time to go home
    purple_timeout_add_seconds(POLLING_SECS, isItTimeToGoHome, plugin);

    return TRUE;
};

static PurplePluginPrefFrame *
get_plugin_pref_frame(PurplePlugin *plugin)
{
	// Create new preferences frame
	PurplePluginPrefFrame *frame;
	PurplePluginPref *pref;

	g_return_val_if_fail(plugin != NULL, FALSE);

	frame = purple_plugin_pref_frame_new();

	pref = purple_plugin_pref_new_with_name_and_label( PREFS_AWAY, _("Set status to AWAY on time arrival ?"));
	purple_plugin_pref_frame_add(frame, pref);

	pref = purple_plugin_pref_new_with_name_and_label( PREFS_HOUR, _("Hour to go home"));
	purple_plugin_pref_set_bounds(pref, 0, 23);
	purple_plugin_pref_frame_add(frame, pref);

	pref = purple_plugin_pref_new_with_name_and_label( PREFS_MINUTES, _("Minute to go home"));
	purple_plugin_pref_set_bounds(pref, 0, 59);
	purple_plugin_pref_frame_add(frame, pref);

	return frame;
}

static PurplePluginUiInfo prefs_info = {
	get_plugin_pref_frame,
	0,   /* page_num (reserved) */
	NULL, /* frame (reserved) */

	/* padding */
	NULL,
	NULL,
	NULL,
	NULL
};

static PurplePluginInfo info = {
    PURPLE_PLUGIN_MAGIC,
    PURPLE_MAJOR_VERSION,
    PURPLE_MINOR_VERSION,
    PURPLE_PLUGIN_STANDARD,
    NULL,
    0,
    NULL,
    PURPLE_PRIORITY_DEFAULT,

    PLUGIN_ID,
    PLUGIN_NAME,
    PLUGIN_VERSION,

    PLUGIN_SUMMARY,
    PLUGIN_DESCRIPTION,
    PLUGIN_AUTHOR,
    PLUGIN_SITE,

    plugin_load,
    NULL,
    NULL,

    NULL,
    NULL,
    &prefs_info,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

static void
init_plugin(PurplePlugin *plugin)
{
    init9to5Preferences();
    get9to5Preferences();
}

PURPLE_INIT_PLUGIN(9to5, init_plugin, info)

