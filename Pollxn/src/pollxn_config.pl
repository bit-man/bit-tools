#!/usr/bin/perl

# --------------
# CONFIGURATION
# --------------

# File system path to your main blog docs folder?
# For example, take this URL: http://myserver/tv/simpsons/homer
# On my system '/tv/simpsons/homer' is under /www/blog/docs
#   (i.e., /www/blog/docs/tv/simpsons/homer)
# So my $datadir is: /www/blog/docs
$datadir = "/home/www/blosxom/entries";

# File system path to Pollxn's templates folder?
# Example: /www/blog/pollxn_templates
$pollxntemplatefolder = "/home/www/blosxom/pollxn_templates";

# Your email address?
$myemail='someone@somewhere.com';

# Title of blog?
$blogtitle='me @ blogosphere';

# Maximum comments shown per page?
$maxperpage=0;   # 0=Show All

# Your time zone
$timezone="GMT-3";

# What file extension do your blog entry files use?
# i.e., .txt, .blog, etc.
$fileextension=".txt";

# Require visitors to enter their names?  Email addresses?
$reqname=0;     # 0=No, 1=Yes
$reqemail=0;

# Maximum comment posts allowed for a blog story?
# (When this number is reached, the comments will
# automatically be archived, disallowing further posts).
$maxpostsallowed=100; # 0=unlimited

# Security - set these to random values:
$delim=5; # number between 5 and 10
$key=123; # between 2 and 1000
$maxmult=345; # between 5 and 10000

