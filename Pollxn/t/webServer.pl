#!/usr/bin/perl

use strict;
use warnings;

use HTTP::Daemon;
use HTTP::Status;

sub startHTTPServer() {
	## TODO fixed port, it would be nice if it's random and can be automatically
	##      passed to the tests !!
	my $d = HTTP::Daemon->new( LocalPort => 43175 ) || return undef;
	return $d;
}

sub runMainLoop($) {
	my ($d) = @_;
	while (my $c = $d->accept) {
	    while (my $r = $c->get_request) {
	        $c->send_error(RC_FORBIDDEN)  if ($r->method ne 'GET');
			if ( $r->url->path =~ /^\/pollxn.pl/ ) {
				print "Preparing HTTP response\n";
				my $res = HTTP::Response->new( 200 );
				$res->header( 'Content-type' => 'text/html');
				$res->content( runPollxn() );
				$c->send_response($res);
	        } else { ## will shutdown on any unknow command !!
	        	$c->send_error(RC_FORBIDDEN);
				print "SHUTTING DOWN !!!\n";
	        	return;
	        }
	    }
	    $c->close;
	    undef($c);
	}
};

sub runPollxn($) {
	print "Executing Pollxn as CGI\n";
	$ENV{'POLLXN_CFG'} = '../src/pollxn_config.pl';
	open(OUT, '../src/pollxn.pl 2>&1 |') || return "can't fork: $!";
	
	my $content;
	while( <OUT> ){
		next
			if /^Content-type/i;
	    $content .= $_; 
};
	close OUT;
	return $content;
};

my $d = startHTTPServer();
if ( ! defined $d ) {
	print "Web server not running\n";
} else {
	print "Server running at ".$d->url()."\n";
	runMainLoop($d);
};

print "Server is closing. Thanks for visiting us !!!!";