/**
 * 
 */
package bitman.Algo3;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author bit-man
 *
 */
public class Misc {

	private static final int ERROR = -1;
	private static final String METODO_RUN = "run";
	private static final String PACKAGE = "bitman.Algo3";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			if ( args.length != 1 ) {
				mostrarUso();
				System.exit( ERROR );
			}
			
			ejecutarDemo( args[0] );
			
		} catch ( ClassNotFoundException e ) {
			System.out.println("Lo lamento, la demostraci�n pedida no existe");
		} catch ( NoSuchMethodException e ) {
			System.out.println("Lo lamento, la demostraci�n no tiene un constructor");
		} catch (Exception e ) {
			System.out.println("Lo lamento, ocurri� el siguiente error ...");
			System.out.println("y no tengo la mas m�nima idea de por qu� :-(");
			e.printStackTrace();
		}
	}

	private static void ejecutarDemo(String demoNombre)
			throws ClassNotFoundException, NoSuchMethodException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException {

		String claseNombre = PACKAGE + "." + demoNombre;
		Class clase = Class.forName( claseNombre );
		Constructor constructor = clase.getConstructor();
		Object demo = constructor.newInstance();
		Method m = clase.getMethod( METODO_RUN );
		m.invoke(demo);
	}
	
	private static void mostrarUso() {
		System.out.println("java " + PACKAGE + ".Misc nombreDeLaDemo");
		// TODO listar los nombres de las demos disponibles
	}

}
