/**
 * 
 */
package bitman.Algo3;

/**
 * @author bit-man
 *
 */
public class MachineModel {

	private static final int ESPERA_EN_MSEC = 1000;
	private static final int CANT_PRUEBAS = 10;

	private int menor;
	private int medio;
	private int mayor;
	
	public MachineModel() {
		menor = 0;
		mayor = Integer.MAX_VALUE;
		medio = (mayor + menor) / 2;
	}
	
	public void run() {
		long[] menorV = new long[CANT_PRUEBAS];
		long[] mayorV = new long[CANT_PRUEBAS];
		int msecPromedioMenor = 0;
		int msecPromedioMayor = 0;

		for( int n = 1; n < CANT_PRUEBAS; n++) {
			menorV[n] = probarElIncremento( menor, medio ); 
			mayorV[n] = probarElIncremento( medio + 1, mayor ); 
		}
		
		System.out.println("Tiempo promedio para el incremento de los números menores : " + promedio( menorV ) + " mseg.");
		System.out.println("Tiempo promedio para el incremento de los números mayores : " + promedio( mayorV ) + " mseg.");
	}

	private long promedio(long[] valor) {
		long prom = 0;
		
		for(int i=0; i < valor.length; i++)
			prom += valor[i];
		
		return ( prom / valor.length );
	}

	private long probarElIncremento( int largada, int llegada) {
		long msecInicial = System.currentTimeMillis();
		// Ver cuánto tiempo tarda en sumar uno a cada número
		int contador = largada; 
		while( contador < llegada )
			contador++;

		long msecTotal = System.currentTimeMillis() - msecInicial;
		System.out.println( "Contar desde " + largada + " hasta " + llegada + " : " + msecTotal + " mseg. Contador : " + contador);
		
		return msecTotal;
	}
}
